package com.practica.runners.metis;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/metis/formAutenticacion/CasosDePrueba.feature",
        glue = "com.practica.definitions.metis",
        plugin = "pretty",
        monochrome = true,
        strict = true,
        snippets= SnippetType.CAMELCASE
)
public class FormAutenticacionRunner {
}
