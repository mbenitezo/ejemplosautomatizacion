package com.practica.steps.metis;

import com.practica.pageobjects.metis.FormAutenticacionPageObject;
import net.thucydides.core.annotations.Step;

public class FormAutenticacionSteps {

    FormAutenticacionPageObject formAutenticacionPageObject;

    @Step
    public void abrirNavegador(){
        formAutenticacionPageObject.open();
    }

    @Step
    public void autenticarseEnColorLib(String usuario, int clave){
        formAutenticacionPageObject.ingresaDatos(usuario, clave);
    }

    @Step
    public void validarAcceso(){
        formAutenticacionPageObject.verificarHome();
    }

}
