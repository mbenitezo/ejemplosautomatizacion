package com.practica.definitions.metis;

import com.practica.steps.metis.FormAutenticacionSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class FormAutenticacionDefinitions {

    @Steps
    public FormAutenticacionSteps formAutenticacionSteps;

    @Given("^Ingreso a la plataforma  colorlib$")
    public void ingresoALaPlataformaColorlib() {
        formAutenticacionSteps.abrirNavegador();
    }


    @When("^Me autentico con usuario \"([^\"]*)\" y clave (\\d+)$")
    public void meAutenticoConUsuarioYClave(String usuario, int clave) {
        formAutenticacionSteps.autenticarseEnColorLib(usuario, clave);
    }

    @Then("^Verifico ingreso exitoso$")
    public void verificoIngresoExitoso() {
        formAutenticacionSteps.validarAcceso();
    }

}
