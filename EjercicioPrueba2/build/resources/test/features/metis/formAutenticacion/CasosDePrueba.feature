@Regresion
Feature: Formulario Popup Validation
  Description: El usuario debe poder ingresar al formulario los datos requeridos.
  Cada campo del formulario realizar validaciones de obligatoriedad,
  longitud y formato, el sistema debe presentar las validaciones respectivas
  para cada campo a través de un globo informativo.

  @UserStory:HU10000
  Scenario: Diligenciamiento exitoso del formulario de Autenticación, no se presenta ningún mensaje de validación.
    Given Ingreso a la plataforma  colorlib
    When Me autentico con usuario "demo" y clave 123
    Then Verifico ingreso exitoso

