package com.choucair.formacion.definition;

/**
 * Estructura básica de clase para Definition UsoCanvasDefinition
 *
 * @author  Mario A. Benítez
 * @version  6/06/2018 - 1:59:28 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;
import com.choucair.formacion.steps.UsoCanvasSteps;

public class UsoCanvasDefinition {

	@Steps
	UsoCanvasSteps usoCanvasSteps; 
	
	
	@Given("^Ingreso a la pagina de the-internet\\.herokuapp\\.com en la opcion challenging dom$")
	public void ingreso_a_la_pagina_de_the_internet_herokuapp_com_en_la_opcion_challenging_dom(){
		
		usoCanvasSteps.abrirSitio();

	}

	@Given("^Realizo cambio del texto en el canvas presionando el boton foo$")
	public void realizo_cambio_del_texto_en_el_canvas_presionando_el_boton_foo(){

		usoCanvasSteps.cambiarCanva();

	}

	@When("^Capturo el texto en canvas y lo envio a consola$")
	public void capturo_el_texto_en_canvas_y_lo_envio_a_consola(){

		usoCanvasSteps.CapturaYEnvioConsola();
		
	}
}