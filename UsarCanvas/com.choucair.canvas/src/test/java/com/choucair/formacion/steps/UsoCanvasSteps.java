package com.choucair.formacion.steps;

/**
 * Estructura básica de clase para Steps UsoCanvasSteps
 *
 * @author  Mario A. Benítez
 * @version  6/06/2018 - 2:22:04 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.UsoCanvasObject;

public class UsoCanvasSteps {
	
	UsoCanvasObject usoCanvasObject; 
	
	@Step
	public void abrirSitio() {
		usoCanvasObject.open();
	}
	
	@Step
	public void cambiarCanva() {
		usoCanvasObject.generarCanva();
	}
	
	@Step
	public void CapturaYEnvioConsola() {
		usoCanvasObject.obtenerCanva();
	}
	
	
}