package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object UsoCanvasObject
 *
 * @author  Mario A. Benítez
 * @version  6/06/2018 - 2:01:16 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("http://the-internet.herokuapp.com/challenging_dom")
public class UsoCanvasObject extends PageObject {
	
	@FindBy(className = "button")
	public WebElementFacade btnBoton;
	
	/* Campo btnBoton  */
	public void generarCanva() {
		btnBoton.click();
	}
	//*[@id="content"]/script
	/* Campo <<Nombre WebElementFacade  */
	public void obtenerCanva() {
		List<WebElement> elJS = getDriver().findElements(By.xpath("//script[text()]"));
		String elTextoEnCanvas = elJS.get(1).getAttribute("innerHTML");
//		elTextoEnCanvas = elTextoEnCanvas.substring(140, 153).replace("'", "");
		System.out.println( elTextoEnCanvas );
		
	}
}