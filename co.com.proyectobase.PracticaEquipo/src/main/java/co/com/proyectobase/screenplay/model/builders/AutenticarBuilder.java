package co.com.proyectobase.screenplay.model.builders;

import co.com.proyectobase.screenplay.model.Autentica;
import co.com.proyectobase.screenplay.tasks.Autenticar;
import net.serenitybdd.screenplay.Tasks;

public class AutenticarBuilder {

    private String usuario;
    private String clave;

    public AutenticarBuilder conElUsuario(String usuario){
        this.usuario = usuario;
        return this;
    }

    public Autenticar yLaClave(String clave){
        return Tasks.instrumented(Autenticar.class, usuario, clave);
    }

    public Autentica build(){
        return new Autentica(this);
    }

    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }
}
