package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.ColorLibPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Ingresar implements Task {

	private ColorLibPage colorLibPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(colorLibPage));
		
	}

	public static Ingresar AlaPagina() {
		
		return Tasks.instrumented(Ingresar.class);
	}

}
