package co.com.proyectobase.screenplay.model;

import co.com.proyectobase.screenplay.model.builders.AutenticarBuilder;

public class Autentica {

    private String usuario;
    private String clave;

    public Autentica(AutenticarBuilder autenticarBuilder) {
        this.usuario = autenticarBuilder.getUsuario();
        this.clave = autenticarBuilder.getClave();
    }

    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }
}
