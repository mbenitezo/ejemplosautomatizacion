package com.choucair.formacion.steps;

/**
 * Estructura básica de clase para Steps CRMCreacionTareaSteps
 *
 * @author  Mario A. Benítez
 * @version  18/05/2018 - 4:05:15 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import com.choucair.formacion.pageobjects.CRMCreacionTareaPage;

public class CRMCreacionTareaSteps {
	
	CRMCreacionTareaPage cRMCreacionTareaPage;
	
	@Step
	public void Login(String Usuario, String Password) {
		cRMCreacionTareaPage.open();
		cRMCreacionTareaPage.IniciarSesion();
		cRMCreacionTareaPage.IngresarDatos(Usuario, Password);
	}
	
	@Step
	public void AccesoTarea() {
		cRMCreacionTareaPage.OpcionTarea();
	}

	@Step
	public void RegistrarDatosTarea(List<List<String>> data, int id) {
		cRMCreacionTareaPage.Asunto(data.get(id).get(0).trim());
		Serenity.takeScreenshot();
		cRMCreacionTareaPage.FechaVencimiento();
		Serenity.takeScreenshot();
		cRMCreacionTareaPage.Contacto(data.get(id).get(1).trim());
		Serenity.takeScreenshot();
		cRMCreacionTareaPage.Cuenta(data.get(id).get(2).trim());
		Serenity.takeScreenshot();
		cRMCreacionTareaPage.Estado();
		Serenity.takeScreenshot();
		cRMCreacionTareaPage.Notificacion();
		Serenity.takeScreenshot();
		cRMCreacionTareaPage.Descripcion(data.get(id).get(3).trim());
		Serenity.takeScreenshot();

	}
	
	@Step
	public void GuardarNuevaTarea() {
		cRMCreacionTareaPage.GuardarTarea();
		Serenity.takeScreenshot();
	}
	
	@Step
	public void VolveraInicio() {
		cRMCreacionTareaPage.Inicio();
		Serenity.takeScreenshot();
	}
	
	@Step
	public void verificaTarea() {
		cRMCreacionTareaPage.verificaTarea();
		Serenity.takeScreenshot();
		}
	

	
}