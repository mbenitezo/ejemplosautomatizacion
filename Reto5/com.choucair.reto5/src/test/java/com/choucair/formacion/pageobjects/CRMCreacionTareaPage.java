package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object CRMCreacionTareaPage
 *
 * @author  Mario A. Benítez
 * @version  18/05/2018 - 4:00:16 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.zoho.com/es-xl/crm/")
public class CRMCreacionTareaPage extends PageObject {

/***** FORMULARIO DE LOGIN ********/
	@FindBy(xpath = "//*[@id=\'block-block-3\']/div/div[1]/a[2]")
	public WebElementFacade btnIniciarSesion;
	
	@FindBy(xpath = "//*[@id=\'lid\']")
		public WebElementFacade txtUsuario;
	
	@FindBy(xpath = "//*[@id=\'pwd\']")
		public WebElementFacade txtPassword;
	
	@FindBy(xpath = "//*[@id=\'signin_submit\']")
		public WebElementFacade btnLogin;
/***** FIN FORMULARIO DE LOGIN ********/	

/***** ACCESO A CREACIÓN DE TAREA ********/
	@FindBy(xpath = "//*[@id=\'tabSettingsmenu\']/div[1]/crm-create-menu")
		public WebElementFacade bntMenu;
	
	@FindBy(xpath = "//*[@id=\'submenu_Tasks\']")
		public WebElementFacade opcTarea;
/***** FIN ACCESO A CREACIÓN DE TAREA ********/
	
	
	/***** FORMULARIO DE CREACIÓN DE TAREA ********/

	@FindBy(xpath = "//*[@id=\'Crm_Tasks_SUBJECT\']")
		public WebElementFacade txtAsunto;
	
	@FindBy(xpath = "//*[@id=\'Crm_Tasks_DUEDATE\']")
		public WebElementFacade txtFechaVencimiento;
	
	@FindBy(xpath = "//*[@id=\'calHeader\']/tbody/tr[5]/td[1]")
		public WebElementFacade selFechaVencimiento;
	
			@FindBy(xpath = "//*[@id=\'Crm_Tasks_SEID\']")
			public WebElementFacade txtcuenta;

			@FindBy(xpath = "//*[@id=\'Crm_Tasks_CONTACTID\']")
			public WebElementFacade txtContacto;	
			
			
			

	@FindBy(xpath = "//*[@id=\'Tasks_fldRow_STATUS\']/div[2]/div/span/span[1]/span")
	public WebElementFacade cmbEstado;  /* 2 abajo y ENTER */	

	@FindBy(xpath = "//TEXTAREA[@id='Crm_Tasks_DESCRIPTION']")
	public WebElementFacade txtDescripcionTarea;
	
	@FindBy(xpath = "//*[@id=\'saveTasksBtn_Bottom\']")
	public WebElementFacade btnguardarTarea;

	@FindBy(xpath = "//*[@id=\'backArrowDV\']/span", timeoutInSeconds="5")
	public WebElementFacade btnInicio;
	
	@FindBy(xpath = "//*[@id=\'layout_3253271000000154651\']/div[2]/form/table/tbody/tr/td/table/tbody/tr[1]/th[1]", timeoutInSeconds="10")
		public WebElementFacade lblasunto;
	
/***** FIN FORMULARIO DE CREACIÓN DE TAREA ********/
	
	
/***** METODOS FORMULARIO DE LOGIN ***********/	
	/* Campo <<Nombre btnIniciarSesion  */
	public void IniciarSesion() {
		btnIniciarSesion.click();
	}
	
	public void IngresarDatos(String User, String Pass) {
		txtUsuario.click();
		txtUsuario.clear();
		txtUsuario.sendKeys(User);
		
		txtPassword.click();
		txtPassword.clear();
		txtPassword.sendKeys(Pass);
		
		btnLogin.click();
	}
/***** METODOS FORMULARIO DE LOGIN ***********/

	/***** METODO ACCESO A TAREAS ***********/
	/* Campo <<bntMenu WebElementFacade  */
	public void OpcionTarea() {
		bntMenu.click();
		opcTarea.click();
	}

	/* Campo <<txtAsunto WebElementFacade  */
public void Asunto(String datoTarea) {
	txtAsunto.click();
	txtAsunto.clear();
	txtAsunto.sendKeys(datoTarea);
}

/* Campo <<Nombre WebElementFacade  */
public void FechaVencimiento() {
	txtFechaVencimiento.click();
	selFechaVencimiento.click();
}

/* Campo <<txtContacto WebElementFacade  */
public void Contacto(String datoTarea) {
	txtContacto.click();
	txtContacto.clear();
	txtContacto.sendKeys(datoTarea);
	txtContacto.sendKeys(Keys.TAB);
}

/* Campo <<txtcuenta WebElementFacade  */
public void Cuenta(String datoTarea) {
	txtcuenta.click();
	txtcuenta.clear();
	txtcuenta.sendKeys(datoTarea);
	txtcuenta.sendKeys(Keys.TAB);
}

public void	Estado() {
	cmbEstado.click();
	cmbEstado.sendKeys(Keys.DOWN);
	cmbEstado.sendKeys(Keys.DOWN);
	cmbEstado.sendKeys(Keys.ENTER);
}
	
public void Notificacion() {
	  WebDriver driver = this.getDriver();
	  WebElement input = driver.findElement(By.id("Crm_Tasks_SENDNOTIFICATION"));
	  new Actions(driver).moveToElement(input).click().perform();

}

public void Descripcion(String DatoTarea) {
	JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
	js.executeScript("window.scrollBy(0,700)");
	txtDescripcionTarea.click();
	txtDescripcionTarea.clear();
	txtDescripcionTarea.sendKeys(DatoTarea);

//	this.getDriver().switchTo().activeElement().click();
}

public void GuardarTarea() {
	btnguardarTarea.click();
}

public void Inicio() {
	try {
		Thread.sleep(5000);
	} catch (Exception e) {

	}
	
	btnInicio.click();	
}

public void verificaTarea() {
	String labelv = "ASUNTO";
	String strMensaje = lblasunto.getText();
	assertThat(strMensaje, containsString(labelv));
}

/***** FIN METODO ACCESO A TAREAS ***********/

} 