package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object CRMRegistroUsuarioPage
 *
 * @author  Mario A. Benítez
 * @version  17/05/2018 - 4:33:58 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.zoho.com/es-xl/crm/")
public class CRMRegistroUsuarioPage extends PageObject {

/***** FORMULARIO DE REGISTRO DE USUARIO ********/
	@FindBy(xpath = "//INPUT[@id='namefield']")
	public WebElementFacade txtNombre;
	
	@FindBy(xpath = "//INPUT[@id='email']")
	public WebElementFacade txtEmail;
	
	@FindBy(xpath = "//INPUT[@class='sgnpaswrd']")
	public WebElementFacade txtPassword;
	
	@FindBy(xpath = "//*[@id=\'signupbtn\']", timeoutInSeconds="10")
	public WebElementFacade bntRegistrar;
/***** FIN FORMULARIO DE REGISTRO DE USUARIO ********/	
	
/***** MODAL DE DETALLES DE CONFIGURACIÓN ***********/
	
	@FindBy(xpath = "//*[@id=\'orgName\']")
	public WebElementFacade txtNombreOrganizacion;
	
	@FindBy(xpath = "//INPUT[@id='orgPhone']")
	public WebElementFacade txtTelefonoOrganizacion;
	
	@FindBy(xpath = "//SPAN[@id='select2-userTimeZone-container']")
	public WebElementFacade cmbZonaHoraria;
	
	@FindBy(xpath = "//INPUT[@class='select2-search__field']")
	public WebElementFacade txtZonaHoraria;
	
	@FindBy(xpath = "//SPAN[@id='select2-currencyLocale-container']")
	public WebElementFacade cmbMonedaLocal;
	
	@FindBy(xpath = "//INPUT[@class='select2-search__field']")
	public WebElementFacade txtMonedaLocal;
	
	@FindBy(xpath = "//INPUT[@id='profileDetailBtn']")
	public WebElementFacade btnConfirmarDetalle;
	
/***** FIN MODAL DE DETALLES DE CONFIGURACIÓN ***********/
	

/***** VERIFICACION DE REGISTRO ***********/

	@FindBy(xpath = "//*[@id=\'show-uName\']")
	public WebElementFacade lblBienvenido;
	

	
/***** VERIFICACION DE REGISTRO ***********/
	
	
	
	
/***** METODOS FORMULARIO DE REGISTRO ***********/
	/* Campo <<chkTos WebElementFacade  */
	public void Tos() {
		  WebDriver driver = this.getDriver();
		  WebElement input = driver.findElement(By.id("tos"));
		  new Actions(driver).moveToElement(input).click().perform();
	}
	
	/* Campo <<txtNombre WebElementFacade  */
	public void Nombre(String datoUsuario) {
		txtNombre.click();
		txtNombre.clear();
		txtNombre.sendKeys(datoUsuario);
	}
	
	/* Campo <<txtEmail WebElementFacade  */
	public void Email(String datoUsuario) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoUsuario);
	}
	
	/* Campo <<txtPassword WebElementFacade  */
	public void Password(String datoUsuario) {
		txtPassword.click();
		txtPassword.clear();
		txtPassword.sendKeys(datoUsuario);
	}

	
	/* Campo <<bntRegistrar WebElementFacade  */
	public void Registrar() {
		 WebDriver driver = this.getDriver();
	       JavascriptExecutor js = null;
	       if (driver instanceof JavascriptExecutor) {
	           js = (JavascriptExecutor) driver;
	       }

		bntRegistrar.click();

	}
/***** FIN METODOS FORMULARIO DE REGISTRO ***********/
	

/***** METODOS FORMULARIO DE MODAL ***********/
	/* Campo <<txtNombreOrganizacion WebElementFacade  */
	public void NombreOrganizacion(String datoUsuario) {
		try {
			Thread.sleep(30000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
	}
		txtNombreOrganizacion.click();
		txtNombreOrganizacion.clear();
		txtNombreOrganizacion.sendKeys(datoUsuario);
	
	}
	
	/* Campo <<txtTelefonoOrganizacion WebElementFacade  */
	public void TelefonoOrganizacion(String datoUsuario) {
		txtTelefonoOrganizacion.click();
		txtTelefonoOrganizacion.clear();
		txtTelefonoOrganizacion.sendKeys(datoUsuario);
	
	}
	
	/* Campo <<cmbZonaHoraria WebElementFacade  */
	public void ZonaHoraria(String datoUsuario) {
		cmbZonaHoraria.click();
		txtZonaHoraria.click();
		txtZonaHoraria.clear();
		txtZonaHoraria.sendKeys(datoUsuario);
		txtZonaHoraria.sendKeys(Keys.ENTER);
//		this.getDriver().findElement(By.id("srch-term")).sendKeys(Keys.ENTER);
	}
	
	/* Campo <<cmbMonedaLocal WebElementFacade  */
	public void MonedaLocal(String datoUsuario) {
		cmbMonedaLocal.click();
		txtMonedaLocal.click();
		txtMonedaLocal.clear();
		txtMonedaLocal.sendKeys(datoUsuario);
		txtMonedaLocal.sendKeys(Keys.ENTER);
//		this.getDriver().findElement(By.id("srch-term")).sendKeys(Keys.ENTER);
	}
	
	/* Campo <<btnConfirmarDetalle WebElementFacade  */
	public void ConfirmarDetalles() {
		btnConfirmarDetalle.click();
	}
	
/***** FIN METODOS FORMULARIO DE MODAL ***********/	


/***** METODOS CONFIRMACION DE REGISTRO ***********/
	/* Campo <<lblBienvenido WebElementFacade  */
	public void VerificacionRegistro() {
		String strMensaje = "Bienvenido";
		String textCompleto = lblBienvenido.getText();
		if(textCompleto.contains( strMensaje )) {
			System.out.println("Registro exitoso.");
		}
	}
	

/***** FIN METODOS CONFIRMACION DE REGISTRO ***********/	
	
}	