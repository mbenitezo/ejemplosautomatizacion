package com.choucair.formacion.steps;

/**
 * Estructura básica de clase para Steps CRMRegistroUsuariosSteps
 *
 * @author  Mario A. Benítez
 * @version  17/05/2018 - 4:40:30 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import com.choucair.formacion.pageobjects.CRMRegistroUsuarioPage;

public class CRMRegistroUsuariosSteps {
	
	CRMRegistroUsuarioPage cRMRegistroUsuarioPage;
	
	@Step
	public void RegistrarUsuario(List<List<String>> data, int id) {
		cRMRegistroUsuarioPage.open();
		cRMRegistroUsuarioPage.Nombre(data.get(id).get(0).trim());
		Serenity.takeScreenshot();
		cRMRegistroUsuarioPage.Email(data.get(id).get(1).trim());
		Serenity.takeScreenshot();
		cRMRegistroUsuarioPage.Password(data.get(id).get(2).trim());
		Serenity.takeScreenshot();
		
	}
	
	@Step
	public void SeleccionaTOS() {
		cRMRegistroUsuarioPage.Tos();
		Serenity.takeScreenshot();		
	}
	
	@Step
	public void ValidarRegistro() {
		cRMRegistroUsuarioPage.Registrar();
	}

	@Step
	public void RegistrarDetalles(List<List<String>> data, int id) {
		cRMRegistroUsuarioPage.NombreOrganizacion(data.get(id).get(0).trim());
		Serenity.takeScreenshot();
		cRMRegistroUsuarioPage.TelefonoOrganizacion(data.get(id).get(1).trim());
		Serenity.takeScreenshot();
		cRMRegistroUsuarioPage.ZonaHoraria(data.get(id).get(2).trim());
		Serenity.takeScreenshot();
		cRMRegistroUsuarioPage.MonedaLocal(data.get(id).get(3).trim());
		Serenity.takeScreenshot();
		
	}
	
	@Step
	public void ValidarDetalles() {
		cRMRegistroUsuarioPage.ConfirmarDetalles();
	}
	
	@Step
	public void VerificacionRegistro() {
		cRMRegistroUsuarioPage.VerificacionRegistro();
	}
	
}