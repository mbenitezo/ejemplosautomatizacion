package com.choucair.formacion.definition;

/**
 * Estructura básica de clase para Definition CRMDefinition
 *
 * @author  Mario A. Benítez
 * @version  17/05/2018 - 4:29:25 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;

import com.choucair.formacion.steps.CRMRegistroUsuariosSteps;
import com.choucair.formacion.steps.CRMCreacionTareaSteps;

public class CRMDefinition {
	
	@Steps
	CRMRegistroUsuariosSteps cRMRegistroUsuariosSteps;
	
	@Steps
	CRMCreacionTareaSteps cRMCreacionTareaSteps;
	
	@Given("^Dado que el usuario quiere utilizar el CRM Zoho, diligencia el formulario de registro$")
	public void dado_que_el_usuario_quiere_utilizar_el_CRM_Zoho_diligencia_el_formulario_de_registro(DataTable dtDatosFiltro){

		List<List<String>> data = dtDatosFiltro.raw();
		   
		for(int i=1; i<data.size(); i++) {
			cRMRegistroUsuariosSteps.RegistrarUsuario(data, i);
		}
			cRMRegistroUsuariosSteps.SeleccionaTOS();
			cRMRegistroUsuariosSteps.ValidarRegistro();
	}

	@When("^Realizo el registro exitoso,$")
	public void realizo_el_registro_exitoso(DataTable dtDatosFiltro){
		List<List<String>> data = dtDatosFiltro.raw();
		   
		for(int i=1; i<data.size(); i++) {
			cRMRegistroUsuariosSteps.RegistrarDetalles(data, i);
		}
			cRMRegistroUsuariosSteps.ValidarDetalles();
	}

	@Then("^Verifico acceso exitoso$")
	public void verifico_acceso_exitoso(){
		cRMRegistroUsuariosSteps.VerificacionRegistro();
	}


/*  SEGUNDO ESCENARIO */	
	
	@Given("^Al ingresar con un usuario \"([^\"]*)\" y con una clave \"([^\"]*)\" al sistema$")
	public void al_ingresar_con_un_usuario_y_con_una_clave_al_sistema(String Usuario, String Password){

		cRMCreacionTareaSteps.Login(Usuario, Password);

	}

	@When("^Realizo la creación de una tarea con los datos de la tarea$")
	public void realizo_la_creación_de_una_tarea_con_los_datos_de_la_tarea(DataTable dtDatosCuenta) {

		cRMCreacionTareaSteps.AccesoTarea();
		List<List<String>> data = dtDatosCuenta.raw();
		   
		for(int i=1; i<data.size(); i++) {
			cRMCreacionTareaSteps.RegistrarDatosTarea(data, i);
		}
		cRMCreacionTareaSteps.GuardarNuevaTarea();
		cRMCreacionTareaSteps.VolveraInicio();
	}		
		

	@Then("^Verifico tarea creada exitosamente$")
	public void verifico_tarea_creada_exitosamente() {

		cRMCreacionTareaSteps.verificaTarea();

	}
	
	
	
}