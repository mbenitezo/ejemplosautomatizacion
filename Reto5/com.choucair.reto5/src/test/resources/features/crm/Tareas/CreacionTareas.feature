#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Trabajar con el CRM Zoho
  Se desea realizar el registro a la plataforma CRM Zoho,
  con el fin de crear tareas de campaña de relacionamiento de clientes.

  @CasoRegistro
  Scenario: Registro de usuario en la plataforma
    Given Dado que el usuario quiere utilizar el CRM Zoho, diligencia el formulario de registro
    	|	Nombre 				|	Email										|	Clave			|
    	|	Anacleto3000	|	anacleto3010@gmail.com	| 12345678	|	
    When Realizo el registro exitoso,
    	|	NombreEmpresa		|	TelefonoEmpresa	|	ZonaHoraria	|	MonedaLocal	|
    	|	Empresa prueba	|	5556666					|	Colombia		|	Colombia		|
    Then Verifico acceso exitoso

	@CasoCreacionTarea
	Scenario: Creacion de tareas
    Given Al ingresar con un usuario "maalben@gmail.com" y con una clave "12345678" al sistema
    When Realizo la creación de una tarea con los datos de la tarea
    	|	Asunto							|	Contacto			|	Cuenta						|	Descripcion									|
    	|	3253271000000003447	|	Juana de Arco	|	Cuenta de prueba	|	Prueba de registro exitoso.	|
    Then Verifico tarea creada exitosamente

#  @tag2
#  Scenario Outline: Title of your scenario outline
#    Given I want to write a step with <name>
#    When I check for the <value> in step
#    Then I verify the <status> in step

#    Examples: 
#      | name  | value | status  |
#      | name1 |     5 | success |
#      | name2 |     7 | Fail    |
