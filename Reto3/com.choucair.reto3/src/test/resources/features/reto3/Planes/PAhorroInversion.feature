#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Formulario Plan de Ahorro
  Se desea poder realizar la simulación de ahorro e inversión,
  donde se agregue el plan de ahorro y/o inversión,
  y que se logre saber el valor que se debe ahorrar mensualmente.

  @CasoIngresoAFormulario
  Scenario: Acceder al sistema de Simulación de ahorros
    Given Ingresar al menu de necesidades
    And 	Luego acceder al submenu Estudio
    When 	Luego acceder a la opcion de Simulacion de tus ahorros
		Then 	Ver formulario de Simulacion de ahorro e inversion
		
		Given Completar los campos del ahorro a simular
    When 	Luego agrego el plan de ahorros
    Then 	Verifico que se visualicen los valores capturados del formulario
		
		Given Accedo al segundo paso
		And   Luego acepto las disposiciones legales
		When  Luego calculo el ahorro
		Then  Verifico que muestre el valor a ahorrar
		
 # @casoVerificarValoresCapturadosDeFormularios
 # Scenario: Haber accedido a la página de Simulación


 #   Examples: 
 #     | name  | value | status  |
 #     | name1 |     5 | success |
 #     | name2 |     7 | Fail    |
