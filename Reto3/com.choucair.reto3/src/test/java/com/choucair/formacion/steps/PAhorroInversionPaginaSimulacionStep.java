package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PAhorroInversionValidaAccesoSimuladorPage;

import net.thucydides.core.annotations.Step;

public class PAhorroInversionPaginaSimulacionStep {

	PAhorroInversionValidaAccesoSimuladorPage pAhorroInversionValidaAccesoSimuladorPage;
	
	@Step
	public void VerificaPaginaSimulacion() {
	
		pAhorroInversionValidaAccesoSimuladorPage.validaPaginaSimulador();
		
	}
	
}
