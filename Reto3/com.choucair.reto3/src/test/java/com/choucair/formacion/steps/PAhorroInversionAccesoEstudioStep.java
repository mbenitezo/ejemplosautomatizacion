package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PAhorroInversionEstudioPage;

import net.thucydides.core.annotations.Step;

public class PAhorroInversionAccesoEstudioStep {

	PAhorroInversionEstudioPage pAhorroInversionEstudioPage;
	
	@Step
	public void IngresarAEstudio() {
		pAhorroInversionEstudioPage.Estudio();
	}
	
}
