package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class PAhorroInversionValidaAccesoSimuladorPage extends PageObject{

	@FindBy(xpath="//*[@id=\"sim-description\"]/div[1]/h1")
	private WebElementFacade lblTitulo;
	
	public void validaPaginaSimulador() {
		
		String lblmensaje = "Bienvenidos al Simulador de Ahorro e Inversión";
		String mensaje = lblTitulo.getText();
		assertThat(mensaje, containsString(lblmensaje));
		
	}
	
}
