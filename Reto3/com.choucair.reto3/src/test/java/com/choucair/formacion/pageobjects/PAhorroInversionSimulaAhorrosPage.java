package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PAhorroInversionSimulaAhorrosPage extends PageObject {

	@FindBy(xpath="//*[@id=\"wizard1\"]/div/div[1]/div/div/div[1]/p[2]/strong/a")
	private WebElementFacade btnSimulaAhorro;
	
	public void SimulaTuAhorro() {
		
		btnSimulaAhorro.click();
		
	}
	
}
