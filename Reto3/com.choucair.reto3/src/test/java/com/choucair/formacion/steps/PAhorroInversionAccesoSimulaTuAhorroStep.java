package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PAhorroInversionSimulaAhorrosPage;

import net.thucydides.core.annotations.Step;

public class PAhorroInversionAccesoSimulaTuAhorroStep {

	PAhorroInversionSimulaAhorrosPage pAhorroInversionSimulaAhorrosPage;
	
	@Step
	public void AccesoASimulaAhorros() {
		
		pAhorroInversionSimulaAhorrosPage.SimulaTuAhorro();
		
	}
	
}
