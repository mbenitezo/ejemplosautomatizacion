package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PAhorroInversionAceptarLegalesPage extends PageObject{

	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[2]/div[3]/form/input")
	private WebElementFacade chbDisposicionesLegales;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[2]/div[3]/form/button")
	private WebElementFacade btnCalcularAhorro;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/ul/li[3]/a/div[2]/div/p")
	private WebElementFacade btnPaso3;
	
	public void AceptarDisposicionesLegales() {
		
		chbDisposicionesLegales.click();
		
	}
	
	public void PresionarBotonDisposicionesLegales() {
		
		btnCalcularAhorro.click();
		
	}
	
	public void IngresalPaso3() {
		
		btnPaso3.click();
		
	}
	
}
