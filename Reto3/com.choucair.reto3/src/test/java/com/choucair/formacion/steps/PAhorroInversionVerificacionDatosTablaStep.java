package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PAhorroInversionVerificaDatosTablaPage;

import net.thucydides.core.annotations.Step;

public class PAhorroInversionVerificacionDatosTablaStep{

	PAhorroInversionVerificaDatosTablaPage pAhorroInversionVerificaDatosTablaPage;
	
	
	@Step
	public void VerificarCargaTabla() {
		
		pAhorroInversionVerificaDatosTablaPage.VerificaCargaTabla();
		
	}
	
	@Step
	public void AccedeSegundoPaso() {
		
		pAhorroInversionVerificaDatosTablaPage.AccederSegundoPaso();
		
	}

}
