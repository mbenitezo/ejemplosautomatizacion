package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PAhorroInversionFormSimulacionPage;

import net.thucydides.core.annotations.Step;

public class PAhorroInversionFormValidationStep {

	PAhorroInversionFormSimulacionPage pAhorroInversionFormSimulacionPage;
	
	@Step
	public void IngresaDatosFormulario() {
		
		pAhorroInversionFormSimulacionPage.Select_TipoAhorro();
		pAhorroInversionFormSimulacionPage.Meses();
		pAhorroInversionFormSimulacionPage.Producto();
		pAhorroInversionFormSimulacionPage.Dinero();
	}
	
	@Step
	public void AgregarValoresAlPlan() {
		
		pAhorroInversionFormSimulacionPage.AgregarAlPlan();
		
	}
	
}
