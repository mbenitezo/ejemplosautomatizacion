package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PAhorroInversionEstudioPage extends PageObject {

	@FindBy(xpath="//*[@id=\'necesidadesPersonas\']/div/div[1]/div[1]/div/div[3]/div/a")
	private WebElementFacade btnEstudio;
	
	public void Estudio() {
		
		btnEstudio.click();
		
	}
	
}
