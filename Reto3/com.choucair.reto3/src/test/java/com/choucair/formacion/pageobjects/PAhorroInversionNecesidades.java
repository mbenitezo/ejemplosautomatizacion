package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class PAhorroInversionNecesidades extends PageObject {

	@FindBy(xpath="//*[@id=\"main-menu\"]/div[2]/ul[1]/li[2]/a")
	private WebElementFacade btnNecesidades;
	
	public void Necesidades() {
		btnNecesidades.click();
	}
	
}
