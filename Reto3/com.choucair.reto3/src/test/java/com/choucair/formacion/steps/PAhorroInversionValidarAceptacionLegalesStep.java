package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PAhorroInversionAceptarLegalesPage;

import net.thucydides.core.annotations.Step;

public class PAhorroInversionValidarAceptacionLegalesStep {

	PAhorroInversionAceptarLegalesPage pAhorroInversionAceptarLegalesPage;
	
	@Step
	public void ValidarAceptacionDisposicionesLegales() {
		
		pAhorroInversionAceptarLegalesPage.AceptarDisposicionesLegales();
		
	}
	
	public void CalcularAhorro() {
		
		pAhorroInversionAceptarLegalesPage.PresionarBotonDisposicionesLegales();
		
	}
	
	public void AccederPaso3() {
		
		pAhorroInversionAceptarLegalesPage.IngresalPaso3();
		
	}
}
