package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PAhorroInversionCapturarValorAhorroPage;

import net.thucydides.core.annotations.Step;

public class PAhorroInversionGrabarConsolaStep {

	PAhorroInversionCapturarValorAhorroPage pAhorroInversionCapturarValorAhorroPage;
	
	@Step
	public void GrabarEnConsola() {
		
		pAhorroInversionCapturarValorAhorroPage.DatoAConsola();
		
	}
	
}
