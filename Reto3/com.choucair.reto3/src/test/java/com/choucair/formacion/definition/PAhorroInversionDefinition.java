package com.choucair.formacion.definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.choucair.formacion.steps.PAhorroInversionAccesoNecesidadesStep;
import com.choucair.formacion.steps.PAhorroInversionAccesoEstudioStep;
import com.choucair.formacion.steps.PAhorroInversionAccesoSimulaTuAhorroStep;
import com.choucair.formacion.steps.PAhorroInversionPaginaSimulacionStep;
import com.choucair.formacion.steps.PAhorroInversionVerificacionDatosTablaStep;
import com.choucair.formacion.steps.PAhorroInversionValidarAceptacionLegalesStep;
import com.choucair.formacion.steps.PAhorroInversionGrabarConsolaStep;

import com.choucair.formacion.steps.PAhorroInversionFormValidationStep;

public class PAhorroInversionDefinition {

	@Steps
	PAhorroInversionAccesoNecesidadesStep pAhorroInversionAccesoNecesidadesStep;
	
	@Steps
	PAhorroInversionAccesoEstudioStep pAhorroInversionAccesoEstudioStep; 
	
	@Steps
	PAhorroInversionAccesoSimulaTuAhorroStep pAhorroInversionAccesoSimulaTuAhorroStep;
	
	@Steps
	PAhorroInversionPaginaSimulacionStep pAhorroInversionPaginaSimulacionStep;
	
	
	@Steps
	PAhorroInversionFormValidationStep pAhorroInversionFormValidationStep;
	
	@Steps
	PAhorroInversionVerificacionDatosTablaStep pAhorroInversionVerificacionDatosTablaStep;
	
	@Steps
	PAhorroInversionValidarAceptacionLegalesStep pAhorroInversionValidarAceptacionLegalesStep;
	
	@Steps
	PAhorroInversionGrabarConsolaStep pAhorroInversionGrabarConsolaStep;
	
	@Given("^Ingresar al menu de necesidades$")
	public void ingresar_al_menu_de_necesidades() {
		pAhorroInversionAccesoNecesidadesStep.IngresarANecesidades();
	}

	@Given("^Luego acceder al submenu Estudio$")
	public void luego_acceder_al_submenu_Estudio() {
	   
		pAhorroInversionAccesoEstudioStep.IngresarAEstudio();

	}

	@When("^Luego acceder a la opcion de Simulacion de tus ahorros$")
	public void luego_acceder_a_la_opcion_de_Simulacion_de_tus_ahorros()  {
	   
		pAhorroInversionAccesoSimulaTuAhorroStep.AccesoASimulaAhorros();
		
	}

	@Then("^Ver formulario de Simulacion de ahorro e inversion$")
	public void ver_formulario_de_Simulacion_de_ahorro_e_inversion(){
	    
		pAhorroInversionPaginaSimulacionStep.VerificaPaginaSimulacion();

	}
	
	
/* Segundo Paso directamente en el formulario */
	
	@Given("^Completar los campos del ahorro a simular$")
	public void completar_los_campos_del_ahorro_a_simular() {
	   
		pAhorroInversionFormValidationStep.IngresaDatosFormulario();
		
	}

	@When("^Luego agrego el plan de ahorros$")
	public void luego_agrego_el_plan_de_ahorros()  {

		pAhorroInversionFormValidationStep.AgregarValoresAlPlan();
		
	}

	@Then("^Verifico que se visualicen los valores capturados del formulario$")
	public void verifico_que_se_visualicen_los_valores_capturados_del_formulario() {

		pAhorroInversionVerificacionDatosTablaStep.VerificarCargaTabla();
		
	}
	
	
	/* Se confirman las disposiciones legales */
	
	@Given("^Accedo al segundo paso$")
	public void accedo_al_segundo_paso() {
	    
		pAhorroInversionVerificacionDatosTablaStep.AccedeSegundoPaso();
		
	}

	@Given("^Luego acepto las disposiciones legales$")
	public void luego_acepto_las_disposiciones_legales() {

		pAhorroInversionValidarAceptacionLegalesStep.ValidarAceptacionDisposicionesLegales();
		
	}

	@When("^Luego calculo el ahorro$")
	public void luego_calculo_el_ahorro() {

		pAhorroInversionValidarAceptacionLegalesStep.CalcularAhorro();
		pAhorroInversionValidarAceptacionLegalesStep.AccederPaso3();

	}

	@Then("^Verifico que muestre el valor a ahorrar$")
	public void verifico_que_muestre_el_valor_a_ahorrar(){

		pAhorroInversionGrabarConsolaStep.GrabarEnConsola();

	}
	
	
}
