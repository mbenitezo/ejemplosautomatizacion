package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.PAhorroInversionNecesidades;

import net.thucydides.core.annotations.Step;

public class PAhorroInversionAccesoNecesidadesStep {

	PAhorroInversionNecesidades pAhorroInversionNecesidades;
	
	@Step
	public void IngresarANecesidades() {
		
		pAhorroInversionNecesidades.open();
		pAhorroInversionNecesidades.Necesidades();
		
	}
	
}
