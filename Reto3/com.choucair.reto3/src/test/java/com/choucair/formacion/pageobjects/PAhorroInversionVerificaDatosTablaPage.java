package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class PAhorroInversionVerificaDatosTablaPage extends PageObject {

	private String tituloColumna1 = "Sueño";
	private String tituloColumna2 = "Meses";
	private String tituloColumna3 = "Monto de Ahorro";
	private String tituloColumna4 = "Producto";
	private String tituloColumna5 = "Tasa Efectiva Anual";
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[9]/table/tbody/tr[1]/th[1]")
	private WebElementFacade lblTituloPrimeraColumna;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[9]/table/tbody/tr[1]/th[2]")
	private WebElementFacade lblTituloSegundaColumna;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[9]/table/tbody/tr[1]/th[3]")
	private WebElementFacade lblTituloTerceraColumna;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[9]/table/tbody/tr[1]/th[4]")
	private WebElementFacade lblTituloCuartaColumna;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[9]/table/tbody/tr[1]/th[5]")
	private WebElementFacade lblTituloQuintaColumna;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/ul/li[2]/a/div[2]/div/p")
	private WebElementFacade btnPaso2;
	
	public void VerificaCargaTabla() {
		
		String columna1 = lblTituloPrimeraColumna.getText();
		String columna2 = lblTituloSegundaColumna.getText();
		String columna3 = lblTituloTerceraColumna.getText();
		String columna4 = lblTituloCuartaColumna.getText();
		String columna5 = lblTituloQuintaColumna.getText();
		
		assertThat(tituloColumna1, containsString(columna1));
		assertThat(tituloColumna2, containsString(columna2));
		assertThat(tituloColumna3, containsString(columna3));
		assertThat(tituloColumna4, containsString(columna4));
		assertThat(tituloColumna5, containsString(columna5));
		
	}
	
	public void AccederSegundoPaso() {
		
		btnPaso2.click();
		
	}
	
}
