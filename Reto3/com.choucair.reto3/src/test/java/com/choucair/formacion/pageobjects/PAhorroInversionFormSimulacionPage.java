package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PAhorroInversionFormSimulacionPage extends PageObject {

	private String opcionTipoAhorro = "Entretenimiento";
	private String meses = "12";
	private String producto = "Fiducuenta";
	private String dinero = "500000";
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[2]/select")
	private WebElementFacade cmbTipoAhorro;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[3]/input")
	private WebElementFacade txtMeses;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[4]/select")
	private WebElementFacade cmbProducto;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[5]/input")
	private WebElementFacade txtDineroFaltante;
	
	@FindBy(xpath="//*[@id=\'browser-Off\']/div/div/div[1]/form/div[8]/button")
	private WebElementFacade btnAgregarAlPlan;
	
	public void Select_TipoAhorro() {
		cmbTipoAhorro.click();
		cmbTipoAhorro.selectByVisibleText(opcionTipoAhorro);
	}
	
	public void Meses() {
		txtMeses.click();
		txtMeses.clear();
		txtMeses.sendKeys(meses);
	}
	
	public void Producto() {
		cmbProducto.click();
		cmbProducto.selectByVisibleText(producto);
	}
	
	public void Dinero() {
		txtDineroFaltante.click();
		txtDineroFaltante.clear();
		txtDineroFaltante.sendKeys(dinero);
	}
	
	public void AgregarAlPlan() {
		
		btnAgregarAlPlan.click();
		
	}
	
}
