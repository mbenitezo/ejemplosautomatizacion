package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PAhorroInversionCapturarValorAhorroPage extends PageObject {

	@FindBy(xpath="//*[@id=\'tablaAhorro\']/table/tbody/tr[2]/td[2]")
	private WebElementFacade lblValorAhorrar;
	
	public void DatoAConsola() {
		
		System.out.print("El valor a ahorrar es de: " + lblValorAhorrar.getText() );
		
	}
	
}
