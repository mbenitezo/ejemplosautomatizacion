#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Formulario de solicitud de Tarjeta de crédito
  El usuario debe poder ingresar a la opción de productos y servicios,
  luego ingresar a la opción de Tarjetas de Crédito,
  se debe almacenar en consola la información de la tarjeta de crédito American Express Green y Mastercard Black,
  luego solicitar la tarjeta una tarjeta de crédito.

  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario de solicitud de tarjeta de crédito
		Given Ingreso a la opcion de productos y servicios
    And Ingreso a la opcion Tarjetas de credito
    And Muestro informacion por consola de las tarjetas de credito American Express Green y  Mastercard Black
    And Ingreso a la opcion Solicitala aqui de la tarjeta American Express
    When Diligencio formulario de Solicitud de Tarjeta de Credito
    	|	Nombres		|	Apellidos	|	SelectTipoDocumento		|	NumeroDocumento	|	FechaNacimiento	|	Ingresos	|	CiudadDepartamento		|
    	|	Anacleto	|	Perez			|	Cédula de Ciudadanía	|	11111111111			|	1986-01-01			|	500000		|	Medellin - Antioquia	|
    Then Verifico ingreso de datos exitoso

  @tag2
  Scenario Outline: Title of your scenario outline
    Given I want to write a step with <name>
    When I check for the <value> in step
    Then I verify the <status> in step

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |
