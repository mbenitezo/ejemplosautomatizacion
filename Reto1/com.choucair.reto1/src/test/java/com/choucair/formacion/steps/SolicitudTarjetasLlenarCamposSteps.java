package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.SolicitudTarjetaPaginaTarjetaFormValidacionCampos;

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class SolicitudTarjetasLlenarCamposSteps {

	SolicitudTarjetaPaginaTarjetaFormValidacionCampos solicitudTarjetaPaginaTarjetaFormValidacionCampos;
	
	@Step
	public void LlenarCampos(List<List<String>> data, int id) {
		
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.Nombre(data.get(id).get(0).trim());
		Serenity.takeScreenshot();
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.Apellido(data.get(id).get(1).trim());
		Serenity.takeScreenshot();
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.TipoDocumento(data.get(id).get(2).trim());
		Serenity.takeScreenshot();
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.NumeroDocumento(data.get(id).get(3).trim());
		Serenity.takeScreenshot();
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.FechaNacimiento(data.get(id).get(4).trim());
		Serenity.takeScreenshot();
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.Ingresos(data.get(id).get(5).trim());
		Serenity.takeScreenshot();
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.CiudadDepartamento(data.get(id).get(6).trim());
		Serenity.takeScreenshot();
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.valida();
		
	}
	
	@Step
	public void verificar_ingreso_datos_formulario_exitoso() {
		solicitudTarjetaPaginaTarjetaFormValidacionCampos.form_sin_errores();
	}
	
}
