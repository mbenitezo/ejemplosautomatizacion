package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class SolicitudTarjetaPaginaPrincipal extends PageObject {

	//Link Productos y servicios
	@FindBy(xpath="//*[@id=\'main-menu\']/div[2]/ul[1]/li[3]/a")
	public WebElementFacade linkProductosServicios;
	
	@FindBy(xpath="//*[@id=\'productosPersonas\']/div/div[1]/div/div/div[7]/div/a")
	public WebElementFacade OptTarjetasCreditos;
	
	public void OpcionDeProdyServ() {
		
		linkProductosServicios.click();
		OptTarjetasCreditos.click();
	}
	
}
