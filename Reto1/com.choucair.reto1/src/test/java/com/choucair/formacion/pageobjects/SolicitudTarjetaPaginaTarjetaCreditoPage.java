package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class SolicitudTarjetaPaginaTarjetaCreditoPage extends PageObject {

	@FindBy(xpath="//*[@id=\'card_0\']/div[2]/h2")
	public WebElementFacade lblTituloTarjetaCredito;
	
	@FindBy(xpath="//*[@id=\'card_0\']/div[3]/ul/li[1]")
	public WebElementFacade lblPrimerItem;
	
	@FindBy(xpath="//*[@id=\'card_0\']/div[3]/ul/li[2]")
	public WebElementFacade lblSegundoItem;
	
	@FindBy(xpath="//*[@id=\'card_0\']/div[3]/ul/li[3]")
	public WebElementFacade lblTercerItem;
	
	@FindBy(xpath="//*[@id=\'card_0\']/div[3]/ul/li[4]")
	public WebElementFacade lblCuartoItem;
	
	@FindBy(xpath="//*[@id=\'card_0\']/div[4]/a")
	public WebElementFacade btnSolicitalaAqui;
	
	public void PasarTituloConsola() {
		
		System.out.println( lblTituloTarjetaCredito.getText() );
		
	}
	
	public void DetalleInformacion() {
		
		System.out.println( lblPrimerItem.getText() );
		System.out.println( lblSegundoItem.getText() );
		System.out.println( lblTercerItem.getText() );
		System.out.println( lblCuartoItem.getText() );
		
	}
	
	public void AccesoSolicitalaAqui() {
		
		btnSolicitalaAqui.click();
		
	}
	
	
	
}
