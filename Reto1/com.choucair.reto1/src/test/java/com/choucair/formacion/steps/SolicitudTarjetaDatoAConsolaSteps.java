package com.choucair.formacion.steps;


import net.thucydides.core.annotations.Step;

import com.choucair.formacion.pageobjects.SolicitudTarjetaPaginaTarjetaCreditoPage;

public class SolicitudTarjetaDatoAConsolaSteps {

	SolicitudTarjetaPaginaTarjetaCreditoPage solicitudTarjetaPaginaTarjetaCredito;
	
@Step
public void PasarDataAConsola() {

	solicitudTarjetaPaginaTarjetaCredito.PasarTituloConsola();
	solicitudTarjetaPaginaTarjetaCredito.DetalleInformacion();
	
}

public void AccesoASolicitalaAqui() {

	solicitudTarjetaPaginaTarjetaCredito.AccesoSolicitalaAqui();
	
}


	
}
