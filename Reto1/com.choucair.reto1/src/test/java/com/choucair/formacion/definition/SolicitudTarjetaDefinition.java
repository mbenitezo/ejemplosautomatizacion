package com.choucair.formacion.definition;

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;

import com.choucair.formacion.steps.SolicitudTarjetaSteps;
import com.choucair.formacion.steps.SolicitudTarjetaDatoAConsolaSteps;
import com.choucair.formacion.steps.SolicitudTarjetasLlenarCamposSteps;

public class SolicitudTarjetaDefinition {

	@Steps
	SolicitudTarjetaSteps solicitudTarjetaSteps;
	
	@Steps
	SolicitudTarjetaDatoAConsolaSteps solicitudTarjetaDatoAConsolaSteps;
	
	@Steps
	SolicitudTarjetasLlenarCamposSteps solicitudTarjetasLlenarCamposSteps;
	
	@Given("^Ingreso a la opcion de productos y servicios$")
	public void ingreso_a_la_opcion_de_productos_y_servicios(){
	    
		solicitudTarjetaSteps.AccederOpcionesProductosyServicios();
	   
	}

	@Given("^Ingreso a la opcion Tarjetas de credito$")
	public void ingreso_a_la_opcion_Tarjetas_de_credito(){
	   
		solicitudTarjetaSteps.SeleccionarTarjetaCredito();
	    
	}

	@Given("^Muestro informacion por consola de las tarjetas de credito American Express Green y  Mastercard Black$")
	public void muestro_informacion_por_consola_de_las_tarjetas_de_credito_American_Express_Green_y_Mastercard_Black(){
	    
		solicitudTarjetaDatoAConsolaSteps.PasarDataAConsola();
		
	}

	@Given("^Ingreso a la opcion Solicitala aqui de la tarjeta American Express$")
	public void ingreso_a_la_opcion_Solicitala_aqui_de_la_tarjeta_American_Express(){

		solicitudTarjetaDatoAConsolaSteps.AccesoASolicitalaAqui();
		
	}

	@When("^Diligencio formulario de Solicitud de Tarjeta de Credito$")
	public void diligencio_formulario_de_Solicitud_de_Tarjeta_de_Credito(DataTable dtDatosForm){
		
		List<List<String>> data = dtDatosForm.raw();
		   
		for(int i=1; i<data.size(); i++) {
			solicitudTarjetasLlenarCamposSteps.LlenarCampos(data, i);
	
		}

	}

	@Then("^Verifico ingreso de datos exitoso$")
	public void verifico_ingreso_de_datos_exitoso(){
	
		solicitudTarjetasLlenarCamposSteps.verificar_ingreso_datos_formulario_exitoso();
		
	}
	
}
