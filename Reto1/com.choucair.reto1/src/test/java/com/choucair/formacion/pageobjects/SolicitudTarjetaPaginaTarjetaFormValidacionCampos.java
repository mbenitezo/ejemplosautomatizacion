package com.choucair.formacion.pageobjects;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class SolicitudTarjetaPaginaTarjetaFormValidacionCampos extends PageObject {

	@FindBy(xpath="//*[@id=\'nombresReq\']")
	public WebElementFacade txtNombre; 
	
	@FindBy(xpath="//*[@id=\'apellidosReq\']")
	public WebElementFacade txtApellido;
	
	@FindBy(xpath="//*[@id=\'typedocreq\']")
	public WebElementFacade cmbTipoDocumento;
	
	@FindBy(xpath="//*[@id=\'numeroDocumento\']")
	public WebElementFacade txtNumeroDocumento;
	
	@FindBy(xpath="//*[@id=\'fechaNacimientoReq\']")
	public WebElementFacade txtFechaNacimiento;
	
	@FindBy(xpath="//*[@id=\'ingresos-mensuales\']")
	public WebElementFacade txtIngresos;
	
	@FindBy(xpath="//*[@id=\'reqCiuidadDpto_value\']")
	public WebElementFacade txtCiudadDepartamento;
	
	@FindBy(xpath="//*[@id=\'reqCiuidadDpto_dropdown\']")
	public WebElementFacade txtOpcionCiudadDepartamento;
	
	@FindBy(xpath="//*[@id=\'conIframe_rm\']/div/form/div[7]/div[2]/button")
	public WebElementFacade btnContinuar;
	
	@FindBy(xpath="//*[@id=\'conIframe_rm\']/div/form/div[4]/div[2]/div[2]/div/div/span[3]")
	public WebElementFacade TextoInformativo;
		
	public void Nombre(String datoUsuario) {
//		this.getDriver().switchTo().frame("Demos").findElement(By.id("nombresReq")).sendKeys(datoPrueba);
		this.getDriver().switchTo().frame("Demos");
		txtNombre.click();
		txtNombre.clear();
		txtNombre.sendKeys(datoUsuario);
	//	this.getDriver().switchTo().defaultContent();
	}
	
	public void Apellido(String datoUsuario) {
		//this.getDriver().switchTo().frame("Demos");
		txtApellido.click();
		txtApellido.clear();
		txtApellido.sendKeys(datoUsuario);
	}
	
	public void TipoDocumento(String datoUsuario) {
		cmbTipoDocumento.click();
		cmbTipoDocumento.selectByVisibleText(datoUsuario);
	}
	
	public void NumeroDocumento(String datoUsuario) {
		txtNumeroDocumento.click();
		txtNumeroDocumento.clear();
		txtNumeroDocumento.sendKeys(datoUsuario);
	}	
	
	public void FechaNacimiento(String datoUsuario) {
		txtFechaNacimiento.click();
		txtFechaNacimiento.clear();
		txtFechaNacimiento.sendKeys(datoUsuario);
	}
	
	public void Ingresos(String datoUsuario) {
		txtIngresos.click();
		txtIngresos.clear();
		txtIngresos.sendKeys(datoUsuario);
	}
	
	public void CiudadDepartamento(String datoUsuario) {
		txtCiudadDepartamento.click();
		txtCiudadDepartamento.clear();
		txtCiudadDepartamento.sendKeys(datoUsuario);
		txtOpcionCiudadDepartamento.click();
	}
	
	public void valida() {
		
		btnContinuar.click();
	}
	
	public void form_sin_errores() {
		assertThat(TextoInformativo.isCurrentlyVisible(), is(false));
	}
	
}