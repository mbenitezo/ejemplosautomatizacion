package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;
import net.thucydides.core.annotations.Step;


public class PopupValidationSteps {

	ColorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage colorlibMenuPage;
@Step	
	public void login_colorlib(String strUsuario, String strPass) {
		// a. Abrir navegador con la url de prueba
		colorlibLoginPage.open();
		
		//b. Ingresar Usuario demo
		//c. Ingresar Contraseña demo
		//d. Click en el botón Sign In
		colorlibLoginPage.IngresarDatos(strUsuario, strPass);
		//e. Verificar la Autenticación (Label en home)
		colorlibLoginPage.VerificaHome();
	}

@Step
	public void ingresar_form_validation() {
		colorlibMenuPage.menuFormValidation();
		
	}
}
