package com.choucair.formacion.definition;

import com.choucair.formacion.steps.PopupValidationSteps;

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;
import com.choucair.formacion.steps.ColorlibFormValidationSteps;

public class PopupValidationDefinition {

	@Steps
	PopupValidationSteps popupValidationSteps;
	
	@Steps
	ColorlibFormValidationSteps colorlibFormValidationSteps;
	
	@Given("^Autentico en colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_en_colorlib_con_usuario_y_clave(String arg1, String arg2)  {
		
popupValidationSteps.login_colorlib(arg1, arg2);
	  
	}

	@Given("^Ingreso a la funcionalidad Forms Validation$")
	public void ingreso_a_la_funcionalidad_Forms_Validation()  {
popupValidationSteps.ingresar_form_validation();
	    
	}

	@When("^Diligencio Formulario Popup Validation$")
	public void diligencio_Formulario_Popup_Validation(DataTable dtDatosForm)  {
		List<List<String>> data = dtDatosForm.raw();
	   
		for(int i=1; i<data.size(); i++) {
			colorlibFormValidationSteps.diligenciar_popup_datos_tabla(data, i);
		}
	}

	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso()  {

		colorlibFormValidationSteps.verificar_ingreso_datos_formulario_exitoso();
	    
	}
	
}
