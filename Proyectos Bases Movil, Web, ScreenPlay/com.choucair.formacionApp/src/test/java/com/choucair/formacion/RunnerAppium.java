package com.choucair.formacion;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/APKWordPress/Login/LoginTest.feature" , 
				plugin = {"json:target/cucumber_json/cucumber.json"}, tags = "@Critico" )
public class RunnerAppium {

}
