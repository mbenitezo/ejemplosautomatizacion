package clases;

public class Matematicas {
	
	private double numero1;
	private double numero2;
	
	public void setNumero1(double valor) {
		this.numero1 = valor;
	}
	
	public double getNumero1() {
		return this.numero1;
	}
	
	public void setNumero2(double valor) {
		this.numero2 = valor;
	}
	
	public double getNumero2() {
		return this.numero2;
	}
	
	public double sumar() {
		double resultado;
		resultado = this.getNumero1() + this.getNumero2();
		return resultado;
	}

}
