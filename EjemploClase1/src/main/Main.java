package main;

import java.util.Scanner;
import clases.Matematicas;

public class Main {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		Matematicas matematicas = new Matematicas();
		
		System.out.println("Ingrese el primer número");
		double valor1 = teclado.nextDouble();
		
		System.out.println("Ingrese el segundo número");
		double valor2 = teclado.nextDouble();
		
		matematicas.setNumero1(valor1);
		matematicas.setNumero2(valor2);
		
		System.out.println("La suma es " + matematicas.sumar());
		
		
		
	}

}
