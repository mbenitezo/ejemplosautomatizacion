package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class CompraArticuloEscogerProductoFiltroResolucionPage extends PageObject {
	
	@FindBy(xpath="//*[@id=\"filterBy\"]/div[3]/div[2]/ul/li")
	List<WebElement> TodasLasResoluciones;
	
	public void SeleccionarResolucionProducto(String datoPrueba) {

		JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
	//	js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		js.executeScript("window.scrollBy(0,1000)");
	
		String textoComparar = datoPrueba;

		WebElement auxiliar = null;
		for( WebElement Resoluciones : TodasLasResoluciones ){
			if(Resoluciones.getText().contains( textoComparar )) {
				auxiliar = Resoluciones;
				break;
			}
		}
		auxiliar.click();
		
	}
	
}
