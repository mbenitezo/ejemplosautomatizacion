package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class CompraArticuloListadoAConsolaPage extends PageObject {

	@FindBy(xpath="//*[@id=\'plp\']/div[4]/div[2]/div/div[3]/div")
	List<WebElement> ListaDeProductos;
	
	@FindBy(xpath="//*[@id=\'prd0002523836676001\']/div/div/a/div[1]/h1")
	public WebElementFacade btnPrimerProducto;
	
	public void ListadoProductosEnConsola() {
		for( WebElement Productos : ListaDeProductos ){
			System.out.println("************************************");
			System.out.println("*");
			System.out.println("* " + Productos.getText());
			System.out.println("*");
			System.out.println("************************************");
		}
	}
	
	public void SeleccionPrimerProducto() {
		btnPrimerProducto.click();
	}
	
}
