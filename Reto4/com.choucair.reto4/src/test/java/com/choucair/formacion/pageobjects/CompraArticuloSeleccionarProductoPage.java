package com.choucair.formacion.pageobjects;

import org.openqa.selenium.Keys;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


//@DefaultUrl("https://www.exito.com/Tecnologia-TV_y_Video-Televisores/_/N-2csnZ1z0wac7")
@DefaultUrl("https://www.exito.com")
public class CompraArticuloSeleccionarProductoPage extends PageObject{

	@FindBy(xpath="//*[@id=\"close-wisepop-88598\"]/img")
	private WebElementFacade BtnCerrarPublicidad;
	
	@FindBy(xpath="//*[@id=\'tbxSearch\']")
	private WebElementFacade txtBuscarProductoDeseado;

 public void CerrarPublicidad() {
	
	 if(BtnCerrarPublicidad.isVisible()) {
	 	BtnCerrarPublicidad.click();
	 }
	 
	}
	
	public void EscribirProductoDeseado(String valor) {

		txtBuscarProductoDeseado.click();
		txtBuscarProductoDeseado.clear();
		txtBuscarProductoDeseado.sendKeys(valor);
		this.getDriver().findElement(By.id("tbxSearch")).sendKeys(Keys.ENTER);
		
	}
	
	
}
