package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class CompraArticuloEscogerProductoFiltroMarcaPage extends PageObject {

	@FindBy(xpath="//*[@id=\'filterBy\']/div[2]/div[2]/ul/li/label")
	List<WebElement> TodasLasMarcas;
	
	
	public void SeleccionarMarcaProducto(String datoPrueba){
		
		String textoComparar = datoPrueba;

		WebElement auxiliar = null;
		for( WebElement Marcas : TodasLasMarcas ){
			if(Marcas.getText().contains( textoComparar )) {
				auxiliar = Marcas;
				break;
			}
		}
		auxiliar.click();
	}
	
}
