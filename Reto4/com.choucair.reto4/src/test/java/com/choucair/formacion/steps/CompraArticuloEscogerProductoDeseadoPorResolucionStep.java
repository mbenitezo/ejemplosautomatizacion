package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CompraArticuloEscogerProductoFiltroResolucionPage;

import net.thucydides.core.annotations.Step;

public class CompraArticuloEscogerProductoDeseadoPorResolucionStep {

	CompraArticuloEscogerProductoFiltroResolucionPage compraArticuloEscogerProductoFiltroResolucionPage;
	
	@Step
	public void SeleccionarResolucionDelProducto(List<List<String>> data, int id) {
		
		compraArticuloEscogerProductoFiltroResolucionPage.SeleccionarResolucionProducto(data.get(id).get(2).trim());
		
	}
	
}
