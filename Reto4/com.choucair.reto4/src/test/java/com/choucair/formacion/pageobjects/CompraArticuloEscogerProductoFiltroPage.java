package com.choucair.formacion.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class CompraArticuloEscogerProductoFiltroPage extends PageObject {

	@FindBy(xpath="//*[@id=\'filterBy\']/div[2]/div[2]/ul/li")
	List<WebElement> allProduct;

	public void SeleccionarTamanioProducto(String datoPrueba){
		
		String textoComparar = datoPrueba;

		WebElement auxiliar = null;
		for( WebElement product : allProduct ){
			if(product.getText().contains( textoComparar )) {
				auxiliar = product;
				break;
			}
		}
		auxiliar.click();

	}
}
