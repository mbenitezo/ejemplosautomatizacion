package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CompraArticuloEscogerProductoFiltroPage;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class CompraArticuloEscogerProductoDeseadoPorFiltroStep {

	CompraArticuloEscogerProductoFiltroPage compraArticuloEscogerProductoFiltroPage;
	
	@Step
	public void SeleccionarTamanioDelProducto(List<List<String>> data, int id) {
		
		compraArticuloEscogerProductoFiltroPage.SeleccionarTamanioProducto(data.get(id).get(0).trim());
		Serenity.takeScreenshot();
		
	}
	

}
