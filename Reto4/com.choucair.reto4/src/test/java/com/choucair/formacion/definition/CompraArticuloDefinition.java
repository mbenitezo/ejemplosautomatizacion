package com.choucair.formacion.definition;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.choucair.formacion.steps.CompraArticuloBuscarProductoStep;
import com.choucair.formacion.steps.CompraArticuloEscogerProductoDeseadoPorFiltroStep;
import com.choucair.formacion.steps.CompraArticuloEscogerProductoDeseadoPorMarcaStep;
import com.choucair.formacion.steps.CompraArticuloEscogerProductoDeseadoPorResolucionStep;
import com.choucair.formacion.steps.CompraArticuloListadoProductosEnConsolaStep;

import java.util.List;

import com.choucair.formacion.steps.CompraArticuloAnadirProductoCarritoStep;


public class CompraArticuloDefinition {

	@Steps
	CompraArticuloBuscarProductoStep compraArticuloBuscarProductoStep;
	
	@Steps
	CompraArticuloEscogerProductoDeseadoPorFiltroStep compraArticuloEscogerProductoDeseadoPorFiltroStep;
	
	@Steps
	CompraArticuloEscogerProductoDeseadoPorMarcaStep compraArticuloEscogerProductoDeseadoPorMarcaStep;
	
	@Steps
	CompraArticuloEscogerProductoDeseadoPorResolucionStep compraArticuloEscogerProductoDeseadoPorResolucionStep;
	
	@Steps
	CompraArticuloListadoProductosEnConsolaStep compraArticuloListadoProductosEnConsolaStep;
	
	@Steps
	CompraArticuloAnadirProductoCarritoStep compraArticuloAnadirProductoCarritoStep;
	
	@Given("^Quiero comprar un \"([^\"]*)\" para ver el mundial de futbol$")
	public void quiero_comprar_un_para_ver_el_mundial_de_futbol(String valor) {
		
		compraArticuloBuscarProductoStep.BuscarProducto(valor);
		
	}
	

	@When("^Ingreso a la pagina virtual del exito, selecciono el que mas me gusta$")
	public void ingreso_a_la_pagina_virtual_del_exito_selecciono_el_que_mas_me_gusta(DataTable dtDatosFiltro){
		
		List<List<String>> data = dtDatosFiltro.raw();
		   
		for(int i=1; i<data.size(); i++) {
			compraArticuloEscogerProductoDeseadoPorFiltroStep.SeleccionarTamanioDelProducto(data, i);
			compraArticuloEscogerProductoDeseadoPorMarcaStep.FiltrarPorMarcaProducto(data, i);
			compraArticuloEscogerProductoDeseadoPorResolucionStep.SeleccionarResolucionDelProducto(data, i);
		}
		
		compraArticuloListadoProductosEnConsolaStep.ListadoProductosEnConsola();
		compraArticuloListadoProductosEnConsolaStep.PrimerProducto();
	}

	@Then("^Realizo la compra para que sea enviado a la casa$")
	public void realizo_la_compra_para_que_sea_enviado_a_la_casa(){

		compraArticuloAnadirProductoCarritoStep.LlevarACarritoDeCompra();
		compraArticuloAnadirProductoCarritoStep.VerProductosCarrito();
		
	}
	
}
