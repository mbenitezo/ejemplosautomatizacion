package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CompraArticuloEscogerProductoFiltroMarcaPage;
import net.thucydides.core.annotations.Step;

public class CompraArticuloEscogerProductoDeseadoPorMarcaStep {

	CompraArticuloEscogerProductoFiltroMarcaPage compraArticuloEscogerProductoFiltroMarcaPage;
	
	@Step
	public void FiltrarPorMarcaProducto(List<List<String>> data, int id) {
		
		compraArticuloEscogerProductoFiltroMarcaPage.SeleccionarMarcaProducto(data.get(id).get(1).trim());
		
	}
	
}
