package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class CompraArticuloComprarProductoPage extends PageObject {

	
	@FindBy(xpath="//BUTTON[@class='btn btn-warning btn-lg btn-block btn-add-cart'][text()='Añadir al carrito']")
	private WebElementFacade btnCarritoCompra;
	
	@FindBy(xpath="//*[@id=\'headerUserInfo\']/div[2]/a/span[1]", timeoutInSeconds="5")
	private WebElementFacade btnCerrarModal;
	
	@FindBy(xpath="//A[@class='close_bunting_lightbox close_bunting_lightbox_X'][text()='X']", timeoutInSeconds="5")
	private WebElementFacade btnCerrarModal2;
	
	@FindBy(xpath="//SPAN[@class='icon icoe-carro-compras-lleno']", timeoutInSeconds="5")
	private WebElementFacade btnVerCarritoCompras;
	
	public void PresionarBotorCompra() {
		btnCarritoCompra.click();
	}
	
	public void PresionarBotonCerrarModal() {
		if(btnCerrarModal2.isVisible()) {
			btnCerrarModal2.click();			
		}else {
			btnCerrarModal.click();
		}
	}
	
	public void PresionarBotonVerCarrito() {
		btnVerCarritoCompras.click();
	}
	
}
