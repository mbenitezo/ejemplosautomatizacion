package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.CompraArticuloComprarProductoPage;

import net.thucydides.core.annotations.Step;

public class CompraArticuloAnadirProductoCarritoStep {

	CompraArticuloComprarProductoPage compraArticuloComprarProductoPage;
	
	@Step
	public void LlevarACarritoDeCompra() {
		
		compraArticuloComprarProductoPage.PresionarBotorCompra();
		compraArticuloComprarProductoPage.PresionarBotonCerrarModal();
	}
	
	@Step
	public void VerProductosCarrito() {
		compraArticuloComprarProductoPage.PresionarBotonVerCarrito();
	}
	
}
