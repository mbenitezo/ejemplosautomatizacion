package com.choucair.formacion.steps;

import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.CompraArticuloListadoAConsolaPage;

public class CompraArticuloListadoProductosEnConsolaStep {

	CompraArticuloListadoAConsolaPage compraArticuloListadoAConsolaPage;
	
	@Step
	public void ListadoProductosEnConsola() {
		compraArticuloListadoAConsolaPage.ListadoProductosEnConsola();
	}
	
	@Step
	public void PrimerProducto() {
		compraArticuloListadoAConsolaPage.SeleccionPrimerProducto();
	}
	
}
