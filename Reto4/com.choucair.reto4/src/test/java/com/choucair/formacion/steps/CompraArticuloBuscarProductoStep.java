package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.CompraArticuloSeleccionarProductoPage;

import net.thucydides.core.annotations.Step;

public class CompraArticuloBuscarProductoStep {

	CompraArticuloSeleccionarProductoPage compraArticuloSeleccionarProductoPage;
	
	@Step
	public void BuscarProducto(String valor) {
		
		compraArticuloSeleccionarProductoPage.open();
		compraArticuloSeleccionarProductoPage.CerrarPublicidad();
		compraArticuloSeleccionarProductoPage.EscribirProductoDeseado(valor);
		
	}
}
