package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.Usuario;
import co.com.proyectobase.screenplay.ui.GBFormularioChat;
import co.com.proyectobase.screenplay.ui.GBPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Diligenciar implements Task {

	private List<Usuario> formulario;
	
	public Diligenciar(List<Usuario> formulario) {
		this.formulario = formulario;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
	
		actor.attemptsTo(Click.on(GBPage.BOTON_CHAT));
		
		actor.attemptsTo(Enter.theValue(formulario.get(0).getNombre()).into(GBFormularioChat.NOMBRE));
		
	}

	public static Diligenciar informacionNecesaria(List<Usuario> formulario) {

		return Tasks.instrumented(Diligenciar.class, formulario);
	}


}
