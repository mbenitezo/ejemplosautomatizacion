package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.VerificacionMensajeNuevaCuenta;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class SeMuestra implements Question<String> {

	VerificacionMensajeNuevaCuenta verificacionMensajeNuevaCuenta; 
	
	@Override
	public String answeredBy(Actor actor) {

		return Text.of(VerificacionMensajeNuevaCuenta.MENSAJE_NUEVA_CUENTA).viewedBy(actor).asString();

	}

	public static SeMuestra elMensaje() {

		return new SeMuestra();
	}
}
