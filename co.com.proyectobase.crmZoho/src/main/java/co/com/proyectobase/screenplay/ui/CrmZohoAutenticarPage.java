package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class CrmZohoAutenticarPage extends PageObject{

	public static final Target CORREO_ELECTRONICO  = Target.the("Correo de autenticación").located(By.id("lid"));
	public static final Target CLAVE = Target.the("Clave de autenticación").located(By.id("pwd"));
	public static final Target MANTENER_SESION = Target.the("Check mantener abierta sesión").located(By.id("keepme"));
	public static final Target BOTON_INICIAR_SESION = Target.the("Botón de inicio de sesión").located(By.id("signin_submit"));
	
}
