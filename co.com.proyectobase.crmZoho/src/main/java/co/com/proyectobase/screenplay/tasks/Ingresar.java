package co.com.proyectobase.screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import co.com.proyectobase.screenplay.ui.CrmZohoPage;;

public class Ingresar implements Task {

	CrmZohoPage crmZohoPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(crmZohoPage));
		
	}

	public static Ingresar AlaPaginaDeCrmZoho() {
	
		return instrumented(Ingresar.class);
	}



}
