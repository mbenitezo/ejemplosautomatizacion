package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.google.common.annotations.VisibleForTesting;

import co.com.proyectobase.screenplay.model.Usuario;
import co.com.proyectobase.screenplay.ui.CrmZohoPage;
import co.com.proyectobase.screenplay.ui.FormularioCrmZohoPage;
import co.com.proyectobase.screenplay.util.UtilSeleccionarDeLista;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
//import co.com.proyectobase.screenplay.util.UtilSeleccionarDeLista;
import net.serenitybdd.screenplay.targets.Target;

public class Diligenciar implements Task {

	private List<Usuario> formulario;
	private CrmZohoPage page;
	
	@VisibleForTesting
	FormularioCrmZohoPage formularioCrmZohoPage; 
	
	public Diligenciar(List<Usuario> formulario) {
		this.formulario = formulario;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(formulario.get(0).getNombreCompleto()).into(FormularioCrmZohoPage.NOMBRE_COMPLETO));
		actor.attemptsTo(Enter.theValue(formulario.get(0).getCorreoElectronico()).into(FormularioCrmZohoPage.CORREO_ELECTRONICO));
		actor.attemptsTo(Enter.theValue(formulario.get(0).getClave()).into(FormularioCrmZohoPage.CLAVE));
		actor.attemptsTo(Enter.theValue(formulario.get(0).getTelefono()).into(FormularioCrmZohoPage.TELEFONO));
		UtilSeleccionarDeLista.SeleccionarListaSimple(FormularioCrmZohoPage.PAIS, formulario.get(0).getPais(), actor, "country");
		SelectCheck2(actor);
		actor.attemptsTo(Click.on(FormularioCrmZohoPage.BOTON_REGISTRARSE));
		FuncionEsperar(30);
		actor.attemptsTo(Enter.theValue(formulario.get(0).getNombreEmpresa()).into(FormularioCrmZohoPage.NOMBRE_EMPRESA));
		actor.attemptsTo(Click.on(FormularioCrmZohoPage.ZONA_HORARIA));
		UtilSeleccionarDeLista.SeleccionarListaDiferenteSelect(FormularioCrmZohoPage.LISTA_ZONAS_HORARIAS, formulario.get(0).getZonaHoraria(), actor, "li");
		actor.attemptsTo(Click.on(FormularioCrmZohoPage.MONEDA_LOCAL));
		UtilSeleccionarDeLista.SeleccionarListaDiferenteSelect(FormularioCrmZohoPage.LISTA_MONEDA_LOCAL, formulario.get(0).getMonedaLocal(), actor, "li");
		actor.attemptsTo(Click.on(FormularioCrmZohoPage.BOTOS_EXPLORAR_ZOHO));
//////		actor.attemptsTo(Enter.theValue(formulario.get(0).getNombreEmpresa()).into().thenHit(Keys.TAB));
	}

	public void SelectCheck() {
		  WebDriver driver = page.getDriver();
		  WebElement input = driver.findElement(By.id("tos"));
		  new Actions(driver).moveToElement(input).click().perform();
	}
	
	public void SelectCheck2(Actor actor) {
		JavascriptExecutor executor = (JavascriptExecutor)page.getDriver();
		executor.executeScript("arguments[0].click()", FormularioCrmZohoPage.TERMINOS.resolveFor(actor));
	}
	
	public static void FuncionEsperar(long length) {
		length = length * 1000;
		try {
			Thread.sleep(length);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
	
	public static Diligenciar informacionNecesaria(List<Usuario> formulario) {
		
		return Tasks.instrumented(Diligenciar.class, formulario);
				
	}

}
