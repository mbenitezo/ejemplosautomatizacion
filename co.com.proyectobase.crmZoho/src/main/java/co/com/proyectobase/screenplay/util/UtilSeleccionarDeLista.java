package co.com.proyectobase.screenplay.util;

import org.openqa.selenium.support.ui.Select;

import java.util.List;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.targets.Target;

public class UtilSeleccionarDeLista 
{
	public static void SeleccionarListaSimple(Target elemento, String strValor, Actor actor, String ElementoLista) 
	{
		WebElement lista = (elemento.resolveFor(actor).findElement(By.id(ElementoLista)).equals("") ? elemento.resolveFor(actor).findElement(By.name(ElementoLista)) : elemento.resolveFor(actor).findElement(By.id(ElementoLista)));
		Select drpPais = new Select(lista);
		List<WebElement> options = drpPais.getOptions();
		for(int i=0;i<options.size();i++)
		{
			if(options.get(i).getText().equalsIgnoreCase(strValor))
			{	
				options.get(i).click();
				break;
			}
		}
	}
	
	public static void SeleccionarListaDiferenteSelect(Target objeto, String strValor, Actor actor, String tag) {
		List<WebElement> listObjeto = objeto.resolveFor(actor).findElements(By.tagName(tag));
		for(int i=0; i < listObjeto.size(); i++){
			if (listObjeto.get(i).getText().equals(strValor)) {
				listObjeto.get(i).click();
				break;
			}			 
		}		
	}
	
}