package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.findby.By;

public class FormularioCrmZohoPage {

	public static final Target NOMBRE_COMPLETO = Target.the("El nombre completo").located(By.id("namefield"));
	public static final Target CORREO_ELECTRONICO = Target.the("El correo electr�nico").located(By.id("email"));
	public static final Target CLAVE = Target.the("La contrase�a").located(By.name("password"));
	public static final Target TELEFONO = Target.the("El telefono").located(By.id("mobile"));
	public static final Target PAIS = Target.the("El Pais").located(By.id("country"));
	public static final Target TERMINOS = Target.the("Los t�rminos de condiciones").located(By.id("tos"));
	public static final Target BOTON_REGISTRARSE = Target.the("Bot�n para confirmar los datos del registro").located(By.id("signupbtn"));
	
	public static final Target NOMBRE_EMPRESA = Target.the("El nombre de la empresa").located(By.id("orgName"));
	public static final Target ZONA_HORARIA = Target.the("Zona horaria").located(By.id("select2-userTimeZone-container"));
	public static final Target LISTA_ZONAS_HORARIAS = Target.the("Lista de zonas horarias").located(By.className("select2-results"));
	public static final Target MONEDA_LOCAL = Target.the("Moneda local").located(By.id("select2-currencyLocale-container"));
	public static final Target LISTA_MONEDA_LOCAL = Target.the("Lista de moneda local").located(By.className("select2-results"));
	public static final Target BOTOS_EXPLORAR_ZOHO = Target.the("Bot�n para guardar los par�metros del perfil de la cuenta").located(By.id("profileDetailBtn"));
}
