package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;

import co.com.proyectobase.screenplay.model.Login;
import co.com.proyectobase.screenplay.ui.CrmZohoAutenticarPage;
import co.com.proyectobase.screenplay.ui.CrmZohoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;;

public class Autenticar implements Task {

	private List<Login> formulario;
	private CrmZohoAutenticarPage crmZohoAutenticarPage; 
	private CrmZohoPage page;
	
	public Autenticar(List<Login> formulario) {
		this.formulario = formulario;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(formulario.get(0).getCorreoElectronico()).into(CrmZohoAutenticarPage.CORREO_ELECTRONICO));
		actor.attemptsTo(Enter.theValue(formulario.get(0).getClave()).into(CrmZohoAutenticarPage.CLAVE));
		seleccionarCheck(actor);
		actor.attemptsTo(Click.on(CrmZohoAutenticarPage.BOTON_INICIAR_SESION));
	}

	public void seleccionarCheck(Actor actor)
	{
		JavascriptExecutor executor = (JavascriptExecutor)page.getDriver();
		executor.executeScript("arguments[0].click()", CrmZohoAutenticarPage.MANTENER_SESION.resolveFor(actor));
	}	
	
	public static Autenticar ConLosDatosCorrectos(List<Login> formulario) {

		return instrumented(Autenticar.class, formulario);
		
	}

}
