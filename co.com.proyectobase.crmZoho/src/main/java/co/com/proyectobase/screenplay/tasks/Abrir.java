package co.com.proyectobase.screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import static net.serenitybdd.screenplay.Tasks.instrumented;

import com.google.common.annotations.VisibleForTesting;

import co.com.proyectobase.screenplay.ui.CrmZohoPage;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

	@VisibleForTesting
	CrmZohoPage crmZohoPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {

		actor.attemptsTo(Open.browserOn(crmZohoPage));
		actor.attemptsTo(Click.on(CrmZohoPage.INICIAR_SESION));
		
	}

	public static Abrir LaPagina() {

		return instrumented(Abrir.class);
		
	}

}
