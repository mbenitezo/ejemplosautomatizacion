package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class VerificacionMensajeNuevaCuenta {

	public static final Target MENSAJE_NUEVA_CUENTA = Target.the("Mensaje de confirmación de nueva cuenta").located(By.id("onboaringhome"));
	
}
