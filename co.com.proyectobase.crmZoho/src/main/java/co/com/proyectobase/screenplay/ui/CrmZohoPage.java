package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.zoho.com/es-xl/crm/")
public class CrmZohoPage extends PageObject {

	public static final Target INICIAR_SESION = Target.the("Link de inicio de sesi�n").located(By.className("signin"));
	
}
