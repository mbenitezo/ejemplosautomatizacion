#Author: mbenitezo@choucairtesting.com
@Regresion
Feature: El cliente se registra en la plataforma de zoho y crea un tarea

  @RegistroExitoso
  Scenario: Register user in the crm zoho
    Given Anacleto quiere registrarse en la web del crm zoho
    When el ingresa a la pagina de registro y diligencia la informacion necesaria
       |nombreCompleto 					| correoElectronico 				 | clave   	  | telefono | pais   |	nombreEmpresa		| telefonoEmpresa	|	zonaHoraria																				|	monedaLocal	|      	  
    	 |Anacleto de los angeles | anacletoabc10007@gmail.com | 12345678	  | 1112233  | Spain  |	Empresa prueba	|	5556666					|	(GMT -5:0 ) Hora de Colombia( America/Bogota )		|	Colombia		|
    Then el debe ver el mensaje Estamos contentos de tenerle con nosotros
   
  @CreacionTareaExitosa
	Scenario: Creacion de tareas
    Given Anacleto desea ingresar al crm zoho para crear una tarea
    When el ingresa a la pagina de login y diligencia la informacion requerida
      |	correoElectronico	| clave   	  |      	  
    	|	maalben@gmail.com	|	12345678	  |
    And Realizo la creacion de una tarea con los datos de la tarea
    	|	Asunto							|	Contacto			|	Cuenta						|	Descripcion									|
    	|	3253271000000003447	|	Juana de Arco	|	Cuenta de prueba	|	Prueba de registro exitoso.	|
    Then Verifico tarea creada exitosamente