package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.Login;
import co.com.proyectobase.screenplay.model.Tarea;
import co.com.proyectobase.screenplay.model.Usuario;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Autenticar;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import co.com.proyectobase.screenplay.tasks.SeMuestra;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class ProcesoRegistroCrmZohoStepDefinition {

	@Managed(driver = "firefox")
	private WebDriver hisBrowser;
	Actor Anacleto = Actor.named("Anacleto");
	
	@Before
	public void setup() {

		Anacleto.can(BrowseTheWeb.with(hisBrowser));
	
	}
	
	
	@Given("^Anacleto quiere registrarse en la web del crm zoho$")
	public void anacletoQuiereRegistrarseEnLaWebDelCrmZoho(){

		Anacleto.wasAbleTo(Ingresar.AlaPaginaDeCrmZoho());
	
	}

	@When("^el ingresa a la pagina de registro y diligencia la informacion necesaria$") 
	public void elIngresaALaPaginaDeRegistroYDiligenciaLaInformacionNecesaria(List<Usuario> formulario) {

		Anacleto.attemptsTo(Diligenciar.informacionNecesaria(formulario));
		
	}

	@Then("^el debe ver el mensaje (.*)$")
	public void elDebeVerElMensajeEstamosContentosDeTenerleConNosotros(String mensaje){
		
		Anacleto.should(GivenWhenThen.seeThat(SeMuestra.elMensaje(), Matchers.containsString(mensaje)));
		
	}
	
	
	@Given("^Anacleto desea ingresar al crm zoho para crear una tarea$")
	public void anacletoDeseaIngresarAlCrmZohoParaCrearUnaTarea(){
		Anacleto.wasAbleTo(Abrir.LaPagina());
	}


	@When("^el ingresa a la pagina de login y diligencia la informacion requerida$")
	public void elIngresaALaPaginaDeLoginYDiligenciaLaInformacionRequerida(List<Login> formulario){

		Anacleto.attemptsTo(Autenticar.ConLosDatosCorrectos(formulario));
		
	}

		
	@When("^Realizo la creacion de una tarea con los datos de la tarea$")
	public void realizoLaCreaciNDeUnaTareaConLosDatosDeLaTarea(List<Tarea> formulario){

	//	Anacleto.attemptsTo(Crear.LaTareaConLosDatosSolicitados);

	}

	@Then("^Verifico tarea creada exitosamente$")
	public void verificoTareaCreadaExitosamente(){


	}
	
	
	
}
