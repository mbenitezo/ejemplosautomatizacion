package co.com.proyectobase.screenplay.runners;
//"src/test/resources/features/procesos_crm_zoho.feature"
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features=
		{"src/test/resources/features/procesos_crm_zoho.feature"},
		tags= "@CreacionTareaExitosa",
		glue="co.com.proyectobase.screenplay.stepdefinitions",
		snippets=SnippetType.CAMELCASE		)
public class RunnerTags {

}