#Author: mbenitezo@choucairtesting.com
@Regresion
Feature: El cliente se registra en la plataforma de zoho

  @RegistroExitoso
  Scenario: Register user in the crm zoho
    Given Anacleto quiere registrarse en la web del crm zoho
    When el ingresa a la pagina de registro y diligencia la informacion necesaria
       |nombreCompleto 					| correoElectronico 				 | clave   	  | telefono | pais   |	nombreEmpresa		| telefonoEmpresa	|	zonaHoraria																				|	monedaLocal	|      	  
    	 |Anacleto de los angeles | anacletoabc10004@gmail.com | 12345678	  | 1112233  | Spain  |	Empresa prueba	|	5556666					|	(GMT -5:0 ) Hora de Colombia( America/Bogota )		|	Colombia		|
    Then el debe ver el mensaje Estamos contentos de tenerle con nosotros
    
  