package main;

import java.util.ArrayList;
import java.util.List;

import clases.Gato;
import clases.Perro;
import clases.Puma;
import interfaces.IAnimal;

public class Main {

	public static void main(String[] args) {
	
		IAnimal gato = new Gato();
		//IAnimal perro = new Perro();
		//IAnimal puma = new Puma();
		
		List<IAnimal> animales = new ArrayList<IAnimal>();
		animales.add(gato);
		//animales.add(perro);
		//animales.add(puma);
		//for(int i = 0; i < animales.size(); i++) {
		ejecutarAnimal(animales.get(0));
		//	ejecutarAnimal(animales.get(i));
		//}
	
	}
	
	public static void ejecutarAnimal(IAnimal animal) {
		System.out.println("********************");
		animal.emitirRuido();
		animal.atacar();
	}

}
