package abstracta;

public abstract class Felino {
	public void maullar() {
		System.out.println("Maulla");
	}
	
	public void ejecutarAtaque() {
		System.out.println("me despierto.");
		System.out.println("busco a mi dueño.");
		System.out.println("me acerco a él.");
		this.ultimaAccion();
	}
	
	public abstract void ultimaAccion();
}
