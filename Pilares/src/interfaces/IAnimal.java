package interfaces;

public interface IAnimal {
	void emitirRuido();
	void atacar();
}
