package clases;

import abstracta.Felino;
import interfaces.IAnimal;

public class Puma extends Felino implements IAnimal {

	@Override
	public void emitirRuido() {
		// TODO Auto-generated method stub
		this.maullar();
	}
	
	@Override
	public void atacar() {
		// TODO Auto-generated method stub
		this.ejecutarAtaque();
	}

	@Override
	public void ultimaAccion() {
		// TODO Auto-generated method stub
		System.out.println("me lo como.");
	}
}
