package com.choucair.formacion.steps;

/**
 * Estructura básica de clase para Steps RegistroDatoSteps
 *
 * @author  Mario A. Benítez
 * @version  16 jun. 2018 - 23:56:00
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import com.choucair.formacion.pageobjects.RegistroDatoObject;
import com.choucair.formacion.utilities.ExcelReader;

public class RegistroDatoSteps {
	
	RegistroDatoObject registroDatoObject;
	
	@Step
	public void AbrirSitio() {
		registroDatoObject.open();
	}
	
	@Step
	public void RegistroCorreo() {
		registroDatoObject.IngresarEmail();
		registroDatoObject.PresionarBotonEntrar();
	}
	
	@Step
	public void DatosFormulario(int i) throws Exception {
		registroDatoObject.IngresarNombre( ExcelReader.getCellData(i, 0).trim() );
		registroDatoObject.IngresarApellido( ExcelReader.getCellData(i, 1).trim() );
		registroDatoObject.IngresarDireccion( ExcelReader.getCellData(i, 2).trim() );
		registroDatoObject.IngresarCorreo( ExcelReader.getCellData(i, 3).trim() );
		registroDatoObject.IngresarTelefono( ExcelReader.getCellData(i, 4).trim() );
		RegistroDatoObject.SeleccionarGenero( ExcelReader.getCellData(i, 5).trim() );
		registroDatoObject.SeleccionarHobbies();
	}
	
	
}