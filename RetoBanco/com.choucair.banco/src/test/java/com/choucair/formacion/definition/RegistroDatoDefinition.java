package com.choucair.formacion.definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.choucair.formacion.utilities.ExcelReader;

import com.choucair.formacion.steps.RegistroDatoSteps;

public class RegistroDatoDefinition {

	private String rutaArchivo = "D:\\PracticasAutomatizacion\\RetoBanco\\com.choucair.banco\\src\\test\\resources\\Datadriven\\Datos.xlsx";
	private String hojaDeCalculo = "Hoja1";
	private int cantidadFilas;
	
		public RegistroDatoDefinition() throws Exception {
			ExcelReader.setExcelFile(this.rutaArchivo, this.hojaDeCalculo);
			this.cantidadFilas = ExcelReader.ContarFilas();
		}
	
	@Steps
	RegistroDatoSteps registroDatoSteps;
	
	@Given("^Dado que quiero ingresar a la pagina de retos$")
	public void dado_que_quiero_ingresar_a_la_pagina_de_retos(){

		registroDatoSteps.AbrirSitio();
		
	}

	@Given("^Realizo el ingreso de un correo$")
	public void realizo_el_ingreso_de_un_correo(){

		registroDatoSteps.RegistroCorreo();
		
	}

	@When("^Realice el llenado del formulario$")
	public void realice_el_llenado_del_formulario() throws Exception{
		int posicionFila = 4;
		registroDatoSteps.DatosFormulario( posicionFila );
		ExcelReader.CerrarBook();
	}

	@Then("^Validar el registro exitoso$")
	public void validar_el_registro_exitoso(){

		
	}
	
}
