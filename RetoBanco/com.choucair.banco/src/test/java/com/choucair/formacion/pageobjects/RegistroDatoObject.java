package com.choucair.formacion.pageobjects;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Estructura básica de clase para Object RegistroDatoObject
 *
 * @author  Mario A. Benítez
 * @version  16 jun. 2018 - 23:52:26
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.CoreMatchers.*;


import com.choucair.formacion.utilities.Utiles;

//@DefaultUrl("http://demo.automationtesting.in/Index.html")
//@DefaultUrl("http://demo.automationtesting.in/Frames.html")
@DefaultUrl("http://www.eltiempo.com/zona-usuario/crear")
public class RegistroDatoObject extends PageObject {
	
//	@FindBy(id = "email")
//	public WebElementFacade txtEmail;
//	
//	@FindBy(id = "enterimg")
//	public WebElementFacade btnEntrar;
//	
//	@FindBy(ngModel = "FirstName")
//	public WebElementFacade txtNombre;
//
//	@FindBy(ngModel = "LastName")
//	public WebElementFacade txtApellido;
//
//	@FindBy(ngModel = "Adress")
//	public WebElementFacade txtDireccion;
//	
//	@FindBy(ngModel = "EmailAdress")
//	public WebElementFacade txtCorreo;

//	@FindBy(ngModel = "Phone")
//	public WebElementFacade txtTelefono;

//	@FindBy(tagName = "input")
//	public WebElementFacade txtTelefono;
//	
//	@FindBy(name = "radiooptions")
//	public static List<WebElement> radioGenero;


	
	
//	@FindBy(xpath="//*[@id=\\'basicBootstrapForm\\']/div[6]/div")
//	public List<WebElement> contenedorCheck;

	
//	@FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div/div/div[1]/div[2]/div[2]/div[2]/a")
//	private List<WebElement> LinksBancos;
	
	public WebElement algo(String xpathValue, String substitutionValue) {
		//@FindBy(id = dato)
		//private WebElementFacade dato2;
		return getDriver().findElement(By.id(xpathValue.replace("xxxxx", substitutionValue)));	
	}
	
	public void SeleccionarHobbies(){
		
		
//		int i=0;
//		for( WebElement Enlaces : LinksBancos ){
//			i++;
//			Enlaces.click();
//			if(i==10) {	break; }
//		}
//		Object[] TodasLasVentanasCargadas = getDriver().getWindowHandles().toArray();
//		
//		for(int m=0; m<10; m++) {
//			getDriver().switchTo().window(TodasLasVentanasCargadas[m].toString());
//			Utiles.FuncionEsperar(5);
//		}
		
//		getDriver().findElement(By.cssSelector(".example a")).click();
//		getDriver().findElement(By.cssSelector(".example a")).click();
//		getDriver().findElement(By.cssSelector(".example a")).click();
//        Object[] TodasLasVentanasCargadas = getDriver().getWindowHandles().toArray();
//        System.out.println("Array: "+ getDriver().getWindowHandles().toArray() );
//        
//        
//        getDriver().switchTo().window(TodasLasVentanasCargadas[0].toString());
//        System.out.println("Principal "+ getDriver().switchTo().window(TodasLasVentanasCargadas[0].toString()) );
//        //assertThat(getDriver().getTitle(), is(not("New Window")));
//        getDriver().switchTo().window(TodasLasVentanasCargadas[1].toString());
//        System.out.println("Secundaria "+ getDriver().switchTo().window(TodasLasVentanasCargadas[1].toString()) );
//       // assertThat(getDriver().getTitle(), is("New Window"));
//        Utiles.FuncionEsperar(5);
//        getDriver().close();
//        getDriver().switchTo().window(TodasLasVentanasCargadas[0].toString());
//        Utiles.FuncionEsperar(5);
//        getDriver().findElement(By.cssSelector(".example a")).click();
//         Utiles.FuncionEsperar(5);
//        getDriver().switchTo().window(TodasLasVentanasCargadas[0].toString());
//        Utiles.FuncionEsperar(5);
        
//		WebElement ElementoPrincipal = getDriver().findElement(By.id("basicBootstrapForm"));
//		WebElement ElementoHijo1 = ElementoPrincipal.findElement(By.className("form-group"));
//		WebElement ElementoHijo2 = ElementoHijo1.findElement(By.xpath("//*[@id=\'basicBootstrapForm\']/div[6]/div"));
//		WebElement ElementoHijo3 = ElementoHijo2.findElement(By.tagName("div"));
//		WebElement ElementoHijo4 = ElementoHijo3.findElement(By.id("checkbox1"));
//		ElementoHijo4.click();

		
//		String textoComparar = "Movies";
//		WebElement auxiliar = null;
//		for( WebElement Opcion : contenedorCheck ){
//			System.out.println("-----  "+ Opcion + "  -----");
//			if(Opcion.getText().contains( textoComparar )) {
//				auxiliar = Opcion;
//				break;
//			}
//		}
//		auxiliar.click();
	}

	
	/* Campo txtEmail  */
	public void IngresarEmail() {
//		txtEmail.click();
//		txtEmail.clear();
//		txtEmail.sendKeys("correoprueba@correo.com");
	}
	
	/* Campo btnEntrar */
	public void PresionarBotonEntrar() {
//		btnEntrar.click();
	}
	
	/* Campo txtNombre  */
	public void IngresarNombre(String Dato){
//		Utiles.FuncionTextoEnCampo(txtNombre, Dato);
	}
	
	/* Campo txtApellido  */
	public void IngresarApellido(String Dato) {
//		Utiles.FuncionTextoEnCampo(txtApellido, Dato);
	}
	
	/* Campo txtDireccion  */
	public void IngresarDireccion(String Dato) {
//		Utiles.FuncionTextoEnCampo(txtDireccion, Dato);
	}
	
	/* Campo txtCorreo  */
	public void IngresarCorreo(String Dato) {
//		Utiles.FuncionTextoEnCampo(txtCorreo, Dato);
	}
	
	/* Campo txtTelefono  */
	public void IngresarTelefono(String Dato) {
//		Utiles.FuncionMoverPagina(getDriver(), "Vertical", 200);
//		Utiles.FuncionIngresarAFrame(getDriver(), 0);
//		Utiles.FuncionTextoEnCampo(txtTelefono, Dato);
//		Utiles.FuncionSalirDeFrame(getDriver());
//		Utiles.FuncionMoverPagina(getDriver(), "Vertical", -200);
	}
	

	
	/* Campo radioGenero */
	public static void SeleccionarGenero(String Dato) {
//		Utiles.FuncionSeleccionarRadio(radioGenero, Dato);
	}
}