package com.choucair.formacion.utilities;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;

public class Utiles {

/**
 * 
 * <center><b><font color='#FF2D00'>Método para seleccionar un Radio de un contenedor que tenga varios Radiobutton</font></b></center>
 * 	
 * @author ChoucairTesting
 * @since Version 0.1
 * 
 * @param Elemento <br>Variable que se define en el WebElementFacade del contenedor. Tenga en cuenta que en el pageObject debe ser <br><b>@FindBy(name = "nombre en la Web") <br>public static List'<'WebElement'>' nombreElemento; </b><br>Sin las comillas simples en el WebElement y que no necesariamente debe ser name, también puede ser id ó xpath.<br> 
 * @param Texto <br>El valor que debe buscar en el contenedor para dar el clic.
 * 
 */
	public static void FuncionSeleccionarRadio(List<WebElement> Elemento, String Texto) {
		int contador = Elemento.size();
		for(int i=0; i<contador; i++) {
			String valor = Elemento.get(i).getAttribute("value");
				if(valor.equalsIgnoreCase(Texto)) {
					Elemento.get(i).click();					
					break;
				}
		}
	}
	
	public static void FuncionMoverPagina(WebDriver getDriver, String Direccion, int PixelesAMover) {
		JavascriptExecutor js = (JavascriptExecutor) getDriver;
		switch(Direccion) {
		case "Horizontal":
			js.executeScript("window.scrollBy("+PixelesAMover+",0)");
			break;
		case "Vertical":	
			js.executeScript("window.scrollBy(0,"+PixelesAMover+")");
			break;
		}
	}
	
	public static void FuncionIngresarAFrame(WebDriver getDriver, String Frame) {
		getDriver.switchTo().frame(Frame);		
	}
	
	public static void FuncionIngresarAFrame(WebDriver getDriver, int IndiceFrame) {
		getDriver.switchTo().frame(IndiceFrame);		
	}
	
	public static void FuncionSalirDeFrame(WebDriver getDriver) {
		getDriver.switchTo().defaultContent();	
	}
	
	public static void FuncionEsperar(long length) {
		length = length * 1000;
		try {
			Thread.sleep(length);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}
	

	/**
	 * 
	 * <center><b><font color='#FF2D00'>Método para ver qué campos hay visibles en una página</font></b></center>
	 * 	
	 * @author ChoucairTesting
	 * @since Version 0.1
	 * 
	 * @param getDriver <br>Solo se debe colocar getDriver() para que haga referencia al sitio en cuestión.<br>Tenga en cuenta que los resultados se ven por consola.<br> 
	 * 
	 */	
	public static void FuncionCamposVisibles(WebDriver getDriver) {
		List<WebElement> AlgunElemento = getDriver.findElements(By.tagName("input"));
		for (WebElement UnElemento : AlgunElemento) {
			if(UnElemento.getAttribute("type").equals("text")) {
				System.out.println("Campo de tipo texto: " + UnElemento.getAttribute("id"));
			}else {
				if(UnElemento.getAttribute("type").equals("password")) {
					System.out.println("Campo de tipo password: " + UnElemento.getAttribute("id"));
				}else {
					if(UnElemento.getAttribute("type").equals("checkbox")) {
						System.out.println("Campo de tipo Checkbox: " + UnElemento.getAttribute("id"));
					}else {
						if(UnElemento.getAttribute("type").equals("radio")) {
							System.out.println("Campo de tipo Radio: " + UnElemento.getAttribute("id"));
						}
					}
				}
			}
		}
	}
	
	public static void FuncionDatosCamposTextos(WebDriver getDriver, String camposValidosTextos[], String textoscamposValidos[]) {
		int i = 0;
		List<WebElement> AlgunElemento = getDriver.findElements(By.tagName("input")); 
		for (WebElement UnElemento : AlgunElemento) {
			if(UnElemento.getAttribute("type").equals("text")) {
				if(UnElemento.getAttribute("id").equals(camposValidosTextos[i])) {
//					System.out.println( UnElemento.getAttribute("id") );
					Elemento(getDriver, UnElemento.getAttribute("id")).sendKeys( textoscamposValidos[i] );
					i++;					
				}
			}		
		}	
	}
	
	public static void FuncionDatosCamposPassword(WebDriver getDriver, String camposValidosPass[], String PasscamposValidos[]) {
		int i = 0;
		List<WebElement> AlgunElemento = getDriver.findElements(By.tagName("input")); 
		for (WebElement UnElemento : AlgunElemento) {
			if(UnElemento.getAttribute("type").equals("password")) {
				if(UnElemento.getAttribute("id").equals(camposValidosPass[i])) {
//					System.out.println( UnElemento.getAttribute("id") );
					Elemento(getDriver, UnElemento.getAttribute("id")).sendKeys( PasscamposValidos[i] );
					i++;					
				}
			}		
		}	
	}

	public static WebElement Elemento(WebDriver getDriver, String valorID) {
		return getDriver.findElement(By.id(valorID));	
	}
/*/*//*/**//*/*//**//*/*//**//*/*//**//*/*/	
	
	
	
	
	public static void FuncionTextoEnCampo(WebElementFacade Elemento, String Dato) {
		Elemento.click();
		Elemento.clear();
		Elemento.sendKeys( Dato );
	}
	
	
}
