#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: App WordPress
  El usuario debe poder cargar la APP y realizar la autenticación
  de manera exitosa en la misma. Posteriormente verificar que se 
  haya ingresado en la APP a través de un Label que se leerá en
  la misma.

  @CasoExitoso
  Scenario: Usar credenciales valido para Logueo
    Given Inicio la app para la prueba
    When ingreso credenciales correctas "pruebaappappium@gmail.com" y "pruebaapp99"
    Then verificar el acceso a la APP


