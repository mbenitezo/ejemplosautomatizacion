package com.choucair.formacion.definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.choucair.formacion.steps.LoginTestSteps;

public class LoginTestDefinition {

	@Steps
	LoginTestSteps loginTestSteps;
	
	@Given("^Inicio la app para la prueba$")
	public void inicio_la_app_para_la_prueba() throws Throwable{
		loginTestSteps.iniciar_autenticacion();
	}

	@When("^ingreso credenciales correctas \"([^\"]*)\" y \"([^\"]*)\"$")
	public void ingreso_credenciales_correctas_y(String strEmail, String strPass) throws Throwable{
		loginTestSteps.ingresar_credenciales(strEmail, strPass);
	}

	@Then("^verificar el acceso a la APP$")
	public void verificar_el_acceso_a_la_APP(){
		loginTestSteps.verificar_acceso_exitoso_app();
	}
	
}
