package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.choucair.formacion.util.MobilePageObject;

import io.appium.java_client.pagefactory.AndroidFindBy;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class LoginTestPage extends MobilePageObject {

	public LoginTestPage(WebDriver driver) {
		super(driver);

	}

//campo login
@AndroidFindBy(id = "org.wordpress.android:id/login_button")
private WebElement btnLogin;

//campo email
@AndroidFindBy(className = "android.widget.EditText")
private WebElement txtEmailAddress;

//botón siguiente
@AndroidFindBy(id = "org.wordpress.android:id/primary_button")
private WebElement btnNextEmail;

//enlace link ingresar contraseña 
@AndroidFindBy(id = "org.wordpress.android:id/login_enter_password")
private WebElement lnkPassword;

//campo contraseña
@AndroidFindBy(className = "android.widget.EditText")
private WebElement txtPassword;

//botón Siguiente
@AndroidFindBy(id = "org.wordpress.android:id/primary_button")
private WebElement btnNextPass;

//encabezado acceso al sistema
@AndroidFindBy(id = "org.wordpress.android:id/logged_in_as_heading")
private WebElement lblLoged;

public void LoginApp() throws InterruptedException {
	esperar_segundos(5);
	if(btnLogin.isEnabled()) {
//		System.out.println("Se encuentra habilitado");
		btnLogin.click();
		btnLogin.click();
		esperar_segundos(2);
	}else {
		if(btnLogin.isDisplayed()) {
//			System.out.println("Apareceeeee");
			btnLogin.click();
			btnLogin.click();
			esperar_segundos(2);
		}
		
	}

}

public void ingresar_credenciales(String StrEmail, String strPassword) throws InterruptedException {
	txtEmailAddress.sendKeys(StrEmail);
	btnNextEmail.click();
	esperar_segundos(2);
	lnkPassword.click();
	txtPassword.sendKeys(strPassword);
	btnNextPass.click();
	esperar_segundos(4);
}

public void VerificarAccesoApp() {
	String lblMensajeLogged = "Conectado como";
	String strMensajePantalla = lblLoged.getText();
	assertThat(strMensajePantalla, containsString(lblMensajeLogged));
}

public void esperar_segundos(int intEspera) throws InterruptedException {
	Thread.sleep(intEspera * 1000);
}


}
