package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.LoginTestPage;
import net.thucydides.core.annotations.Step;

public class LoginTestSteps {

	LoginTestPage loginTestPage;
	
	@Step
	public void iniciar_autenticacion() throws InterruptedException {
		//a. Abrir la APP.
		//b. Dar clic en el botón LOG IN
		loginTestPage.LoginApp();
	}
	
	@Step
	public void ingresar_credenciales(String strEmail, String strPassword) throws InterruptedException {
		//c. Ingresar pruebaappappium@gmail.com en el campo Email Address
		//d. Dar clic en el botón NEXT
		//e. Dar clic en el link "ENTER your password instead"
		//f. Ingresar "pruebaapp99" en el campo Password
		//g. Dar clic en el botón NEXT
		loginTestPage.ingresar_credenciales(strEmail, strPassword);
	}
	
	@Step
	public void verificar_acceso_exitoso_app() {
		//h. Verificar la autenticación (Logged in as)
		loginTestPage.VerificarAccesoApp();
	}
	
}
