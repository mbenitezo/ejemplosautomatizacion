package co.com.proyectobase.screenplay.runners;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.runner.RunWith;

import co.com.proyectobase.screenplay.utilexceldata.BeforeSuite;
import co.com.proyectobase.screenplay.utilexceldata.DataToFeature;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;



@CucumberOptions (features = "src/test/resources/features/grupo_chat.feature",
					tags= {"@casoExitoso"},
					glue = "co.com.proyectobase.screenplay.stepdefinitions",
					snippets= SnippetType.CAMELCASE)
@RunWith(RunnerPersonalizado.class)
public class RunnerTags {
	@BeforeSuite
	public static void test() throws InvalidFormatException, IOException {
			DataToFeature.overrideFeatureFiles("./src/test/resources/features/grupo_chat.feature");
	}
}



//import org.junit.runner.RunWith;
//
//import cucumber.api.CucumberOptions;
//import cucumber.api.SnippetType;
//import net.serenitybdd.cucumber.CucumberWithSerenity;
//
//@RunWith(CucumberWithSerenity.class)
//@CucumberOptions(
//		features="src/test/resources/features/grupo_chat.feature",
//		tags= "@casoExitoso",
//		glue="co.com.proyectobase.screenplay.stepdefinitions",
//		snippets=SnippetType.CAMELCASE		)
//public class RunnerTags {
//
//}
