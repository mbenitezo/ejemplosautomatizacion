package main;

import java.util.Scanner;

import clases.Autobus;
import clases.Taxi;

public class Main {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		Autobus autobus = new Autobus();
		Taxi taxi = new Taxi();
		
		autobus.encenderVehiculo();
		System.out.println("Cuántos puesto tiene el autobus?");
		autobus.setPuestos(teclado.nextInt());
		System.out.println("La cantidad de puesto del autobus es: " + autobus.getPuestos());
		
		taxi.encenderVehiculo();
		
	}

}
