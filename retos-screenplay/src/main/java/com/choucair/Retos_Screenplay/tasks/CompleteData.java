package com.choucair.Retos_Screenplay.tasks;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.choucair.Retos_Screenplay.model.RegisterAutomationDemo;
import com.choucair.Retos_Screenplay.ui.WebAutomationDemoSitePageRegister;
import static com.choucair.Retos_Screenplay.ui.WebAutomationDemoSitePageRegister.*;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;


public class CompleteData implements Task{
	WebAutomationDemoSitePageRegister element;
	private  List<RegisterAutomationDemo> data;
	
	public CompleteData(List<RegisterAutomationDemo> data2) {
		this.data = data2;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		String directory = System.getProperty("user.dir");
		String filepath = directory + "//src//test//resources//images//imagen.png";
		
		actor.attemptsTo(Enter.theValue(data.get(0).getFirstname()).into(TXT_FIRST_NAME));
		actor.attemptsTo(Enter.theValue(data.get(0).getLastname()).into(TXT_LAST_NAME));
		actor.attemptsTo(Enter.theValue(data.get(0).getAddress()).into(ADDRESS));
		actor.attemptsTo(Enter.theValue(data.get(0).getEmail()).into(EMAIL));
		actor.attemptsTo(Enter.theValue(data.get(0).getPhone()).into(PHONE));
		actor.attemptsTo(Click.on(GENDER));
		actor.attemptsTo(Click.on(HOBBIES));
		
		UPLOAD.resolveFor(actor).sendKeys(filepath);
		
		actor.attemptsTo(Click.on(LANGUAGES_0));
		
		for (WebElementFacade elementFacade : LANGUAGES_1.resolveAllFor(actor)) {
			if (elementFacade.getText().contains(data.get(0).getLanguages())) {
				element.getDriver().findElement(By.linkText(data.get(0).getLanguages())).click();
				actor.attemptsTo(Click.on(BODY_LANGUAGES));
			}
		}		
		
		
		actor.attemptsTo(Click.on(SKILLS));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getSkills()).from(SKILLS));
		
		actor.attemptsTo(Click.on(COUNTRY));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getCountry()).from(COUNTRY));
		
		actor.attemptsTo(Click.on(SELECT_COUNTRY_0));
		actor.attemptsTo(Enter.theValue(data.get(0).getSelectcountry()).into(SELECT_COUNTRY_1));
		SELECT_COUNTRY_1.resolveFor(actor).sendKeys(Keys.RETURN);
		
		actor.attemptsTo(Click.on(YEAR));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getYear()).from(YEAR));
		
		actor.attemptsTo(Click.on(MONTH));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getMonth()).from(MONTH));
		
		actor.attemptsTo(Click.on(DAY));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(0).getDay()).from(DAY));
		
		actor.attemptsTo(Enter.theValue(data.get(0).getPass1()).into(PASS1));
		actor.attemptsTo(Enter.theValue(data.get(0).getPass2()).into(PASS2));	

		actor.attemptsTo(Click.on(SUBMIT));
		
		
	}

	public static CompleteData inTheForm(List<RegisterAutomationDemo> data2) {
		return new CompleteData(data2);
	}

		

}
