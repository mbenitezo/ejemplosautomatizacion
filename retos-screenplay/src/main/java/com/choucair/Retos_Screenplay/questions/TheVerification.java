package com.choucair.Retos_Screenplay.questions;

import static com.choucair.Retos_Screenplay.ui.WebAutomationDemoSitePageHome.LBL_HOME;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class TheVerification implements Question<String>{

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(LBL_HOME).viewedBy(actor).asString();
	}

	public static TheVerification Home() {
		// TODO Auto-generated method stub
		return new TheVerification();
	}

}
