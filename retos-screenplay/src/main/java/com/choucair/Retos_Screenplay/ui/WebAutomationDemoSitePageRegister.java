package com.choucair.Retos_Screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class WebAutomationDemoSitePageRegister extends PageObject{
	
	public static final Target TXT_FIRST_NAME = Target.the("Campo donde se ingresa el nombre").located(By.xpath("//*[contains(@ng-model,'FirstName')]"));
	public static final Target TXT_LAST_NAME  = Target.the("Campo donde se ingresa el apellido").located(By.xpath("//*[@id='basicBootstrapForm']/div[1]/div[2]/input"));
	public static final Target ADDRESS = Target.the("Campo donde se ingresa direccio").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[2]/div/textarea"));
	public static final Target EMAIL = Target.the("Campo donde se ingresa el correo electronico").located(By.xpath("//*[@id=\"eid\"]/input"));
	public static final Target PHONE = Target.the("Campo donde se ingresa el telefono").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[4]/div/input"));
	
	public static final Target LANGUAGES_0 = Target.the("Campo donde se ingresa el idioma").located(By.xpath("//*[@id=\"msdd\"]"));
	public static final Target LANGUAGES_1 = Target.the("Campo donde se ingresa el idioma").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[7]/div/multi-select/div[2]/ul"));
	public static final Target BODY_LANGUAGES = Target.the("Campo donde se ingresa el idioma").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[6]"));
						                  			
	
	public static final Target SKILLS = Target.the("Campo donde se ingresa sus avilidades").located(By.xpath("//*[@id=\"Skills\"]"));
	public static final Target COUNTRY = Target.the("Campo donde se ingresa pais").located(By.xpath("//*[@id=\"countries\"]"));
	
	public static final Target SELECT_COUNTRY_0 = Target.the("Campo donde se selecciona otro pais").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[10]/div/span/span[1]/span"));
	public static final Target SELECT_COUNTRY_1 = Target.the("Campo donde se selecciona otro pais").located(By.xpath("/html/body/span/span/span[1]/input"));
	
	public static final Target YEAR = Target.the("Campo donde se ingresa el año").located(By.xpath("//*[@id=\"yearbox\"]"));
	public static final Target MONTH = Target.the("Campo donde se ingresa el mes").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select"));
	public static final Target DAY = Target.the("Campo donde se ingresa el dia").located(By.xpath("//*[@id=\"daybox\"]"));
	
	
	public static final Target PASS1 = Target.the("Campo donde se ingresa la contraseña").located(By.xpath("//*[@id=\"firstpassword\"]"));
	public static final Target PASS2 = Target.the("Campo donde se ingresa la contraseña").located(By.xpath("//*[@id=\"secondpassword\"]"));
	
	public static final Target GENDER = Target.the("seleccion de genero").located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[5]/div/label[1]/input"));
	public static final Target HOBBIES = Target.the("seleccion de pasatiempos").located(By.xpath("//*[@id=\"checkbox1\"]"));
	
	
	public static final Target UPLOAD = Target.the("seleccion de pasatiempos").located(By.xpath("//*[@id=\"imagesrc\"]"));
	
	public static final Target SUBMIT = Target.the("seleccion de pasatiempos").located(By.xpath("//*[@id=\"submitbtn\"]"));
	

}


