package com.choucair.Retos_Screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.targets.Target;

public class WebAutomationDemoSitePageHome {
	
	public static final Target LBL_HOME = Target.the("Label de home").located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));

}
