package com.choucair.Retos_Screenplay.stepdefinitions;

import java.util.List;

import static org.hamcrest.Matchers.*;
import org.openqa.selenium.WebDriver;

import com.choucair.Retos_Screenplay.model.RegisterAutomationDemo;
import com.choucair.Retos_Screenplay.questions.TheVerification;
import com.choucair.Retos_Screenplay.tasks.CompleteData;
import com.choucair.Retos_Screenplay.tasks.Enter;


import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import static net.serenitybdd.screenplay.GivenWhenThen.*;

public class WebAutomationDemoSiteDefinitions {

	
	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor pepe = Actor.named("pepe");

	@Before
	public void InitialConfiguration() {
		pepe.can(BrowseTheWeb.with(hisBrowser));
	}

	@Given("^that Pepe wants to access the Web Automation Demo Site$")
	public void thatPepeWantsToAccessTheWebAutomationDemoSite() throws Exception {
		pepe.wasAbleTo(Enter.toWebAutomationDemoSite());
	}

	@When("^he performs the registration on the page$")
	public void hePerformsTheRegistrationOnThePage(List<RegisterAutomationDemo> data) throws Exception {
		pepe.attemptsTo(CompleteData.inTheForm(data));
	}

	@Then("^verify that the screen is loaded with text (.*)$")
	public void verifyThatTheScreenIsLoadedWithTextDoubleClickOnEditIconToEDITTheTableRow(String verification) throws Exception {
		pepe.should(seeThat(TheVerification.Home(),equalToIgnoringCase(verification)));
	}
}
