#Author: mbenitezo@choucairtesting.com
@Regression
Feature: As a user I want to register in the web Automation Demo Site verify the home screen

@Register
Scenario: Register in the Web Automation Demo Site
Given that Pepe wants to access the Web Automation Demo Site 
When he performs the registration on the page
|firstname |lastname |address        |email     	    |phone     |languages|skills|country |selectcountry|year|month    |day |pass1        |       pass2 |
|Anacleto  |Soler    |Cra 35 # 23-11 |anacleto@gmail.com|0234567890|English  |Java  |Colombia|India    	   |1987|September|25  |Medellin2018+|Medellin2018+|
 
Then verify that the screen is loaded with text - Double Click on Edit Icon to EDIT the Table Row.