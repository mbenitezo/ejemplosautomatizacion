package main;

import static constantes.Constantes.SALUDO_PROGRAMA;
import static constantes.Constantes.PI;
import static constantes.Constantes.CALCULO;

public class Main {

	public static void main(String[] args) {
		
		System.out.println(SALUDO_PROGRAMA);
		System.out.println("El valor de PI es " + PI);
		System.out.println("El cálculo es " + CALCULO);

		
	}

}
