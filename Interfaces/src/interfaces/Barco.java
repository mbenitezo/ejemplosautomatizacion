package interfaces;

public interface Barco {
	
	void moverPosicion(int x, int y);
	void disparar();

}
