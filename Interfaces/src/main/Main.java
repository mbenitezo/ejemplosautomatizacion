package main;

import java.util.Scanner;

import clases.BarcoPirata;

public class Main {
	
	public static void main(String args[]) {
		Scanner teclado = new Scanner(System.in);
		BarcoPirata barcoPirata = new BarcoPirata();
		
		System.out.println("Ingresa el movimiento en X:");
		int x = teclado.nextInt();
		
		System.out.println("Ingresa el movimiento en Y:");
		int y = teclado.nextInt();
		
		barcoPirata.setX(x);
		barcoPirata.setY(y);
		
		barcoPirata.moverPosicion(50, 20);
		barcoPirata.conocerPosicion();
		barcoPirata.disparar();
		
	}
	
	
	

}
