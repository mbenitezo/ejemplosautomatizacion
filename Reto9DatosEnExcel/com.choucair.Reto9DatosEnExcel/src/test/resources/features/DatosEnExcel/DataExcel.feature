#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Realizar la consulta de bancos en las páginas amarillas

  @CasoExitoso
  Scenario: Buscar y verificar páginas de bancos encontrados en las páginas amarillas
    Given Quiero realizar la busqueda de los "Bancos"
    When Se despliega la informacion de estos
    And Registro las urls en un archivo de Excel
    Then Verifico que el link de la pagina web funcione

