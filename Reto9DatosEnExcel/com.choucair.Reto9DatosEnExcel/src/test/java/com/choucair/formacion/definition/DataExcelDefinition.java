package com.choucair.formacion.definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.choucair.formacion.steps.DataExcelSteps;

public class DataExcelDefinition {

 	private String archivo = "D://PracticasAutomatizacion//Reto9DatosEnExcel//com.choucair.Reto9DatosEnExcel//src//test//resources//Datadriven//Datos.xlsx"; 
 	private String hoja = "Hoja1"; 
	
	@Steps
	DataExcelSteps dataExcelSteps;
	
	@Given("^Quiero realizar la busqueda de los \"([^\"]*)\"$")
	public void quiero_realizar_la_busqueda_de_los_en(String criterio)  {
		dataExcelSteps.AbrirPagina();
		dataExcelSteps.BuscarBancos(criterio);	
	}

	@When("^Se despliega la informacion de estos$")
	public void se_despliega_la_informacion_de_estos() throws Exception{

		dataExcelSteps.ProcesoExcel(archivo, hoja);
	

	}

	@When("^Registro las urls en un archivo de Excel$")
	public void registro_las_urls_en_un_archivo_de_Excel() throws Throwable {
		dataExcelSteps.RecuperarBancos();


	}

	@Then("^Verifico que el link de la pagina web funcione$")
	public void verifico_que_el_link_de_la_pagina_web_funcione() throws Throwable {
		dataExcelSteps.CerrarProcesoExcel(archivo);

	}
	
}
