package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import com.choucair.formacion.utilities.ExcelReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

@DefaultUrl("http://www.paginasamarillas.com.co/")
public class DataExcelPage extends PageObject {

	public static WebDriver driver;
	
	@FindBy(id="keyword")
	private WebElementFacade txtCriterioBusqueda;
	
	@FindBy(id="buscar")
	private WebElementFacade btnBuscar;	

	@FindBy(xpath = "/html/body/div[1]/div[2]/div/div[2]/div/div/div[1]/div[2]/div[2]/div[2]/a")
	private List<WebElement> LinksBancos;
	
	public void IngresarDatosDeBusqueda(String criterio) {
		txtCriterioBusqueda.click();
		txtCriterioBusqueda.clear();
		txtCriterioBusqueda.sendKeys(criterio);
		btnBuscar.click();
	}
	
	private void ejecutarJavaScript(String pagina) {
		WebDriver driver = this.getDriver();
		JavascriptExecutor js = null;
		String comandoJs = "window.open('" + pagina + "', 'new_window')";
		if (driver instanceof JavascriptExecutor) {
			js = (JavascriptExecutor) driver;
			js.executeScript(comandoJs);
		}
	}
	
	public void AbrirExcel(String archivo, String hoja) throws Exception {
		ExcelReader.setExcelFile(archivo, hoja);
	}

	public void CerrarExcel(String archivo) throws IOException, Exception {
		ExcelReader.SaveData( archivo );		
	}
	
	public String NombreSitio(String link) {
 		 String url = link;
		 String[] separar = url.split("\\.");
		 return separar[1];
		}
	
	public void bajarPagina(int valor) {
		JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
		js.executeScript("window.scrollBy(0,"+valor+")");
	}
	
	public void ListadoResultados() throws Exception {
		String parentWindow = getDriver().getWindowHandle();
		int i=0;
		for( WebElement Enlaces : LinksBancos ){
			i++;
//			System.out.println(Resultados.getText());
			ejecutarJavaScript(Enlaces.getText());
			ArrayList<String> pestanas = new ArrayList<String>(getDriver().getWindowHandles());
			getDriver().switchTo().window(pestanas.get(1));
			String url = getDriver().getCurrentUrl().toString();
			getDriver().switchTo().window(parentWindow);

			if(url.contains( NombreSitio(Enlaces.getText()) )) {
			ExcelReader.setCellData(i, 0, Enlaces.getText() + "- Verificado");
			}else {
			ExcelReader.setCellData(i, 0, Enlaces.getText() + "- Hay algo extraño");
			}
			if(i==10) {	break; }
			bajarPagina(270);
			Thread.sleep(5000);
			
		}
	}
}
