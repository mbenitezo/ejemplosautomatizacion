package com.choucair.formacion.steps;

import java.io.IOException;

import com.choucair.formacion.pageobjects.DataExcelPage;
import net.thucydides.core.annotations.Step;

public class DataExcelSteps {

	DataExcelPage dataExcelPage; 
	
	@Step
	public void AbrirPagina() {
		dataExcelPage.open();
	}
	
	@Step
	public void BuscarBancos(String datoCriterio) {
		dataExcelPage.IngresarDatosDeBusqueda(datoCriterio);
	}
	
	@Step
	public void ProcesoExcel(String archivo, String hoja) throws Exception {
		dataExcelPage.AbrirExcel(archivo, hoja);
	}
	
	@Step
	public void RecuperarBancos() throws Exception{
		dataExcelPage.ListadoResultados();
	}
	
	@Step
	public void CerrarProcesoExcel(String archivo) throws IOException, Exception {
		dataExcelPage.CerrarExcel(archivo);
	}
	
}
