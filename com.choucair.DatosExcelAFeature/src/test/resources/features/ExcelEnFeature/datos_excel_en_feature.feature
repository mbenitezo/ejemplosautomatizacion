#Author: mbenitezo@choucairtesting.com

@Regresion
Feature: Cargar datos de archivo Excel en archivo feature

  @CasoExitoso
  Scenario Outline: Carga de datos a feature
    Given Se encuentra un archivo de excel con datos
    When ejecuto el test
    |	<Nombre>	|	<Apellido>	|	<Direccion>	|	<Correo>	|	<Telefono>	|	<Genero>	|
    Then se cargan los datos de Excel en la feature

    Examples: 
	  |	Nombre	|	Apellido	|	Direccion	|	Correo	|	Telefono	|	Genero	|
		##@externaldata@./src/test/resources/Datadriven/Datos.xlsx@Hoja1
   |1   |Juana de Arco   |Pérez López   |Ésta es mi dirección.   |correo@email.co   |7777777   |Masculino|
   |2   |Juana de Arco   |Pérez López   |Ésta es mi dirección.   |correo@email.co   |7777777   |Masculino|
   |3   |Juana de Arco   |Pérez López   |Ésta es mi dirección.   |correo@email.co   |7777777   |Masculino|
   |4   |Juana de Arco   |Pérez López   |Ésta es mi dirección.   |correo@email.co   |7777777   |Masculino|
