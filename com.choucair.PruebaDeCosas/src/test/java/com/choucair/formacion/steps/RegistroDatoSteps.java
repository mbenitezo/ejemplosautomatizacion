package com.choucair.formacion.steps;

/**
 * Estructura básica de clase para Steps RegistroDatoSteps
 *
 * @author  Mario A. Benítez
 * @version  16 jun. 2018 - 23:56:00
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.RegistroDatoObject;


public class RegistroDatoSteps {
	
	RegistroDatoObject registroDatoObject;
	
	@Step
	public void AbrirSitio() {
		registroDatoObject.open();
	}
	
	@Step
	public void RegistroCorreo(String Datos[]) {
		registroDatoObject.IngresarEmail(Datos);
		registroDatoObject.PresionarBotonEntrar();
	}
	
	@Step
	public void DatosFormulario(String Datos[]) throws Exception {
		registroDatoObject.SeleccionarHobbies(Datos);
	}
	
	
}