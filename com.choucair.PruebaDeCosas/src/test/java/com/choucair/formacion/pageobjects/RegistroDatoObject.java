package com.choucair.formacion.pageobjects;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Estructura básica de clase para Object RegistroDatoObject
 *
 * @author  Mario A. Benítez
 * @version  16 jun. 2018 - 23:52:26
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.CoreMatchers.*;


import com.choucair.formacion.utilities.Utiles;
@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class RegistroDatoObject extends PageObject {
	
	public void IngresarEmail(String Datos[]) {
		//Utiles.FuncionCamposVisibles(getDriver());
		//String camposValidosTextos[] = {"email"};
		//Utiles.FuncionDatosCamposTextos(getDriver(), camposValidosTextos, Datos);
	}
	
	public void PresionarBotonEntrar() {
	//	Utiles.Elemento(getDriver(), "enterimg").click();
	}

	public void SeleccionarHobbies(String Datos[]){
		//Utiles.FuncionCamposVisibles(getDriver());
		String camposValidosTextos[] = {"FirstName", "LastName"};
		Utiles.FuncionDatosCamposTextosAngular(getDriver(), camposValidosTextos, Datos);
		Utiles.ElementoName(getDriver(), "Adress").sendKeys("prueba");
	}
}