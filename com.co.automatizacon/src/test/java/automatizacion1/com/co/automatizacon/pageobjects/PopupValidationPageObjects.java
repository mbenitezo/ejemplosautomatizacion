package automatizacion1.com.co.automatizacon.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class PopupValidationPageObjects extends PageObject {

	//Campo Usuario
	@FindBy(xpath = "//input[@placeholder='Username']")
	public WebElementFacade txtUsername;
	
	//Campo Password
	@FindBy(xpath = "//input[@placeholder='Password']")
	public WebElementFacade txtPassword;
	
	//Campo botón sign in
	@FindBy(xpath = "//button[@type='submit' and @class='btn btn-lg btn-primary btn-block']")
	public WebElementFacade btnsignin;
	
	@FindBy(id = "bootstrap-admin-template")
	public WebElementFacade lblHomePpal;
	
	public void ingresaDatos(String usuario, int pass) {
		txtUsername.sendKeys(usuario);
		txtPassword.sendKeys(String.valueOf(pass));
		btnsignin.click();
	}
	
	public void verificarHome() {
		String labelEsperado = "Bootstrap-Admin-Template";
		String mensaje = lblHomePpal.getText();
		assertThat(mensaje, containsString(labelEsperado));
	}
	
}
