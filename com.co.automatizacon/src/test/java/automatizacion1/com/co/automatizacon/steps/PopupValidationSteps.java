package automatizacion1.com.co.automatizacon.steps;

import automatizacion1.com.co.automatizacon.pageobjects.PopupValidationPageObjects;
import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {

	PopupValidationPageObjects popupValidationPageObjects;
	
	@Step
	public void loginColorLib(String strUsuario, int strPass) {
		popupValidationPageObjects.open();
		popupValidationPageObjects.ingresaDatos(strUsuario, strPass);
		popupValidationPageObjects.verificarHome();
	}
	
}
