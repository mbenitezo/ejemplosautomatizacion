package automatizacion1.com.co.automatizacon.definition;

import automatizacion1.com.co.automatizacon.steps.PopupValidationSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {

	@Steps
	PopupValidationSteps popupValidationSteps;
	
	
	@Given("^Autentico en colorlib con usuario \"([^\"]*)\" y clave (\\d+)$")
	public void autentico_en_colorlib_con_usuario_y_clave(String usuario, int pass) {
	    popupValidationSteps.loginColorLib(usuario, pass);

	}

	@Given("^Ingreso a la funcionalidad Forms Validation$")
	public void ingreso_a_la_funcionalidad_Forms_Validation() {
	    
	}

	@When("^Diligencio formulario Popup Validation$")
	public void diligencio_formulario_Popup_Validation() {
	    
	}

	@Then("^Verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso() {
	    
	}

	
}
