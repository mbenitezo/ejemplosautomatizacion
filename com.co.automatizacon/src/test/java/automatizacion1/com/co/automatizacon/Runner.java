package automatizacion1.com.co.automatizacon;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "src/test/resources/features/colorlib/PopupValidation.feature",
		tags = "@CasoExitoso"
		)

public class Runner {

}
