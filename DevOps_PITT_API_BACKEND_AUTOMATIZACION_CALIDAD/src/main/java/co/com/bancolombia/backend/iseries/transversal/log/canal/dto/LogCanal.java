package co.com.bancolombia.backend.iseries.transversal.log.canal.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

// TODO: Auto-generated Javadoc
/**
 * The Class LogCanal.
 */
public class LogCanal extends LogParent {

	// estos valores son para las trn de transferencias, pago...

	/** The str producto origen. */
	private String strProductoOrigen; // valido para cuentas de deposito, tarjetas de credito y fondos

	/** The str tipo producto origen. */
	private String strTipoProductoOrigen; // valido para cuentas de deposito, tarjetas de credito y fondos

	/** The str producto destino. */
	private String strProductoDestino; // valido para cuentas de deposito, tarjetas de credito y fondos

	/** The str tipo producto destino. */
	private String strTipoProductoDestino; // valido para cuentas de deposito, tarjetas de credito y fondos

	/** The str valor transferir. */
	private String strValorTransferir;

	/** The str documento inscrito. */
	private String strDocumentoInscrito;

	/** The str tipo documento inscrito. */
	private String strTipoDocumentoInscrito;

	/** The str cuenta inscrita. */
	private String strCuentaInscrita;
	
	/** The str tipo log canal. */
	private String strTipoLogCanal;

	/**
	 * Instantiates a new log canal.
	 */
	public LogCanal() {
		super();
	}

	/* (non-Javadoc)
	 * @see bancolombia.backendautomatizacion.logcanal.LogParent#setStrDoc(java.lang.String)
	 */
	@Override
	public void setStrDoc(String documento) {
		Long documentoLong = Long.parseLong(documento);
		String documentoFormat = String.format(ConstantManager.FORMATO15CEROSIZQ, documentoLong);
		super.setStrDoc(documentoFormat);
	}

	/**
	 * Gets the str producto origen.
	 *
	 * @return the str producto origen
	 */
	public String getStrProductoOrigen() {
		return strProductoOrigen;
	}

	/**
	 * Sets the str producto origen.
	 *
	 * @param strProductoOrigen the new str producto origen
	 */
	public void setStrProductoOrigen(String strProductoOrigen) {
		this.strProductoOrigen = String.format(ConstantManager.FORMATO16CEROSIZQ, Long.parseLong(strProductoOrigen));
	}

	/**
	 * Gets the str tipo producto origen.
	 *
	 * @return the str tipo producto origen
	 */
	public String getStrTipoProductoOrigen() {
		return strTipoProductoOrigen;
	}

	/**
	 * Sets the str tipo producto origen.
	 *
	 * @param strTipoProductoOrigen the new str tipo producto origen
	 */
	public void setStrTipoProductoOrigen(String strTipoProductoOrigen) {
		this.strTipoProductoOrigen = strTipoProductoOrigen;
	}

	/**
	 * Gets the str producto destino.
	 *
	 * @return the str producto destino
	 */
	public String getStrProductoDestino() {
		return strProductoDestino;
	}

	/**
	 * Sets the str producto destino.
	 *
	 * @param strProductoDestino the new str producto destino
	 */
	public void setStrProductoDestino(String strProductoDestino) {
		this.strProductoDestino = String.format(ConstantManager.FORMATO16CEROSIZQ, Long.parseLong(strProductoDestino));
	}

	/**
	 * Gets the str tipo producto destino.
	 *
	 * @return the str tipo producto destino
	 */
	public String getStrTipoProductoDestino() {
		return strTipoProductoDestino;
	}

	/**
	 * Sets the str tipo producto destino.
	 *
	 * @param strTipoProductoDestino the new str tipo producto destino
	 */
	public void setStrTipoProductoDestino(String strTipoProductoDestino) {
		this.strTipoProductoDestino = strTipoProductoDestino;
	}

	/**
	 * Gets the str valor transferir.
	 *
	 * @return the str valor transferir
	 */
	public String getStrValorTransferir() {
		return strValorTransferir;
	}

	/**
	 * Sets the str valor transferir.
	 *
	 * @param strValorTransferir the new str valor transferir
	 */
	public void setStrValorTransferir(String strValorTransferir) {
		this.strValorTransferir = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strValorTransferir));
	}

	/**
	 * Gets the str documento inscrito.
	 *
	 * @return the str documento inscrito
	 */
	public String getStrDocumentoInscrito() {
		return strDocumentoInscrito;
	}

	/**
	 * Sets the str documento inscrito.
	 *
	 * @param strDocumentoInscrito the new str documento inscrito
	 */
	public void setStrDocumentoInscrito(String strDocumentoInscrito) {
		this.strDocumentoInscrito = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumentoInscrito));
	}

	/**
	 * Gets the str tipo documento inscrito.
	 *
	 * @return the str tipo documento inscrito
	 */
	public String getStrTipoDocumentoInscrito() {
		return strTipoDocumentoInscrito;
	}

	/**
	 * Sets the str tipo documento inscrito.
	 *
	 * @param strTipoDocumentoInscrito the new str tipo documento inscrito
	 */
	public void setStrTipoDocumentoInscrito(String strTipoDocumentoInscrito) {
		this.strTipoDocumentoInscrito = strTipoDocumentoInscrito;
	}

	/**
	 * Gets the str cuenta inscrita.
	 *
	 * @return the str cuenta inscrita
	 */
	public String getStrCuentaInscrita() {
		return strCuentaInscrita;
	}

	/**
	 * Sets the str cuenta inscrita.
	 *
	 * @param strCuentaInscrita the new str cuenta inscrita
	 */
	public void setStrCuentaInscrita(String strCuentaInscrita) {
		this.strCuentaInscrita = String.format(ConstantManager.FORMATO16CEROSIZQ, Long.parseLong(strCuentaInscrita));
	}

	/**
	 * Gets the str tipo log canal.
	 *
	 * @return the str tipo log canal
	 */
	public String getStrTipoLogCanal() {
		return strTipoLogCanal;
	}

	/**
	 * Sets the str tipo log canal.
	 *
	 * @param strTipoLogCanal the new str tipo log canal
	 */
	public void setStrTipoLogCanal(String strTipoLogCanal) {
		this.strTipoLogCanal = strTipoLogCanal;
	}
	
	

}
