package co.com.bancolombia.backend.iseries.transversal.edblibramd.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.edblibramd.dto.DatosCuenta;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Back edbffstcre.
 */
public class BackEDBFFSTCRE {
	private static final Logger LOGGER = LogManager.getLogger(BackEDBFFSTCRE.class.getName());

	/**
	 * <b>Descripción:</b> Método que permite obtener del archivo
	 * EDBLIBRAMD.EDBFFSTCRE, datos de las cuentas depósitos que tiene un cliente
	 * tales como el Tipo de cuenta, Número de cuenta y Saldo; utilizando como datos
	 * de entrada el documento del cliente y el trace de la transacción. El
	 * resultado se almacena en un objeto tipo lista.
	 *
	 * @param strDocumento
	 * @param strTrace
	 *
	 * @return
	 */

	public List<DatosCuenta> consultaEDBFFSTCRE(String strDocumento, String strTrace) throws SQLException {
		List<DatosCuenta> listaTarjetasCredito = new ArrayList<>();
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.EDBFFSTCRE.consulta");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strDocumento);
			consulta.setString(2, strTrace);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strNroCuenta = objResult.getString("STCRENUMTA").trim();
					String strTipoCuenta = objResult.getString("STCREEMISO").trim();
					String strDeudaTotalPesos = objResult.getString("STCREDEUTP").trim();
					String strDeudaTotalDolar = objResult.getString("STCREDEUTD").trim();

					DatosCuenta datosCuenta = new DatosCuenta(strNroCuenta, strTipoCuenta, strDeudaTotalPesos, strDeudaTotalDolar);
					listaTarjetasCredito.add(datosCuenta);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return listaTarjetasCredito;
	}

	/**
	 * @param listaObtenida
	 * @param listaEsperada
	 * @param strCanal
	 *
	 * @return
	 */

	public Boolean validarResultado(List<DatosCuenta> listaObtenida, List<DatosCuenta> listaEsperada, String strCanal) {
		boolean resultado = false;

		for (int i = 0; i < listaEsperada.size(); i++) {

			String strTipoEsperado = listaEsperada.get(i).getStrTipoCuenta();
			String strNroCuentaEsperado = listaEsperada.get(i).getStrNroCuenta();
			String strDeudaTotalPesosEsperado = listaEsperada.get(i).getStrDeudaTotalPesos();
			String strDeudaTotalDolarEperado = listaEsperada.get(i).getStrDeudaTotalDolar();

			for (int j = 0; j < listaObtenida.size(); j++) {
				resultado = false;
				String strTipoObtenido = listaObtenida.get(j).getStrTipoCuenta();
				String strNroCuentaObtenido = listaObtenida.get(j).getStrNroCuenta();
				String strDeudaTotalPesosObtenido = listaObtenida.get(j).getStrDeudaTotalPesos();
				String strDeudaTotalDolarObtenido = listaObtenida.get(i).getStrDeudaTotalDolar();

				if (ConstantManager.APP.equals(strCanal) || ConstantManager.SVE.equals(strCanal) || ConstantManager.SVP.equals(strCanal)) {
					strTipoEsperado = strTipoEsperado.toUpperCase();

					if (null == strDeudaTotalDolarEperado) {
						strDeudaTotalDolarEperado = "0,00";
					} else {
						strDeudaTotalDolarEperado = strDeudaTotalDolarEperado.replace(",", "");
						strDeudaTotalDolarEperado = strDeudaTotalDolarEperado.replace(".", ",");
					}

					strDeudaTotalPesosEsperado = strDeudaTotalPesosEsperado.replace(",", "");
					strDeudaTotalPesosEsperado = strDeudaTotalPesosEsperado.replace(".", ",");

					strNroCuentaObtenido = strNroCuentaObtenido.substring(strNroCuentaObtenido.length() - 4,
							strNroCuentaObtenido.length());

					if (ConstantManager.SVE.equals(strCanal) || ConstantManager.SVP.equals(strCanal)) {
						if (strTipoEsperado.contains("MASTERCARD")) {
							strTipoEsperado = "2";
						}
						if (strTipoEsperado.contains("AMERICAN")) {
							strTipoEsperado = "4";
						}
						if (strTipoEsperado.contains("VISA")) {
							strTipoEsperado = "1";
						}
						if (strTipoEsperado.equals(strTipoObtenido)
								&& strNroCuentaEsperado.equals(strNroCuentaObtenido)
								&& strDeudaTotalPesosEsperado.equals(strDeudaTotalPesosObtenido)
								&& strDeudaTotalDolarEperado.equals(strDeudaTotalDolarObtenido)) {
							resultado = true;
							LOGGER.info(ExceptionCodesManager.SAME_PRODUCT.getMsg());
							break;
						}
					}
					if (ConstantManager.APP.equals(strCanal)) {
						if (strNroCuentaEsperado.equals(strNroCuentaObtenido)
								&& strDeudaTotalPesosEsperado.equals(strDeudaTotalPesosObtenido)
								&& strDeudaTotalDolarEperado.equals(strDeudaTotalDolarObtenido)) {
							resultado = true;
							LOGGER.info(ExceptionCodesManager.SAME_PRODUCT.getMsg());
							break;
						}
					}
				}
			}
			if (!resultado) {
				LOGGER.info(ExceptionCodesManager.VALUE_NOTFOUND.getMsg());
				break;
			}
		}
		return resultado;
	}
}
