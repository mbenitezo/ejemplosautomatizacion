package co.com.bancolombia.backend.iseries.transversal.saldos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.saldos.dto.SaldoCuenta;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantOrientacion;

/**
 * Clase que se encarga de manejar todos los componentes del log de alertas se
 * tocan los archivos SCILIBRAMD.SCIFFSALDO
 *
 * @author david.c.gonzalez
 */
public class BackSaldosDeposito {
	private static final Logger LOGGER = LogManager.getLogger(BackSaldosDeposito.class.getName());

	/**
	 * Metodo para consultar el saldo de una cuenta de deposito
	 *
	 * @param strCuenta     : Define la cuenta de deposito para hacer la consulta de su saldo.
	 * @param strTipoCuenta : Define el tipo de la cuenta de deposito para hacer la consulta de su saldo (D si es corriente o S si es Ahorro).
	 *
	 * @return saldo cuenta
	 *
	 * @throws SQLException the sql exception
	 */
	public SaldoCuenta consultarSaldo(String strCuenta, String strTipoCuenta) throws SQLException {
		SaldoCuenta resultSaldo = null;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.SCIFFSALDO.consultarSaldo");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strCuenta);
			consulta.setString(2, strTipoCuenta);
			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					double dblSaldoDisponible = objResult.getDouble("SDSDODSP");
					double dblSaldoCanje = objResult.getDouble("SDFLTDSP");
					int intDiasSobregiro = objResult.getInt("SDDSPINI1");

					String strTipoSobregiro = objResult.getString("SDTIPOSOB").trim();
					String strTipoPlanCuenta = objResult.getString("SDPLAN").trim();
					double dblSaldoInicioDia = objResult.getDouble("SDSDOINIC");
					double dblSaldoRetenidoInicio = objResult.getDouble("SDRETINIC");
					double dblSaldoRetenidoActual = objResult.getDouble("SDRETACT");
					double dblValorEmbargo = objResult.getDouble("SDVLREMB");

					double dblFlotInicioDia = objResult.getDouble("SDFLTINIC");
					double dblSaldoMinimo = objResult.getDouble("SDSDOMIN");
					double dblTopeFlotante = objResult.getDouble("SDTPEFLT");

					double dblTotalNotaCreditoHoy = objResult.getDouble("SDNOTACR");
					double dblTotalNotaDebitoHoy = objResult.getDouble("SDNOTADB");
					double dblSaldoTotal = dblSaldoDisponible + dblSaldoCanje;
					resultSaldo = new SaldoCuenta(dblSaldoDisponible, dblSaldoCanje, dblSaldoTotal,
							intDiasSobregiro, strTipoSobregiro, strTipoPlanCuenta, dblSaldoInicioDia,
							dblSaldoRetenidoInicio, dblSaldoRetenidoActual, dblValorEmbargo, dblFlotInicioDia,
							dblSaldoMinimo, dblTopeFlotante, dblTotalNotaCreditoHoy, dblTotalNotaDebitoHoy);
				}
			} else {
				resultSaldo = new SaldoCuenta();
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}

		}
		return resultSaldo;
	}

	/**
	 * Metodo que se encarga de validar si se hizo el debito correctamente de una
	 * cuenta de deposito, depende del valor a transferir y la orientacion
	 *
	 * @param saldoAntes      :      Define el valor del saldo antes de la transaccion.
	 * @param saldoDespues    :    Define el valor del saldo despues de la transaccion
	 * @param valorTransferir : Define el valor a transferir en la transaccion.
	 * @param orientacion     :     Define la orientacion de la prueba (ACIERTO, ERROR)
	 *
	 * @return boolean : Devuelve true si la validacion fue correcta, de lo contrario un false, de acuerdo a su orientacion.
	 */
	public boolean validarDebitoDeposito(double saldoAntes, double saldoDespues, double valorTransferir, String orientacion) {
		if (ConstantOrientacion.ACIERTO.equalsIgnoreCase(orientacion)) {
			return (saldoAntes - valorTransferir) == saldoDespues;
		} else if (ConstantOrientacion.ERROR.equalsIgnoreCase(orientacion)) {
			return saldoAntes == saldoDespues;
		}
		return false;
	}

	/**
	 * Metodo que se encarga de validar si se hizo correctamente del credito de la
	 * cuenta de deposito, depende del valor a transferir y la orientacion
	 *
	 * @param SaldoAntes      :      Define el valor del saldo antes de la transaccion.
	 * @param saldoDespues    :    Define el valor del saldo despues de la transaccion
	 * @param valorTransferir : Define el valor a transferir en la transaccion.
	 * @param orientacion     :     Define la orientacion de la prueba (ACIERTO, ERROR)
	 *
	 * @return boolean : Devuelve true si la validacion fue correcta, de lo contrario un false, de acuerdo a su orientacion.
	 */
	public boolean validarCreditoDeposito(double SaldoAntes, double saldoDespues, double valorTransferir, String orientacion) {
		if (ConstantOrientacion.ACIERTO.equalsIgnoreCase(orientacion)) {
			return (SaldoAntes + valorTransferir) == saldoDespues;
		} else if (ConstantOrientacion.ERROR.equalsIgnoreCase(orientacion)) {
			return SaldoAntes == saldoDespues;
		}
		return false;
	}

}
