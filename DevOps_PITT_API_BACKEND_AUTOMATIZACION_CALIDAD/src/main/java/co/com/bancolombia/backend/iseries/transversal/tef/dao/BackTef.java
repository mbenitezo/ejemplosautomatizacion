package co.com.bancolombia.backend.iseries.transversal.tef.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.tef.dto.LogTef;
import co.com.bancolombia.backend.iseries.transversal.tef.dto.MovTef;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Back tef.
 */
public class BackTef {
	private static final Logger LOGGER = LogManager.getLogger(BackTef.class.getName());

	/**
	 * Metodo que se encarga de obtener el registro mov tef del archivo
	 * PCCLIBRAMD.PCCFFMOVTF de acuerdo a un identificador unico.
	 *
	 * @param idSistemaTef :            Define el identificador unico para encontrar el mov tef.
	 *
	 * @return mov tef
	 *
	 * @throws SQLException the sql exception
	 */
	public MovTef consultarMovTef(String idSistemaTef) throws SQLException {
		MovTef resultMovTef = null;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.PCCFFMOVTF.consultarMovTef");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, idSistemaTef);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strSistemaOrigen = objResult.getString("MVSISORG").trim();
					String strIdSistema = objResult.getString("MVIDENTI").trim();

					String strAno = objResult.getString("MVANO").trim();
					String strMes = objResult.getString("MVMES").trim();
					String strDia = objResult.getString("MVDIACLA").trim();

					String strOfiCuentaDebitar = objResult.getString("MVOFIDB").trim();
					String strCuentaDebitar = objResult.getString("MVCTADB").trim();
					String strTipCuentaDebitar = objResult.getString("MVTIPCDB").trim();

					String strOficinaCreditar = objResult.getString("MVOFICR").trim();
					String strCuentaCreditar = objResult.getString("MVCTACR").trim();
					String strTipoCuentaCreditar = objResult.getString("MVTIPCCR").trim();

					String strValorPago = objResult.getString("MVVLRTF").trim();

					String strIndContable = objResult.getString("MVINDCTB").trim();
					String strValorComision = objResult.getString("MVVLRCOM").trim();
					String strIndicadorComision = objResult.getString("MVINDCOM").trim();
					String strEstado = objResult.getString("MVESTADO").trim();

					String strSaldoInicioTRX = objResult.getString("MVSDOINITR").trim();

					resultMovTef = new MovTef();
					resultMovTef.setStrSistemaOrigen(strSistemaOrigen);
					resultMovTef.setStrIdSistema(strIdSistema);
					resultMovTef.setStrAno(strAno);
					resultMovTef.setStrMes(strMes);
					resultMovTef.setStrDia(strDia);
					resultMovTef.setStrOfiCuentaDebitar(strOfiCuentaDebitar);
					resultMovTef.setStrCuentaDebitar(strCuentaDebitar);
					resultMovTef.setStrTipCuentaDebitar(strTipCuentaDebitar);
					resultMovTef.setStrOficinaCreditar(strOficinaCreditar);
					resultMovTef.setStrCuentaCreditar(strCuentaCreditar);
					resultMovTef.setStrTipoCuentaCreditar(strTipoCuentaCreditar);
					resultMovTef.setStrValorPago(strValorPago);
					resultMovTef.setStrIndContable(strIndContable);
					resultMovTef.setStrValorComision(strValorComision);
					resultMovTef.setStrIndicadorComision(strIndicadorComision);
					resultMovTef.setStrEstado(strEstado);
					resultMovTef.setStrSaldoInicioTRX(strSaldoInicioTRX);
				}
			} else {
				resultMovTef = new MovTef();
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return resultMovTef;
	}

	/**
	 * Metodo que se encarga de validar cada uno de los parametros del mov tef.
	 *
	 * @param movTefEsperado :            Define la variable mov tef que se ESPERA de acuerdo a la            transaccion
	 * @param movTefObtenido :            Define la variable mov tef que se OBTIENE de la base de datos.
	 *
	 * @return boolean boolean
	 */
	public boolean validateMovTef(MovTef movTefEsperado, MovTef movTefObtenido) {
		boolean resultValidate = true;

		String strSistemaOrigenEsperado = movTefEsperado.getStrSistemaOrigen();
		String strSistemaOrigenObtenido = movTefObtenido.getStrSistemaOrigen();
		resultValidate = validarResultadoActual(strSistemaOrigenEsperado, strSistemaOrigenObtenido, resultValidate);

		String strIdSistemaEsperado = movTefEsperado.getStrIdSistema();
		String strIdSistemaObtenido = movTefObtenido.getStrIdSistema();
		resultValidate = validarResultadoActual(strIdSistemaEsperado, strIdSistemaObtenido, resultValidate);

		String strAnoEsperado = movTefEsperado.getStrAno();
		String strAnoObtenido = movTefObtenido.getStrAno();
		resultValidate = validarResultadoActual(strAnoEsperado, strAnoObtenido, resultValidate);

		String strMesEsperado = movTefEsperado.getStrMes();
		String strMesObtenido = movTefObtenido.getStrMes();
		resultValidate = validarResultadoActual(strMesEsperado, strMesObtenido, resultValidate);

		String strDiaEsperado = movTefEsperado.getStrDia();
		String strDiaObtenido = movTefObtenido.getStrDia();
		resultValidate = validarResultadoActual(strDiaEsperado, strDiaObtenido, resultValidate);

		String strOfiCuentaDebitarEsperado = movTefEsperado.getStrOfiCuentaDebitar();
		String strOfiCuentaDebitarObtenido = movTefObtenido.getStrOfiCuentaDebitar();
		resultValidate = validarResultadoActual(strOfiCuentaDebitarEsperado, strOfiCuentaDebitarObtenido, resultValidate);

		String strCuentaDebitarEsperado = movTefEsperado.getStrCuentaDebitar();
		String strCuentaDebitarObtenido = movTefObtenido.getStrCuentaDebitar();
		resultValidate = validarResultadoActual(strCuentaDebitarEsperado, strCuentaDebitarObtenido, resultValidate);

		String strTipCuentaDebitarEsperado = movTefEsperado.getStrTipCuentaDebitar();
		String strTipCuentaDebitarObtenido = movTefObtenido.getStrTipCuentaDebitar();
		resultValidate = validarResultadoActual(strTipCuentaDebitarEsperado, strTipCuentaDebitarObtenido, resultValidate);

		String strOficinaCreditarEsperado = movTefEsperado.getStrOficinaCreditar();
		String strOficinaCreditarObtenido = movTefObtenido.getStrOficinaCreditar();
		resultValidate = validarResultadoActual(strOficinaCreditarEsperado, strOficinaCreditarObtenido, resultValidate);

		String strCuentaCreditarEsperado = movTefEsperado.getStrCuentaCreditar();
		String strCuentaCreditarObtenido = movTefObtenido.getStrCuentaCreditar();
		resultValidate = validarResultadoActual(strCuentaCreditarEsperado, strCuentaCreditarObtenido, resultValidate);

		String strTipoCuentaCreditarEsperado = movTefEsperado.getStrTipoCuentaCreditar();
		String strTipoCuentaCreditarObtenido = movTefObtenido.getStrTipoCuentaCreditar();
		resultValidate = validarResultadoActual(strTipoCuentaCreditarEsperado, strTipoCuentaCreditarObtenido, resultValidate);

		String strSaldoInicioTRXEsperado = movTefEsperado.getStrSaldoInicioTRX();
		String strSaldoInicioTRXObtenido = movTefObtenido.getStrSaldoInicioTRX();
		resultValidate = validarResultadoActual(strSaldoInicioTRXEsperado, strSaldoInicioTRXObtenido, resultValidate);

		String strValorPagoEsperado = movTefEsperado.getStrValorPago();
		String strValorPagoObtenido = movTefObtenido.getStrValorPago();
		resultValidate = validarResultadoActual(strValorPagoEsperado, strValorPagoObtenido, resultValidate);

		String strIndContableEsperado = movTefEsperado.getStrIndContable();
		String strIndContableObtenido = movTefObtenido.getStrIndContable();
		resultValidate = validarResultadoActual(strIndContableEsperado, strIndContableObtenido, resultValidate);

		String strValorComisionEsperado = movTefEsperado.getStrValorComision();
		String strValorComisionObtenido = movTefObtenido.getStrValorComision();
		resultValidate = validarResultadoActual(strValorComisionEsperado, strValorComisionObtenido, resultValidate);

		String strIndicadorComisionEsperado = movTefEsperado.getStrIndicadorComision();
		String strIndicadorComisionObtenido = movTefObtenido.getStrIndicadorComision();
		resultValidate = validarResultadoActual(strIndicadorComisionEsperado, strIndicadorComisionObtenido, resultValidate);

		String strEstadoEsperado = movTefEsperado.getStrEstado();
		String strEstadoObtenido = movTefObtenido.getStrEstado();
		resultValidate = validarResultadoActual(strEstadoEsperado, strEstadoObtenido, resultValidate);

		return resultValidate;
	}

	/**
	 * Metodo que se encarga de consultar el log tef, del archivo
	 * PCCLIBRAMD.PCCFFLOGTF, de acuerdo a un identificador unico.
	 *
	 * @param idSistemaTef :            Define el identificador unico para encontrar el log tef
	 *
	 * @return log tef
	 *
	 * @throws SQLException the sql exception
	 */
	public LogTef consultarLogTf(String idSistemaTef) throws SQLException {
		LogTef resultLogTef = null;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.PCCFFLOGTF.consultarLogTf");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			String busqueda = String.format("%s%s%s", ConstantManager.PORCENTAJE, idSistemaTef, ConstantManager.PORCENTAJE);
			consulta.setString(1, busqueda);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strAno = objResult.getString("LGANO").trim(); // 2 digitos
					String strMes = objResult.getString("LGMES").trim();
					String strDia = objResult.getString("LGDIA").trim();
					String strError = objResult.getString("LGERROR").trim(); // default 99
					String strEstado = objResult.getString("LGESTADO").trim(); // default T
					String strSistema = objResult.getString("LGSIS").trim(); // PARA SVP Y APP = SVP, PARA NEQUI =
					// NEQ
					String strTrama = objResult.getString("LGDATOS").trim();
					String strTipoTrn = objResult.getString("LGTRN").trim();
					String strIdSistema = ConstantManager.VACIO;

					String strCuentaOrigen = strTrama.substring(0, 16); // tiene 0s al principio
					String strTipoCuentaOrigen = strTrama.substring(16, 17); // es el numero
					String strCuentaDest = ConstantManager.VACIO;
					String strTipoCuentaDest = ConstantManager.VACIO;
					String strValorTransfer = ConstantManager.VACIO;

					// hace referencia a transaferencias con dos cuentas de deposito
					if (ConstantManager.NUMERO_1.equalsIgnoreCase(strTipoTrn)) {
						strCuentaDest = strTrama.substring(17, 33); // tiene 0s al principio
						strTipoCuentaDest = strTrama.substring(33, 34); // es el numero
						strValorTransfer = strTrama.substring(34, 51); // el valor debe ser multiplicado *100
						strIdSistema = strTrama.substring(255, 265);
					} else if (ConstantManager.NUMERO_3.equalsIgnoreCase(strTipoTrn)) {
						strIdSistema = strTrama.substring(105, 115);
						strValorTransfer = strTrama.substring(17, 34); // el valor debe ser multiplicado *100
						if (ConstantManager.NEQUI.equalsIgnoreCase(strSistema)) {
							strCuentaDest = strTrama.substring(115, 132); // tiene 0s al principio
						}
					} else {
						LOGGER.info(ExceptionCodesManager.TRANS_NOT_FOUND.getMsg());
					}
					resultLogTef = new LogTef();
					resultLogTef.setStrAno(strAno);
					resultLogTef.setStrMes(strMes);
					resultLogTef.setStrDia(strDia);
					resultLogTef.setStrError(strError);
					resultLogTef.setStrEstado(strEstado);
					resultLogTef.setStrSistema(strSistema);
					resultLogTef.setStrIdSistema(strIdSistema);
					resultLogTef.setStrCuentaOrigen(strCuentaOrigen);
					resultLogTef.setStrTipoCuentaOrigen(strTipoCuentaOrigen);
					resultLogTef.setStrCuentaDest(strCuentaDest);
					resultLogTef.setStrTipoCuentaDest(strTipoCuentaDest);
					resultLogTef.setStrValorTransfer(strValorTransfer);

				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return resultLogTef;
	}

	/**
	 * Metodo que se encarga de validar cada uno de los parametros del mov tef.
	 *
	 * @param logTefEsperado :            Define la variable log tef que se ESPERA de acuerdo a la            transaccion
	 * @param logTefObtenido :            Define la variable log tef que se OBTIENE de la base de datos.
	 *
	 * @return boolean boolean
	 */
	public boolean validarLogTef(LogTef logTefEsperado, LogTef logTefObtenido) {
		boolean resultValidate = true;

		String strAnoEsperado = logTefEsperado.getStrAno();
		String strAnoObtenido = logTefObtenido.getStrAno();
		resultValidate = validarResultadoActual(strAnoEsperado, strAnoObtenido, resultValidate);

		String strMesEsperado = logTefEsperado.getStrMes();
		String strMesObtenido = logTefObtenido.getStrMes();
		resultValidate = validarResultadoActual(strMesEsperado, strMesObtenido, resultValidate);

		String strDiaEsperado = logTefEsperado.getStrDia();
		String strDiaObtenido = logTefObtenido.getStrDia();
		resultValidate = validarResultadoActual(strDiaEsperado, strDiaObtenido, resultValidate);

		String strErrorEsperado = logTefEsperado.getStrError();
		String strErrorObtenido = logTefObtenido.getStrError();
		resultValidate = validarResultadoActual(strErrorEsperado, strErrorObtenido, resultValidate);

		String strEstadoEsperado = logTefEsperado.getStrEstado();
		String strEstadoObtenido = logTefObtenido.getStrEstado();
		resultValidate = validarResultadoActual(strEstadoEsperado, strEstadoObtenido, resultValidate);

		String strSistemaEsperado = logTefEsperado.getStrSistema();
		String strSistemaObtenido = logTefObtenido.getStrSistema();
		resultValidate = validarResultadoActual(strSistemaEsperado, strSistemaObtenido, resultValidate);

		String strIdSistemaEsperado = logTefEsperado.getStrIdSistema();
		String strIdSistemaObtenido = logTefObtenido.getStrIdSistema();
		resultValidate = validarResultadoActual(strIdSistemaEsperado, strIdSistemaObtenido, resultValidate);

		String strCuentaOrigenEsperado = logTefEsperado.getStrCuentaOrigen();
		String strCuentaOrigenObtenido = logTefObtenido.getStrCuentaOrigen();
		resultValidate = validarResultadoActual(strCuentaOrigenEsperado, strCuentaOrigenObtenido, resultValidate);

		String strTipoCuentaOrigenEsperado = logTefEsperado.getStrTipoCuentaOrigen();
		String strTipoCuentaOrigenObtenido = logTefObtenido.getStrTipoCuentaOrigen();
		resultValidate = validarResultadoActual(strTipoCuentaOrigenEsperado, strTipoCuentaOrigenObtenido,
				resultValidate);

		String strCuentaDestEsperado = logTefEsperado.getStrCuentaDest();
		String strCuentaDestObtenido = logTefObtenido.getStrCuentaDest();
		resultValidate = validarResultadoActual(strCuentaDestEsperado, strCuentaDestObtenido, resultValidate);

		String strTipoCuentaDestEsperado = logTefEsperado.getStrTipoCuentaDest();
		String strTipoCuentaDestObtenido = logTefObtenido.getStrTipoCuentaDest();
		resultValidate = validarResultadoActual(strTipoCuentaDestEsperado, strTipoCuentaDestObtenido,
				resultValidate);

		String strValorTransferEsperado = logTefEsperado.getStrValorTransfer();
		String strValorTransferObtenido = logTefObtenido.getStrValorTransfer();
		resultValidate = validarResultadoActual(strValorTransferEsperado, strValorTransferObtenido,
				resultValidate);

		return resultValidate;
	}

	private boolean validarResultadoActual(String strEsperado, String strObtenido, boolean resultBefore) {
		boolean result = true;
		if (strEsperado != null && !strEsperado.isEmpty()) {
			if (resultBefore) {
				result = strEsperado.equalsIgnoreCase(strObtenido);
			}
		} else {
			result = resultBefore;
		}

		return result;
	}
}
