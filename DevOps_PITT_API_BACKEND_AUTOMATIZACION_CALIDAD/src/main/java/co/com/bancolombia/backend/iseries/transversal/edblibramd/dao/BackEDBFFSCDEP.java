package co.com.bancolombia.backend.iseries.transversal.edblibramd.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.edblibramd.dto.DatosCuenta;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Back edbffscdep.
 */
public class BackEDBFFSCDEP {
	private static final Logger LOGGER = LogManager.getLogger(BackEDBFFSCDEP.class.getName());

	/**
	 * <b>Descripción:</b> Método que permite obtener del archivo
	 * EDBLIBRAMD.EDBFFSCDEP, datos de las cuentas depósitos que tiene un cliente
	 * tales como el Tipo de cuenta, Número de cuenta y Saldo; utilizando como datos
	 * de entrada el documento del cliente y el trace de la transacción. El
	 * resultado se almacena en un objeto tipo lista.
	 *
	 * @param strDocumento:</b> esta variable almacena el documento de identificación del cliente
	 * @param strTrace:</b>     esta variable almacena el código único de identificación de la transacción
	 *
	 * @return lista de cuentas dósito del cliente consultado en el backend
	 */


	public List<DatosCuenta> consultaEDBFFSCDEP(String strDocumento, String strTrace) throws SQLException {
		List<DatosCuenta> listaCuentaDepositos = new ArrayList<>();
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.EDBFFSCDEP.consulta");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consultas = conexion.prepareCall(sql)) {
			consultas.setString(1, strDocumento);
			consultas.setString(2, strTrace);
			objResult = consultas.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strNroCuenta = objResult.getString("SCDEPNUMCU").trim();
					String strTipoCuenta = objResult.getString("SCDEPTIPCU").trim();
					String strSaldoDisp = objResult.getString("SCDEPSALDO").trim();

					DatosCuenta datosCuenta = new DatosCuenta(strNroCuenta, strTipoCuenta, strSaldoDisp);
					listaCuentaDepositos.add(datosCuenta);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return listaCuentaDepositos;
	}

	/**
	 * <b>Descripción:</b> Método que recibe la lista de datos de las cuentas
	 * depósitos que tiene un cliente pero que vienen del lado front. El resultado
	 * llega almacenado en un objeto tipo list y se verifica: el canal por el que
	 * llega, el tipo de cuenta y se cambia a su correspondiente valor numérico (1)
	 * o (7); ahorros o corriente. Una vez tratados los datos, se comparan con la
	 * lista obtenida del lado backend y se reortorna un resultado.
	 *
	 * @param listaObtenida: es la lista que se consulta previamente del lado backend
	 * @param listaEsperada: es la lista esperada del lado front, la cual se compara con lista
	 *                       obtenida del lado backend
	 * @param strCanal:      es el canal por el cual se recibe la lista esperada ya sea APP,
	 *                       SVP o SVE.
	 *
	 * @return
	 */

	public Boolean validarResultado(List<DatosCuenta> listaObtenida, List<DatosCuenta> listaEsperada, String strCanal) {
		boolean resultado = false;

		for (DatosCuenta cuentaEsperada : listaEsperada) {

			String strTipoEsperado = cuentaEsperada.getStrTipoCuenta();
			String strNroCuentaEsperado = cuentaEsperada.getStrNroCuenta();
			String strSaldoEsperado = cuentaEsperada.getStrSaldoDisp();

			for (DatosCuenta cuentaObtenida : listaObtenida) {
				resultado = false;
				String strTipoObtenido = cuentaObtenida.getStrTipoCuenta();
				String strNroCuentaObtenido = cuentaObtenida.getStrNroCuenta();
				String strSaldoObtenido = cuentaObtenida.getStrSaldoDisp();

				if (ConstantManager.APP.equals(strCanal) || ConstantManager.SVP.equals(strCanal) || ConstantManager.SVE.equals(strCanal)) {
					strTipoEsperado = strTipoEsperado.toUpperCase();
					if (strTipoEsperado.contains("AHORRO")) {
						strTipoEsperado = "7";
					}
					if (strTipoEsperado.contains("CORRIENTE")) {
						strTipoEsperado = "1";
					}
					strSaldoEsperado = strSaldoEsperado.replace(",", "");
					strSaldoEsperado = strSaldoEsperado.replace(".", ",");

					if (ConstantManager.SVE.equals(strCanal) || ConstantManager.SVP.equals(strCanal)) {
						strNroCuentaObtenido = strNroCuentaObtenido.replace("-", "");
					}
					if (ConstantManager.APP.equals(strCanal)) {
						strNroCuentaObtenido = strNroCuentaObtenido.substring(strNroCuentaObtenido.length() - 4, strNroCuentaObtenido.length());
					}
					if (strTipoEsperado.equals(strTipoObtenido) && strNroCuentaEsperado.equals(strNroCuentaObtenido) && strSaldoEsperado.equals(strSaldoObtenido)) {
						resultado = true;
						LOGGER.info(ExceptionCodesManager.SAME_PRODUCT.getMsg());
						break;
					}
				}
			}
			if (!resultado) {
				LOGGER.info(ExceptionCodesManager.VALUE_NOTFOUND.getMsg());
				break;
			}
		}
		return resultado;
	}
}
