package co.com.bancolombia.backend.iseries.transversal.fondos.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.utilidades.*;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Consultar log.
 */
public class ConsultarLog {
	private static final Logger LOGGER = LogManager.getLogger(ConsultarLog.class.getName());
	private List<String> listaTransacciones;

	/**
	 * Instantiates a new Consultar log.
	 */
	public ConsultarLog() {
		listaTransacciones = new ArrayList<>();
		cagarListaTrasacciones();
	}

	private void cagarListaTrasacciones() {
		listaTransacciones.add(TransactionCodes.TRANS_8.getId());
		listaTransacciones.add(TransactionCodes.TRANS_9.getId());
		listaTransacciones.add(TransactionCodes.TRANS_10.getId());
	}

	/**
	 * <pre>
	 * <b>Descripción:</b> El siguiente componente se encarga de preparar los datos
	 * para realizar la consulta a la FSFFFLGFID.
	 *
	 * <b>prepararFSFFFLGFID - Ejemplo :</b> public boolean
	 * prepararFSFFFLGFID(String "12345678901234567890", String "BLP", String
	 * "0369", String "93405404", String "1")
	 *
	 * @param strTrace Es el trace de la transacción de 20 digitos que se extrae del log del canal.
	 * @param strCanal Los valores esperados en este parametro son : "BLP" y/ "BLE"
	 * @param strCodTransaccion Este componente solo espera los siguientes códigos "0369", "0317", "0410" y "0411"
	 * @param strDocumento Domunento del cliente
	 * @param strTipoDocumento Se esperan valores "1", "3" etc.
	 * @return Se retorna un valor boolean "true" o "false" en la variable "resultadoFinal"
	 * @throws SQLException the sql exception
	 */
	public boolean validarConsultaFSFFFLGFID(String strTrace, String strCanal, String strCodTransaccion, String strDocumento, String strTipoDocumento) throws SQLException {
		if (StringUtils.isNoneBlank(strTrace, strCanal, strCodTransaccion, strDocumento, strTipoDocumento)) {
			strDocumento = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumento));
			String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
			String strCodTrn = ConstantManager.VACIO;

			if (TransactionCodes.TRANS_8.getId().equals(strCodTransaccion)) {
				strCodTrn = ConstantManager.CODIGO_UNO;
			} else if (TransactionCodes.TRANS_9.getId().equals(strCodTransaccion)) {
				strCodTrn = ConstantManager.CODIGO_DOS;
			} else if (TransactionCodes.TRANS_10.getId().equals(strCodTransaccion)) {
				strCodTrn = ConstantManager.CODIGO_TRES;
			}
			if (listaTransacciones.contains(strCodTransaccion)) {
				return validarConsultaFSFFFLGFID(strDocumento, strTipoDocumento, strCodTrn, strCanal, strFecha, strTrace);
			}
			return false;
		} else {
			LOGGER.info("No se validó la Consulta FSFFFLGFID");
			return false;
		}
	}

	/**
	 * <pre>
	 * <b>Descripción:</b> Se encarga de armar el Query, ejecutar la consulta y
	 * validar que el registro se en FSFFFLGFID.
	 *
	 * -Parametros que se recibe de la clase "prepararFSFFFLGFID"
	 *
	 * @param strDocumento
	 * @param strTipoDocumento
	 * @param strCodTrn
	 * @param strCanal
	 * @param strFecha
	 * @param strTrace
	 * @return Retorna un valor boolean "true" o "false" en la variable "resultadoConsulta".
	 */
	private boolean validarConsultaFSFFFLGFID(String strDocumento, String strTipoDocumento, String strCodTrn, String strCanal, String strFecha, String strTrace) throws SQLException {
		if (StringUtils.isNoneBlank(strDocumento, strTipoDocumento, strCodTrn, strCanal, strFecha, strTrace)) {
			ResultSet objResult = null;
			boolean resultadoConsulta = false;
			String sql = QueryManager.ISERIES.getString("SQL.FSFFFLGFID.validarConsulta");

			try (Connection conexion = IseriesConnectionManager.getConnection();
			     PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, strDocumento);
				consulta.setString(2, strTipoDocumento);
				consulta.setString(3, strCodTrn);
				consulta.setString(4, strCanal);
				consulta.setString(5, strFecha);
				consulta.setString(6, String.format("%s%s%s", ConstantManager.PORCENTAJE, strTrace, ConstantManager.PORCENTAJE));
				objResult = consulta.executeQuery();
				if (objResult.isBeforeFirst()) {
					resultadoConsulta = true;
				} else {
					LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				}
			} finally {
				if (objResult != null) {
					objResult.close();
				}
			}
			return resultadoConsulta;
		}
		LOGGER.info("No se validó la consulta en FSFFFLGFID");
		return false;
	}

	/**
	 * <pre>
	 * <b>Descripción:</b>
	 * Se encargar de armar la consulta y verificar que la transacción se presente en el
	 * Log FSFLIBRAMD.FSFFFLGFID la realizar transferencias desde y hacia cuentas deposito.
	 *
	 * @param strDocumento - Número de cedula del cliente sin puntos ni comas.
	 * @param strTipoDocumento - Tipo de documento del cliente ej: "1" o "3" etc.
	 * @param strCodTrn - Código de la transacción en curso ej: "0439" o "0449" etc.
	 * @param strCanal - Canal por donde se realizo la transacción Ej: "BLP" o "BLE".
	 * @param strHoraAntes - Esta hora militar debe ser calculada en la pantalla de confirmación con el siguiente formato Ej: "140101" o "090101" etc.
	 * @param strCuentaOrigen - Corresponde al encargo o a la cuenta deposito de donde salio el dinero Ej: "40612400026" o "1000300930" etc.
	 * @param strCuentaDestino - Corresponde al encargo o a la cuenta deposito que resibe el dinero Ej: "40612400026" o "1000300930" etc.
	 * @return  - Se retorna true o false en la variable "resultadoConsulta", indicando si fue encontrado o no.
	 * @throws SQLException the sql exception
	 */
	public boolean validarTransferenciasFSFFFLGFID(String strDocumento, String strTipoDocumento, String strCodTrn, String strCanal, String strHoraAntes, String strCuentaOrigen, String strCuentaDestino) throws SQLException {
		if (StringUtils.isNoneBlank(strDocumento, strDocumento, strCodTrn, strCanal, strHoraAntes, strCuentaOrigen, strCuentaDestino)) {
			boolean resultadoConsulta = false;
			String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
			strDocumento = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumento));
			strHoraAntes = String.format(ConstantManager.FORMATO6CEROSIZQ, Long.parseLong(strHoraAntes));
			strCuentaOrigen = strCuentaOrigen.replace(ConstantManager.GUION, ConstantManager.VACIO);
			strCuentaDestino = strCuentaDestino.replace(ConstantManager.GUION, ConstantManager.VACIO);

			ResultSet objResult = null;
			String sql = ConstantManager.VACIO;
			String datoDinamico1 = ConstantManager.VACIO;
			String datoDinamico2 = ConstantManager.VACIO;
			String datoDinamico3 = ConstantManager.VACIO;
			if (TransactionCodes.TRANS_AHRRO_FONDO.getId().equals(strCodTrn)) {
				sql = QueryManager.ISERIES.getString("SQL.FSFFFLGFID.validarTransferencias0539");
				datoDinamico1 = strCuentaOrigen;
				datoDinamico2 = strCuentaDestino;
				datoDinamico3 = ConstantManager.CODIGO_CUATRO;
			} else if (TransactionCodes.TRANS_FONDO_AHORR.getId().equals(strCodTrn)) {
				sql = QueryManager.ISERIES.getString("SQL.FSFFFLGFID.validarTransferencias0549");
				datoDinamico1 = strCuentaDestino;
				datoDinamico2 = strCuentaOrigen;
				datoDinamico3 = ConstantManager.CODIGO_CINCO;
			}
			try (Connection conexion = IseriesConnectionManager.getConnection();
			     PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, strDocumento);
				consulta.setString(2, strTipoDocumento);
				consulta.setString(3, datoDinamico3);
				consulta.setString(4, strCanal);
				consulta.setString(5, strFecha);
				consulta.setString(6, strHoraAntes);
				consulta.setString(7, datoDinamico2);
				consulta.setString(8, datoDinamico1);
				objResult = consulta.executeQuery();
				if (objResult.isBeforeFirst()) {
					while (objResult.next()) {
						resultadoConsulta = true;
					}
				} else {
					LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				}
			} finally {
				if (objResult != null) {
					objResult.close();
				}
			}
			return resultadoConsulta;
		} else {
			LOGGER.info("No se validó la transferencia en FSFFFLGFID");
			return false;
		}
	}
}
