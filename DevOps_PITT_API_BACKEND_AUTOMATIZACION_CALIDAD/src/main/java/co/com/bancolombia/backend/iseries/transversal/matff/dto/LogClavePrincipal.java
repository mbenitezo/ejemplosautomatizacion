package co.com.bancolombia.backend.iseries.transversal.matff.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Log clave principal.
 */
public class LogClavePrincipal {

	private String strCodigoClvTrn;
	private String strDescripcionClvTrn;
	private String strEstadoClv;
	private String strDescripcionEstClv;


	/**
	 * Instantiates a new Log clave principal.
	 *
	 * @param strCodigoClvTrn      the str codigo clv trn
	 * @param strDescripcionClvTrn the str descripcion clv trn
	 * @param strEstadoClv         the str estado clv
	 * @param strDescripcionEstClv the str descripcion est clv
	 */
	public LogClavePrincipal(String strCodigoClvTrn, String strDescripcionClvTrn, String strEstadoClv, String strDescripcionEstClv) {

		this.strCodigoClvTrn = strCodigoClvTrn;
		this.strDescripcionClvTrn = strDescripcionClvTrn;
		this.strEstadoClv = strEstadoClv;
		this.strDescripcionEstClv = strDescripcionEstClv;
	}

	/**
	 * Instantiates a new Log clave principal.
	 */
	public LogClavePrincipal() {

		strCodigoClvTrn = ConstantManager.VACIO;
		strDescripcionClvTrn = ConstantManager.VACIO;
		strEstadoClv = ConstantManager.VACIO;
		strDescripcionEstClv = ConstantManager.VACIO;
	}

	/**
	 * Gets str codigo clv trn.
	 *
	 * @return the str codigo clv trn
	 */
	public String getStrCodigoClvTrn() {
		return strCodigoClvTrn;
	}

	/**
	 * Sets str codigo clv trn.
	 *
	 * @param strCodigoClvTrn the str codigo clv trn
	 */
	public void setStrCodigoClvTrn(String strCodigoClvTrn) {
		this.strCodigoClvTrn = strCodigoClvTrn;
	}

	/**
	 * Gets str descripcion clv trn.
	 *
	 * @return the str descripcion clv trn
	 */
	public String getStrDescripcionClvTrn() {
		return strDescripcionClvTrn;
	}

	/**
	 * Sets str descripcion clv trn.
	 *
	 * @param strDescripcionClvTrn the str descripcion clv trn
	 */
	public void setStrDescripcionClvTrn(String strDescripcionClvTrn) {
		this.strDescripcionClvTrn = strDescripcionClvTrn;
	}

	/**
	 * Gets str estado clv.
	 *
	 * @return the str estado clv
	 */
	public String getStrEstadoClv() {
		return strEstadoClv;
	}

	/**
	 * Sets str estado clv.
	 *
	 * @param strEstadoClv the str estado clv
	 */
	public void setStrEstadoClv(String strEstadoClv) {
		this.strEstadoClv = strEstadoClv;
	}

	/**
	 * Gets str descripcion est clv.
	 *
	 * @return the str descripcion est clv
	 */
	public String getStrDescripcionEstClv() {
		return strDescripcionEstClv;
	}

	/**
	 * Sets str descripcion est clv.
	 *
	 * @param strDescripcionEstClv the str descripcion est clv
	 */
	public void setStrDescripcionEstClv(String strDescripcionEstClv) {
		this.strDescripcionEstClv = strDescripcionEstClv;
	}
}
