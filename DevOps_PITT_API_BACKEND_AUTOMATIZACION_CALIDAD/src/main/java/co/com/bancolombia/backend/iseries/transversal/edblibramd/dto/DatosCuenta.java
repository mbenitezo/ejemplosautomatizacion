package co.com.bancolombia.backend.iseries.transversal.edblibramd.dto;

/**
 * The type Datos cuenta.
 */
public class DatosCuenta {

	private String strNroCuenta;
	private String strTipoCuenta;
	private String strSaldoDisp;

	private String strDeudaTotalDolar;
	private String strDeudaTotalPesos;


	/**
	 * Instantiates a new Datos cuenta.
	 *
	 * @param strNroCuenta  the str nro cuenta
	 * @param strTipoCuenta the str tipo cuenta
	 * @param strSaldoDisp  the str saldo disp
	 */
	public DatosCuenta(String strNroCuenta, String strTipoCuenta, String strSaldoDisp) {
		// TODO Auto-generated constructor stub

		this.strNroCuenta = strNroCuenta;
		this.strTipoCuenta = strTipoCuenta;
		this.strSaldoDisp = strSaldoDisp;

	}

	/**
	 * Instantiates a new Datos cuenta.
	 *
	 * @param strNroCuenta       the str nro cuenta
	 * @param strTipoCuenta      the str tipo cuenta
	 * @param strDeudaTotalPesos the str deuda total pesos
	 * @param strDeudaTotalDolar the str deuda total dolar
	 */
	public DatosCuenta(String strNroCuenta, String strTipoCuenta, String strDeudaTotalPesos, String strDeudaTotalDolar) {
		// TODO Auto-generated constructor stub

		this.strNroCuenta = strNroCuenta;
		this.strTipoCuenta = strTipoCuenta;
		this.strDeudaTotalPesos = strDeudaTotalPesos;
		this.strDeudaTotalDolar = strDeudaTotalDolar;

	}

	/**
	 * Gets str nro cuenta.
	 *
	 * @return the str nro cuenta
	 */
	public String getStrNroCuenta() {
		return strNroCuenta;
	}


	/**
	 * Sets str nro cuenta.
	 *
	 * @param strNroCuenta the str nro cuenta
	 */
	public void setStrNroCuenta(String strNroCuenta) {
		this.strNroCuenta = strNroCuenta;
	}

	/**
	 * Gets str tipo cuenta.
	 *
	 * @return the str tipo cuenta
	 */
	public String getStrTipoCuenta() {
		return strTipoCuenta;
	}

	/**
	 * Sets str tipo cuenta.
	 *
	 * @param strTipoCuenta the str tipo cuenta
	 */
	public void setStrTipoCuenta(String strTipoCuenta) {
		this.strTipoCuenta = strTipoCuenta;
	}

	/**
	 * Gets str saldo disp.
	 *
	 * @return the str saldo disp
	 */
	public String getStrSaldoDisp() {
		return strSaldoDisp;
	}

	/**
	 * Sets str saldo disp.
	 *
	 * @param strSaldoDisp the str saldo disp
	 */
	public void setStrSaldoDisp(String strSaldoDisp) {
		this.strSaldoDisp = strSaldoDisp;
	}

	/**
	 * Gets str deuda total dolar.
	 *
	 * @return the str deuda total dolar
	 */
	public String getStrDeudaTotalDolar() {
		return strDeudaTotalDolar;
	}

	/**
	 * Sets str deuda total dolar.
	 *
	 * @param strDeudaTotalDolar the str deuda total dolar
	 */
	public void setStrDeudaTotalDolar(String strDeudaTotalDolar) {
		this.strDeudaTotalDolar = strDeudaTotalDolar;
	}

	/**
	 * Gets str deuda total pesos.
	 *
	 * @return the str deuda total pesos
	 */
	public String getStrDeudaTotalPesos() {
		return strDeudaTotalPesos;
	}

	/**
	 * Sets str deuda total pesos.
	 *
	 * @param strDeudaTotalPesos the str deuda total pesos
	 */
	public void setStrDeudaTotalPesos(String strDeudaTotalPesos) {
		this.strDeudaTotalPesos = strDeudaTotalPesos;
	}
}
