package co.com.bancolombia.backend.iseries.transversal.alertas.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.alertas.dto.DestinoAlerta;
import co.com.bancolombia.backend.iseries.transversal.alertas.dto.LogAlerta;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que se encarga de manejar todos los componentes del log de alertas se
 * tocan los archivos PCCLIBRAMD.PCCFFLGAEN
 *
 * @author david.c.gonzalez
 */
public class BackLogAlerta {

	private static final Logger LOGGER = LogManager.getLogger(BackLogAlerta.class.getName());

	/**
	 * Consultar log alerta list.
	 *
	 * @param tipoDocumento the tipo documento
	 * @param numDocumento  the num documento
	 * @param codOperacion  the cod operacion
	 * @param hora          the hora
	 *
	 * @return the list
	 *
	 * @throws SQLException the sql exception
	 */
	public List<LogAlerta> consultarLogAlerta(String tipoDocumento, String numDocumento, String codOperacion, String hora) throws SQLException {
		List<LogAlerta> logAlertasObtenido = new ArrayList<>();
		if (StringUtils.isNoneBlank(tipoDocumento, numDocumento, codOperacion, hora)) {
			ResultSet objResult = null;
			String diaActual = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
			String sql = QueryManager.ISERIES.getString("SQL.PCCFFLGAEN.consultarLogAlerta");

			try (Connection conexion = IseriesConnectionManager.getConnection();
			     PreparedStatement consulta = conexion.prepareStatement(sql)) {

				consulta.setString(1, numDocumento);
				consulta.setString(2, codOperacion);
				consulta.setString(3, tipoDocumento);
				consulta.setString(4, diaActual);
				consulta.setString(5, hora);
				consulta.executeQuery();

				if (objResult.isBeforeFirst()) {
					while (objResult.next()) {
						LogAlerta objAlerta = new LogAlerta();
						objAlerta.setTipoAlerta(objResult.getString("AENTIPALE").trim());
						objAlerta.setDestinoAlerta(objResult.getString("AENDESTIN").trim());
						objAlerta.setCodigoRespuesta(objResult.getString("AENNROOPE").trim());
						logAlertasObtenido.add(objAlerta);
					}
				} else {
					LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				}
			} finally {
				if (objResult != null) {
					objResult.close();
				}
			}
			return logAlertasObtenido;
		} else {
			LOGGER.info("No se consultó el Log de Alerta");
			return new ArrayList<>();
		}
	}

	/**
	 * Metodo que se encarga de varificar cada uno de los campos de log de alertas.
	 *
	 * @param objAlertaObtenida the obj alerta obtenida
	 * @param strDoc            the str doc
	 * @param strTipoDoc        the str tipo doc
	 *
	 * @return boolean boolean
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean verificarLogAlerta(List<LogAlerta> objAlertaObtenida, String strDoc, String strTipoDoc) throws SQLException {
		if (ObjectUtils.allNotNull(objAlertaObtenida) && StringUtils.isNoneBlank(strDoc, strTipoDoc)) {
			int intBandera = 0;
			DestinoAlerta destinoAlerta = consultarCorreoMovilAlertas(strTipoDoc, strDoc);
			if (null != destinoAlerta && ConstantManager.SI.equals(destinoAlerta.getIndicadorActivo())) {
				for (LogAlerta logAlerta : objAlertaObtenida) {
					if (ConstantManager.CERO.equals(logAlerta.getCodigoRespuesta())) {
						String strDestinoObtenido = logAlerta.getDestinoAlerta();
						if (ConstantManager.LETRA_C.equals(logAlerta.getTipoAlerta())) {
							String strCorreoParametrizado = destinoAlerta.getCorreoAlerta();
							if (!strCorreoParametrizado.equals(strDestinoObtenido)) {
								intBandera++;
							}
						} else if (ConstantManager.LETRA_M.equals(logAlerta.getTipoAlerta())) {
							String strMovilParametrizado = destinoAlerta.getMovilAlerta();
							if (!strMovilParametrizado.equals(strDestinoObtenido)) {
								intBandera++;
							}
						} else {
							intBandera++;
						}
					} else {
						intBandera++;
					}
				}
			} else {
				intBandera++;
			}
			return intBandera == 0;
		} else {
			LOGGER.info("No se verificó el Log de Alerta");
			return false;
		}
	}

	/**
	 * Consulta de iseries el correo y el numero de celular del cliente en (Alerta)
	 * Archivo PCCLIBRAMD.PCCFFDGAL Si no trae ningun registro devuelve un objeto
	 * null
	 *
	 * @param strDoc:     Recibe el docuemnto del cliente para hacer la consulta.
	 * @param strTipoDoc: Recibe el tipo de docuemnto del cliente para hacer la consulta.
	 *
	 * @return Alerta
	 *
	 * @throws SQLException Lanza excepciones de tipo SQL
	 */
	private DestinoAlerta consultarCorreoMovilAlertas(String strTipoDoc, String strDoc) throws SQLException {
		if (StringUtils.isNoneBlank(strDoc, strTipoDoc)) {
			DestinoAlerta objAlertaCorreoMovil = null;
			ResultSet objResult = null;
			String sql = QueryManager.ISERIES.getString("SQL.PCCFFDGAL.consultarCorreoMovilAlertas");
			try (Connection conexion = IseriesConnectionManager.getConnection();
			     PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, strTipoDoc);
				consulta.setString(2, strDoc);
				consulta.executeQuery();
				if (objResult.isBeforeFirst()) {
					while (objResult.next()) {
						String strCelular = objResult.getString("ALNROCEL");
						String strCorreo = objResult.getString("ALEMAIL");
						String strIndicadorActivo = objResult.getString("ALINDACT");
						objAlertaCorreoMovil = new DestinoAlerta(strCelular, strCorreo, strIndicadorActivo);
					}
				} else {
					LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
					objAlertaCorreoMovil = new DestinoAlerta();
				}
			} finally {
				if (objResult != null) {
					objResult.close();
				}
			}
			return objAlertaCorreoMovil;
		} else {
			LOGGER.info("No se consultó el Correo Movil Alertas");
			return new DestinoAlerta();
		}
	}
}