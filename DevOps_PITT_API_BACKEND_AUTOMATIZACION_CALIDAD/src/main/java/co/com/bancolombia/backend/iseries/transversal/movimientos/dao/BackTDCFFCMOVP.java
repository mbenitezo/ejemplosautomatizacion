package co.com.bancolombia.backend.iseries.transversal.movimientos.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.movimientos.dto.DatosMovimiento;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Back tdcffcmovp.
 */
public class BackTDCFFCMOVP {
	private static final Logger LOGGER = LogManager.getLogger(BackTDCFFCMOVP.class.getName());

	/**
	 * Consulta tdcffcmovp list.
	 *
	 * @param strNumeroTarjeta the str numero tarjeta
	 * @param strCanal         the str canal
	 *
	 * @return the list
	 *
	 * @throws SQLException the sql exception
	 */
	public List<DatosMovimiento> consultaTDCFFCMOVP(String strNumeroTarjeta, String strCanal) throws SQLException {
		List<DatosMovimiento> listaMovimientos = new ArrayList<>();
		String numeroTarjeta = String.format(ConstantManager.FORMATO16CEROSIZQ, Long.parseLong(strNumeroTarjeta));
		String strFechaActual = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		String strFechaAntes = DateManager.obtenerFechaAntes(ConstantManager.FORMATO_FECHA, -2, 0);
		ResultSet objResult = null;
		String limite = "10";
		if (ConstantManager.SVP.equalsIgnoreCase(strCanal) || ConstantManager.SVE.equalsIgnoreCase(strCanal)) {
			limite = "20";
		}
		String sql = String.format(QueryManager.ISERIES.getString("SQL.TDCFFCMOVP.consulta"), limite);
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, numeroTarjeta);
			consulta.setString(2, strFechaActual);
			consulta.setString(3, strFechaAntes);
			objResult = consulta.executeQuery(sql);
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strFechaMovimiento = objResult.getString("CMFECMOV").trim();
					String strDescripcion = objResult.getString("CMNOMESA").trim();
					String strValorMovimiento = objResult.getString("CMVLRTOT").trim();

					DatosMovimiento datosMovimiento = new DatosMovimiento(strFechaMovimiento, strDescripcion,
							strValorMovimiento);
					listaMovimientos.add(datosMovimiento);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return listaMovimientos;
	}

	/**
	 * <pre>
	 * <b>Descripción:</b> Método que recibe la lista de datos de las cuentas
	 * depósitos que tiene un cliente pero que vienen del lado front. El resultado
	 * llega almacenado en un objeto tipo list y se verifica: el canal por el que
	 * llega, el tipo de cuenta y se cambia a su correspondiente valor numérico (1)
	 * o (7); ahorros o corriente. Una vez tratados los datos, se comparan con la
	 * lista obtenida del lado backend y se retorna un resultado.
	 *
	 * @param listaObtenida - es la lista que se consulta previamente del lado backend
	 * @param listaEsperada - es la lista esperada del lado front, la cual se compara con lista            obtenida del lado backend
	 * @param strCanal - es el canal por el cual se recibe la lista esperada ya sea APP,            SVP o SVE.
	 * @return boolean boolean
	 */
	public Boolean validarMovimientos(List<DatosMovimiento> listaObtenida, List<DatosMovimiento> listaEsperada, String strCanal) {
		boolean resultado = false;

		for (DatosMovimiento movimientosEsperados : listaEsperada) {

			String strFechaMovEsperado = movimientosEsperados.getStrFechaMovimiento();
			String strDescripcionMovEsperado = movimientosEsperados.getStrDescripcion().toUpperCase();
			String strValorMovEsperado = movimientosEsperados.getStrValorMovimiento();

			for (DatosMovimiento movimientoObtenido : listaObtenida) {
				resultado = false;
				String strFechaMovObtenido = movimientoObtenido.getStrFechaMovimiento();
				String strDescripcionMovObtenido = movimientoObtenido.getStrDescripcion().toUpperCase();
				String strValorMovObtenido = movimientoObtenido.getStrValorMovimiento();
				if (ConstantManager.APP.equals(strCanal) || ConstantManager.SVP.equals(strCanal) || ConstantManager.SVE.equals(strCanal)) {
					strFechaMovEsperado = strFechaMovEsperado.replace("/", ConstantManager.VACIO);
					strValorMovEsperado = strValorMovEsperado.replace(",", ConstantManager.VACIO);

					if (strFechaMovEsperado.equals(strFechaMovObtenido)
							&& strDescripcionMovEsperado.equals(strDescripcionMovObtenido)
							&& strValorMovEsperado.equals(strValorMovObtenido)) {
						resultado = true;
						LOGGER.info("Valor obtenido es igual al esperado \n" + "Valor Esperado : "
								+ strFechaMovEsperado + " | " + strDescripcionMovEsperado + " | " + strValorMovEsperado
								+ " \n" + "Valor Obtenido : " + strFechaMovObtenido + " | " + strDescripcionMovObtenido
								+ " | " + strValorMovObtenido + " \n");
						break;
					}
				}
			}
			if (!resultado) {
				LOGGER.info("El valor esperado no fue encontrado \n" + "Valor Esperado : " + strFechaMovEsperado
						+ " | " + strDescripcionMovEsperado + " | " + strValorMovEsperado + " \n");
				break;
			}
		}
		return resultado;
	}
}
