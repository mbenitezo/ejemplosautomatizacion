package co.com.bancolombia.backend.iseries.alm.cuenta.dao;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.alm.cuenta.dao.BackPertenenciaCuentaClienteALM;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Back Pin exitoso app ALM.
 */
public class BackPertenenciaCuentaClienteALM {

private static final Logger LOGGER = LogManager.getLogger(BackPertenenciaCuentaClienteALM.class.getName());
	
	/**
	 * Módulo que consulta si la cuenta de destino es válida VISIONR.CADDR
	 *
	 * @param strCuenta : Recibe el número de cuenta para verificar si es válida.
	 *
	 * @return blnresultadoFinal boolean
	 *
	 * @throws SQLException the sql exception
	 */
	
	public boolean consultarPertenenciaCuentaDestinoALM(String strCuenta) throws SQLException {
		String cuentaCliente = String.format(ConstantManager.FORMATO1CEROIZQ, Long.parseLong(strCuenta));
		boolean blnResultadoFinal = false;
		Connection conexion = null;
		ResultSet objResult = null;
		try {
			conexion = IseriesConnectionManager.getConnection();
			String sql = QueryManager.ISERIES.getString("SQL.CADDRconsultarPertenenciaCuentaDestinoALM");
			try (PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, cuentaCliente);
				objResult = consulta.executeQuery();
			}
			if (objResult.isBeforeFirst()) {
				LOGGER.info("Consulta exitosa, la cuenta de destino es válida.");
				blnResultadoFinal = true;
			} else {
				LOGGER.info("Error, la cuenta de destino no es válida.");
			}
			return blnResultadoFinal;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
			if (conexion != null) {
				conexion.close();
			}
		}
	}
	
}
