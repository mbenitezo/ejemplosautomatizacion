package co.com.bancolombia.backend.utilidades;

import java.util.ResourceBundle;


/**
 * The Class QueryManager.
 *
 * @author Oscar Armando Vallejo Tovar
 */
public class QueryManager {

	/**
	 * Instantiates a new query manager.
	 */
	private QueryManager() {
		throw new IllegalStateException("Utilidad de Consultas");
	}

	/**
	 * The Constant ISERIES.
	 */
	public static final ResourceBundle ISERIES = ResourceBundle.getBundle("consultasIseries");

	public static final ResourceBundle CLOUDANT = ResourceBundle.getBundle("consultasCloudant");

	/**
	 * The Constant EXCEL.
	 */
	public static final ResourceBundle EXCEL = ResourceBundle.getBundle("consultasExcel");

}
