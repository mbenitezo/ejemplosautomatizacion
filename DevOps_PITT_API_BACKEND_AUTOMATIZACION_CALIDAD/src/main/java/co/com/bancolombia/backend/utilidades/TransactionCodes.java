package co.com.bancolombia.backend.utilidades;

/**
 * The enum Transaction codes.
 */
public enum TransactionCodes {
	/**
	 * The Trans ahrro fondo.
	 */
	TRANS_AHRRO_FONDO("0539","Transferencia desde Cuenta de Ahorro a un Fondos de Inversión"),
	/**
	 * The Trans corr fondo.
	 */
	TRANS_CORR_FONDO("0439","Transferencia desde Cuenta Corriente a un Fondos de Inversión"),

	/**
	 * The Trans fondo ahorr.
	 */
	TRANS_FONDO_AHORR("0549","Transferencia desde un Fondos de Inversión a una Cuenta de Ahorro"),
	/**
	 * The Trans fondo corr.
	 */
	TRANS_FONDO_CORR("0449","Transferencia desde un Fondos de Inversión a una Cuenta Corriente"),
	/**
	 * Trans 1 transaction codes.
	 */
	TRANS_1("1100", ""),
	/**
	 * Trans 2 transaction codes.
	 */
	TRANS_2("0513", ""),
	/**
	 * Trans 3 transaction codes.
	 */
	TRANS_3("0514", ""),
	/**
	 * Trans 4 transaction codes.
	 */
	TRANS_4("0516", ""),
	/**
	 * Trans 5 transaction codes.
	 */
	TRANS_5("0410", ""),
	/**
	 * Trans 6 transaction codes.
	 */
	TRANS_6("0411", ""),
	/**
	 * Trans 7 transaction codes.
	 */
	TRANS_7("0510", ""),
	/**
	 * Trans 8 transaction codes.
	 */
	TRANS_8("0317", ""),
	/**
	 * Trans 9 transaction codes.
	 */
	TRANS_9("0410", ""),
	/**
	 * Trans 10 transaction codes.
	 */
	TRANS_10("0411",""),

	/** The trans dep cor. */
	TRANS_BANCOLOMBIA_CORR("0438", "Transferencia entre cuentas bancolombia Corriente"),


	TRANS_BANCOLOMBIA_AHORR("0538", "Transferencia entre cuentas bancolombia Ahorro");

	/** The id. */
	private final String id;

	/** The msg. */
	private final String msg;

	/**
	 * Instantiates a new transaction codes.
	 *
	 * @param id  the id
	 * @param msg the msg
	 */
	TransactionCodes(String id, String msg) {
		this.id = id;
		this.msg = msg;
	}

	/**
	 * Gets id.
	 *
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Gets msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return this.msg;
	}
}
