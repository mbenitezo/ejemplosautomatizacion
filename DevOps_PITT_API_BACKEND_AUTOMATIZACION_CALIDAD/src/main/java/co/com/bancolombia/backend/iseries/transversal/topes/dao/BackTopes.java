package co.com.bancolombia.backend.iseries.transversal.topes.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.topes.dto.TopeBanco;
import co.com.bancolombia.backend.iseries.transversal.topes.dto.TopeEprepago;
import co.com.bancolombia.backend.iseries.transversal.topes.dto.TopePersonalizado;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantOrientacion;

/**
 * Clase que se encarga de lo relacionado con los
 * Bancolombia.BackEndAutomatizacion.Topes (Banco: CABLIBRAMD.CABFFLTTRA.
 * Personalizados: PCCLIBRAMD.PCCFFATPCL)
 *
 * @author david.c.gonzalez
 */
public class BackTopes {
	private static final Logger LOGGER = LogManager.getLogger(BackTopes.class.getName());

	/**
	 * Metodo que se encarga de buscar los topes banco (CABLIBRAMD.CABFFLTTRA) para
	 * un cliente.
	 *
	 * @param strDoc : Define el documento del cliente al que se quiere buscar los topes banco.
	 *
	 * @return TopeBanco : Devuelve los topes banco (valor y fecha) de un cliente
	 *
	 * @throws SQLException the sql exception
	 */
	public TopeBanco consultarTopeBanco(String strDoc) throws SQLException {
		TopeBanco resultTope = null;
		String strFormatDoc;
		ResultSet objResult = null;
		strFormatDoc = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDoc));
		String sql = QueryManager.ISERIES.getString("SQL.CABFFLTTRA.consultarTopeBanco");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strFormatDoc);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strValorAcumulado = objResult.getString("LTACMDIA").trim();
					String strFecha = objResult.getString("FECHATR").trim();
					resultTope = new TopeBanco(strValorAcumulado, strFecha);
				}
			} else {
				resultTope = new TopeBanco();
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
			return resultTope;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
	}

	/**
	 * Metodo que se encarga de validar los topes banco de un cliente, de acuerdo a
	 * el valor del tope antes y despues de la transaccion, como tambien de la
	 * orientancion del caso
	 *
	 * @param objTopeBancoAnt    :    Define el tope banco antes de la transaccion.
	 * @param objTopeBacoDesp    :    Define el tope banco despues de la transaccion.
	 * @param strValorTransferir : Define el valor a transferir en la transaccion.
	 * @param strOrientacion     :     Define la orientancion que se quiere dar a la validacion (ACIERTO,                            ERROR)
	 *
	 * @return boolean : Devuelve true si la validacion fue correcta, de lo contrario un false, de acuerdo a su orientacion.
	 */
	public boolean validarTopeBanco(TopeBanco objTopeBancoAnt, TopeBanco objTopeBacoDesp, String strValorTransferir, String strOrientacion) {
		boolean resultTopeBanco = false;

		String strFechaHoy = DateManager.obtenerFechaSistema("yy-M-d");

		String dateTopeFechaAntes = objTopeBancoAnt.getStrFechaTrn();
		String dateTopeFechaDespues = objTopeBacoDesp.getStrFechaTrn();

		double dblValorTransferir = Double.parseDouble(strValorTransferir);

		double dblValorAntes = objTopeBancoAnt.getDblValorAcumulado();
		double dblValorDesp = objTopeBacoDesp.getDblValorAcumulado();

		if (ConstantOrientacion.ACIERTO.equalsIgnoreCase(strOrientacion)) {
			resultTopeBanco = strFechaHoy.equals(dateTopeFechaDespues)
					&& (dblValorTransferir + dblValorAntes) == dblValorDesp;
		} else if (ConstantOrientacion.ERROR.equalsIgnoreCase(strOrientacion)) {
			resultTopeBanco = dateTopeFechaAntes.equals(dateTopeFechaDespues) && dblValorAntes == dblValorDesp;
		}
		return resultTopeBanco;
	}

	/**
	 * Metodo que se encarga de buscar los topes personalziados
	 * (PCCLIBRAMD.PCCFFATPCL) de una transaccion para un cliente.
	 *
	 * @param strDoc             :             Define el documento del cliente al que se quiere buscar los topes                            personalizados
	 * @param strTipoDoc         :         Define el tipo de documento del cliente para la busqueda
	 * @param strCodigoOperacion : Define el codigo de operacion de la transaccion.
	 * @param strCanal           the str canal
	 *
	 * @return TopePersonalizado : Devuelve los topes personalizados (numero de operaciones y monto) de la transaccion para el cliente en particular
	 *
	 * @throws SQLException the sql exception
	 */
	public TopePersonalizado consultarTopePersonalizado(String strDoc, String strTipoDoc, String strCodigoOperacion, String strCanal) throws SQLException {
		TopePersonalizado resultTope = null;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.PCCFFATPCL.consultarTopePersonalizado");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strCanal);
			consulta.setString(2, strCodigoOperacion);
			consulta.setString(3, strDoc);
			consulta.setString(4, strTipoDoc);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strNumeroOperacion = objResult.getString("NROOPERA").trim();
					String strMonto = objResult.getString("MONTO").trim();
					resultTope = new TopePersonalizado(strNumeroOperacion, strMonto);
				}
			} else {
				resultTope = new TopePersonalizado();
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
			return resultTope;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
	}

	/**
	 * Metodo que se encarga de validar los topes perzonalisados de un cliente, de
	 * acuerdo al valor del tope antes y despues de la transaccion, como tambien de
	 * la orientancion del caso, tambien valida que exista la parametrizacion de los
	 * topes para esa transaccion(PCCLIBRAMD.PCCFFPPCLI)
	 *
	 * @param objTopePersonalAnt  : Define el tope personalizado antes de la transaccion.
	 * @param objTopePersonalDesp : Define el tope personalizado despues de la transaccion.
	 * @param strValorTransferir  : Define el valor a transferir de la transaccion.
	 * @param strOrientacion      : Define la orientancion que se quiere dar a la validacion (ACIERTO, ERROR)
	 * @param strDoc              : Define el documento del cliente para buscar la parametrizacion de los topes personalizados
	 * @param strTipoDoc          : Define el tipo documento del cliente para buscar la parametrizacion de los topes personalizados
	 * @param strCodigoOperacion  :  Define codigo de operacion de la transaccion para buscar la parametrizacion de los topes personalizados
	 * @param strCanal            the str canal
	 *
	 * @return boolean : Devuelve true si la validacion fue correcta, de lo contrario un false, de acuerdo a su orientacion.
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean validarTopePersonalizado(TopePersonalizado objTopePersonalAnt, TopePersonalizado objTopePersonalDesp, String strValorTransferir, String strOrientacion, String strDoc, String strTipoDoc, String strCodigoOperacion, String strCanal) throws SQLException {
		boolean resultTopeBanco = false;

		double dblValorTransferir = Double.parseDouble(strValorTransferir);

		double dblMontoAntes = objTopePersonalAnt.getDblMontoAcumulado();
		double dblMontoDespues = objTopePersonalDesp.getDblMontoAcumulado();

		int intNumOperacionAntes = objTopePersonalAnt.getIntNumeroOperacion();
		int intNumOperacionDespues = objTopePersonalDesp.getIntNumeroOperacion();

		TopePersonalizado topeParametrizado = consultarTopeParametrizado(strDoc, strTipoDoc, strCodigoOperacion, strCanal);

		if (null != topeParametrizado && topeParametrizado.getDblMontoAcumulado() == 0 && topeParametrizado.getIntNumeroOperacion() == 0) {
			resultTopeBanco = (dblMontoAntes == dblMontoDespues) && (intNumOperacionDespues == intNumOperacionAntes);
			LOGGER.info("no esta parametrizado");
			LOGGER.info(resultTopeBanco);
		} else {
			if (ConstantOrientacion.ACIERTO.equalsIgnoreCase(strOrientacion)) {
				resultTopeBanco = ((dblMontoAntes + dblValorTransferir) == dblMontoDespues)
						&& ((intNumOperacionAntes + 1) == intNumOperacionDespues);
			} else if (ConstantOrientacion.ERROR.equalsIgnoreCase(strOrientacion)) {
				resultTopeBanco = (dblMontoAntes == dblMontoDespues)
						&& (intNumOperacionDespues == intNumOperacionAntes);
			}
		}
		return resultTopeBanco;
	}

	/**
	 * Metodo que se encarga de buscar los topes parametrizados
	 * (PCCLIBRAMD.PCCFFPPCLI) de una transaccion para un cliente.
	 *
	 * @param strDoc:             Define el documento del cliente al que se quiere buscar los topes.
	 * @param strTipoDoc:         Define el tipo de documento del cliente al que se quiere buscar
	 *                            los topes
	 * @param strCodigoOperacion: Define el codigo de operacion de la transaccion al que se quiere
	 *                            buscar los topes
	 *
	 * @return
	 */
	private TopePersonalizado consultarTopeParametrizado(String strDoc, String strTipoDoc, String strCodigoOperacion, String strCanal) throws SQLException {
		TopePersonalizado resultTope = null;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.PCCFFPPCLI.consultarTopeParametrizado");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strCanal);
			consulta.setString(2, strCodigoOperacion);
			consulta.setString(3, strDoc);
			consulta.setString(4, strTipoDoc);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strNumeroOperacion = objResult.getString("NROOPERA").trim();
					String strMonto = objResult.getString("MONTO").trim();
					resultTope = new TopePersonalizado(strNumeroOperacion, strMonto);
				}
			} else {
				resultTope = new TopePersonalizado(ConstantManager.CERO, ConstantManager.CERO);
			}
			return resultTope;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
	}

	/**
	 * Devuelve un objeto de tipo TopeEprepago con los topes de la tarjeta
	 * e-prepago que están registrados en la librería cablibramd/cabffcfepp
	 * <p>
	 * Este método siempre retorna, existan o no los topes de la tarjeta e-prepago
	 *
	 * @return Los topes de la tarjeta e-prepago
	 *
	 * @throws SQLException the sql exception
	 * @author Oscar Armando Vallejo Tovar
	 */
	public TopeEprepago consultarTopesEprepago() throws SQLException {
		TopeEprepago topeEprepago = null;
		List<String> listaTopesEprepago = new ArrayList<>();
		Connection conexion = null;
		ResultSet objResult = null;
		try {
			conexion = IseriesConnectionManager.getConnection();
			String sql = QueryManager.ISERIES.getString("SQL.CABFFCFEPP.consultarTopesEprepago");
			try (PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, ConstantManager.CARGA);
				consulta.setString(2, ConstantManager.DESCARGA);
				objResult = consulta.executeQuery(sql);
			}
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					listaTopesEprepago.add(objResult.getString("DATO1N"));
					listaTopesEprepago.add(objResult.getString("DATO2N"));
				}
				topeEprepago = new TopeEprepago(listaTopesEprepago.get(0), listaTopesEprepago.get(1),
						listaTopesEprepago.get(2), listaTopesEprepago.get(3));
			} else {
				topeEprepago = new TopeEprepago(ConstantManager.CERO, ConstantManager.CERO, ConstantManager.CERO, ConstantManager.CERO);
			}
			return topeEprepago;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
			if (conexion != null) {
				conexion.close();
			}
		}
	}
}
