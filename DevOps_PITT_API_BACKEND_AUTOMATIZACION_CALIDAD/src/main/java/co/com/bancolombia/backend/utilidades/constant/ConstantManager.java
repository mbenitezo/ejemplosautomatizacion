package co.com.bancolombia.backend.utilidades.constant;

import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;

/**
 * The Class ConstantManager.
 *
 * @author Oscar Armando Vallejo Tovar
 */
public class ConstantManager {

	/**
	 * Instantiates a new constant manager.
	 */
	private ConstantManager() {
		throw new IllegalStateException(ExceptionCodesManager.UTILITY_CLASS.getMsg());
	}

	/**
	 * The constant APP.
	 */
	public static final String APP = "APP";
	/**
	 * The constant SVP.
	 */
	public static final String SVP = "SVP";
	/**
	 * The constant SVE.
	 */
	public static final String SVE = "SVE";
	/**
	 * The constant FORMATO_FECHA.
	 */
	public static final String FORMATO_FECHA = "yyyyMMdd";
	/**
	 * The constant FORMATO_DIA.
	 */
	public static final String FORMATO_DIA = "dd";
	/**
	 * The constant FORMATO_MES.
	 */
	public static final String FORMATO_MES = "MM";
	/**
	 * The constant CODIGO_UNO.
	 */
	public static final String CODIGO_UNO = "0001";
	/**
	 * The constant CODIGO_DOS.
	 */
	public static final String CODIGO_DOS = "0002";
	/**
	 * The constant CODIGO_TRES.
	 */
	public static final String CODIGO_TRES = "0003";
	/**
	 * The constant CODIGO_CUATRO.
	 */
	public static final String CODIGO_CUATRO = "0004";
	/**
	 * The constant CODIGO_CINCO.
	 */
	public static final String CODIGO_CINCO = "0005";
	/**
	 * The constant GUION.
	 */
	public static final String GUION = "-";
	/**
	 * The constant VACIO.
	 */
	public static final String VACIO = "";
	/**
	 * The constant UNO.
	 */
	public static final String UNO = "1";
	/**
	 * The constant CUATRO.
	 */
	public static final String CUATRO = "4";
	/**
	 * The constant OCHO.
	 */
	public static final String OCHO = "8";
	/**
	 * The constant CARGA.
	 */
	public static final String CARGA = "CARGA";
	/**
	 * The constant DESCARGA.
	 */
	public static final String DESCARGA = "DESCARGA";
	/**
	 * The constant FORMATO17CEROSIZQ.
	 */
	public static final String FORMATO17CEROSIZQ = "%017d";
	/**
	 * The constant FORMATO2CEROSIZQ.
	 */
	public static final String FORMATO2CEROSIZQ = "%02d";
	/**
	 * The constant NUMERO_99.
	 */
	public static final String NUMERO_99 = "99";

	/**
	 * The constant NEQUI.
	 */
	public static final String NEQUI = "NEQ";
	/**
	 * The constant NUMERO_3.
	 */
	public static final String NUMERO_3 = "3";
	/**
	 * The constant NUMERO_1.
	 */
	public static final String NUMERO_1 = "1";
	/**
	 * The constant INI_CTA_CORRIENTE.
	 */
	public static final String INI_CTA_CORRIENTE = "D";
	/**
	 * The constant INI_TARJETA_CREDITO.
	 */
	public static final String INI_TARJETA_CREDITO = "TDC";
	/**
	 * The constant INI_TARJETA_DEPOSITO.
	 */
	public static final String INI_TARJETA_DEPOSITO = "TDEP";
	/**
	 * The constant FORMATO13CEROSIZQ.
	 */
	public static final String FORMATO13CEROSIZQ = "%013d";
	/**
	 * The constant INI_CTA_AHORROS.
	 */
	public static final String INI_CTA_AHORROS = "S";
	/**
	 * The constant FORMATO8CEROSIZQ.
	 */
	public static final String FORMATO8CEROSIZQ = "%08d";

	/** The Constant PRESTAMO. */
	public static final String PRESTAMO = "Préstamo";

	/** The Constant HIPOTECARIO. */
	public static final String HIPOTECARIO = "Hipotecario";

	// LETRAS
	/**
	 * The constant LETRA_A.
	 */
	public static final String LETRA_A = "A";
	/**
	 * The Constant LETRA_C.
	 */
	public static final String LETRA_C = "C";
	/**
	 * The constant LETRA_D.
	 */
	public static final String LETRA_D = "D";
	/**
	 * The Constant LETRA_M.
	 */
	public static final String LETRA_M = "M";
	/**
	 * The constant LETRA_T.
	 */
	public static final String LETRA_T = "T";
	/**
	 * The constant LETRA_V.
	 */
	public static final String LETRA_V = "V";

	/**
	 * The Constant CORRECTO.
	 */
	public static final String CORRECTO = "CORRECTO";

	/**
	 * The Constant SI.
	 */
	public static final String SI = "SI";

	/**
	 * The Constant CERO.
	 */
	public static final String CERO = "0";

	/**
	 * The Constant ESPACIO_BLANCO.
	 */
	public static final String ESPACIO_BLANCO = " ";

	/**
	 * The Constant DOCUMENTO.
	 */
	public static final String DOCUMENTO = "DOCUMENTO";

	/**
	 * The Constant TIPODOCUMENTO.
	 */
	public static final String TIPO_DOCUMENTO = "TIPODOCUMENTO";

	/**
	 * The Constant TRACE.
	 */
	public static final String TRACE = "TRACE";

	/**
	 * The Constant CODIGOTRANSACCION.
	 */
	public static final String CODIGOTRANSACCION = "CODIGOTRANSACCION";

	/**
	 * The Constant FECHA.
	 */
	public static final String FECHA = "FECHA";

	/**
	 * The Constant CODIGOERROR.
	 */
	public static final String CODIGOERROR = "CODIGOERROR";

	/**
	 * The Constant TIP_CTAORIGEN.
	 */
	public static final String TIP_CTAORIGEN = "TIP_CTAORIGEN";

	/**
	 * The Constant TARJETACREDITO.
	 */
	public static final String TARJETACREDITO = "TARJETACREDITO";

	/**
	 * The Constant TIP_CTADESTINO.
	 */
	public static final String TIP_CTADESTINO = "TIP_CTADESTINO";

	/**
	 * The Constant VALOR_TRANSFER.
	 */
	public static final String VALOR_TRANSFER = "VALOR_TRANSFER";

	/**
	 * The Constant DOCUMENTOINSCRITO.
	 */
	public static final String DOCUMENTOINSCRITO = "DOCUMENTOINSCRITO";

	/**
	 * The Constant TIPODOCUMENTOINS.
	 */
	public static final String TIPODOCUMENTOINS = "TIPODOCUMENTOINS";

	/**
	 * The Constant CUENTAINSCRITA.
	 */
	public static final String CUENTAINSCRITA = "CUENTAINSCRITA";

	/**
	 * The Constant FORMATO15CEROS.
	 */
	public static final String FORMATO15CEROSIZQ = "%015d";
	/**
	 * The constant FORMATO6CEROSIZQ.
	 */
	public static final String FORMATO6CEROSIZQ = "%06d";

	/**
	 * The Constant FORMATO16CEROS.
	 */
	public static final String FORMATO16CEROSIZQ = "%016d";
	
	/**
	 * The Constant FORMATO1CERO.
	 */
	public static final String FORMATO1CEROIZQ = "%01d";

	/**
	 * The constant PREBLOQUEADO.
	 */
	public static final String PREBLOQUEADO = "PREBLOQUEADO";
	/**
	 * The constant BLOQUEADO.
	 */
	public static final String BLOQUEADO = "BLOQUEADO";

	/**
	 * The constant ACTIVO.
	 */
	public static final String ACTIVO = "ACTIVO";
	/**
	 * The constant INVALIDO.
	 */
	public static final String INVALIDO = "INVALIDO";
	/**
	 * The constant CUENTA_ORIGEN.
	 */
	public static final String CUENTA_ORIGEN = "CTAORIGEN";
	/**
	 * The constant CUENTA_DESTINO.
	 */
	public static final String CUENTA_DESTINO = "CTADESTINO";
	/**
	 * The constant PORCENTAJE.
	 */
	public static final String PORCENTAJE = "%";
	/**
	 * The constant FORMATO_COPIA.
	 */
	public static final String FORMATO_COPIA = "CP%s_%s";
	/**
	 * The constant PARAMETROS.
	 */
	public static final String PARAMETROS = "Parametros";
	/**
	 * The constant CANAL_FONDO.
	 */
	public static final String CANAL_FONDO = "AUD";
	/**
	 * The constant ACTUALIZA_INTENTOS.
	 */
	public static final String ACTUALIZA_INTENTOS = "Actualiza intentos";
	/**
	 * The constant ACTIVA.
	 */
	public static final String ACTIVA = "ACTIVA";
	/**
	 * The constant BLOQUE_INT_FALLIDOS.
	 */
	public static final String BLOQUE_INT_FALLIDOS = "Bloqueo por intentos fallidos";
	/**
	 * The constant REINICIO_INTENTOS.
	 */
	public static final String REINICIO_INTENTOS = "Reinicio intentos";
	/**
	 * The constant REINICIO_INTENTOS.
	 */
	public static final String FORMATO_LIKE = "%s%s%s";

}
