package co.com.bancolombia.backend.iseries.personas.usuario.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * The type Back usuario personas.
 */
public class BackUsuarioPersonas {
	private static final Logger LOGGER = LogManager.getLogger(BackUsuarioPersonas.class.getName());

	/**
	 * DESCRIPCIÓN: Componente que adecua las propiedades de la clave de acuerdo a
	 * las necesidades de la transacción.
	 *
	 * @param strDocumento     Documento del cliente.
	 * @param strTipoDocumento Tipo del documento del cliente.
	 * @param strEstadoClave   Estado de la clave.
	 *
	 * @return TRUE si el metodo resolvio correctamente, FALSE si el strEstadoClave no es encontrado o porque el metodo ActualizarEstadoClave no resulve correctamente.
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean prepararClavePrincipal(String strDocumento, String strTipoDocumento, String strEstadoClave) throws SQLException {
		boolean resultadoFinal = false;
		String strFechaAnterior = DateManager.obtenerFechaAnterior(("yyyyMMdd"));
		String strEstado = ConstantManager.VACIO;
		String strDescripcionEstado = ConstantManager.VACIO;
		String strNumeroIntentos = ConstantManager.VACIO;

		switch (strEstadoClave) {
			case "ACTIVA":
				strEstado = "A";
				strDescripcionEstado = "ACTIVA";
				strNumeroIntentos = "0";
				resultadoFinal = true;
				break;
			case "CLAVEREINICIO":
				strEstado = "A";
				strDescripcionEstado = "ACTIVA";
				strNumeroIntentos = "2";
				resultadoFinal = true;
				break;
			case "BLOQUEOPIF":
				strEstado = "F";
				strDescripcionEstado = "BLOQUEADO POR INTENTOS FALLIDOS";
				strNumeroIntentos = "3";
				resultadoFinal = true;
				break;
			case "BLOQUEOPSB":
				strEstado = "S";
				strDescripcionEstado = "Bloqueado por Seguridad Bancaria";
				strNumeroIntentos = "3";
				resultadoFinal = true;
				break;
			case "BLOQUEOV":
				strEstado = "V";
				strDescripcionEstado = "BLOQUEADO VOLUNTARIO";
				strNumeroIntentos = "1";
				resultadoFinal = true;
				break;
			case "CLAVENV":
				strEstado = "A";
				strDescripcionEstado = "ACTIVA";
				strNumeroIntentos = "0";
				resultadoFinal = true;
				break;
			case "ACTIVAPIF":
				strEstado = "A";
				strDescripcionEstado = "ACTIVA";
				strNumeroIntentos = "2";
				resultadoFinal = true;
				break;
			default:
				break;
		}
		if (resultadoFinal) {
			return actualizarEstadoClave(strDocumento, strTipoDocumento, strEstado, strDescripcionEstado,
					strNumeroIntentos, strFechaAnterior);
		}
		return false;
	}

	/**
	 * DESCRIPCIÓN:
	 * Metodo que se encarga de armar la consulta con los parametros recibidos,
	 * Realiza el llamado al metodo "ejecutarUpdateQuery" para la ejecución del Query.
	 *
	 * @param strDocumento
	 * @param strTipoDocumento
	 * @param strEstado
	 * @param strDescripcionEstado
	 * @param strNumeroIntentos
	 * @param strFechaAnterior
	 *
	 * @return La siguiente variable "resultadoActualizacion" retorna una boolean (true , false).
	 */
	private boolean actualizarEstadoClave(String strDocumento, String strTipoDocumento, String strEstado,
	                                      String strDescripcionEstado, String strNumeroIntentos, String strFechaAnterior) throws SQLException {

		boolean resultadoActualizacion = false;
		Connection conexion = null;
		PreparedStatement consulta = null;
		try {
			conexion = IseriesConnectionManager.getConnection();
			String sql = QueryManager.ISERIES.getString("SQL.MATFFESTCL.ActualizarEstadoClave");
			consulta = conexion.prepareStatement(sql);
			consulta.setString(1, strEstado);
			consulta.setString(2, strDescripcionEstado);
			consulta.setString(3, strNumeroIntentos);
			consulta.setString(4, strFechaAnterior);
			consulta.setString(5, strDocumento);
			consulta.setString(6, strTipoDocumento);
			int intResult = consulta.executeUpdate();
			if (intResult != -1) {
				if (intResult > 0) {
					resultadoActualizacion = true;
				} else {
					LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				}
			} else {
				LOGGER.warn(ExceptionCodesManager.QUERY_FAILED.getMsg());
			}
			return resultadoActualizacion;
		} finally {
			if (consulta != null) {
				consulta.close();
			}
			if (conexion != null) {
				conexion.close();
			}
		}
	}
}
