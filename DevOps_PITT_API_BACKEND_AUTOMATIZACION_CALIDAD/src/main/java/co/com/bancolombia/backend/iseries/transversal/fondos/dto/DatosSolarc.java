package co.com.bancolombia.backend.iseries.transversal.fondos.dto;



/**
 * The type DatosSolarc principal.
 */

public class DatosSolarc {
	private double ValorTransferir;
	private String strCodTransaccion;
	
	/**
	 * Instantiates a new DatosSolarc principal.
	 *
	 * @param strValorTransferir       the str valor a transferir
	 */
	public DatosSolarc(double ValorTransferir,  String strCodTransaccion){
		this.ValorTransferir = ValorTransferir;
		this.strCodTransaccion = strCodTransaccion;

	}
	
	/**
	 * Instantiates a new Clave principal.
	 */
	public DatosSolarc() {
		strCodTransaccion = "";
	}
	
	/**
	 * Gets str valor a transferir
	 *
	 * @return the str valor a transferir
	 */
	public double getStrValorTransferir() {
		return ValorTransferir;
	}
	
	/**
	 * Sets str valor a transferir
	 *
	 * @param strValorTransferir the str valor a transferir
	 */
	public void setStrValorTransferir(double ValorTransferir) {
		this.ValorTransferir = ValorTransferir;
	}

	public String getStrCodTransaccion() {
		return strCodTransaccion;
	}

	public void setStrCodTransaccion(String strCodTransaccion) {
		this.strCodTransaccion = strCodTransaccion;
	}
	
	

}
