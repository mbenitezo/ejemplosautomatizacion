package co.com.bancolombia.backend.iseries.transversal.matff.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Log inscripcion cuentas.
 */
public class LogInscripcionCuentas {

	private String strSistema;
	private String strTipoCtaIns;
	private String strNroCtaIns;
	private String strTipoDocIns;
	private String strDocumentoIns;
	private String strTarjetaVirtual;
	private String strEstado;
	private String strDescripcionEst;
	private String strTransaccion;


	/**
	 * Instantiates a new Log inscripcion cuentas.
	 *
	 * @param strSistema        the str sistema
	 * @param strTipoCtaIns     the str tipo cta ins
	 * @param strNroCtaIns      the str nro cta ins
	 * @param strTipoDocIns     the str tipo doc ins
	 * @param strDocumentoIns   the str documento ins
	 * @param strTarjetaVirtual the str tarjeta virtual
	 * @param strEstado         the str estado
	 * @param strDescripcionEst the str descripcion est
	 * @param strTransaccion    the str transaccion
	 */
	public LogInscripcionCuentas(String strSistema, String strTipoCtaIns, String strNroCtaIns, String strTipoDocIns, String strDocumentoIns, String strTarjetaVirtual, String strEstado, String strDescripcionEst, String strTransaccion) {

		this.strSistema = strSistema;
		this.strTipoCtaIns = strTipoCtaIns;
		this.strNroCtaIns = strNroCtaIns;
		this.strTipoDocIns = strTipoDocIns;
		this.strDocumentoIns = strDocumentoIns;
		this.strTarjetaVirtual = strTarjetaVirtual;
		this.strEstado = strEstado;
		this.strDescripcionEst = strDescripcionEst;
		this.strTransaccion = strTransaccion;
	}

	/**
	 * Instantiates a new Log inscripcion cuentas.
	 */
	public LogInscripcionCuentas() {

		strSistema = ConstantManager.VACIO;
		strTipoCtaIns = ConstantManager.VACIO;
		strNroCtaIns = ConstantManager.VACIO;
		strTipoDocIns = ConstantManager.VACIO;
		strDocumentoIns = ConstantManager.VACIO;
		strTarjetaVirtual = ConstantManager.VACIO;
		strEstado = ConstantManager.VACIO;
		strDescripcionEst = ConstantManager.VACIO;
		strTransaccion = ConstantManager.VACIO;
	}

	/**
	 * Gets str sistema.
	 *
	 * @return the str sistema
	 */
	public String getStrSistema() {
		return strSistema;
	}

	/**
	 * Sets str sistema.
	 *
	 * @param strSistema the str sistema
	 */
	public void setStrSistema(String strSistema) {
		this.strSistema = strSistema;
	}

	/**
	 * Gets str tipo cta ins.
	 *
	 * @return the str tipo cta ins
	 */
	public String getStrTipoCtaIns() {
		return strTipoCtaIns;
	}

	/**
	 * Sets str tipo cta ins.
	 *
	 * @param strTipoCtaIns the str tipo cta ins
	 */
	public void setStrTipoCtaIns(String strTipoCtaIns) {
		this.strTipoCtaIns = strTipoCtaIns;
	}

	/**
	 * Gets str nro cta ins.
	 *
	 * @return the str nro cta ins
	 */
	public String getStrNroCtaIns() {
		return strNroCtaIns;
	}

	/**
	 * Sets str nro cta ins.
	 *
	 * @param strNroCtaIns the str nro cta ins
	 */
	public void setStrNroCtaIns(String strNroCtaIns) {
		this.strNroCtaIns = strNroCtaIns;
	}

	/**
	 * Gets str tipo doc ins.
	 *
	 * @return the str tipo doc ins
	 */
	public String getStrTipoDocIns() {
		return strTipoDocIns;
	}

	/**
	 * Sets str tipo doc ins.
	 *
	 * @param strTipoDocIns the str tipo doc ins
	 */
	public void setStrTipoDocIns(String strTipoDocIns) {
		this.strTipoDocIns = strTipoDocIns;
	}

	/**
	 * Gets str documento ins.
	 *
	 * @return the str documento ins
	 */
	public String getStrDocumentoIns() {
		return strDocumentoIns;
	}

	/**
	 * Sets str documento ins.
	 *
	 * @param strDocumentoIns the str documento ins
	 */
	public void setStrDocumentoIns(String strDocumentoIns) {
		this.strDocumentoIns = strDocumentoIns;
	}

	/**
	 * Gets str tarjeta virtual.
	 *
	 * @return the str tarjeta virtual
	 */
	public String getStrTarjetaVirtual() {
		return strTarjetaVirtual;
	}

	/**
	 * Sets str tarjeta virtual.
	 *
	 * @param strTarjetaVirtual the str tarjeta virtual
	 */
	public void setStrTarjetaVirtual(String strTarjetaVirtual) {
		this.strTarjetaVirtual = strTarjetaVirtual;
	}

	/**
	 * Gets str estado.
	 *
	 * @return the str estado
	 */
	public String getStrEstado() {
		return strEstado;
	}

	/**
	 * Sets str estado.
	 *
	 * @param strEstado the str estado
	 */
	public void setStrEstado(String strEstado) {
		this.strEstado = strEstado;
	}

	/**
	 * Gets str descripcion est.
	 *
	 * @return the str descripcion est
	 */
	public String getStrDescripcionEst() {
		return strDescripcionEst;
	}

	/**
	 * Sets str descripcion est.
	 *
	 * @param strDescripcionEst the str descripcion est
	 */
	public void setStrDescripcionEst(String strDescripcionEst) {
		this.strDescripcionEst = strDescripcionEst;
	}

	/**
	 * Gets str transaccion.
	 *
	 * @return the str transaccion
	 */
	public String getStrTransaccion() {
		return strTransaccion;
	}

	/**
	 * Sets str transaccion.
	 *
	 * @param strTransaccion the str transaccion
	 */
	public void setStrTransaccion(String strTransaccion) {
		this.strTransaccion = strTransaccion;
	}

}
