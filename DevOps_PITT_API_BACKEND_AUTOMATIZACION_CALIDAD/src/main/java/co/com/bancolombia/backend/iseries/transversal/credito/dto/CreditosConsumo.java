package co.com.bancolombia.backend.iseries.transversal.credito.dto;

/**
 * The type Creditos consumo.
 */
public class CreditosConsumo {

	private String numeroCreditoConsumo;
	private String planCreditoConsumo;

	/**
	 * Gets numero credito consumo.
	 *
	 * @return the numero credito consumo
	 */
	public String getNumeroCreditoConsumo() {
		return numeroCreditoConsumo;
	}

	/**
	 * Sets numero credito consumo.
	 *
	 * @param numeroCreditoConsumo the numero credito consumo
	 */
	public void setNumeroCreditoConsumo(String numeroCreditoConsumo) {
		this.numeroCreditoConsumo = numeroCreditoConsumo;
	}

	/**
	 * Gets plan credito consumo.
	 *
	 * @return the plan credito consumo
	 */
	public String getPlanCreditoConsumo() {
		return planCreditoConsumo;
	}

	/**
	 * Sets plan credito consumo.
	 *
	 * @param planCreditoConsumo the plan credito consumo
	 */
	public void setPlanCreditoConsumo(String planCreditoConsumo) {
		this.planCreditoConsumo = planCreditoConsumo;
	}


}
