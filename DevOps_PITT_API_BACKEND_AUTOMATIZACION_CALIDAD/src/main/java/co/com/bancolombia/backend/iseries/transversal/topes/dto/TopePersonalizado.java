package co.com.bancolombia.backend.iseries.transversal.topes.dto;

/**
 * Clase para encapsular los datos recibidos de los archivos
 * PCCLIBRAMD.PCCFFATPCL y PCCLIBRAMD.PCCFFPPCLI
 *
 * @author david.c.gonzalez
 */
public class TopePersonalizado {

	private double dblMontoAcumulado;
	private int intNumeroOperaciones;

	/**
	 * CONSTRUCTOR Crea el objeto de los topes banco.
	 *
	 * @param strNumeroOperaciones :            Define el numero operaciones, recibido de la columna (NROOPERA)
	 * @param strMontoAcumulado    :            Define el monto acumulado, recibido de la columna (MONTO)
	 */
	public TopePersonalizado(String strNumeroOperaciones, String strMontoAcumulado) {
		this.dblMontoAcumulado = Double.parseDouble(strMontoAcumulado);
		this.intNumeroOperaciones = Integer.parseInt(strNumeroOperaciones);
	}

	/**
	 * Instantiates a new Tope personalizado.
	 */
	public TopePersonalizado() {
		this.dblMontoAcumulado = 0;
		this.intNumeroOperaciones = 0;
	}

	/**
	 * Gets dbl monto acumulado.
	 *
	 * @return the dbl monto acumulado
	 */
	public double getDblMontoAcumulado() {
		return dblMontoAcumulado;
	}

	/**
	 * Sets dbl monto acumulado.
	 *
	 * @param dblMontoAcumulado the dbl monto acumulado
	 */
	public void setDblMontoAcumulado(double dblMontoAcumulado) {
		this.dblMontoAcumulado = dblMontoAcumulado;
	}

	/**
	 * Gets int numero operacion.
	 *
	 * @return the int numero operacion
	 */
	public int getIntNumeroOperacion() {
		return intNumeroOperaciones;
	}

	/**
	 * Sets int numero operacion.
	 *
	 * @param intNumeroOperacion the int numero operacion
	 */
	public void setIntNumeroOperacion(int intNumeroOperacion) {
		this.intNumeroOperaciones = intNumeroOperacion;
	}

}
