package co.com.bancolombia.backend.iseries.transversal.inscripcion.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.alertas.dao.BackAlerta;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Back cuenta inscrita otros bancos.
 */
public class BackCuentaInscritaOtrosBancos {
	private static final Logger LOGGER = LogManager.getLogger(BackAlerta.class.getName());

	/**
	 * Verifica si la cuenta ACH (Otros bancos)  esta inscrita para un cliente en particular
	 *
	 * @param strDocumentoInscribe : 			Recibe el docuemnto del cliente que inscribe para hacer la consulta.
	 * @param strCodBancoDestino   :			Recibe el codigo del banco de la cuenta inscrita para hacer la consulta.
	 * @param strCuentaDestino     :			Recibe la cuenta inscrita por el cliente para hacer la consulta.
	 *
	 * @return boolean : Si el cliente no tiene la cuenta inscrita se devuelve false
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean estaInscritaCuentaAchSTFFFCTAS(String strDocumentoInscribe, String strCodBancoDestino, String strCuentaDestino) throws SQLException {
		boolean blnresultadoFinal = false;
		String strDocFormat = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumentoInscribe));
		String strCuentaDestinoFormat = String.format("%s%s%s", ConstantManager.PORCENTAJE, strCuentaDestino, ConstantManager.PORCENTAJE);
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.STFFFCTAS.estaInscritaCuentaAch");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strDocFormat);
			consulta.setString(2, strCuentaDestinoFormat);
			consulta.setString(3, strCodBancoDestino);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strEstado = objResult.getString("ESTADO").trim();
					blnresultadoFinal = ConstantManager.LETRA_A.equalsIgnoreCase(strEstado);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return blnresultadoFinal;
	}

	/**
	 * Verifica si la cuenta NEQUI esta inscrita para un cliente en particular
	 *
	 * @param strDocumentoInscribe     : 			Recibe el numero de docuemnto del cliente que inscribe para hacer la consulta.
	 * @param strTipoDocumentoInscribe : 			Recibe el tipo de docuemnto del cliente que inscribe para hacer la consulta.
	 * @param strNumeroCuenta          : 			Recibe la cuenta inscrita por el cliente para hacer la consulta.
	 * @param strCodigoBanco           : 			Recibe el codigo del banco de la cuenta inscrita para hacer la consulta.
	 *
	 * @return boolean : Si el cliente no tiene la cuenta inscrita se devuelve false
	 *
	 * @throws SQLException the sql exception
	 * @author david.c.gonzalez
	 */
	public boolean estaInscritaCuentaNequi(String strDocumentoInscribe, String strTipoDocumentoInscribe, String strNumeroCuenta, String strCodigoBanco) throws SQLException {
		boolean result = false;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.MATFFOTBAN.estaInscritaCuentaNequi");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strNumeroCuenta);
			consulta.setString(2, strDocumentoInscribe);
			consulta.setString(3, strTipoDocumentoInscribe);
			consulta.setString(4, strCodigoBanco);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strEstadoCuenta = objResult.getString("CIESTADO").trim();
					result = ConstantManager.LETRA_A.equalsIgnoreCase(strEstadoCuenta);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}
}
