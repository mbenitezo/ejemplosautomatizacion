package co.com.bancolombia.backend.iseries.personas.usuario.dao;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Back usuario app.
 */
public class BackUsuarioApp {
	private static final Logger LOGGER = LogManager.getLogger(BackUsuarioApp.class.getName());

	/**
	 * Módulo que consulta el archivo WWWFFCOAPP en el cual se encuentra
	 * el listado de los clientes registrados en la App, en él se visualizan
	 * número de documento, tipo documento, tipo celular, sistema operativo,
	 * fecha y hora del ingreso.
	 *
	 * @param strDocumento : Recibe el documento del cliente para hacer la consulta.
	 * @param strTipoDoc   :   Recibe el tipo documento del cliente para hacer la consulta.
	 *
	 * @return blnresultadoFinal boolean
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean consultarClienteApp(String strDocumento, String strTipoDoc) throws SQLException {
		String documentoCliente = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumento));
		boolean blnResultadoFinal = false;
		Connection conexion = null;
		ResultSet objResult = null;
		try {
			conexion = IseriesConnectionManager.getConnection();
			String sql = QueryManager.ISERIES.getString("SQL.PCCFFMOVTF.consultarIdSistema");
			try (PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, documentoCliente);
				consulta.setString(2, strTipoDoc);
				objResult = consulta.executeQuery();
			}
			if (objResult.isBeforeFirst()) {
				LOGGER.info("Consulta exitosa, Cliente existente en Bancolombia App");
				blnResultadoFinal = true;
			} else {
				LOGGER.info("Error, Cliente No existe en Bancolombia App");
			}
			return blnResultadoFinal;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
			if (conexion != null) {
				conexion.close();
			}
		}
	}

	/**
	 * Verificar cliente app boolean.
	 *
	 * @param strCuenta     the str cuenta
	 * @param strTipoCuenta the str tipo cuenta
	 *
	 * @return the boolean
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean verificarClienteApp(String strCuenta, String strTipoCuenta) throws SQLException {
		boolean blnResultado = false;
		Connection conexion = null;
		ResultSet objResult = null;
		try {
			conexion = IseriesConnectionManager.getConnection();
			String sql = QueryManager.ISERIES.getString("SQL.CNNOSS.vertificarClienteApp");
			try (PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, strCuenta);
				consulta.setString(2, strTipoCuenta);

				objResult = consulta.executeQuery();
			}

			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String documentoAPP = objResult.getString("DOCUMENTO").trim();
					String tipodocumentoAPP = objResult.getString("TIPODOC").trim();
					blnResultado = consultarClienteApp(documentoAPP, tipodocumentoAPP);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
			return blnResultado;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
			if (conexion != null) {
				conexion.close();
			}
		}
	}
}
	
