package co.com.bancolombia.backend.iseries.transversal.movimientos.dto;

/**
 * The type Valor equivalencia movimiento.
 */
public class ValorEquivalenciaMovimiento {

	private String strMovCodTrn;
	private String strMovDescrip;

	/**
	 * Instantiates a new Valor equivalencia movimiento.
	 *
	 * @param strMovCodTrn  the str mov cod trn
	 * @param strMovDescrip the str mov descrip
	 */
	public ValorEquivalenciaMovimiento(String strMovCodTrn, String strMovDescrip) {
		this.strMovCodTrn = strMovCodTrn;
		this.strMovDescrip = strMovDescrip;
	}

	/**
	 * Gets str mov cod trn.
	 *
	 * @return the str mov cod trn
	 */
	public String getStrMovCodTrn() {
		return strMovCodTrn;
	}

	/**
	 * Sets str mov cod trn.
	 *
	 * @param strMovCodTrn the str mov cod trn
	 */
	public void setStrMovCodTrn(String strMovCodTrn) {
		this.strMovCodTrn = strMovCodTrn;
	}

	/**
	 * Gets str mov descrip.
	 *
	 * @return the str mov descrip
	 */
	public String getStrMovDescrip() {
		return strMovDescrip;
	}

	/**
	 * Sets str mov descrip.
	 *
	 * @param strMovDescrip the str mov descrip
	 */
	public void setStrMovDescrip(String strMovDescrip) {
		this.strMovDescrip = strMovDescrip;
	}

}
