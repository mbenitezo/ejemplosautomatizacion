package co.com.bancolombia.backend.iseries.transversal.movimientos.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Movimiento.
 */
public class Movimiento {

	private String strDescripcionTrn;
	private String strvIndReverso;

	// Variables de recaudos
	private String strvCodOficina;
	private String strvValorCanje;
	private String strvConvenio;
	private String strvTipoTrans;

	/**
	 * Instantiates a new Movimiento.
	 *
	 * @param strDescripcionTrn the str descripcion trn
	 * @param strvIndReverso    the strv ind reverso
	 */
	public Movimiento(String strDescripcionTrn, String strvIndReverso) {
		this.strDescripcionTrn = strDescripcionTrn;
		this.strvIndReverso = strvIndReverso;
	}

	/**
	 * Instantiates a new Movimiento.
	 *
	 * @param strDescripcionTrn the str descripcion trn
	 * @param strvIndReverso    the strv ind reverso
	 * @param strvCodOficina    the strv cod oficina
	 * @param strvValorCanje    the strv valor canje
	 * @param strvConvenio      the strv convenio
	 * @param strvTipoTrans     the strv tipo trans
	 */
	public Movimiento(String strDescripcionTrn, String strvIndReverso, String strvCodOficina,
	                  String strvValorCanje, String strvConvenio, String strvTipoTrans) {

		this.strDescripcionTrn = strDescripcionTrn;
		this.strvIndReverso = strvIndReverso;
		this.strvCodOficina = strvCodOficina;
		this.strvValorCanje = strvValorCanje;
		this.strvConvenio = strvConvenio;
		this.strvTipoTrans = strvTipoTrans;
	}

	/**
	 * Instantiates a new Movimiento.
	 */
	public Movimiento() {

		strDescripcionTrn = ConstantManager.VACIO;
		strvIndReverso = ConstantManager.VACIO;
		strvCodOficina = ConstantManager.VACIO;
		strvValorCanje = ConstantManager.VACIO;
		strvConvenio = ConstantManager.VACIO;
		strvTipoTrans = ConstantManager.VACIO;
	}


	/**
	 * Gets str descripcion trn.
	 *
	 * @return the str descripcion trn
	 */
	public String getStrDescripcionTrn() {
		return strDescripcionTrn;
	}

	/**
	 * Sets str descripcion trn.
	 *
	 * @param strDesTrn the str des trn
	 */
	public void setStrDescripcionTrn(String strDesTrn) {
		this.strDescripcionTrn = strDesTrn;
	}

	/**
	 * Gets strv ind reverso.
	 *
	 * @return the strv ind reverso
	 */
	public String getStrvIndReverso() {
		return strvIndReverso;
	}

	/**
	 * Sets strv ind reverso.
	 *
	 * @param strvIndReverso the strv ind reverso
	 */
	public void setStrvIndReverso(String strvIndReverso) {
		this.strvIndReverso = strvIndReverso;
	}

	/**
	 * Gets strv cod oficina.
	 *
	 * @return the strv cod oficina
	 */
	public String getStrvCodOficina() {
		return strvCodOficina;
	}

	/**
	 * Sets strv cod oficina.
	 *
	 * @param strvCodOficina the strv cod oficina
	 */
	public void setStrvCodOficina(String strvCodOficina) {
		this.strvCodOficina = strvCodOficina;
	}

	/**
	 * Gets strv valor canje.
	 *
	 * @return the strv valor canje
	 */
	public String getStrvValorCanje() {
		return strvValorCanje;
	}

	/**
	 * Sets strv valor canje.
	 *
	 * @param strvValorCanje the strv valor canje
	 */
	public void setStrvValorCanje(String strvValorCanje) {
		this.strvValorCanje = strvValorCanje;
	}

	/**
	 * Gets strv convenio.
	 *
	 * @return the strv convenio
	 */
	public String getStrvConvenio() {
		return strvConvenio;
	}

	/**
	 * Sets strv convenio.
	 *
	 * @param strvConvenio the strv convenio
	 */
	public void setStrvConvenio(String strvConvenio) {
		this.strvConvenio = strvConvenio;
	}

	/**
	 * Gets strv tipo trans.
	 *
	 * @return the strv tipo trans
	 */
	public String getStrvTipoTrans() {
		return strvTipoTrans;
	}

	/**
	 * Sets strv tipo trans.
	 *
	 * @param strvTipoTrans the strv tipo trans
	 */
	public void setStrvTipoTrans(String strvTipoTrans) {
		this.strvTipoTrans = strvTipoTrans;
	}
}
