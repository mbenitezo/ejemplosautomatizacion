package co.com.bancolombia.backend.iseries.transversal.log.trace;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.log.canal.dao.BackLogCanal;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BackTrace {

	private static final Logger LOGGER = LogManager.getLogger(BackLogCanal.class.getName());
	private static final String CAMPO = "SECTRANS";

	/**
	 * Consulta si existe el trace COMLIBRAMD.COMFFLGWWW, Si el trace no existe
	 * devuelve vacio
	 *
	 * @param strDoc:              Recibe el docuemnto del cliente para hacer la consulta.
	 * @param strCodigoTrasaccion: Recibe el codigo de transaccion para hacer la consulta.
	 * @param strHora:             Recibe la hora antes de realizar la transaccion para hacer la
	 *                             consulta. FORMATO hhmmss
	 *
	 * @return Si el trace se genero devuelve un string con el trace, de lo
	 * contrario devuelve vacio
	 *
	 * @throws SQLException
	 */
	public String consultarTrace(String strDoc, String strCodigoTrasaccion, String strHora) throws SQLException {
		String documentoFormat = String.format(ConstantManager.FORMATO_LIKE, ConstantManager.PORCENTAJE, strDoc, ConstantManager.PORCENTAJE);
		String strMes = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_MES);
		String strDia = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_DIA);
		String result = "";
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.COMFFLGWWW.consultarTrace");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {

			consulta.setString(1, strMes);
			consulta.setString(2, strDia);
			consulta.setString(3, documentoFormat);
			consulta.setString(4, strCodigoTrasaccion);
			consulta.setString(5, strHora);

			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					result = objResult.getString("clvtrn");
					int intPosFinal = result.length();
					int intPosInicial = intPosFinal - 12;
					result = result.substring(intPosInicial, intPosFinal);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}

	/**
	 * Método que se encarga de consultar la secuencia de la transacción del log STIFFLGSAG
	 * después de hacer una transacción de autenticación.
	 *
	 * @param strDoc:  Define el documento del cliente como criterio de búsqueda.
	 * @param strHora: Define la hora antes de realizar la transacción. FORMATO hhmmsscc
	 *
	 * @return
	 *
	 * @author Sergio Alejandro Alvarez
	 */

	public String consultaTraceSAG(String strDoc, String strHora) throws SQLException {
		String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		String result = ConstantManager.VACIO;
		ResultSet objResult = null;
		String sql = QueryManager.CLOUDANT.getString("SQL.STIFFLGSAG");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strFecha);
			consulta.setString(2, strHora);
			consulta.setString(3, String.format(ConstantManager.FORMATO_LIKE, ConstantManager.PORCENTAJE, strDoc, ConstantManager.PORCENTAJE));
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				if (objResult.next()) {
					result = objResult.getString(CAMPO);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST);
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}

	/**
	 * Método que se encarga de consultar la secuencia de la transacción del log STIFFLGPIN
	 * después de hacer una transacción de vinculación.
	 *
	 * @param strDoc:  Define el documento del cliente como criterio de búsqueda.
	 * @param strHora: Define la hora antes de realizar la transacción. FORMATO hhmmsscc
	 *
	 * @return
	 *
	 * @author Sergio Alejandro Alvarez
	 */
	public String consultaTracePIN(String strDoc, String strHora) throws SQLException {
		String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		String result = ConstantManager.VACIO;
		ResultSet objResult = null;
		String sql = QueryManager.CLOUDANT.getString("SQL.STIFFLGPIN");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strFecha);
			consulta.setString(2, strHora);
			consulta.setString(3, String.format(ConstantManager.FORMATO_LIKE, ConstantManager.PORCENTAJE, strDoc, ConstantManager.PORCENTAJE));
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				if (objResult.next()) {
					result = objResult.getString(CAMPO);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST);
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}

	/**
	 * Método que se encarga de consultar la secuencia de la transacción del log STIFFLGCGP
	 * después de hacer una transacción de consulta de movimientos.
	 *
	 * @param strDoc:  Define el documento del cliente como criterio de búsqueda.
	 * @param strHora: Define la hora antes de realizar la transacción. FORMATO hhmmsscc
	 *
	 * @return
	 *
	 * @author Sergio Alejandro Alvarez
	 */
	public String consultaTraceCGP(String strDoc, String strHora) throws SQLException {
		String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		String result = ConstantManager.VACIO;
		ResultSet objResult = null;
		String sql = QueryManager.CLOUDANT.getString("SQL.STIFFLGCGP");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {

			consulta.setString(1, strFecha);
			consulta.setString(2, strHora);
			consulta.setString(3, String.format(ConstantManager.FORMATO_LIKE, ConstantManager.PORCENTAJE, strDoc, ConstantManager.PORCENTAJE));
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				if (objResult.next()) {
					result = objResult.getString(CAMPO);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST);
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}

	/**
	 * Método que se encarga de consultar la secuencia de la transacción del log STIFFLGDEP
	 * después de hacer una transacción de consulta de saldos.
	 *
	 * @param strDoc:  Define el documento del cliente como criterio de búsqueda.
	 * @param strHora: Define la hora antes de realizar la transacción. FORMATO hhmmsscc
	 *
	 * @return
	 *
	 * @author Sergio Alejandro Alvarez
	 */
	public String consultaTraceDEP(String strDoc, String strHora) throws SQLException {
		String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		String result = ConstantManager.VACIO;
		ResultSet objResult = null;
		String sql = QueryManager.CLOUDANT.getString("SQL.STIFFLGDEP");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareCall(sql)) {
			consulta.setString(1, strFecha);
			consulta.setString(2, strHora);
			consulta.setString(3, String.format(ConstantManager.FORMATO_LIKE, ConstantManager.PORCENTAJE, strDoc, ConstantManager.PORCENTAJE));
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				if (objResult.next()) {
					result = objResult.getString(CAMPO);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST);
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}

	/**
	 * Método que se encarga de consultar la secuencia de la transacción del log STIFFLGMGP
	 * después de hacer una transacción de transferencia.
	 *
	 * @param strDoc:  Define el documento del cliente como criterio de búsqueda.
	 * @param strHora: Define la hora antes de realizar la transacción. FORMATO hhmmsscc
	 *
	 * @return
	 *
	 * @author Sergio Alejandro Alvarez
	 */
	public String consultaTraceMGP(String strDoc, String strHora) throws SQLException {
		String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		String result = ConstantManager.VACIO;
		ResultSet objResult = null;
		String sql = QueryManager.CLOUDANT.getString("SQL.STIFFLGMGP");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strFecha);
			consulta.setString(2, strHora);
			consulta.setString(3, String.format(ConstantManager.FORMATO_LIKE, ConstantManager.PORCENTAJE, strDoc, ConstantManager.PORCENTAJE));
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				if (objResult.next()) {
					result = objResult.getString(CAMPO);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST);
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}
}
