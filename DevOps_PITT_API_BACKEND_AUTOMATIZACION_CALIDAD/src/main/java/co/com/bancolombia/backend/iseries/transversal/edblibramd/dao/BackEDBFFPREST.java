package co.com.bancolombia.backend.iseries.transversal.edblibramd.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.edblibramd.dto.DatosCuenta;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Back edbffprest.
 */
public class BackEDBFFPREST {
	private static final Logger LOGGER = LogManager.getLogger(BackEDBFFPREST.class.getName());

	/**
	 * <b>Descripción:</b> Método que permite obtener del archivo EDBLIBRAMD.EDBFFPREST, datos de inversión
	 * virtual que tiene un cliente tales como Número de cuenta y Saldo; utilizando como datos de entrada el
	 * documento del cliente y el trace de la transacción. El resultado se almacena en un objeto tipo lista.
	 *
	 * @param strDocumento:</b> ésta variable almacena el documento de identificación del cliente
	 * @param strTrace:</b>     ésta variable almacena el código único de identificación de la transacción
	 *
	 * @return lista de créditos del cliente consultado en el backend
	 */

	public List<DatosCuenta> consultaEDBFFPREST(String strDocumento, String strTrace) throws SQLException {
		List<DatosCuenta> listaPrestamos = new ArrayList<>();
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.EDBFFPREST.consulta");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strDocumento);
			consulta.setString(2, strTrace);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strNroCuenta = objResult.getString("PRESTNUMPA").trim();
					String strSaldoDisp = objResult.getString("PRESTSALDO").trim();
					String strTipoCuenta = objResult.getString("PRESTDESCR").trim();

					DatosCuenta datosCuenta = new DatosCuenta(strNroCuenta, strTipoCuenta, strSaldoDisp);
					listaPrestamos.add(datosCuenta);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return listaPrestamos;
	}

	public Boolean validarResultado(List<DatosCuenta> listaObtenida, List<DatosCuenta> listaEsperada, String strCanal) {
		boolean resultado = false;
		for (DatosCuenta cuentaEsperada : listaEsperada) {
			String strTipoEsperado = cuentaEsperada.getStrTipoCuenta();
			String strNroCuentaEsperado = cuentaEsperada.getStrNroCuenta();
			String strSaldoEsperado = cuentaEsperada.getStrSaldoDisp();
			for (DatosCuenta cuentaObtenida : listaObtenida) {
				resultado = false;
				String strTipoObtenido = cuentaObtenida.getStrTipoCuenta();
				String strNroCuentaObtenido = cuentaObtenida.getStrNroCuenta();
				String strSaldoObtenido = cuentaObtenida.getStrSaldoDisp();
				if (ConstantManager.APP.equals(strCanal) || ConstantManager.SVP.equals(strCanal)) {
					strTipoEsperado = strTipoEsperado.toUpperCase();
					strSaldoEsperado = strSaldoEsperado.replace(",", "");
					strSaldoEsperado = strSaldoEsperado.replace(".", ",");
					if (ConstantManager.APP.equals(strCanal)) {
						strNroCuentaObtenido = strNroCuentaObtenido.substring(strNroCuentaObtenido.length() - 4, strNroCuentaObtenido.length());
					}
					if (strNroCuentaEsperado.equals(strNroCuentaObtenido) && strSaldoEsperado.equals(strSaldoObtenido)) {
						resultado = true;
						LOGGER.info(ExceptionCodesManager.SAME_PRODUCT);
						break;
					}
				}
				if (ConstantManager.SVE.equals(strCanal)) {
					resultado = true;
				}
			}
			if (!resultado) {
				LOGGER.info(ExceptionCodesManager.VALUE_NOTFOUND);
				break;
			}
		}
		return resultado;
	}
}
