package co.com.bancolombia.backend.iseries.transversal.matff.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Clave principal.
 */
public class ClavePrincipal {
	private String strNroIntentos;
	private String strEstadoClv;
	private String strFechaTrn;
	private String strCanalBloqueo;
	private String strDescripcionEstClv;

	/**
	 * Instantiates a new Clave principal.
	 *
	 * @param strNroIntentos       the str nro intentos
	 * @param strEstadoClv         the str estado clv
	 * @param strFechaTrn          the str fecha trn
	 * @param strCanalBloqueo      the str canal bloqueo
	 * @param strDescripcionEstClv the str descripcion est clv
	 */
	public ClavePrincipal(String strNroIntentos, String strEstadoClv, String strFechaTrn, String strCanalBloqueo, String strDescripcionEstClv) {

		this.strNroIntentos = strNroIntentos;
		this.strEstadoClv = strEstadoClv;
		this.strFechaTrn = strFechaTrn;
		this.strCanalBloqueo = strCanalBloqueo;
		this.strDescripcionEstClv = strDescripcionEstClv;
	}

	/**
	 * Instantiates a new Clave principal.
	 */
	public ClavePrincipal() {

		strNroIntentos = ConstantManager.VACIO;
		strEstadoClv = ConstantManager.VACIO;
		strFechaTrn = ConstantManager.VACIO;
		strCanalBloqueo = ConstantManager.VACIO;
		strDescripcionEstClv = ConstantManager.VACIO;
	}

	/**
	 * Gets str nro intentos.
	 *
	 * @return the str nro intentos
	 */
	public String getStrNroIntentos() {
		return strNroIntentos;
	}

	/**
	 * Sets str nro intentos.
	 *
	 * @param strNroIntentos the str nro intentos
	 */
	public void setStrNroIntentos(String strNroIntentos) {
		this.strNroIntentos = strNroIntentos;
	}

	/**
	 * Gets str estado clv.
	 *
	 * @return the str estado clv
	 */
	public String getStrEstadoClv() {
		return strEstadoClv;
	}

	/**
	 * Sets str estado clv.
	 *
	 * @param strEstadoClv the str estado clv
	 */
	public void setStrEstadoClv(String strEstadoClv) {
		this.strEstadoClv = strEstadoClv;
	}

	/**
	 * Gets str fecha trn.
	 *
	 * @return the str fecha trn
	 */
	public String getStrFechaTrn() {
		return strFechaTrn;
	}

	/**
	 * Sets str fecha trn.
	 *
	 * @param strFechaTrn the str fecha trn
	 */
	public void setStrFechaTrn(String strFechaTrn) {
		this.strFechaTrn = strFechaTrn;
	}

	/**
	 * Gets str canal bloqueo.
	 *
	 * @return the str canal bloqueo
	 */
	public String getStrCanalBloqueo() {
		return strCanalBloqueo;
	}

	/**
	 * Sets str canal bloqueo.
	 *
	 * @param strCanalBloqueo the str canal bloqueo
	 */
	public void setStrCanalBloqueo(String strCanalBloqueo) {
		this.strCanalBloqueo = strCanalBloqueo;
	}

	/**
	 * Gets str descripcion est clv.
	 *
	 * @return the str descripcion est clv
	 */
	public String getStrDescripcionEstClv() {
		return strDescripcionEstClv;
	}

	/**
	 * Sets str descripcion est clv.
	 *
	 * @param strDescripcionEstClv the str descripcion est clv
	 */
	public void setStrDescripcionEstClv(String strDescripcionEstClv) {
		this.strDescripcionEstClv = strDescripcionEstClv;
	}
}
