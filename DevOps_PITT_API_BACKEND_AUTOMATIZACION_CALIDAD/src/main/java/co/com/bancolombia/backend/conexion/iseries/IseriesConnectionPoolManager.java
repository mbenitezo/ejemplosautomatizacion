package co.com.bancolombia.backend.conexion.iseries;

import co.com.bancolombia.backend.excepciones.BackEndExceptions;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.PropertiesManager;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

/**
 * The type Iseries pool connection manager.
 *
 * @author Oscar Armando Vallejo Tovar
 */
public class IseriesConnectionPoolManager {

	private static final Logger LOGGER = LogManager.getLogger(IseriesConnectionPoolManager.class.getName());
	private static PoolingDataSource poolingDataSource = null;
	private static GenericObjectPool connectionPool = null;

	private IseriesConnectionPoolManager() {

	}

	/**
	 * Este m&eacute;todo se encarga de iniciar la conexi&oacute; con la base de datos
	 *
	 * @author Oscar Armando Vallejo Tovar
	 */
	private static void initConnection() {
		Properties dbProp = null;
		try {
			dbProp = PropertiesManager.getDbProperties();
			Class.forName(dbProp.getProperty("DRIVER"));
		} catch (IOException e) {
			throw new BackEndExceptions(ExceptionCodesManager.BDPROP_NOTFOUND.getMsg(), e);
		} catch (ClassNotFoundException e) {
			throw new BackEndExceptions(ExceptionCodesManager.CONNECTION_FAILED.getMsg(), e);
		}

		String urlHost = String.format("%s:%s", dbProp.getProperty("JDBC"), dbProp.getProperty("HOST"));
		String usuario = dbProp.getProperty("USER");
		String clave = dbProp.getProperty("PASS");
		int conexionesActivas = Integer.parseInt(dbProp.getProperty("QUANTITY"));

		// Crea una instancia de GenericObjectPool que contiene nuestro conjunto de conexiones
		connectionPool = new GenericObjectPool();
		connectionPool.setMaxActive(conexionesActivas);

		// Crea un objeto ConnectionFactory que será utilizado por el pool para crear el objeto de conexión
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(urlHost, usuario, clave);

		// Crea un PoolableConnectionFactory que envolverá el objeto de conexión
		// Creado por ConnectionFactory para agregar funcionalidad de agrupación de objetos!
		new PoolableConnectionFactory(connectionFactory, connectionPool, null, null, false, true);

		poolingDataSource = new PoolingDataSource(connectionPool);
	}

	/**
	 * El m&eacute;todo getDataSource se encarga de
	 *
	 * @return data source
	 *
	 * @author Oscar Armando Vallejo Tovar
	 */
	protected static DataSource getDataSource() {
		if (null == poolingDataSource) {
			initConnection();
		}
		return poolingDataSource;
	}

	/**
	 * El m&eacute;todo connectionStatus se encarga de
	 *
	 * @author Oscar Armando Vallejo Tovar
	 */
	protected static void connectionStatus() {
		LOGGER.debug(String.format("Max.: %s Active: %d Idle: %d", connectionPool.getMaxActive(),
				connectionPool.getNumActive(), connectionPool.getNumIdle()));

	}

}
