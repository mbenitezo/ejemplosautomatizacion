package co.com.bancolombia.backend.iseries.transversal.credito.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.alertas.dao.BackAlerta;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Back creditos consumo.
 */
public class BackCreditosConsumo {

	private static final Logger LOGGER = LogManager.getLogger(BackAlerta.class.getName());

	/**
	 * Consulta si existe el credito de consumo VISIONR.LMBAL, Si el credito de
	 * consumo existe devuelve el estado del credito.
	 *
	 * @param strNumeroCreditoConsumo : Recibe el numero de credito de consumo para hacer la consulta.
	 * @param strPlan                 : Recibe el plan para hacer la consulta.
	 *
	 * @return boolean : Si el credito de consumo existe y devuelve el estado de lo contrario devuelve vacio.
	 *
	 * @throws SQLException the sql exception
	 */
	public String consultarEstadoCreditoConsumo(String strNumeroCreditoConsumo, String strPlan) throws SQLException {
		String strEstadoCredito = ConstantManager.VACIO;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.LMBAL.consultarEstadoCreditoConsumo");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strNumeroCreditoConsumo);
			consulta.setString(2, strPlan);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					strEstadoCredito = objResult.getString("LMCDST").trim();
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return strEstadoCredito;
	}

	/**
	 * Verifica si estado de credito de consumo es 1, 4 u 8.
	 *
	 * @param strEstadoCredito : 			Recibe el estado de credito de consumo en lentra.
	 *
	 * @return boolean : 			Si el credito de consumo existe y el estado es 1, 4 u 8 debuelve true.
	 */
	public boolean verificarEstadoCreditoConsumo(String strEstadoCredito) {
		if (StringUtils.isNotBlank(strEstadoCredito)) {
			return ConstantManager.UNO.equals(strEstadoCredito) || ConstantManager.CUATRO.equals(strEstadoCredito) || ConstantManager.OCHO.equals(strEstadoCredito);
		}
		return false;
	}

}
