package co.com.bancolombia.backend.utilidades.constant;

/**
 * The Class ConstantFondo.
 */
public class ConstantFondo {

	/** The Constant CODIGO_ADICION. */
	public static final String CODIGO_ADICION = "ADI";

	/** The Constant CODIGO_RETIRO. */
	public static final String CODIGO_RETIRO = "RET";

	/** The Constant CODIGO_CONSTITUCION. */
	public static final String CODIGO_CONSTITUCION = "CON";

	/** The Constant CODIGO_CANCELACION. */
	public static final String CODIGO_CANCELACION = "CAN";

	/** The Constant LIBRERIA_FIDUCUENTA. */
	public static final String LIBRERIA_FIDUCUENTA = "FIDLIBC1MD";

	/** The Constant LIBRERIA_FIDURENTA. */
	public static final String LIBRERIA_FIDURENTA = "FIDLIBC7MD";

}
