package co.com.bancolombia.backend.iseries.transversal.topes.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * Clase para encapsular los datos recibidos del archivo CABLIBRAMD.CABFFLTTRA
 *
 * @author david.c.gonzalez
 */
public class TopeBanco {

	private String strFechaTrn;
	private double dblValorAcumulado;

	/**
	 * CONSTRUCTOR. Crea un objeto de tipo tope banco para encapsular los datos de
	 * valor acumulado y fecha de la transaccion.
	 *
	 * @param strValorAcumulado :            Define el valor acumulado, recibido de la columna (LTACMDIA)
	 * @param strFechaTrn       :            Define la fecha de la transaccion, recibido de la columna            (FECHATR)
	 */
	public TopeBanco(String strValorAcumulado, String strFechaTrn) {
		this.strFechaTrn = strFechaTrn;
		this.dblValorAcumulado = Double.parseDouble(strValorAcumulado);
	}

	/**
	 * Instantiates a new Tope banco.
	 */
	public TopeBanco() {
		this.strFechaTrn = ConstantManager.VACIO;
		this.dblValorAcumulado = 0;
	}

	/**
	 * Gets dbl valor acumulado.
	 *
	 * @return the dbl valor acumulado
	 */
	public double getDblValorAcumulado() {
		return dblValorAcumulado;
	}

	/**
	 * Sets dbl valor acumulado.
	 *
	 * @param dblValorAcumulado the dbl valor acumulado
	 */
	public void setDblValorAcumulado(double dblValorAcumulado) {
		this.dblValorAcumulado = dblValorAcumulado;
	}

	/**
	 * Gets str fecha trn.
	 *
	 * @return the str fecha trn
	 */
	public String getStrFechaTrn() {
		return strFechaTrn;
	}

	/**
	 * Sets str fecha trn.
	 *
	 * @param strFechaTrn the str fecha trn
	 */
	public void setStrFechaTrn(String strFechaTrn) {
		this.strFechaTrn = strFechaTrn;
	}

}
