package co.com.bancolombia.backend.iseries.transversal.log.canal.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.log.canal.dto.LogCanal;
import co.com.bancolombia.backend.utilidades.ExcelManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantLogCanal;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantOrientacion;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que se encarga de manejar todos los componentes del log del canal se
 * tocan los archivos COMLIBRAMD.COMFFLGWWW
 *
 * @author david.c.gonzalez
 */
public class BackLogCanal {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(BackLogCanal.class.getName());


	/** The obj excel. */
	private ExcelManager objExcel;

	/** The obj canal. */
	private LogCanal objCanal;

	/** The str trama input obtenida. */
	private String strTramaInputObtenida;
	
	/** The str trama output obtenida. */
	private String strTramaOutputObtenida;
	
	/** The str ruta archivo. */
	private String strRutaArchivo;

	/**
	 * Instantiates a new back log canal.
	 */
	public BackLogCanal(LogCanal objCanal, String strRutaPlantilla, String strNombreEvidencia) {
		this.objCanal = objCanal;
		objExcel = new ExcelManager();
		this.strRutaArchivo = strRutaPlantilla;
		crearCopiaEvidencia(strNombreEvidencia);
	}

	/**
	 * * Este metodo encargado de validar alguna de las tramas de log canal de acuerdo a la
	 * hoja del archivo de excel que se quiera validar.
	 *
	 * @param strSheetname Define la ubicac&oacute;n la trama que se quiere validar.
	 * @param objCanal the obj canal
	 * @return Un TRUE si la validaci&oacute; fue exitosa, o FALSE si la validaci&oacute;n falló
	 * @throws SQLException the sql exception
	 */
	public boolean verificarLogCanal(String strSheetname) throws SQLException {
		
		llenarResultadoEsperado(strSheetname, traerResultadoEsperado(strSheetname));
		traerResultadoObtenido(objCanal.getStrTipoLogCanal());
		llenarResultadoObtenido(strSheetname);
		String strQueryExcel = QueryManager.EXCEL.getString("SQL.validarLogCanal");
		String strQueryExcelFormat = String.format(strQueryExcel, strSheetname);
		Recordset objRecord = objExcel.leerExcel(strQueryExcelFormat);
		boolean blnResult = true;
		boolean blnEsValido = true;
		int cont = 1;
		try {
			String strResult;
			while (blnEsValido) {
				objRecord.moveNext();
				String strResultE = objRecord.getField("Resultado_esperado").trim();
				String strResultO = objRecord.getField("Resultado_obtenido").trim();
				if (!strResultE.equalsIgnoreCase(ConstantLogCanal.IGNORE)) {
					strResult = ConstantLogCanal.PASO;
					if (!strResultE.equalsIgnoreCase(strResultO)) {
						strResult = ConstantOrientacion.ERROR;
						blnResult = false;
					}
				} else {
					strResult = ConstantLogCanal.NO_APLICA;
				}
				strQueryExcel = String.format(QueryManager.EXCEL.getString("SQL.UPDATE.validarLogCanal"), strSheetname,
						strResult, cont);

				objExcel.ModificarRegistrosExcel(strQueryExcel);
				cont++;
			}
		} catch (FilloException e) {
			blnEsValido = false;
		}
		return blnResult;
	}

	/**
	 * Metodo que se encarga de llenar el resultado esperado en la hoja de
	 * evidencias de acuerdo a la trama que se este validando.
	 *
	 * @param strNombreHoja Define la trama que se queira validar.
	 * @param arrVariable   Define un arreglo de caracteres, que contiene las variables
	 *                      esperadas.
	 */
	private void llenarResultadoEsperado(String strNombreHoja, List<String> arrVariable) {
		modificarRegistroEsperado(strNombreHoja, ConstantLogCanal.IGNORE, ConstantLogCanal.IGNORE);
		String strCurrentValue = ConstantManager.VACIO;
		for (String strVariableActual : arrVariable) {
			if (StringUtils.isNotEmpty(strVariableActual) && !ConstantLogCanal.IGNORE.equals(strVariableActual)) {
				if (strVariableActual.equalsIgnoreCase(ConstantManager.DOCUMENTO)) {
					strCurrentValue = objCanal.getStrDoc();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.TIPO_DOCUMENTO)) {
					strCurrentValue = objCanal.getStrTipoDoc();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.TRACE)) {
					strCurrentValue = objCanal.getStrTrace();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.CODIGOTRANSACCION)) {
					strCurrentValue = objCanal.getStrCodigoTransaccion();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.FECHA)) {
					strCurrentValue = objCanal.getStrFecha();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.CODIGOERROR)) {
					strCurrentValue = objCanal.getStrCodigoError();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.CUENTA_ORIGEN)) {
					strCurrentValue = objCanal.getStrProductoOrigen();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.TIP_CTAORIGEN)) {
					strCurrentValue = objCanal.getStrTipoProductoOrigen();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.TARJETACREDITO)
						|| strVariableActual.equalsIgnoreCase(ConstantManager.CUENTA_DESTINO)) {
					strCurrentValue = objCanal.getStrProductoDestino();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.TIP_CTADESTINO)) {
					strCurrentValue = objCanal.getStrTipoProductoDestino();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.VALOR_TRANSFER)) {
					strCurrentValue = objCanal.getStrValorTransferir();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.DOCUMENTOINSCRITO)) {
					strCurrentValue = objCanal.getStrDocumentoInscrito();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.TIPODOCUMENTOINS)) {
					strCurrentValue = objCanal.getStrTipoDocumentoInscrito();
				} else if (strVariableActual.equalsIgnoreCase(ConstantManager.CUENTAINSCRITA)) {
					strCurrentValue = objCanal.getStrCuentaInscrita();
				}
				modificarRegistroEsperado(strNombreHoja, strCurrentValue, strVariableActual);
			}
		}
	}

	/**
	 * Metodo que se encarga de llenar los valores obtenidos de la base de datos.
	 *
	 * @param strSheetName the str sheet name
	 */
	private void llenarResultadoObtenido(String strSheetName) {
		String strQueryExcel = String.format(QueryManager.EXCEL.getString("SQL.llenarResultadoObtenido"), strSheetName);
		Recordset objRecord = objExcel.leerExcel(strQueryExcel);
		boolean blnIsValid = true;
		int cont = 1;
		String strValor;
		try {
			while (blnIsValid) {
				objRecord.moveNext();
				int intIndexInit = Integer.parseInt(objRecord.getField("Pos_ini")) - 1;
				int intIndexFinal = Integer.parseInt(objRecord.getField("Pos_fin"));
				if (strSheetName.equalsIgnoreCase(ConstantLogCanal.TRAMA_INPUT)) {
					strValor = strTramaInputObtenida.substring(intIndexInit, intIndexFinal);
				} else {
					strValor = strTramaOutputObtenida.substring(intIndexInit, intIndexFinal);
				}
				strQueryExcel = String.format(QueryManager.EXCEL.getString("SQL.UPDATE.llenarResultadoObtenido"), strSheetName, strValor, cont);
				objExcel.ModificarRegistrosExcel(strQueryExcel);
				cont++;
			}
		} catch (FilloException e) {
			blnIsValid = false;
		}
	}

	/**
	 * Metodo que se encarga de trar los resultados de ambas tramas de la base de
	 * datos.
	 *
	 * @param archivo the archivo
	 * @throws SQLException the SQL exception
	 */
	private void traerResultadoObtenido(String archivo) throws SQLException {
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.COMFFLG.traerResultadoObtenido");
		String sqlFormat = String.format(sql, archivo);

		try (Connection conexion = IseriesConnectionManager.getConnection();
				PreparedStatement consulta = conexion.prepareStatement(sqlFormat)) {			
			String traceFormat = String.format(ConstantManager.FORMATO_LIKE, ConstantManager.PORCENTAJE, objCanal.getStrTrace(), ConstantManager.PORCENTAJE);
			consulta.setString(1, traceFormat);
			objResult = consulta.executeQuery();
			int cont = 1;
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					if (cont == 1) {
						strTramaInputObtenida = objResult.getString("DATOS");
					} else {
						strTramaOutputObtenida = objResult.getString("DATOS");
					}
					cont++;
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}

		} finally {
			if (objResult != null) {
				objResult.close();
			}

		}

	}

	/**
	 * Metodo que se encarga de trar el sultado esperado de la hoja de excel de
	 * acuerdo a la hoja.
	 *
	 * @param strSheetName the str sheet name
	 * @return the list
	 */
	private List<String> traerResultadoEsperado(String strSheetName) {
		List<String> arrVaribale = new ArrayList<>();
		String strQuery = String.format(QueryManager.EXCEL.getString("SQL.traerResultadoEsperado"), strSheetName);
		Recordset objRecord = objExcel.leerExcel(strQuery);
		boolean blnIsValid = true;
		try {
			while (blnIsValid) {
				objRecord.moveNext();
				String strCurrentVariable = objRecord.getField("Variable").trim();
				arrVaribale.add(strCurrentVariable);
			}
		} catch (FilloException e) {
			blnIsValid = false;
		}
		return arrVaribale;
	}

	/**
	 * Metodo que se encarga de crear la evidencia de acuerdo al nombre dado.
	 * internamente se concatena el caso de prueba que se este ejecutando.
	 *
	 * @param strNombreArchivo the str nombre archivo
	 */
	private void crearCopiaEvidencia(String strNombreArchivo) {
		String strNameFileCopy = String.format(ConstantManager.FORMATO_COPIA, objCanal.getStrCasoId(), strNombreArchivo);
		String strFilePathEvidence = objExcel.crearCopiaExcel(strRutaArchivo, objCanal.getStrRutaEvidencia(), strNameFileCopy);
		objExcel.strRutaArchivo(strFilePathEvidence);
	}

	/**
	 * Metodo que se encarga de modificar un registro en el archivo de excel.
	 *
	 * @param sheetName the sheet name
	 * @param strValue the str value
	 * @param strCondition the str condition
	 */
	private void modificarRegistroEsperado(String sheetName, String strValue, String strCondition) {
		String strQuery = String.format(QueryManager.EXCEL.getString("SQL.modificarRegistroEsperado"), sheetName, strValue, strCondition);
		objExcel.ModificarRegistrosExcel(strQuery);
	}
}
