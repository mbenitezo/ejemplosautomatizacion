package co.com.bancolombia.backend.iseries.personas.clavedinamica.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.log.canal.dao.BackLogCanal;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The Class BackClaveDinamica.
 */
public class BackClaveDinamica {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(BackLogCanal.class.getName());

	/**
	 * Consultar clave dinamica PCCLIBRAMD.PCCFFLGAEN si la clave dinamica no se
	 * genero devuelve vacio
	 *
	 * @param strDoc  Recibe el docuemnto del cliente para hacer la consulta.
	 * @param strHora Recibe hora antes de la transaccion para hacer la consulta. FORMATO hhmmss
	 *
	 * @return the string
	 *
	 * @throws SQLException the SQL exception
	 */
	public String consultarClaveDinamica(String strDoc, String strHora) throws SQLException {
		String result = "";
		String strFecha = DateManager.obtenerFechaSistema("yyyyMMdd");
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.PCCFFLGAEN.consultaClaveDinamica");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {

			consulta.setString(1, strDoc);
			consulta.setString(2, strFecha);
			consulta.setString(3, strHora);

			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				if (objResult.next()) {
					result = objResult.getString("AENMSGENV");
					result = result.substring(41, 47);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}
}
