package co.com.bancolombia.backend.utilidades.constant;

import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;

public class ConstantOrientacion {

	/**
	 * Instantiates a new constant log canal.
	 */
	private ConstantOrientacion() {
		throw new IllegalStateException(ExceptionCodesManager.UTILITY_CLASS.getMsg());
	}

	/**
	 * The Constant ERROR.
	 */
	public static final String ERROR = "ERROR";

	/**
	 * The Constant ACIERTO.
	 */
	public static final String ACIERTO = "ACIERTO";

}
