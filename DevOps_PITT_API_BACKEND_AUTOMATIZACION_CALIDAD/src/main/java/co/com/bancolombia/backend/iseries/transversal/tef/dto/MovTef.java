package co.com.bancolombia.backend.iseries.transversal.tef.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Mov tef.
 */
public class MovTef {

	private String strSistemaOrigen;
	private String strIdSistema;

	private String strAno;
	private String strMes;
	private String strDia;

	private String strOfiCuentaDebitar;
	private String strCuentaDebitar;
	private String strSaldoInicioTRX;
	private String strTipCuentaDebitar;

	private String strOficinaCreditar;
	private String strCuentaCreditar;
	private String strTipoCuentaCreditar;

	private String strValorPago;

	private String strIndContable;
	private String strValorComision;
	private String strIndicadorComision;
	private String strEstado;

	/**
	 * Gets str sistema origen.
	 *
	 * @return the str sistema origen
	 */
	public String getStrSistemaOrigen() {
		return strSistemaOrigen;
	}

	/**
	 * Sets str sistema origen.
	 *
	 * @param strSistemaOrigen the str sistema origen
	 */
	public void setStrSistemaOrigen(String strSistemaOrigen) {
		this.strSistemaOrigen = strSistemaOrigen;
	}

	/**
	 * Gets str id sistema.
	 *
	 * @return the str id sistema
	 */
	public String getStrIdSistema() {
		return strIdSistema;
	}

	/**
	 * Sets str id sistema.
	 *
	 * @param strIdSistema the str id sistema
	 */
	public void setStrIdSistema(String strIdSistema) {
		this.strIdSistema = strIdSistema;
	}

	/**
	 * Gets str ano.
	 *
	 * @return the str ano
	 */
	public String getStrAno() {
		return strAno;
	}

	/**
	 * Sets str ano.
	 *
	 * @param strAno the str ano
	 */
	public void setStrAno(String strAno) {
		this.strAno = strAno;
	}

	/**
	 * Gets str mes.
	 *
	 * @return the str mes
	 */
	public String getStrMes() {
		return strMes;
	}

	/**
	 * Sets str mes.
	 *
	 * @param strMes the str mes
	 */
	public void setStrMes(String strMes) {
		this.strMes = String.format(ConstantManager.FORMATO2CEROSIZQ, Integer.parseInt(strMes));
	}

	/**
	 * Gets str dia.
	 *
	 * @return the str dia
	 */
	public String getStrDia() {
		return strDia;
	}

	/**
	 * Sets str dia.
	 *
	 * @param strDia the str dia
	 */
	public void setStrDia(String strDia) {
		this.strDia = strDia;
	}

	/**
	 * Gets str ofi cuenta debitar.
	 *
	 * @return the str ofi cuenta debitar
	 */
	public String getStrOfiCuentaDebitar() {
		return strOfiCuentaDebitar;
	}

	/**
	 * Sets str ofi cuenta debitar.
	 *
	 * @param strOfiCuentaDebitar the str ofi cuenta debitar
	 */
	public void setStrOfiCuentaDebitar(String strOfiCuentaDebitar) {
		this.strOfiCuentaDebitar = strOfiCuentaDebitar;
	}

	/**
	 * Gets str cuenta debitar.
	 *
	 * @return the str cuenta debitar
	 */
	public String getStrCuentaDebitar() {
		return strCuentaDebitar;
	}

	/**
	 * Sets str cuenta debitar.
	 *
	 * @param strCuentaDebitar the str cuenta debitar
	 */
	public void setStrCuentaDebitar(String strCuentaDebitar) {
		this.strCuentaDebitar = strCuentaDebitar;
	}

	/**
	 * Gets str saldo inicio trx.
	 *
	 * @return the str saldo inicio trx
	 */
	public String getStrSaldoInicioTRX() {
		return strSaldoInicioTRX;
	}

	/**
	 * Sets str saldo inicio trx.
	 *
	 * @param strSaldoInicioTRX the str saldo inicio trx
	 */
	public void setStrSaldoInicioTRX(String strSaldoInicioTRX) {
		this.strSaldoInicioTRX = strSaldoInicioTRX;
	}

	/**
	 * Gets str tip cuenta debitar.
	 *
	 * @return the str tip cuenta debitar
	 */
	public String getStrTipCuentaDebitar() {
		return strTipCuentaDebitar;
	}

	/**
	 * Sets str tip cuenta debitar.
	 *
	 * @param strTipCuentaDebitar the str tip cuenta debitar
	 */
	public void setStrTipCuentaDebitar(String strTipCuentaDebitar) {
		this.strTipCuentaDebitar = strTipCuentaDebitar;
	}

	/**
	 * Gets str oficina creditar.
	 *
	 * @return the str oficina creditar
	 */
	public String getStrOficinaCreditar() {
		return strOficinaCreditar;
	}

	/**
	 * Sets str oficina creditar.
	 *
	 * @param strOficinaCreditar the str oficina creditar
	 */
	public void setStrOficinaCreditar(String strOficinaCreditar) {
		this.strOficinaCreditar = strOficinaCreditar;
	}

	/**
	 * Gets str cuenta creditar.
	 *
	 * @return the str cuenta creditar
	 */
	public String getStrCuentaCreditar() {
		return strCuentaCreditar;
	}

	/**
	 * Sets str cuenta creditar.
	 *
	 * @param strCuentaCreditar the str cuenta creditar
	 */
	public void setStrCuentaCreditar(String strCuentaCreditar) {
		this.strCuentaCreditar = strCuentaCreditar;
	}

	/**
	 * Gets str tipo cuenta creditar.
	 *
	 * @return the str tipo cuenta creditar
	 */
	public String getStrTipoCuentaCreditar() {
		return strTipoCuentaCreditar;
	}

	/**
	 * Sets str tipo cuenta creditar.
	 *
	 * @param strTipoCuentaCreditar the str tipo cuenta creditar
	 */
	public void setStrTipoCuentaCreditar(String strTipoCuentaCreditar) {
		this.strTipoCuentaCreditar = strTipoCuentaCreditar;
	}

	/**
	 * Gets str valor pago.
	 *
	 * @return the str valor pago
	 */
	public String getStrValorPago() {
		return strValorPago;
	}

	/**
	 * Sets str valor pago.
	 *
	 * @param strValorPago the str valor pago
	 */
	public void setStrValorPago(String strValorPago) {
		this.strValorPago = strValorPago;
	}

	/**
	 * Gets str ind contable.
	 *
	 * @return the str ind contable
	 */
	public String getStrIndContable() {
		return strIndContable;
	}

	/**
	 * Sets str ind contable.
	 *
	 * @param strIndContable the str ind contable
	 */
	public void setStrIndContable(String strIndContable) {
		this.strIndContable = strIndContable;
	}

	/**
	 * Gets str valor comision.
	 *
	 * @return the str valor comision
	 */
	public String getStrValorComision() {
		return strValorComision;
	}

	/**
	 * Sets str valor comision.
	 *
	 * @param strValorComision the str valor comision
	 */
	public void setStrValorComision(String strValorComision) {
		this.strValorComision = strValorComision;
	}

	/**
	 * Gets str indicador comision.
	 *
	 * @return the str indicador comision
	 */
	public String getStrIndicadorComision() {
		return strIndicadorComision;
	}

	/**
	 * Sets str indicador comision.
	 *
	 * @param strIndicadorComision the str indicador comision
	 */
	public void setStrIndicadorComision(String strIndicadorComision) {
		this.strIndicadorComision = strIndicadorComision;
	}

	/**
	 * Gets str estado.
	 *
	 * @return the str estado
	 */
	public String getStrEstado() {
		return strEstado;
	}

	/**
	 * Sets str estado.
	 *
	 * @param strEstado the str estado
	 */
	public void setStrEstado(String strEstado) {
		this.strEstado = strEstado;
	}

}
