package co.com.bancolombia.backend.iseries.transversal.tef.idsistema;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.log.canal.dao.BackLogCanal;
import co.com.bancolombia.backend.utilidades.*;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BackIdSistema {

	private static final Logger LOGGER = LogManager.getLogger(BackLogCanal.class.getName());

	/**
	 * Consulta si existe el id sistema PCCLIBRAMD.PCCFFMOVTF, Si el id del sistema
	 * no existe devuelve vacio
	 *
	 * @param strCuentaDebitar:    Recibe la cuenta de deposito completa sin guiones que se debito
	 *                             para hacer la consulta, puede recibir vacio
	 * @param strCuentaCreditar:   Recibe la cuenta de deposito completa sin guiones que se acredito
	 *                             para hacer la consulta, puede recibir vacio
	 * @param strCodigoTransaccion : Recibe el codigo de la transaccion
	 *
	 * @return String Si el id del sistema se genero devuelve un string, de lo
	 * contrario devuelve vacio
	 *
	 * @throws SQLException
	 */
	public String consultarIdSistema(String strCuentaDebitar, String strCuentaCreditar, String strCodigoTransaccion)
			throws SQLException {
		String result = "";
		String strDia = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_DIA);

		String strCuentaDebitarSinPrefijo = "";
		String strCuentaCreditarSinPrejifo = "";
		String sql = "";

		ResultSet objResult = null;
		boolean tieneCredito = false;
		if (StringUtils.isNotBlank(strCuentaCreditar)) {
			strCuentaCreditarSinPrejifo = strCuentaCreditar.substring(3);
		}
		if (StringUtils.isNotBlank(strCuentaDebitar)) {
			strCuentaDebitarSinPrefijo = strCuentaDebitar.substring(3);
		}
		if (strCodigoTransaccion.equalsIgnoreCase(TransactionCodes.TRANS_FONDO_AHORR.getId())
				|| strCodigoTransaccion.equalsIgnoreCase(TransactionCodes.TRANS_FONDO_CORR.getId())) {
			strCuentaDebitarSinPrefijo = strCuentaCreditarSinPrejifo;
		}

		if (strCodigoTransaccion.equalsIgnoreCase(TransactionCodes.TRANS_BANCOLOMBIA_AHORR.getId())
				|| strCodigoTransaccion.equalsIgnoreCase(TransactionCodes.TRANS_BANCOLOMBIA_CORR.getId())) {
			sql = QueryManager.ISERIES.getString("SQL.PCCFFMOVTF.consultarIdSistemaCredDeb");
			tieneCredito = true;
		} else {
			sql = QueryManager.ISERIES.getString("SQL.PCCFFMOVTF.consultarIdSistemaDeb");
		}

		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strDia);
			consulta.setString(2, strCuentaDebitarSinPrefijo);
			if (tieneCredito) {
				consulta.setString(3, strCuentaCreditarSinPrejifo);
			}
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					result = objResult.getString("MVIDENTI");
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return result;
	}
}
