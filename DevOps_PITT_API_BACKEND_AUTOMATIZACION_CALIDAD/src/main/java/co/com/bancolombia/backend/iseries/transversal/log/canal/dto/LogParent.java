package co.com.bancolombia.backend.iseries.transversal.log.canal.dto;

/**
 * The Class LogParent.
 */
public class LogParent {

	/** The str caso id. */
	private String strCasoId;

	/** The str ruta evidencia. */
	private String strRutaEvidencia;

	/** The str codigo transaccion. */
	private String strCodigoTransaccion;

	/** The str trace. */
	private String strTrace;

	/**
	 * The str doc.
	 */
	private String strDoc;

	/** The str tipo doc. */
	private String strTipoDoc;

	/** The str fecha. */
	private String strFecha;

	/** The str codigo error. */
	private String strCodigoError;

	/**
	 * Gets the str caso id.
	 *
	 * @return the str caso id
	 */
	public String getStrCasoId() {
		return strCasoId;
	}

	/**
	 * Sets the str caso id.
	 *
	 * @param strCasoId
	 *            the new str caso id
	 */
	public void setStrCasoId(String strCasoId) {
		this.strCasoId = strCasoId;
	}

	/**
	 * Gets the str ruta evidencia.
	 *
	 * @return the str ruta evidencia
	 */
	public String getStrRutaEvidencia() {
		return strRutaEvidencia;
	}

	/**
	 * Sets the str ruta evidencia.
	 *
	 * @param strRutaEvidencia
	 *            the new str ruta evidencia
	 */
	public void setStrRutaEvidencia(String strRutaEvidencia) {
		this.strRutaEvidencia = strRutaEvidencia;
	}

	/**
	 * Gets the str codigo transaccion.
	 *
	 * @return the str codigo transaccion
	 */
	public String getStrCodigoTransaccion() {
		return strCodigoTransaccion;
	}

	/**
	 * Sets the str codigo transaccion.
	 *
	 * @param strCodigoTransaccion
	 *            the new str codigo transaccion
	 */
	public void setStrCodigoTransaccion(String strCodigoTransaccion) {
		this.strCodigoTransaccion = strCodigoTransaccion;
	}

	/**
	 * Gets the str trace.
	 *
	 * @return the str trace
	 */
	public String getStrTrace() {
		return strTrace;
	}

	/**
	 * Sets the str trace.
	 *
	 * @param strTrace
	 *            the new str trace
	 */
	public void setStrTrace(String strTrace) {
		this.strTrace = strTrace;
	}

	/**
	 * Gets the str doc.
	 *
	 * @return the str doc
	 */
	public String getStrDoc() {
		return strDoc;
	}

	/**
	 * Sets the str doc.
	 *
	 * @param strDoc
	 *            the new str doc
	 */
	public void setStrDoc(String strDoc) {
		this.strDoc = strDoc;
	}

	/**
	 * Gets the str tipo doc.
	 *
	 * @return the str tipo doc
	 */
	public String getStrTipoDoc() {
		return strTipoDoc;
	}

	/**
	 * Sets the str tipo doc.
	 *
	 * @param strTipoDoc
	 *            the new str tipo doc
	 */
	public void setStrTipoDoc(String strTipoDoc) {
		this.strTipoDoc = strTipoDoc;
	}

	/**
	 * Gets the str fecha.
	 *
	 * @return the str fecha
	 */
	public String getStrFecha() {
		return strFecha;
	}

	/**
	 * Sets the str fecha.
	 *
	 * @param strFecha
	 *            the new str fecha
	 */
	public void setStrFecha(String strFecha) {
		this.strFecha = strFecha;
	}

	/**
	 * Gets the str codigo error.
	 *
	 * @return the str codigo error
	 */
	public String getStrCodigoError() {
		return strCodigoError;
	}

	/**
	 * Sets the str codigo error.
	 *
	 * @param strCodigoError
	 *            the new str codigo error
	 */
	public void setStrCodigoError(String strCodigoError) {
		this.strCodigoError = strCodigoError;
	}

}
