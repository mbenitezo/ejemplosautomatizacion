package co.com.bancolombia.backend.iseries.empresas.usuarios.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * son los componentes relacionados con el archivo CRPLIBRAMD.CRPFFUSRBE
 *
 * @author eagudelo
 */
public class BackUsuarioEmpresas {
	private static final Logger LOGGER = LogManager.getLogger(BackUsuarioEmpresas.class.getName());

	/**
	 * DESCRIPCION: Se hace un update al archivo CRPLIBRAMD.CRPFFUSRBE para preparar el estado del usuario.
	 * <p>
	 * PRECONDICION: Tener autorizacion para leer el archivo CRPFFUSRBE.
	 * <p>
	 * PARAMETROS DE ENTRADA:
	 *
	 * @param strNit           the str nit
	 * @param strTipoDocumento the str tipo documento
	 * @param strUsuario       the str usuario
	 * @param strEstadoUsuario los estados aceptados son PREBLOQUEADO(), BLOQUEADO() y diferentes de estos 2 anteriores. El estado PREBLOQUEADO() es para verificar el mensaje "La proxima clave no valida inhibira la entrada". El estado BLOQUEADO() es para verificar el mensaje "El usuario se encuentra bloqueado".
	 *
	 * @return the boolean
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean prepararUsuario(String strNit, String strTipoDocumento, String strUsuario,
	                               String strEstadoUsuario) throws SQLException {
		boolean blnResult = false;
		int opcion = 0;
		if (ConstantManager.PREBLOQUEADO.equals(strEstadoUsuario)) {
			opcion = 2;
		} else if (ConstantManager.BLOQUEADO.equals(strEstadoUsuario)) {
			opcion = 3;
		}
		String sql = QueryManager.ISERIES.getString("SQL.CRPFFUSRBE.prepararUsuario");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setInt(1, opcion);
			consulta.setInt(2, opcion);
			consulta.setInt(3, opcion);
			consulta.setInt(4, opcion);
			consulta.setString(5, String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strNit)));
			consulta.setString(6, strTipoDocumento);
			consulta.setString(7, strUsuario);

			int intResult = consulta.executeUpdate();

			if (intResult != -1) {
				if (intResult > 0) {
					blnResult = true;
				} else {
					LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				}
			} else {
				LOGGER.warn(ExceptionCodesManager.QUERY_FAILED.getMsg());
			}
		}
		return blnResult;
	}


	/**
	 * DESCRIPCION: Se hace un select al archivo CRPLIBRAMD.CRPFFUSRBE para verificar el estado del usuario.
	 * <p>
	 * PRECONDICION: Tener autorizacion para leer el archivo CRPFFUSRBE.
	 * <p>
	 * PARAMETROS DE ENTRADA:
	 *
	 * @param strNit           the str nit
	 * @param strTipoDocumento the str tipo documento
	 * @param strUsuario       the str usuario
	 * @param strEstadoUsuario PARAMETROS DE SALIDA:
	 *
	 * @return the boolean
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean verificarEstadoUsuario(String strNit, String strTipoDocumento, String strUsuario,
	                                      String strEstadoUsuario) throws SQLException {
		ResultSet objResult = null;
		boolean blnResult = false;
		String sql = QueryManager.ISERIES.getString("SQL.CRPFFUSRBE.verificarEstadoUsuario");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strNit)));
			consulta.setString(2, strTipoDocumento);
			consulta.setString(3, strUsuario);

			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				String opcion = "0";
				String opcionactividad = "0";
				while (objResult.next()) {
					String dia = objResult.getString("NRODIA").trim();
					String mes = objResult.getString("NROMES").trim();
					String anio = objResult.getString("NROANO").trim();
					String actividad = objResult.getString("INDACTI").trim();
					String tefi = objResult.getString("NROTEFI").trim();
					if (ConstantManager.INVALIDO.equals(strEstadoUsuario)) {
						opcion = "1";
					} else if (ConstantManager.PREBLOQUEADO.equals(strEstadoUsuario)) {
						opcion = "3";
					} else if (ConstantManager.BLOQUEADO.equals(strEstadoUsuario)) {
						opcionactividad = "1";
					}
					if (dia.equals(opcion) && mes.equals(opcion) && anio.equals(opcion) && actividad.equals(opcionactividad) && tefi.equals(opcion)) {
						blnResult = true;
					}
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return blnResult;
	}

	/**
	 * <pre><b>Descripción:</b>
	 * Este metodo realiza la consulta al "WWWLIBRAMD.WWWFFEMPRE",
	 * por el campo Nit y el Tipo de Nit de la empresa, para consultar si la
	 * empresa se encuentra matricualda y con estado Activo "A".
	 *
	 * <b>Parametros de Entrada:</b>
	 * @param strNit the str nit
	 * @param strTipoNit </pre> <pre> <b>Parametros de Salida:</b>
	 * @return La consulta retorna el parametro "blnResultadoFinal" con valor "true" y/o "false". <li>ConsultarWWWFFEMPRE(String strNit, String strTipoNit)</li> </pre>
	 *
	 * @throws SQLException the sql exception
	 */
	public boolean validarSiExisteWWWFFEMPRE(String strNit, String strTipoNit) throws SQLException {
		boolean blnResultadoFinal = false;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.WWWFFEMPRE.validarSiExisteWWWFFEMPRE");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql);) {
			consulta.setString(1, String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strNit)));
			consulta.setString(2, strTipoNit);
			consulta.setString(3, ConstantManager.LETRA_A);

			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				blnResultadoFinal = true;
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return blnResultadoFinal;
	}
}
