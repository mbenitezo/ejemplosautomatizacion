package co.com.bancolombia.backend.iseries.transversal.log.sti.dto;


import co.com.bancolombia.backend.iseries.transversal.log.canal.dto.LogParent;

// TODO: Auto-generated Javadoc
/**
 * The type Log sti.
 */
public class LogSti extends LogParent {

	/** The str sistema. */
	private String strSistema;
	
	/** The str canal. */
	private String strCanal;

	/**
	 * Instantiates a new Log sti.
	 */
	public LogSti() {
		super();
	}

	/**
	 * Gets str sistema.
	 *
	 * @return the str sistema
	 */
	public String getStrSistema() {
		return strSistema;
	}

	/**
	 * Sets str sistema.
	 *
	 * @param strSystem the str system
	 */
	public void setStrSistema(String strSystem) {
		this.strSistema = strSystem;
	}

	/**
	 * Gets str canal.
	 *
	 * @return the str canal
	 */
	public String getStrCanal() {
		return strCanal;
	}

	/**
	 * Sets str canal.
	 *
	 * @param strChannel the str channel
	 */
	public void setStrCanal(String strChannel) {
		this.strCanal = strChannel;
	}

}
