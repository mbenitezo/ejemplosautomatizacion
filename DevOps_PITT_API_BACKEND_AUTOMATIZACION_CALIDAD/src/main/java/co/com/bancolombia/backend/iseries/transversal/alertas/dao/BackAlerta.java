package co.com.bancolombia.backend.iseries.transversal.alertas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.alertas.dto.Alerta;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantOrientacion;

/**
 * Clase que se encarga de manejar todos los componentes de alertas
 * (parametrizadas y acumuladas) se tocan los archivos PCCLIBRAMD.PCCFFACACL,
 * PCCLIBRAMD.PCCFFPALCL
 *
 * @author david.c.gonzalez
 */
public class BackAlerta {
	private static final Logger LOGGER = LogManager.getLogger(BackAlerta.class.getName());

	/**
	 * Consulta de iseries el monto y el numero de operaciones (Alerta) Archivo
	 * PCCLIBRAMD.PCCFFACACL Si no trae ningun registro devuelve un objeto null
	 *
	 * @param strDoc
	 *            : Recibe el docuemnto del cliente para hacer la consulta.
	 * @param strTipoDoc
	 *            : Recibe el tipo de docuemnto del cliente para hacer la consulta.
	 * @param strOperationCode
	 *            : Recibe el codigo de operacion de la transaccion.
	 *
	 * @return Alerta : Si hacer la consulta no funciona se devuelve un null
	 *
	 * @throws SQLException
	 *             the sql exception
	 */
	public Alerta consultarAlertaAcumuladas(String strDoc, String strTipoDoc, String strOperationCode)
			throws SQLException {
		if (StringUtils.isNoneBlank(strDoc, strTipoDoc, strOperationCode)) {
			Alerta objAlerta = null;
			String strDocFormat = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDoc));
			ResultSet objResult = null;
			String sql = QueryManager.ISERIES.getString("SQL.PCCFFACACL.consultarAlertaAcumuladas");
			try (Connection conexion = IseriesConnectionManager.getConnection();
					PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, strTipoDoc);
				consulta.setString(2, strOperationCode);
				consulta.setString(3, strDocFormat);
				objResult = consulta.executeQuery();
				if (objResult.isBeforeFirst()) {
					while (objResult.next()) {
						double dblMontoAcumulado = Double.parseDouble(objResult.getString("CLMONTO").trim());
						int intNumOperation = Integer.parseInt(objResult.getString("CLNROOPE").trim());
						objAlerta = new Alerta(intNumOperation, dblMontoAcumulado);
					}
				} else {
					LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
					objAlerta = new Alerta();
				}
			} finally {
				if (objResult != null) {
					objResult.close();
				}
				return objAlerta;
			}
		} else {
			LOGGER.info("No se consultó Alertas Acumuladas");
			return new Alerta();
		}
	}

	/**
	 * Consulta de iseries el monto y el numero de operaciones (Alerta) Archivo
	 * PCCLIBRAMD.PCCFFPALCL Si no trae ningun registro devuelve un objeto null
	 *
	 * @param strDoc
	 *            : Recibe el docuemnto del cliente para hacer la consulta.
	 * @param strTipoDoc
	 *            : Recibe el tipo de docuemnto del cliente para hacer la consulta.
	 * @param strOperationCode
	 *            : Recibe el codigo de operacion de la transaccion.
	 *
	 * @return Alerta alerta
	 *
	 * @throws SQLException
	 *             the sql exception
	 */
	public Alerta consultarAlertasPersonalizadas(String strDoc, String strTipoDoc, String strOperationCode)
			throws SQLException {

		if (StringUtils.isNoneBlank(strDoc, strTipoDoc, strOperationCode)) {
			Alerta objAlertaParametrizada = null;
			String strDocFormat = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDoc));
			ResultSet objResult = null;
			String sql = QueryManager.ISERIES.getString("SQL.PCCFFPALCL.consultarAlertasPersonalizadas");
			try (Connection conexion = IseriesConnectionManager.getConnection();
					PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, strOperationCode);
				consulta.setString(2, strDocFormat);
				consulta.setString(3, strTipoDoc);
				objResult = consulta.executeQuery();
				if (objResult.isBeforeFirst()) {
					while (objResult.next()) {
						double dblMontoAcumulado = Double.parseDouble(objResult.getString("PAMONHAB"));
						int intNumOperation = Integer.parseInt(objResult.getString("PANROOPE"));
						objAlertaParametrizada = new Alerta(intNumOperation, dblMontoAcumulado);
					}
				} else {
					LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
					objAlertaParametrizada = new Alerta();
				}
			} finally {
				if (objResult != null) {
					objResult.close();
				}
			}
			return objAlertaParametrizada;
		} else {
			LOGGER.info("No se consultó Alertas Personalizadas");
			return new Alerta();
		}
	}

	/**
	 * Verifica el atributo de numero de operacion del objeto alerta de acuerdo a la
	 * orientacion del caso (ACIERTO o ERROR) si no es ningun retorna un valor false
	 *
	 * @param alertaAnt:
	 *            Recibe el objeto alerta antes de disparar la transaccion.
	 * @param alertaDesp:
	 *            Recibe el objeto alerta despues de disparar la transaccion.
	 * @param strOrientacion:
	 *            Recibe la orientacion del caso de prueba (ACIERTO o ERROR)
	 *
	 * @return
	 */
	private boolean verificarNumeroDeOperaciones(Alerta alertaAnt, Alerta alertaDesp, String strOrientacion) {
		if (ObjectUtils.allNotNull(alertaAnt, alertaDesp) && StringUtils.isNotEmpty(strOrientacion)) {
			int intNumOperationAntes = alertaAnt.getNumOperaciones();
			int intNumOperationDesp = alertaDesp.getNumOperaciones();

			if (ConstantOrientacion.ACIERTO.equalsIgnoreCase(strOrientacion)) {
				return (intNumOperationDesp - intNumOperationAntes == 1)
						|| (intNumOperationDesp > intNumOperationAntes);
			} else if (ConstantOrientacion.ERROR.equalsIgnoreCase(strOrientacion)) {
				return intNumOperationDesp == intNumOperationAntes;
			}
		} else {
			LOGGER.info("No se verificó el número de operación");
		}
		return false;
	}

	/**
	 * Metodo que se encarga de verificar el monto acumulado de una alerta de
	 * acuerdo a la orientacion del caso.
	 *
	 * @param alertaAnt:
	 *            Define un objeto alerta antes de hacer la transaccion.
	 * @param alertaDesp:
	 *            Define un objeto alerta despues de hacer la transaccion.
	 * @param strOrientacion:
	 *            Define la orientacion del caso de prueba
	 * @param strValorTransferir:
	 *            Define el valor a transferir en la transaccion
	 *
	 * @return
	 */
	private boolean verificarMontoAcumulado(Alerta alertaAnt, Alerta alertaDesp, String strOrientacion,
			String strValorTransferir) {
		if (ObjectUtils.allNotNull(alertaAnt, alertaDesp)
				&& StringUtils.isNoneBlank(strValorTransferir, strOrientacion)) {
			double dblMontoAntes = alertaAnt.getMontoAcumulado();
			double dblMontoDesp = alertaDesp.getMontoAcumulado();
			double dblValorTransfer = Double.parseDouble(strValorTransferir);

			if (ConstantOrientacion.ACIERTO.equalsIgnoreCase(strOrientacion)) {
				return (dblMontoAntes + dblValorTransfer) == dblMontoDesp;
			} else {
				return dblMontoAntes == dblMontoDesp;
			}
		} else {
			LOGGER.info("No se verificó el monto acumulado");
			return false;
		}
	}

	/**
	 * Metodo que se encarga de llamar los metodos de validar monto y validar numero
	 * de operaciones y devolver un unico valor.
	 *
	 * @param alertaAnt
	 *            : Define un objeto alerta antes de hacer la transaccion.
	 * @param alertaDesp
	 *            : Define un objeto alerta despues de hacer la transaccion.
	 * @param strOrientacion
	 *            : Define la orientacion del caso de prueba
	 * @param strValorTransferir
	 *            : Define el valor a transferir en la transaccion
	 *
	 * @return boolean boolean
	 */
	public boolean verificarAlerta(Alerta alertaAnt, Alerta alertaDesp, String strOrientacion,
			String strValorTransferir) {
		if (StringUtils.isNoneBlank(strOrientacion, strValorTransferir)
				&& ObjectUtils.allNotNull(alertaAnt, alertaDesp)) {
			boolean blnResultMonto = verificarMontoAcumulado(alertaAnt, alertaDesp, strOrientacion, strValorTransferir);
			boolean blnResultNumOper = verificarNumeroDeOperaciones(alertaAnt, alertaDesp, strOrientacion);

			return blnResultMonto && blnResultNumOper;
		} else {
			LOGGER.info("No se verificó Alertas");
			return false;
		}
	}
}