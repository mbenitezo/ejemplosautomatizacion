package co.com.bancolombia.backend.iseries.alm.cuenta.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Back customer register ALM.
 */
public class BackClienteRegistradoALM {
	private static final Logger LOGGER = LogManager.getLogger(BackClienteRegistradoALM.class.getName());

	/**
	 * Módulo que consulta si un cliente está en WWWLIBRAMD.WWWFFRGMOV 
	 * el cual hace referencia a que se encuentra registrado a ahorro a la mano
	 *
	 * @param strDocumento
	 *            : Recibe el documento del usuario para saber si es cliente de ALM.
	 *
	 * @return blnresultadoFinal boolean
	 *
	 * @throws SQLException
	 *             the sql exception
	 */

	public boolean consultarRegistroClienteALM(String strDocumento) throws SQLException {
		String documentoCliente = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumento));
		boolean blnResultadoFinal = false;
		ResultSet objResult = null;

		String sql = QueryManager.ISERIES.getString("SQL.WWWFFRGMOV.ConsultarClienteRegistradoALM");
		try (Connection conexion = IseriesConnectionManager.getConnection();
				PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, documentoCliente);
			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				LOGGER.info("Consulta exitosa, Cliente si esta registrado en ahorro a la mano.");
				blnResultadoFinal = true;
			} else {
				LOGGER.info("Error, Cliente si esta registrado en ahorro a la mano.");
			}
			return blnResultadoFinal;
		} finally

		{
			if (objResult != null) {
				objResult.close();
			}
		}
	}

}