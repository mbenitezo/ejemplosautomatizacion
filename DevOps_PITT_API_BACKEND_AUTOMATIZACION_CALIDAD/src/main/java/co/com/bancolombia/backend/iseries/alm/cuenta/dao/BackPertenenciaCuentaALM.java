package co.com.bancolombia.backend.iseries.alm.cuenta.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Back usuario app ALM.
 */
public class BackPertenenciaCuentaALM {
	private static final Logger LOGGER = LogManager.getLogger(BackPertenenciaCuentaALM.class.getName());

	/**
	 * Módulo que consulta la cuenta en VISIONR.DBAL en el cual se encuentra el
	 * listado de los clientes registrados en ALM, en él se visualizan número de
	 * cuenta
	 *
	 * @param strDocumento
	 *            : Recibe el documento del cliente para hacer la consulta.
	 *
	 * @return blnresultadoFinal boolean
	 *
	 * @throws SQLException
	 *             the sql exception
	 */

	public boolean consultarClienteALM(String strDocumento) throws SQLException {
		String documentoCliente = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumento));
		boolean blnResultadoFinal = false;
		ResultSet objResult = null;

		String sql = QueryManager.ISERIES.getString("SQL.DBALconsultarPertenenciaCuentaALM");
		try (Connection conexion = IseriesConnectionManager.getConnection();
				PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, documentoCliente);
			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				LOGGER.info("Consulta exitosa, Cliente tiene cuenta en Ahorro a la mano app.");
				blnResultadoFinal = true;
			} else {
				LOGGER.info("Error, Cliente No tiene cuenta en Ahorro a la mano app");
			}
			return blnResultadoFinal;
		} finally

		{
			if (objResult != null) {
				objResult.close();
			}
		}
	}

}