package co.com.bancolombia.backend.iseries.transversal.productos.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.productos.dto.DatosProducto;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Back productos.
 */
public class BackProductos {

	private static final Logger LOGGER = LogManager.getLogger(BackProductos.class.getName());

	/**
	 * Método que consulta todos los productos asignados a un cliente, se combinan
	 * los archivos del CNAME y del CXREF a partir del documento del cliente, se
	 * extraen: Tipo, Numero y Estado del producto. Se crea una lista en la cual se
	 * almacena la imformacion obtenida del Resulset.
	 *
	 * @param strDocumento : Recibe el documento del cliente para hacer la consulta.
	 *
	 * @return objProductos : <br> Tipo <br> Numero <br> Estado
	 *
	 * @throws SQLException the sql exception
	 */

	public List<DatosProducto> consultarProductos(String strDocumento) throws SQLException {
		List<DatosProducto> listaProductos = new ArrayList<>();
		String documento = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumento));
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.CXREF.consultarProductos");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, documento);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strTipoProducto = objResult.getString("CXCDAP").trim();
					String strNroProducto = objResult.getString("CXNOAC").trim();
					DatosProducto datosProducto = new DatosProducto(strTipoProducto, strNroProducto);
					listaProductos.add(datosProducto);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return listaProductos;
	}

	/**
	 * Método que se encarga de recorrer la lista entregada por el front-end la cual
	 * contiene los productos (tipo de producto específico) y verifica la
	 * información de los mismo en la lista consultada por back-end, se validan los
	 * datos correspondiente a número y tipo del producto.
	 *
	 * @param listaEsperada       the lista esperada
	 * @param listaObtenida       the lista obtenida
	 * @param strCanal            the str canal
	 * @param strProductoConsulta the str producto consulta
	 *
	 * @return resultadoValidacion boolean
	 */

	public boolean validarProductos(List<DatosProducto> listaEsperada, List<DatosProducto> listaObtenida, String strCanal, String strProductoConsulta) {

		boolean resultadoValidacion = false;
		boolean resultadoError = true;

		for (DatosProducto productoEsperado : listaEsperada) {
			String strTipoEsperado = productoEsperado.getTipoProducto();
			String strNumeroProductoEsperado = productoEsperado.getNumeroProducto();
			for (DatosProducto productoObtenido : listaObtenida) {
				String strTipoObtenido = productoObtenido.getTipoProducto();
				String strNumeroProductoObtenido = productoObtenido.getNumeroProducto();

				// Producto a consultar strProductoConsulta
				switch (strProductoConsulta) {
					case "Cuentas de Depósitos":
						resultadoValidacion = cuentaDepositos(strTipoEsperado, strTipoObtenido, strNumeroProductoEsperado, strNumeroProductoObtenido, strCanal);
						break;
					case "Tarjetas de Credito":
						resultadoValidacion = tarjetaCredito(strNumeroProductoObtenido, strNumeroProductoEsperado);
						break;

					case "Inversiones":
						// conversión del tipo de producto
						strTipoEsperado = "8";
						// comparación datos
						if (strTipoEsperado.equals(strTipoObtenido) && strNumeroProductoEsperado.equals(strNumeroProductoObtenido)) {
							LOGGER.info(ExceptionCodesManager.SAME_PRODUCT.getMsg());
							resultadoValidacion = true;
						}
						break;

					case "Prestamos":
						resultadoValidacion = prestamos(strTipoObtenido, strTipoEsperado, strNumeroProductoEsperado, strNumeroProductoObtenido, strCanal);
						break;

					default:
						System.out.println("No se encontro el producto a consultar");
						resultadoError = false;
						break;
				}
				if (resultadoValidacion) {
					break;
				}
				if (!resultadoError) {
					resultadoValidacion = false;
					break;
				}
			}
			if (!resultadoValidacion) {
				break;
			}
		}
		return resultadoValidacion;
	}

	private boolean prestamos(String strTipoObtenido, String strTipoEsperado, String strNumeroProductoEsperado, String strNumeroProductoObtenido, String strCanal) {
		if (strTipoEsperado.contains(ConstantManager.PRESTAMO)) {
			strTipoEsperado = "L";
		}
		if (strTipoEsperado.contains(ConstantManager.HIPOTECARIO)) {
			strTipoEsperado = "4";
		}
		// conversión del numero de producto
		if (ConstantManager.APP.equals(strCanal)) {
			strNumeroProductoObtenido = strNumeroProductoObtenido.substring(strNumeroProductoObtenido.length() - 4, strNumeroProductoObtenido.length());
		}
		if (strTipoEsperado.equals(strTipoObtenido)
				&& strNumeroProductoEsperado.equals(strNumeroProductoObtenido)) {
			LOGGER.info(ExceptionCodesManager.SAME_PRODUCT.getMsg());
			return true;
		}

		return false;
	}

	private boolean tarjetaCredito(String strNumeroProductoObtenido, String strNumeroProductoEsperado) {
		strNumeroProductoObtenido = strNumeroProductoObtenido
				.substring(strNumeroProductoObtenido.length() - 4, strNumeroProductoObtenido.length());
		if (strNumeroProductoEsperado.equals(strNumeroProductoObtenido)) {
			LOGGER.info(ExceptionCodesManager.SAME_PRODUCT.getMsg());
			return true;
		}
		return false;
	}

	private boolean cuentaDepositos(String strTipoEsperado, String strTipoObtenido, String strNumeroProductoEsperado, String strNumeroProductoObtenido, String strCanal) {

		// conversión del tipo de producto
		if (strTipoEsperado.equals("Cuenta Corriente")) {
			strTipoEsperado = ConstantManager.INI_CTA_CORRIENTE;
		} else if (strTipoEsperado.equals("Cuenta de Ahorros")) {
			strTipoEsperado = ConstantManager.INI_CTA_AHORROS;
		}
		// conversión del numero de producto
		if (ConstantManager.APP.equals(strCanal)) {
			strNumeroProductoObtenido = strNumeroProductoObtenido.substring(strNumeroProductoObtenido.length() - 4, strNumeroProductoObtenido.length());
		}
		// comparación datos
		if (strTipoEsperado.equals(strTipoObtenido)
				&& strNumeroProductoEsperado.equals(strNumeroProductoObtenido)) {
			LOGGER.info(ExceptionCodesManager.SAME_PRODUCT.getMsg());
			return true;
		}
		return false;
	}


	/**
	 * Validar estado indicador envio pago tc boolean.
	 *
	 * @param strNumTarjeta the str num tarjeta
	 * @param strValor      the str valor
	 * @param strHoraAntes  the str hora antes
	 *
	 * @return the boolean
	 */

	public boolean validarEstadoIndicadorEnvioPagoTC(String strNumTarjeta, String strValor, String strHoraAntes) throws SQLException {
		boolean resultadoValidacionEstado = false;
		boolean tieneValor = false;
		String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		strHoraAntes = String.format(ConstantManager.FORMATO8CEROSIZQ, Long.parseLong(strHoraAntes));
		ResultSet objResult = null;
		String sql = ConstantManager.VACIO;
		if (StringUtils.isBlank(strValor)) {
			sql = QueryManager.ISERIES.getString("SQL.PCCFFTRXTC.validarEstadoIndicadorEnvioPagoTC");
		} else {
			strValor = strValor.replace(",", ConstantManager.VACIO);
			strValor = strValor.replace(".", ",");
			sql = QueryManager.ISERIES.getString("SQL.PCCFFTRXTC.validarEstadoIndicadorEnvioPagoTCValor");
			tieneValor = true;
		}
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strNumTarjeta);
			consulta.setString(2, strFecha);
			consulta.setString(3, strHoraAntes);
			if (tieneValor) {
				consulta.setString(4, strValor);
			}
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strEstadoEnvioTC = objResult.getString("TRXINDVP").trim();
					if (strEstadoEnvioTC.equals("N")) {
						resultadoValidacionEstado = true;
					}
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return resultadoValidacionEstado;
	}


	/**
	 * <b>Descripción:</b>El siguiente compontente se encarga de validar que el
	 * estado del cliente se encuentre en "A".
	 *
	 * @param strDocumento     the str documento
	 * @param strNumeroTarjeta Se espera número de 16 digitos si es TDC o 11 digitos si es TDEP
	 * @param TipoProducto     Se espera un String "TDC" para Tarjetas de crédito o "TDEP" para Tarjetas cuentas deposito.
	 *
	 * @return Se retorna un valor true o false
	 *
	 * @throws SQLException the sql exception
	 */

	public boolean validarEstadoClienteCABFFTARJ(String strDocumento, String strNumeroTarjeta, String TipoProducto) throws SQLException {
		ResultSet objResult = null;
		boolean resultadoValidacion = false;
		if (ConstantManager.INI_TARJETA_CREDITO.equals(TipoProducto)) {
			strNumeroTarjeta = String.format(ConstantManager.FORMATO16CEROSIZQ, Long.parseLong(strNumeroTarjeta));
			strNumeroTarjeta = strNumeroTarjeta.substring(6);
		} else if (ConstantManager.INI_TARJETA_DEPOSITO.equals(TipoProducto)) {
			strNumeroTarjeta = strNumeroTarjeta.replace(ConstantManager.GUION, ConstantManager.VACIO);
		}
		strDocumento = String.format(ConstantManager.FORMATO13CEROSIZQ, Long.parseLong(strDocumento));
		String sql = QueryManager.ISERIES.getString("SQL.CABFFTARJ.validarEstadoClienteCABFFTARJ");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strNumeroTarjeta);
			consulta.setString(2, strDocumento);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strEstadoCliente = objResult.getString("TJESTADO").trim();
					if (ConstantManager.LETRA_A.equals(strEstadoCliente)) {
						resultadoValidacion = true;
					}
				}
			} else {
				resultadoValidacion = false;
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return resultadoValidacion;
	}


	/**
	 * <pre>
	 * <b>Descripción:</b>El siguiente compontente se encarga de validar que el
	 * cliente dueño de la tarjeta de crédito se encuentre en el
	 * TDCLIBRAMD.TDCFFAMED1.
	 *
	 * @param strDocumento El documento enviado debe venir sin puntos y sin ceros a la izquierda.
	 * @param strNumeroTarjeta El número de la TC debe venir sin ceros a la izquierda y sin  formato (. , * -)
	 * @return Se retorna un valor boolean true o false.
	 * @throws SQLException the sql exception
	 */

	public boolean validarClienteTDCFFAMED1(String strDocumento, String strNumeroTarjeta) throws SQLException {
		boolean resultadoValidacion = false;

		strDocumento = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumento));
		strNumeroTarjeta = String.format(ConstantManager.FORMATO16CEROSIZQ, Long.parseLong(strNumeroTarjeta));
		ResultSet objResult = null;
		String documento = String.format("%s%s%s", ConstantManager.PORCENTAJE, strDocumento, ConstantManager.PORCENTAJE);
		String sql = QueryManager.ISERIES.getString("SQL.TDCFFAMED1.validarClienteTDCFFAMED1");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strNumeroTarjeta);
			consulta.setString(2, documento);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strDocumentoObtneido = objResult.getString("AMED1_048").trim();
					strDocumentoObtneido = strDocumentoObtneido.substring(0, strDocumentoObtneido.length() - 2);

					if (strDocumentoObtneido.equals(strDocumento)) {
						resultadoValidacion = true;
					}
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return resultadoValidacion;
	}
}