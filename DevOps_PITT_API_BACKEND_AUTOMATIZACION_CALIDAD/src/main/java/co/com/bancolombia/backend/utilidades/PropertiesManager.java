package co.com.bancolombia.backend.utilidades;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * The type Properties manager.
 *
 * @author Oscar Armando Vallejo Tovar
 */
public class PropertiesManager {

	private PropertiesManager() {
		throw new IllegalStateException(ExceptionCodesManager.UTILITY_CLASS.getMsg());
	}

	/**
	 * Gets db properties.
	 *
	 * @return objeto de propiedades
	 *
	 * @throws IOException the io exception
	 */
	public static Properties getDbProperties() throws IOException {
		// para cargar las propiedades de la aplicación, utilizamos esta clase
		Properties bdProperties = new Properties();
		FileInputStream file;
		// la base folder es ./, la raíz del archivo de propiedades de la base de datos
		String path = "./bd.properties";
		file = new FileInputStream(path);
		bdProperties.load(file);
		file.close();

		return bdProperties;
	}

}
