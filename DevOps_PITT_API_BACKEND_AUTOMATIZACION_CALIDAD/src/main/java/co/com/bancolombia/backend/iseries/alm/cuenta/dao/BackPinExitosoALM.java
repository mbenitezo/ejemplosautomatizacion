package co.com.bancolombia.backend.iseries.alm.cuenta.dao;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.alm.cuenta.dao.BackPertenenciaCuentaALM;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Back Pin exitoso app ALM.
 */
public class BackPinExitosoALM {

private static final Logger LOGGER = LogManager.getLogger(BackPinExitosoALM.class.getName());
	
	/**
	 * Módulo que consulta si el PIN generado se encuentra exitoso PCCLIBRAMD.PCCFFLGLCC
	 *
	 * @param strCuenta : Recibe el número de cuenta para verificar generación de PIN.
	 *
	 * @return blnresultadoFinal boolean
	 *
	 * @throws SQLException the sql exception
	 */
	
	public boolean consultarClienteALM(String strCuenta) throws SQLException {
		String cuentaCliente = String.format(ConstantManager.FORMATO1CEROIZQ, Long.parseLong(strCuenta));
		boolean blnResultadoFinal = false;
		Connection conexion = null;
		ResultSet objResult = null;
		try {
			conexion = IseriesConnectionManager.getConnection();
			String sql = QueryManager.ISERIES.getString("SQL.PCCFFLGLCCconsultarPinExitosoALM");
			try (PreparedStatement consulta = conexion.prepareStatement(sql)) {
				consulta.setString(1, cuentaCliente);
				objResult = consulta.executeQuery();
			}
			if (objResult.isBeforeFirst()) {
				LOGGER.info("Consulta exitosa, el PIN es generado exitosamente.");
				blnResultadoFinal = true;
			} else {
				LOGGER.info("Error, ha existido un error al momento de generar el PIN.");
			}
			return blnResultadoFinal;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
			if (conexion != null) {
				conexion.close();
			}
		}
	}
	
}
