package co.com.bancolombia.backend.iseries.transversal.matff.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.matff.dto.LogInscripcionCuentas;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Back matfflgins.
 */
public class BackMATFFLGINS {
	private static final Logger LOGGER = LogManager.getLogger(BackMATFFLGINS.class.getName());

	/**
	 * <b>consultarMATFFLGINS:</b> Método que permite consulta el log de las cuentas inscritas
	 * del archivo MATLIBRAMD.MATFFLGINS, tales como Canal, Tipo de cuenta inscrita,
	 * Numero de cuenta inscrita, Tipo de documento del dueño de la cuenta,
	 * Numero de documento del dueño de la cuenta, Fecha y Hora de la inscripción,
	 * Tipo de documento del usuario, Numero de documento del usuario, Numero de tarjeta vitual,
	 * Estado y Descripción de la inscripción.
	 *
	 * @param strDocumento Recibe el documento del cliente para hacer la consulta.
	 * @param strTipoDoc   Recibe el tipo docuemnto del cliente para hacer la consulta.
	 * @param strFecha     Recibe la fecha de la inscripción de la cuenta.
	 * @param strHora      Recibe la hora en que se efectuo la transacción.
	 *
	 * @return LogInscripcionCuentas log inscripcion cuentas
	 *
	 * @throws SQLException the sql exception
	 */
	public LogInscripcionCuentas consultarMATFFLGINS(String strDocumento, String strTipoDoc, String strFecha, String strHora) throws SQLException {
		LogInscripcionCuentas objLogInscripcionCuentas = null;
		strDocumento = String.format(ConstantManager.FORMATO15CEROSIZQ, Long.parseLong(strDocumento));
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.MATFFLGINS.consultar");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strFecha);
			consulta.setString(2, strHora);
			consulta.setString(3, strTipoDoc);
			consulta.setString(4, strDocumento);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strSistema = objResult.getString("LGCANAL").trim();
					String strTipoCtaIns = objResult.getString("LGTIPCTAIN").trim();
					String strNroCtaIns = objResult.getString("LGNROCTAIN").trim();
					String strTipoDocIns = objResult.getString("LGTIPDOCDU").trim();
					String strDocumentoIns = objResult.getString("LGNRODOCDU").trim();
					String strTarjetaVirtual = objResult.getString("LGNROTRJ").trim();
					String strEstado = objResult.getString("LGESTADO");
					String strDescripcionEst = objResult.getString("LGDESEST");
					String strTransaccion = objResult.getString("LGDESTRA");
					objLogInscripcionCuentas = new LogInscripcionCuentas(strSistema, strTipoCtaIns, strNroCtaIns, strTipoDocIns,
							strDocumentoIns, strTarjetaVirtual, strEstado, strDescripcionEst, strTransaccion);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				objLogInscripcionCuentas = new LogInscripcionCuentas();
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return objLogInscripcionCuentas;
	}

	/**
	 * <b>validarLogMATFFLGINS:</b> Método que se encarga de validar la infomación registrada
	 * en el log de las cuentas inscritas a través de los canales, se hace la validación de:
	 * Sistema, Tipo de cuenta inscrita, Número de cuenta inscrita, Tipo de documento inscrito,
	 * Número de documento inscrito, Número de tarjeta virtual creada, Estado de la inscripción,
	 * Descripción del estado y Descripción de la transacción.
	 *
	 * @param datoEsperado the dato esperado
	 * @param datoObtenido the dato obtenido
	 *
	 * @return boolean resultadoValidacion
	 */
	public boolean validarLogMATFFLGINS(LogInscripcionCuentas datoEsperado, LogInscripcionCuentas datoObtenido) {
		boolean resultadoValidacion = true;

		String strSistemaEsperado = datoEsperado.getStrSistema();
		String strSistemaObtenido = datoObtenido.getStrSistema();
		resultadoValidacion = validarResultado(strSistemaEsperado, strSistemaObtenido, resultadoValidacion);

		String strTipoCtaInsEsperado = datoEsperado.getStrTipoCtaIns();
		String strTipoCtaInsObtenido = datoObtenido.getStrTipoCtaIns();
		resultadoValidacion = validarResultado(strTipoCtaInsEsperado, strTipoCtaInsObtenido, resultadoValidacion);

		String strNroCtaInsEsperado = datoEsperado.getStrNroCtaIns();
		String strNroCtaInsObtenido = datoObtenido.getStrNroCtaIns();
		resultadoValidacion = validarResultado(strNroCtaInsEsperado, strNroCtaInsObtenido, resultadoValidacion);

		String strTipoDocInsEsperado = datoEsperado.getStrTipoDocIns();
		String strTipoDocInsObtenido = datoObtenido.getStrTipoDocIns();
		resultadoValidacion = validarResultado(strTipoDocInsEsperado, strTipoDocInsObtenido, resultadoValidacion);

		String strDocumentoInsEsperado = datoEsperado.getStrDocumentoIns();
		String strDocumentoInsObtenido = datoObtenido.getStrDocumentoIns();
		resultadoValidacion = validarResultado(strDocumentoInsEsperado, strDocumentoInsObtenido, resultadoValidacion);

		String strTransaccionEsperado = datoEsperado.getStrTransaccion();
		strTransaccionEsperado = strTransaccionEsperado.toUpperCase();
		String strTransaccionObtenido = datoObtenido.getStrTransaccion();
		strTransaccionObtenido = strTransaccionObtenido.toUpperCase();
		resultadoValidacion = validarResultado(strTransaccionEsperado, strTransaccionObtenido, resultadoValidacion);

		return resultadoValidacion;
	}

	private boolean validarResultado(String strEsperado, String strObtenido, boolean resultadoAnterior) {
		if (resultadoAnterior && !strObtenido.isEmpty()) {
			return strEsperado.equalsIgnoreCase(strObtenido);
		} else {
			return resultadoAnterior;
		}
	}
}
