package co.com.bancolombia.backend.iseries.transversal.inscripcion.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.inscripcion.dto.CuentaInscrita;
import co.com.bancolombia.backend.iseries.transversal.log.canal.dao.BackLogCanal;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * The Class BackCuentasInscritasBancolombia.
 */
public class BackCuentasInscritasBancolombia {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(BackLogCanal.class.getName());

	/**
	 * Verificar cuenta tercero existe.
	 *
	 * @param strCuentaTercero     the str cuenta tercero
	 * @param strTipoCuentaTercero the str tipo cuenta tercero
	 *
	 * @return true, if successful
	 *
	 * @throws SQLException the SQL exception
	 */
	public boolean verificarCuentaTerceroExiste(String strCuentaTercero, String strTipoCuentaTercero)
			throws SQLException {
		boolean blnResult = false;
		ResultSet objResult = null;

		String sql = QueryManager.ISERIES.getString("SQL.MATFFINCTA.verificarCuentaTerceroExiste");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strTipoCuentaTercero);
			consulta.setString(2, strCuentaTercero);
			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strEstadoCuenta = objResult.getString("INESTADO").trim();
					blnResult = strEstadoCuenta.equals(ConstantManager.LETRA_V)
							|| strEstadoCuenta.equals(ConstantManager.LETRA_A);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return blnResult;
	}

	/**
	 * Consultar cta banco inscrita.
	 *
	 * @param strCanal      the str canal
	 * @param strCuenta     the str cuenta
	 * @param strTipoCuenta the str tipo cuenta
	 * @param strNumDocIns  the str num doc ins
	 *
	 * @return the cuenta inscrita
	 *
	 * @throws SQLException the SQL exception
	 */
	public CuentaInscrita consultarCtaBancoInscrita(String strCanal, String strCuenta, String strTipoCuenta,
	                                                String strNumDocIns) throws SQLException {
		CuentaInscrita resultCuenta = new CuentaInscrita();
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.MATFFINCTA.ConsultarCtaBancoInscrita");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strCanal);
			consulta.setString(2, strTipoCuenta);
			consulta.setString(2, strCuenta);
			consulta.setString(2, strNumDocIns);
			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strCanalBack = objResult.getString("INCANAL").trim();
					String strTipoCuentaBack = objResult.getString("INTIPCTAIN");
					String strNumCuentaBack = objResult.getString("INNROCTAIN");
					String strTipoDocPropieBack = objResult.getString("INTIPDOCDU");
					String strNumDocPropieBack = objResult.getString("INNRODOCDU");
					String strFechaInsBack = objResult.getString("INFECINS");
					String strTipoDocInsBack = objResult.getString("INTIPDOCIN");
					String strNumDocInsBack = objResult.getString("INNRODOCIN");
					String strEstCuentaBack = objResult.getString("INESTADO").trim();
					String strDescripEstCuentaBack = objResult.getString("INDESEST").trim();
					resultCuenta = new CuentaInscrita(strCanalBack, strTipoCuentaBack, strNumCuentaBack,
							strTipoDocPropieBack, strNumDocPropieBack, strFechaInsBack, strTipoDocInsBack,
							strNumDocInsBack, strEstCuentaBack, strDescripEstCuentaBack);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return resultCuenta;
	}

	/**
	 * Verificar cuenta para transferir.
	 *
	 * @param strDocumentoQuienInscribe the str documento quien inscribe
	 * @param strNumeroCuenta           the str numero cuenta
	 * @param strTipoCuenta             the str tipo cuenta
	 *
	 * @return true, if successful
	 *
	 * @throws SQLException the SQL exception
	 */
	public boolean verificarCuentaParaTransferir(String strDocumentoQuienInscribe, String strNumeroCuenta, String strTipoCuenta) throws SQLException {
		boolean blnResult = false;
		String strOficina = strNumeroCuenta.substring(0, 3);
		String strNroCuenta = strNumeroCuenta.substring(3, 11);
		ResultSet objResult = null;

		String sql = QueryManager.ISERIES.getString("SQL.CABFFCTAS.verificarCuentaParaTransferir");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strDocumentoQuienInscribe);
			consulta.setString(2, strOficina);
			consulta.setString(3, strNroCuenta);
			consulta.setString(4, strTipoCuenta);

			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strEstadoCuenta = objResult.getString("CAESTADO").trim();
					blnResult = strEstadoCuenta.equalsIgnoreCase(ConstantManager.LETRA_A);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return blnResult;
	}

	/**
	 * Validar cta banco inscrita.
	 *
	 * @param cuentaEsperada the cuenta esperada
	 * @param cuentaObtenida the cuenta obtenida
	 *
	 * @return true, if successful
	 */
	// Metodo para verificar los campos esperados y obtenidos de la prueba
	public boolean validarCtaBancoInscrita(CuentaInscrita cuentaEsperada, CuentaInscrita cuentaObtenida) {
		boolean resultadoFinal = true;

		String strCanalBackEsperado = cuentaEsperada.getStrCanal();
		String strCanalBackObtenido = cuentaObtenida.getStrCanal();
		resultadoFinal = this.validarResultado(strCanalBackEsperado, strCanalBackObtenido, resultadoFinal);

		String strTipoCuentaBackEsperado = cuentaEsperada.getStrTipoCuenta();
		String strTipoCuentaBackObtenido = cuentaObtenida.getStrTipoCuenta();
		resultadoFinal = this.validarResultado(strTipoCuentaBackEsperado, strTipoCuentaBackObtenido, resultadoFinal);

		String strNumCuentaBackEsperado = cuentaEsperada.getStrNumCuenta();
		String strNumCuentaBackObtenido = cuentaObtenida.getStrNumCuenta();
		resultadoFinal = this.validarResultado(strNumCuentaBackEsperado, strNumCuentaBackObtenido, resultadoFinal);

		String strTipoDocPropieBackEsperado = cuentaEsperada.getStrTipoDocPropietario();
		String strTipoDocPropieBackObtenido = cuentaObtenida.getStrTipoDocPropietario();
		resultadoFinal = this.validarResultado(strTipoDocPropieBackEsperado, strTipoDocPropieBackObtenido,
				resultadoFinal);

		String strNumDocPropieBackEsperado = cuentaEsperada.getStrNumDocPropietario();
		String strNumDocPropieBackObtenido = cuentaObtenida.getStrNumDocPropietario();
		resultadoFinal = this.validarResultado(strNumDocPropieBackEsperado, strNumDocPropieBackObtenido,
				resultadoFinal);

		String strFechaInsBackEsperado = cuentaEsperada.getStrFechaInscripcion();
		String strFechaInsBackObtenido = cuentaObtenida.getStrFechaInscripcion();
		resultadoFinal = this.validarResultado(strFechaInsBackEsperado, strFechaInsBackObtenido, resultadoFinal);

		String strTipoDocInsBackEsperado = cuentaEsperada.getStrTipoDocIns();
		String strTipoDocInsBackObtenido = cuentaObtenida.getStrTipoDocIns();
		resultadoFinal = this.validarResultado(strTipoDocInsBackEsperado, strTipoDocInsBackObtenido, resultadoFinal);

		String strNumDocInsBackEsperado = cuentaEsperada.getStrNumDocIns();
		String strNumDocInsBackObtenido = cuentaObtenida.getStrNumDocIns();
		resultadoFinal = this.validarResultado(strNumDocInsBackEsperado, strNumDocInsBackObtenido, resultadoFinal);

		String strEstCuentaBackEsperado = cuentaEsperada.getStrEstCuenta();
		String strEstCuentaBackObtenido = cuentaObtenida.getStrEstCuenta();
		resultadoFinal = this.validarResultado(strEstCuentaBackEsperado, strEstCuentaBackObtenido, resultadoFinal);

		String strDescripEstCuentaBackEsperado = cuentaEsperada.getStrDescripEstCuenta();
		String strDescripEstCuentaBackObtenido = cuentaObtenida.getStrDescripEstCuenta();
		resultadoFinal = this.validarResultado(strDescripEstCuentaBackEsperado, strDescripEstCuentaBackObtenido,
				resultadoFinal);

		return resultadoFinal;
	}

	/**
	 * Validar resultado.
	 *
	 * @param strEsperado    the str esperado
	 * @param strObtenido    the str obtenido
	 * @param resultadoAntes the resultado antes
	 *
	 * @return true, if successful
	 */
	private boolean validarResultado(String strEsperado, String strObtenido, boolean resultadoAntes) {
		boolean resultado = true;
		if (!strObtenido.isEmpty()) {
			if (resultadoAntes) {
				resultado = strEsperado.equalsIgnoreCase(strObtenido);
			}
		} else {
			resultado = resultadoAntes;
		}
		return resultado;
	}
}
