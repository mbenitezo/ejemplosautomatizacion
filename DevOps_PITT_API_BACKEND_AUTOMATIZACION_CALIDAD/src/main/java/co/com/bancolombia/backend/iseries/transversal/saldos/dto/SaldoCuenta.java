package co.com.bancolombia.backend.iseries.transversal.saldos.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Saldo cuenta.
 */
public class SaldoCuenta {

	private double dblSaldoDisponible;
	private double dblSaldoCanje;
	private double dblSaldoTotal;
	private int intDiasSobregiro;

	/**
	 * The Str tipo sobregiro.
	 */
	String strTipoSobregiro;
	/**
	 * The Str tipo plan cuenta.
	 */
	String strTipoPlanCuenta;
	/**
	 * The Dbl saldo inicio dia.
	 */
	double dblSaldoInicioDia;
	/**
	 * The Dbl saldo retenido inicio.
	 */
	double dblSaldoRetenidoInicio;
	/**
	 * The Dbl saldo retenido actual.
	 */
	double dblSaldoRetenidoActual;
	/**
	 * The Dbl valor embargo.
	 */
	double dblValorEmbargo;

	/**
	 * The Dblflot inicio dia.
	 */
	double dblflotInicioDia;
	/**
	 * The Dbl saldo minimo.
	 */
	double dblSaldoMinimo;
	/**
	 * The Dbl tope flotante.
	 */
	double dblTopeFlotante;

	/**
	 * The Dbl total nota credito hoy.
	 */
	double dblTotalNotaCreditoHoy;
	/**
	 * The Dbl total nota debito hoy.
	 */
	double dblTotalNotaDebitoHoy;

	/**
	 * CONSTRUCTOR. Si recibe parametros. crea el objeto saldo de una cuenta del
	 * archivo (SCILIBRAMD.SCIFFSALDO)
	 *
	 * @param dblSaldoDisponible     :     Define el saldo disponible, recibido de la columna (SDSDODSP)
	 * @param dblSaldoCanje          :          Define el saldo del canje, recibido de la columna (SDFLTDSP)
	 * @param dblSaldoTotal          :          Define el saldo total, es la suma del saldo canje con el saldo                                disponible
	 * @param intDiasSobregiro       :       Define el numero de dias de sobregiro, recibido de la columna                                (SDDSPINI1)
	 * @param strTipoSobregiro       :       Define el tipo de sobregiro, recibido de la columna (SDTIPOSOB)
	 * @param strTipoPlanCuenta      :      Define el tipo de plan de la cuenta, recibido de la columna                                (SDPLAN)
	 * @param dblSaldoInicioDia      :      Define el saldo al inicio del dia, recibido de la columna                                (SDSDOINIC)
	 * @param dblSaldoRetenidoInicio : Define el saldo retenido al inicio del dia, recibido de la columna                                (SDRETINIC)
	 * @param dblSaldoRetenidoActual : Define el saldo retenido actual, recibido de la columna (SDRETACT)
	 * @param dblValorEmbargo        :        Define el valor del enbargo, recibido de la columna (SDVLREMB)
	 * @param dblflotInicioDia       :       Define el flotante inicio dia, recibido de la columna (SDFLTINIC)
	 * @param dblSaldoMinimo         :         Define el saldo minimo, recibido de la columna (SDSDOMIN)
	 * @param dblTopeFlotante        :        Define el tope del flotante, recibido de la columna (SDTPEFLT)
	 * @param dblTotalNotaCreditoHoy : Define el total nota credito hoy, recibido de la columna                                (SDNOTACR)
	 * @param dblTotalNotaDebitoHoy  :  Define el total nota debito hoy, recibido de la columna (SDNOTADB)
	 */
	public SaldoCuenta(double dblSaldoDisponible, double dblSaldoCanje, double dblSaldoTotal, int intDiasSobregiro, String strTipoSobregiro, String strTipoPlanCuenta, double dblSaldoInicioDia, double dblSaldoRetenidoInicio, double dblSaldoRetenidoActual, double dblValorEmbargo, double dblflotInicioDia, double dblSaldoMinimo, double dblTopeFlotante, double dblTotalNotaCreditoHoy, double dblTotalNotaDebitoHoy) {
		this.dblSaldoDisponible = dblSaldoDisponible;
		this.dblSaldoCanje = dblSaldoCanje;
		this.dblSaldoTotal = dblSaldoTotal;
		this.intDiasSobregiro = intDiasSobregiro;
		this.strTipoSobregiro = strTipoSobregiro;
		this.strTipoPlanCuenta = strTipoPlanCuenta;
		this.dblSaldoInicioDia = dblSaldoInicioDia;
		this.dblSaldoRetenidoInicio = dblSaldoRetenidoInicio;
		this.dblSaldoRetenidoActual = dblSaldoRetenidoActual;
		this.dblValorEmbargo = dblValorEmbargo;
		this.dblflotInicioDia = dblflotInicioDia;
		this.dblSaldoMinimo = dblSaldoMinimo;
		this.dblTopeFlotante = dblTopeFlotante;
		this.dblTotalNotaCreditoHoy = dblTotalNotaCreditoHoy;
		this.dblTotalNotaDebitoHoy = dblTotalNotaDebitoHoy;
	}

	/**
	 * Instantiates a new Saldo cuenta.
	 */
	public SaldoCuenta() {
		dblSaldoDisponible = 0;
		dblSaldoCanje = 0;
		dblSaldoTotal = 0;
		intDiasSobregiro = 0;
		strTipoSobregiro = ConstantManager.VACIO;
		strTipoPlanCuenta = ConstantManager.VACIO;
		dblSaldoInicioDia = 0;
		dblSaldoRetenidoInicio = 0;
		dblSaldoRetenidoActual = 0;
		dblValorEmbargo = 0;
		dblflotInicioDia = 0;
		dblSaldoMinimo = 0;
		dblTopeFlotante = 0;
		dblTotalNotaCreditoHoy = 0;
		dblTotalNotaDebitoHoy = 0;
	}

	/**
	 * Gets dbl saldo disponible.
	 *
	 * @return the dbl saldo disponible
	 */
	public double getDblSaldoDisponible() {
		return dblSaldoDisponible;
	}

	/**
	 * Sets dbl saldo disponible.
	 *
	 * @param dblSaldoCta the dbl saldo cta
	 */
	public void setDblSaldoDisponible(double dblSaldoCta) {
		this.dblSaldoDisponible = dblSaldoCta;
	}

	/**
	 * Gets dbl saldo canje.
	 *
	 * @return the dbl saldo canje
	 */
	public double getDblSaldoCanje() {
		return dblSaldoCanje;
	}

	/**
	 * Sets dbl saldo canje.
	 *
	 * @param dblSaldoCanje the dbl saldo canje
	 */
	public void setDblSaldoCanje(double dblSaldoCanje) {
		this.dblSaldoCanje = dblSaldoCanje;
	}

	/**
	 * Gets dbl saldo total.
	 *
	 * @return the dbl saldo total
	 */
	public double getDblSaldoTotal() {
		return dblSaldoTotal;
	}

	/**
	 * Sets dbl saldo total.
	 *
	 * @param dblSaldoTotal the dbl saldo total
	 */
	public void setDblSaldoTotal(double dblSaldoTotal) {
		this.dblSaldoTotal = dblSaldoTotal;
	}

	/**
	 * Gets int dias sobregiro.
	 *
	 * @return the int dias sobregiro
	 */
	public int getIntDiasSobregiro() {
		return intDiasSobregiro;
	}

	/**
	 * Sets int dias sobregiro.
	 *
	 * @param intDiasSobregiro the int dias sobregiro
	 */
	public void setIntDiasSobregiro(int intDiasSobregiro) {
		this.intDiasSobregiro = intDiasSobregiro;
	}

	/**
	 * Gets str tipo sobregiro.
	 *
	 * @return the str tipo sobregiro
	 */
	public String getStrTipoSobregiro() {
		return strTipoSobregiro;
	}

	/**
	 * Sets str tipo sobregiro.
	 *
	 * @param strTipoSobregiro the str tipo sobregiro
	 */
	public void setStrTipoSobregiro(String strTipoSobregiro) {
		this.strTipoSobregiro = strTipoSobregiro;
	}

	/**
	 * Gets str tipo plan cuenta.
	 *
	 * @return the str tipo plan cuenta
	 */
	public String getStrTipoPlanCuenta() {
		return strTipoPlanCuenta;
	}

	/**
	 * Sets str tipo plan cuenta.
	 *
	 * @param strTipoPlanCuenta the str tipo plan cuenta
	 */
	public void setStrTipoPlanCuenta(String strTipoPlanCuenta) {
		this.strTipoPlanCuenta = strTipoPlanCuenta;
	}

	/**
	 * Gets dbl saldo inicio dia.
	 *
	 * @return the dbl saldo inicio dia
	 */
	public double getDblSaldoInicioDia() {
		return dblSaldoInicioDia;
	}

	/**
	 * Sets dbl saldo inicio dia.
	 *
	 * @param dblSaldoInicioDia the dbl saldo inicio dia
	 */
	public void setDblSaldoInicioDia(double dblSaldoInicioDia) {
		this.dblSaldoInicioDia = dblSaldoInicioDia;
	}

	/**
	 * Gets dbl saldo retenido inicio.
	 *
	 * @return the dbl saldo retenido inicio
	 */
	public double getDblSaldoRetenidoInicio() {
		return dblSaldoRetenidoInicio;
	}

	/**
	 * Sets dbl saldo retenido inicio.
	 *
	 * @param dblSaldoRetenidoInicio the dbl saldo retenido inicio
	 */
	public void setDblSaldoRetenidoInicio(double dblSaldoRetenidoInicio) {
		this.dblSaldoRetenidoInicio = dblSaldoRetenidoInicio;
	}

	/**
	 * Gets dbl saldo retenido actual.
	 *
	 * @return the dbl saldo retenido actual
	 */
	public double getDblSaldoRetenidoActual() {
		return dblSaldoRetenidoActual;
	}

	/**
	 * Sets dbl saldo retenido actual.
	 *
	 * @param dblSaldoRetenidoActual the dbl saldo retenido actual
	 */
	public void setDblSaldoRetenidoActual(double dblSaldoRetenidoActual) {
		this.dblSaldoRetenidoActual = dblSaldoRetenidoActual;
	}

	/**
	 * Gets dbl valor embargo.
	 *
	 * @return the dbl valor embargo
	 */
	public double getDblValorEmbargo() {
		return dblValorEmbargo;
	}

	/**
	 * Sets dbl valor embargo.
	 *
	 * @param dblValorEmbargo the dbl valor embargo
	 */
	public void setDblValorEmbargo(double dblValorEmbargo) {
		this.dblValorEmbargo = dblValorEmbargo;
	}

	/**
	 * Gets dblflot inicio dia.
	 *
	 * @return the dblflot inicio dia
	 */
	public double getDblflotInicioDia() {
		return dblflotInicioDia;
	}

	/**
	 * Sets dblflot inicio dia.
	 *
	 * @param dblflotInicioDia the dblflot inicio dia
	 */
	public void setDblflotInicioDia(double dblflotInicioDia) {
		this.dblflotInicioDia = dblflotInicioDia;
	}

	/**
	 * Gets dbl saldo minimo.
	 *
	 * @return the dbl saldo minimo
	 */
	public double getDblSaldoMinimo() {
		return dblSaldoMinimo;
	}

	/**
	 * Sets dbl saldo minimo.
	 *
	 * @param dblSaldoMinimo the dbl saldo minimo
	 */
	public void setDblSaldoMinimo(double dblSaldoMinimo) {
		this.dblSaldoMinimo = dblSaldoMinimo;
	}

	/**
	 * Gets dbl tope flotante.
	 *
	 * @return the dbl tope flotante
	 */
	public double getDblTopeFlotante() {
		return dblTopeFlotante;
	}

	/**
	 * Sets dbl tope flotante.
	 *
	 * @param dblTopeFlotante the dbl tope flotante
	 */
	public void setDblTopeFlotante(double dblTopeFlotante) {
		this.dblTopeFlotante = dblTopeFlotante;
	}

	/**
	 * Gets dbl total nota credito hoy.
	 *
	 * @return the dbl total nota credito hoy
	 */
	public double getDblTotalNotaCreditoHoy() {
		return dblTotalNotaCreditoHoy;
	}

	/**
	 * Sets dbl total nota credito hoy.
	 *
	 * @param dblTotalNotaCreditoHoy the dbl total nota credito hoy
	 */
	public void setDblTotalNotaCreditoHoy(double dblTotalNotaCreditoHoy) {
		this.dblTotalNotaCreditoHoy = dblTotalNotaCreditoHoy;
	}

	/**
	 * Gets dbl total nota debito hoy.
	 *
	 * @return the dbl total nota debito hoy
	 */
	public double getDblTotalNotaDebitoHoy() {
		return dblTotalNotaDebitoHoy;
	}

	/**
	 * Sets dbl total nota debito hoy.
	 *
	 * @param dblTotalNotaDebitoHoy the dbl total nota debito hoy
	 */
	public void setDblTotalNotaDebitoHoy(double dblTotalNotaDebitoHoy) {
		this.dblTotalNotaDebitoHoy = dblTotalNotaDebitoHoy;
	}

}
