package co.com.bancolombia.backend.utilidades;

/**
 * The enum Exception codes.
 *
 * @author Oscar Armando Vallejo Tovar
 */
public enum ExceptionCodesManager {
	/**
	 * The Empty request.
	 */
	EMPTY_REQUEST(0, "La consulta no generó ningún resultado"),
	/**
	 * The Utility class.
	 */
	UTILITY_CLASS(1, "Clase de utilidades"),
	/**
	 * The Missing header.
	 */
	MISSING_HEADER(2, "Required header is missing"),
	/**
	 * The Connection failed.
	 */
	CONNECTION_FAILED(3, "Error inesperado al conectase a la base de datos"),
	/**
	 * The General error.
	 */
	GENERAL_ERROR(4, "ERROR GENERICO"),
	/**
	 * The Query failed.
	 */
	QUERY_FAILED(5, "Error inesperdo a ejecutar la consulta"),
	/**
	 * The Empty tops.
	 */
	EMPTY_TOPS(6, "Se produjo un error inesperado al consultar la tarjeta e-prepago, intente nuevamente más tarde."),
	/**
	 * The Bad format.
	 */
	BAD_FORMAT(7, "Se produjo un errpr inesperado al crear el formato de la fecha"),
	/**
	 * The File notfound.
	 */
	FILE_NOTFOUND(8, "El archivo no se encuentra en la ruta especificada"),
	/**
	 * The Bdprop notfound.
	 */
	BDPROP_NOTFOUND(9, "El archivo de propiedades de conexión de la base de datos no se encuentra"),


	/**
	 * The Trans not found.
	 */
	TRANS_NOT_FOUND(10, "La transacción no esta parametrizada"),
	SAME_PRODUCT(11, "Producto obtenido es igual al esperado"),
	VALUE_NOTFOUND(12, "El valor esperado no fue encontrado");

	private final int id;
	private final String msg;

	ExceptionCodesManager(int id, String msg) {
		this.id = id;
		this.msg = msg;
	}

	/**
	 * Gets id.
	 *
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Gets msg.
	 *
	 * @return the msg
	 */
	public String getMsg() {
		return this.msg;
	}
}
