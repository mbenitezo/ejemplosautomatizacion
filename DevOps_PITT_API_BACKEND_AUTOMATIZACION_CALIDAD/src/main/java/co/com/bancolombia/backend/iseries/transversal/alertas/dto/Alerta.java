package co.com.bancolombia.backend.iseries.transversal.alertas.dto;

/**
 * The type Alerta.
 */
public class Alerta {

	private int numOperaciones;
	private Double montoAcumulado;

	/**
	 * CONSTRUCTOR crea un objeto de tipo alerta.
	 *
	 * @param numOperaciones :            Define el numero de operaciones para la alerta.
	 * @param montoAcumulado :            Define el monto acumulado en el instante que se ejecuta la sentencia.
	 */
	public Alerta(int numOperaciones, Double montoAcumulado) {
		this.numOperaciones = numOperaciones;
		this.montoAcumulado = montoAcumulado;
	}

	/**
	 * Instantiates a new Alerta.
	 */
	public Alerta() {
		numOperaciones = 0;
		montoAcumulado = 0D;
	}

	/**
	 * Obtiene el numero de operaciones de la alerta.
	 *
	 * @return the num operaciones
	 *
	 * @return: Devuelve el numero de operaicones.
	 */
	public int getNumOperaciones() {
		return numOperaciones;
	}

	/**
	 * Establece el numero de operaciones de la alerta.
	 *
	 * @param numOperaciones :            Define el numero de operaciones para la alerta en el instante que se ejecuta la sentencia
	 */
	public void setNumOperaciones(int numOperaciones) {
		this.numOperaciones = numOperaciones;
	}

	/**
	 * Obtiene el monto acumulado de la alerta.
	 *
	 * @return Devuelve el monto acumulado en el instante que se ejecuto la sentencia.
	 */
	public Double getMontoAcumulado() {
		return montoAcumulado;
	}

	/**
	 * Establece el monto acumulado de la alerta.
	 *
	 * @param montoAcumulado :            Define el monto acumulado en el instante que se ejecuta la sentencia.
	 */
	public void setMontoAcumulado(Double montoAcumulado) {
		this.montoAcumulado = montoAcumulado;
	}

}
