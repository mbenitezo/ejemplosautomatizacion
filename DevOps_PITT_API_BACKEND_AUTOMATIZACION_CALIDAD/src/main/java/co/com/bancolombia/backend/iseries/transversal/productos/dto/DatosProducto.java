package co.com.bancolombia.backend.iseries.transversal.productos.dto;

/**
 * The type Datos producto.
 */
public class DatosProducto {

	private String TipoProducto;
	private String NumeroProducto;
	private String ValorProducto;
	private String ValorProductoUSD;


	/**
	 * Instantiates a new Datos producto.
	 *
	 * @param tipoProducto     the tipo producto
	 * @param numeroProducto   the numero producto
	 * @param valorProducto    the valor producto
	 * @param valorProductoUSD the valor producto usd
	 */
	public DatosProducto(String tipoProducto, String numeroProducto, String valorProducto, String valorProductoUSD) {
		this.TipoProducto = tipoProducto;
		this.NumeroProducto = numeroProducto;
		this.ValorProducto = valorProducto;
		this.ValorProductoUSD = valorProductoUSD;
	}

	/**
	 * Instantiates a new Datos producto.
	 *
	 * @param TipoProducto   the tipo producto
	 * @param NumeroProducto the numero producto
	 */
	public DatosProducto(String TipoProducto, String NumeroProducto) {
		this.TipoProducto = TipoProducto;
		this.NumeroProducto = NumeroProducto;
	}

	/**
	 * Gets tipo producto.
	 *
	 * @return the tipo producto
	 */
	public String getTipoProducto() {
		return TipoProducto;
	}

	/**
	 * Sets tipo producto.
	 *
	 * @param tipoProducto the tipo producto
	 */
	public void setTipoProducto(String tipoProducto) {
		this.TipoProducto = tipoProducto;
	}

	/**
	 * Gets numero producto.
	 *
	 * @return the numero producto
	 */
	public String getNumeroProducto() {
		return NumeroProducto;
	}

	/**
	 * Sets numero producto.
	 *
	 * @param numeroProducto the numero producto
	 */
	public void setNumeroProducto(String numeroProducto) {
		this.NumeroProducto = numeroProducto;
	}

	/**
	 * Gets valor producto.
	 *
	 * @return the valor producto
	 */
	public String getValorProducto() {
		return ValorProducto;
	}

	/**
	 * Sets valor producto.
	 *
	 * @param valorProducto the valor producto
	 */
	public void setValorProducto(String valorProducto) {
		this.ValorProducto = valorProducto;
	}

	/**
	 * Gets valor producto usd.
	 *
	 * @return the valor producto usd
	 */
	public String getValorProductoUSD() {
		return ValorProductoUSD;
	}

	/**
	 * Sets valor producto usd.
	 *
	 * @param valorProductoUSD the valor producto usd
	 */
	public void setValorProductoUSD(String valorProductoUSD) {
		this.ValorProductoUSD = valorProductoUSD;
	}


}
