package co.com.bancolombia.backend.iseries.transversal.tef.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Log tef.
 */
public class LogTef {

	private String strAno;
	private String strMes;
	private String strDia;
	private String strError;
	private String strEstado;
	private String strSistema; // PARA SVP Y APP = SVP, PARA NEQUI = NEQ
	private String strIdSistema;

	private String strCuentaOrigen; // tiene 0s al principio 16 digitos
	private String strTipoCuentaOrigen; // es el numero 1 o 7
	private String strCuentaDest; // puede ser celular o cuenta deposito ... por el momento
	private String strTipoCuentaDest; // puede ser celular o cuenta deposito ... por el momento
	private String strValorTransfer; // 17 digitos con 0s a la izquierda

	/**
	 * CONSTRUCTOR. crea un objeto de tipo log tef, sus valores por defecto del
	 * error es 99 y el estado es T
	 */
	public LogTef() {
		this.strError = ConstantManager.NUMERO_99;
		this.strEstado = ConstantManager.LETRA_T;
	}

	/**
	 * Gets str ano.
	 *
	 * @return the str ano
	 */
	public String getStrAno() {
		return strAno;
	}

	/**
	 * Establece el valor de la variable año, son los ultimos dos digitos del año.
	 *
	 * @param strAno the str ano
	 */
	public void setStrAno(String strAno) {
		this.strAno = strAno;
	}

	/**
	 * Gets str mes.
	 *
	 * @return the str mes
	 */
	public String getStrMes() {
		return strMes;
	}

	/**
	 * Establece el valor de la variable mes, son los dos digitos del mes.
	 *
	 * @param strMes the str mes
	 */
	public void setStrMes(String strMes) {
		this.strMes = String.format(ConstantManager.FORMATO2CEROSIZQ, Integer.parseInt(strMes));
	}

	/**
	 * Gets str dia.
	 *
	 * @return the str dia
	 */
	public String getStrDia() {
		return strDia;
	}

	/**
	 * Establece el valor de la variable dia, son los ultimos dos digitos del dia.
	 *
	 * @param strDia the str dia
	 */
	public void setStrDia(String strDia) {
		this.strDia = String.format(ConstantManager.FORMATO2CEROSIZQ, Integer.parseInt(strDia));
	}

	/**
	 * Gets str error.
	 *
	 * @return the str error
	 */
	public String getStrError() {
		return strError;
	}

	/**
	 * Sets str error.
	 *
	 * @param strError the str error
	 */
	public void setStrError(String strError) {
		this.strError = strError;
	}

	/**
	 * Gets str estado.
	 *
	 * @return the str estado
	 */
	public String getStrEstado() {
		return strEstado;
	}

	/**
	 * Sets str estado.
	 *
	 * @param strEstado the str estado
	 */
	public void setStrEstado(String strEstado) {
		this.strEstado = strEstado;
	}

	/**
	 * Gets str sistema.
	 *
	 * @return the str sistema
	 */
	public String getStrSistema() {
		return strSistema;
	}

	/**
	 * Sets str sistema.
	 *
	 * @param strSistema the str sistema
	 */
	public void setStrSistema(String strSistema) {
		if (ConstantManager.APP.equalsIgnoreCase(strSistema)) {
			strSistema = ConstantManager.SVP;
		}
		this.strSistema = strSistema;
	}

	/**
	 * Gets str id sistema.
	 *
	 * @return the str id sistema
	 */
	public String getStrIdSistema() {
		return strIdSistema;
	}

	/**
	 * Sets str id sistema.
	 *
	 * @param strIdSistema the str id sistema
	 */
	public void setStrIdSistema(String strIdSistema) {
		this.strIdSistema = strIdSistema;
	}

	/**
	 * Gets str cuenta origen.
	 *
	 * @return the str cuenta origen
	 */
	public String getStrCuentaOrigen() {
		return strCuentaOrigen;
	}

	/**
	 * Sets str cuenta origen.
	 *
	 * @param strCuentaOrigen the str cuenta origen
	 */
	public void setStrCuentaOrigen(String strCuentaOrigen) {
		this.strCuentaOrigen = String.format(ConstantManager.FORMATO16CEROSIZQ, Long.parseLong(strCuentaOrigen));
	}

	/**
	 * Gets str tipo cuenta origen.
	 *
	 * @return the str tipo cuenta origen
	 */
	public String getStrTipoCuentaOrigen() {
		return strTipoCuentaOrigen;
	}

	/**
	 * Sets str tipo cuenta origen.
	 *
	 * @param strTipoCuentaOrigen the str tipo cuenta origen
	 */
	public void setStrTipoCuentaOrigen(String strTipoCuentaOrigen) {
		this.strTipoCuentaOrigen = strTipoCuentaOrigen;
	}

	/**
	 * Gets str cuenta dest.
	 *
	 * @return the str cuenta dest
	 */
	public String getStrCuentaDest() {
		return strCuentaDest;
	}

	/**
	 * Sets str cuenta dest.
	 *
	 * @param strCuentaDest the str cuenta dest
	 */
	public void setStrCuentaDest(String strCuentaDest) {
		this.strCuentaDest = String.format(ConstantManager.FORMATO16CEROSIZQ, Long.parseLong(strCuentaDest));
	}

	/**
	 * Gets str tipo cuenta dest.
	 *
	 * @return the str tipo cuenta dest
	 */
	public String getStrTipoCuentaDest() {
		return strTipoCuentaDest;
	}

	/**
	 * Sets str tipo cuenta dest.
	 *
	 * @param strTipoCuentaDest the str tipo cuenta dest
	 */
	public void setStrTipoCuentaDest(String strTipoCuentaDest) {
		this.strTipoCuentaDest = strTipoCuentaDest;
	}

	/**
	 * Gets str valor transfer.
	 *
	 * @return the str valor transfer
	 */
	public String getStrValorTransfer() {
		return strValorTransfer;
	}

	/**
	 * Sets str valor transfer.
	 *
	 * @param strValorTransfer the str valor transfer
	 */
	public void setStrValorTransfer(String strValorTransfer) {
		strValorTransfer = String.format(ConstantManager.FORMATO17CEROSIZQ, Long.parseLong(strValorTransfer));
		this.strValorTransfer = strValorTransfer;
	}

}
