package co.com.bancolombia.backend.iseries.transversal.matff.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.matff.dto.ClavePrincipal;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Back matffestcl.
 */
public class BackMATFFESTCL {

	private static final Logger LOGGER = LogManager.getLogger(BackMATFFESTCL.class.getName());

	/**
	 * Módulo que permite verificar el conteo y el estado de la clave del cliente
	 * Los intentos cy el estado de la clave son verificados en el archivo MATFFESTCL.
	 *
	 * @param strDocumento : Recibe el documento del cliente para hacer la consulta.
	 * @param strTipoDoc   :   Recibe el tipo docuemnto del cliente para hacer la consulta.
	 *
	 * @return ClavePrincipal clave principal
	 *
	 * @throws SQLException the sql exception
	 */
	public ClavePrincipal consultarClavePpal(String strDocumento, String strTipoDoc) throws SQLException {
		ClavePrincipal objClavePpal = null;
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.MATFFESTCL.consultarClavePpal");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strDocumento);
			consulta.setString(2, strTipoDoc);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strNroIntentos = objResult.getString("ECNROINT").trim();
					String strEstadoClv = objResult.getString("ECESTADO").trim();
					String strFechaTrn = objResult.getString("ECFECHA").trim();
					String strCanalBloqueo = objResult.getString("ECCANALBLQ").trim();
					String strDescripcionEstClv = objResult.getString("ECDSCEST").trim();
					objClavePpal = new ClavePrincipal(strNroIntentos, strEstadoClv, strFechaTrn, strCanalBloqueo, strDescripcionEstClv);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				objClavePpal = new ClavePrincipal();
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return objClavePpal;
	}

	/**
	 * Validar clave boolean.
	 *
	 * @param objValorAntes   the obj valor antes
	 * @param objValorDespues the obj valor despues
	 *
	 * @return the boolean
	 */
	public boolean validarClave(ClavePrincipal objValorAntes, ClavePrincipal objValorDespues) {
		boolean resultadoValidacion;

		int strNroIntentosAntes = Integer.parseInt(objValorAntes.getStrNroIntentos());
		int strNroIntentosDespues = Integer.parseInt(objValorDespues.getStrNroIntentos());
		resultadoValidacion = strNroIntentosAntes + 1 == strNroIntentosDespues;

		String strEstadoClvEsperado = objValorAntes.getStrEstadoClv();
		String strEstadoClvObtenido = objValorDespues.getStrEstadoClv();
		resultadoValidacion = validarResultado(strEstadoClvEsperado, strEstadoClvObtenido, resultadoValidacion);

		String strFechaTrnEsperado = objValorAntes.getStrFechaTrn();
		String strFechaTrnObtenido = objValorDespues.getStrFechaTrn();
		resultadoValidacion = validarResultado(strFechaTrnEsperado, strFechaTrnObtenido, resultadoValidacion);

		String strCanalBloqueoEsperado = objValorAntes.getStrCanalBloqueo();
		String strCanalBloqueoObtenido = objValorDespues.getStrCanalBloqueo();
		resultadoValidacion = validarResultado(strCanalBloqueoEsperado, strCanalBloqueoObtenido, resultadoValidacion);

		String strDescripcionEstClvEsperado = objValorAntes.getStrDescripcionEstClv();
		String strDescripcionEstClvObtenido = objValorDespues.getStrDescripcionEstClv();
		resultadoValidacion = validarResultado(strDescripcionEstClvEsperado, strDescripcionEstClvObtenido, resultadoValidacion);

		return resultadoValidacion;
	}

	private boolean validarResultado(String strEsperado, String strObtenido, boolean resultadoAnterior) {
		if (resultadoAnterior && !strObtenido.isEmpty()) {
			return strEsperado.equalsIgnoreCase(strObtenido);
		} else {
			return resultadoAnterior;
		}
	}
}
