package co.com.bancolombia.backend.iseries.transversal.alertas.dto;

/**
 * The type Log alerta.
 */
public class LogAlerta {

	private String codigoRespuesta;
	private String tipoAlerta;
	private String destinoAlerta;

	/**
	 * Gets codigo respuesta.
	 *
	 * @return the codigo respuesta
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	/**
	 * Sets codigo respuesta.
	 *
	 * @param codigoRespuesta the codigo respuesta
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	/**
	 * Gets tipo alerta.
	 *
	 * @return the tipo alerta
	 */
	public String getTipoAlerta() {
		return tipoAlerta;
	}

	/**
	 * Sets tipo alerta.
	 *
	 * @param tipoAlerta the tipo alerta
	 */
	public void setTipoAlerta(String tipoAlerta) {
		this.tipoAlerta = tipoAlerta;
	}

	/**
	 * Gets destino alerta.
	 *
	 * @return the destino alerta
	 */
	public String getDestinoAlerta() {
		return destinoAlerta;
	}

	/**
	 * Sets destino alerta.
	 *
	 * @param destinoAlerta the destino alerta
	 */
	public void setDestinoAlerta(String destinoAlerta) {
		this.destinoAlerta = destinoAlerta;
	}

}
