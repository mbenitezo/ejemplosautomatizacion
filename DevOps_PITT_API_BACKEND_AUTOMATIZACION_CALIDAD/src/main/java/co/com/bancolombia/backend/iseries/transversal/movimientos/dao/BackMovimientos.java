package co.com.bancolombia.backend.iseries.transversal.movimientos.dao;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.movimientos.dto.DatosMovimiento;
import co.com.bancolombia.backend.iseries.transversal.movimientos.dto.Movimiento;
import co.com.bancolombia.backend.iseries.transversal.movimientos.dto.ValorEquivalenciaMovimiento;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExcelManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantOrientacion;

/**
 * The type Back movimientos.
 */
public class BackMovimientos {
	private static final Logger LOGGER = LogManager.getLogger(BackMovimientos.class.getName());

	private ExcelManager objExcel;

	/**
	 * Instantiates a new Back movimientos.
	 */
	public BackMovimientos() {
		objExcel = new ExcelManager();
		URL is = this.getClass().getClassLoader().getResource("VlrEquivalenciaSVP.xlsx");
		if (null != is) {
			String url = is.getPath();
			LOGGER.info(url);
			objExcel.strRutaArchivo(url);
		}
	}

	/**
	 * DESCRIPCIÓN: Este metodo es el encargado de armar la consulta SQL, recuperar
	 * los datos obtenidos de la consulta.
	 *
	 * @param strCuentaDeposito              the str cuenta deposito
	 * @param strTipCtaDeposito              debe ser "1" Corriente, "7" Ahorro
	 * @param strValorTransferir             the str valor transferir
	 * @param strNaturaleza                  the str naturaleza
	 * @param strHoraMovimiento              the str hora movimiento
	 * @param objValorEquivalenciaMovimiento the obj valor equivalencia movimiento
	 *
	 * @return variable "resultadoMovimiento" que retorna "true" y/0 "false".
	 *
	 * @throws SQLException the sql exception
	 */
	public Movimiento consultarMovimiento(String strCuentaDeposito, String strTipCtaDeposito, String strValorTransferir, String strNaturaleza, String strHoraMovimiento, ValorEquivalenciaMovimiento objValorEquivalenciaMovimiento) throws SQLException {

		Movimiento resultadoMovimiento = null;
		String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		String strCodigoEquivalencia = objValorEquivalenciaMovimiento.getStrMovCodTrn();
		double dblValorTransferir = Double.parseDouble(strValorTransferir);
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		strValorTransferir = decimalFormat.format(dblValorTransferir).replace(",", ".");
		ResultSet objResult = null;

		String sql = QueryManager.ISERIES.getString("SQL.SCIFFMRCMV.ConsultarMovimiento");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strCuentaDeposito);
			consulta.setString(2, strTipCtaDeposito);
			consulta.setString(3, strFecha);
			consulta.setString(4, strCodigoEquivalencia);
			consulta.setString(5, strValorTransferir);
			consulta.setString(6, strNaturaleza);
			consulta.setString(7, strHoraMovimiento);

			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {

					String strDescripcionTrn = objResult.getString("MOVNOMTRN").trim();
					String strvIndReverso = objResult.getString("MOVINDREV").trim();
					String strvCodOficina = objResult.getString("MOVOFCORG").trim();
					String strvValorCanje = objResult.getString("MOVVLRCNJ").trim();
					String strvConvenio = objResult.getString("MOVDOCTRN").trim();
					String strvTipoTrans = objResult.getString("MOVTPOTRN").trim();

					resultadoMovimiento = new Movimiento(strDescripcionTrn, strvIndReverso, strvCodOficina,
							strvValorCanje, strvConvenio, strvTipoTrans);
				}
			} else {
				resultadoMovimiento = new Movimiento();
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return resultadoMovimiento;
	}

	/**
	 * DESCRIPCION: Este metodo es el encargado de verificar el movimiento debito
	 * obtenido vs el resultado esperado segun la orientación del caso "ACIERTO" y/o
	 * "ERROR".
	 *
	 * @param objEsperado    the obj esperado
	 * @param objObtenido    the obj obtenido
	 * @param strOrientacion the str orientacion
	 *
	 * @return La variable "resultadoFinal" retorna "true" y/o "false"
	 */
	public boolean validarMovimientoDebito(Movimiento objEsperado, Movimiento objObtenido, String strOrientacion) {
		boolean resultadoFinal = false;
		if (objObtenido != null) {
			if (ConstantOrientacion.ACIERTO.equals(strOrientacion)) {

				String strvIndReversoObtenido = objObtenido.getStrvIndReverso();
				String strvIndReversoEsperado = objEsperado.getStrvIndReverso();
				resultadoFinal = validarResultado(strvIndReversoEsperado, strvIndReversoObtenido, resultadoFinal);

				String strDescripcionTrnObtenido = objObtenido.getStrDescripcionTrn();
				String strDescripcionTrnEsperado = objEsperado.getStrDescripcionTrn();
				resultadoFinal = validarResultado(strDescripcionTrnEsperado, strDescripcionTrnObtenido,
						resultadoFinal);

				// El siguiente campo strvCodOficinaEsperado lo utiliza recaudos.
				String strvCodOficinaEsperado = objEsperado.getStrvCodOficina();
				if (StringUtils.isNotBlank(strvCodOficinaEsperado)) {
					String strvCodOficinaObtenido = objObtenido.getStrvCodOficina();
					resultadoFinal = validarResultado(strvCodOficinaEsperado, strvCodOficinaObtenido,
							resultadoFinal);
				}
				String strvValorCanjeEsperado = objEsperado.getStrvValorCanje();
				if (StringUtils.isNotBlank(strvValorCanjeEsperado)) {
					String strvValorCanjeObtenido = objObtenido.getStrvValorCanje();
					resultadoFinal = validarResultado(strvValorCanjeEsperado, strvValorCanjeObtenido,
							resultadoFinal);
				}
				String strvTipoTransEsperado = objEsperado.getStrvTipoTrans();
				if (StringUtils.isNotBlank(strvTipoTransEsperado)) {
					String strvTipoTransObtenido = objObtenido.getStrvTipoTrans();
					resultadoFinal = validarResultado(strvTipoTransEsperado, strvTipoTransObtenido,
							resultadoFinal);
				}
				String strvConvenioEsperado = objEsperado.getStrvConvenio();
				if (StringUtils.isNotBlank(strvConvenioEsperado)) {
					String strvTipoTransObtenido = objObtenido.getStrvTipoTrans();
					resultadoFinal = validarResultado(strvTipoTransEsperado, strvTipoTransObtenido,
							resultadoFinal);
				}
			}
		} else {
			if (ConstantOrientacion.ERROR.equals(strOrientacion)) {
				resultadoFinal = true;
			}
		}
		return resultadoFinal;
	}

	/**
	 * DESCRIPCION: Este metodo es el encargado de verificar el movimiento credito
	 * obtenido vs el resultado esperado segun la orientación del caso "ACIERTO" y/o
	 * "ERROR
	 *
	 * @param objEsperado    the obj esperado
	 * @param objObtenido    the obj obtenido
	 * @param strOrientacion the str orientacion
	 *
	 * @return La variable "resultadoFinal" retorna "true" y/o "false"
	 */
	public boolean validarMovimientoCredito(Movimiento objEsperado, Movimiento objObtenido, String strOrientacion) {
		boolean resultadoFinal = false;
		if (objObtenido != null) {
			if (ConstantOrientacion.ACIERTO.equals(strOrientacion)) {
				String strvIndReversoObtenido = objObtenido.getStrvIndReverso();
				String strvIndReversoEsperado = objEsperado.getStrvIndReverso();
				resultadoFinal = validarResultado(strvIndReversoEsperado, strvIndReversoObtenido, resultadoFinal);
				String strDescripcionTrnObtenido = objObtenido.getStrDescripcionTrn();
				String strDescripcionTrnEsperado = objEsperado.getStrDescripcionTrn();
				resultadoFinal = validarResultado(strDescripcionTrnEsperado, strDescripcionTrnObtenido,
						resultadoFinal);
			}
		} else {
			if (ConstantOrientacion.ERROR.equals(strOrientacion)) {
				resultadoFinal = true;
			}
		}
		return resultadoFinal;
	}

	/**
	 * DESCRIPCION: Metodo para devolver el resultado de los campos esperados y
	 * obtenidos de la prueba.
	 *
	 * @param strEsperado:
	 * @param strObtenido
	 * @param resultadoAntes
	 *
	 * @return La variable "resultado" retorna "true" y/o "false"
	 */
	private boolean validarResultado(String strEsperado, String strObtenido, boolean resultadoAntes) {
		if (resultadoAntes && !strObtenido.isEmpty()) {
			return strEsperado.equalsIgnoreCase(strObtenido);
		} else {
			return resultadoAntes;
		}
	}

	/**
	 * DESCRIPCIÓN: Este metodo es el encargado armar el codigo de equivalencia para
	 * transferencias
	 *
	 * @param strNaturaleza        the str naturaleza
	 * @param strTipoCuenta        debe ser "D" y/o "S"
	 * @param strCodigoTransaccion the str codigo transaccion
	 *
	 * @return return this.buscarValorEquivalencia(strCodigo, strNaturaleza);
	 */
	public ValorEquivalenciaMovimiento buscarValorEquivalencia(String strNaturaleza, String strTipoCuenta,
	                                                           String strCodigoTransaccion) {
		String strCodigo = String.format("%s%s", strTipoCuenta, strCodigoTransaccion);
		return buscarValorEquivalencia(strCodigo, strNaturaleza);
	}

	/**
	 * DESCRIPCIÓN: Este metodo es el encargado armar el codigo de equivalencia para
	 * pagos de tarjetas de credito
	 *
	 * @param strNaturaleza        debe ser "D" Debito y/o "C" Credito
	 * @param strTipoCuenta        debe ser "D" Corriente y/o "S" Ahorro
	 * @param strCodigoTransaccion the str codigo transaccion
	 * @param strTipoTarjeta       debe ser "A" AMEX, "V" Visa y/o "M" MasterCard
	 * @param strTipoPago          debe ser "COP" Pesos y/o "USD" Dolar
	 *
	 * @return Retorna un objeto ValorEquivalenciaMovimiento que contine la descripción del movimiento y el código del movimiento.
	 */
	public ValorEquivalenciaMovimiento buscarValorEquivalencia(String strNaturaleza, String strTipoCuenta,
	                                                           String strCodigoTransaccion, String strTipoTarjeta, String strTipoPago) {
		String strCodigo = String.format("%s%s%s%s", strTipoCuenta, strCodigoTransaccion, strTipoTarjeta, strTipoPago);
		return buscarValorEquivalencia(strCodigo, strNaturaleza);
	}

	/**
	 * DESCRIPCIÓN: Metodo encargado de consultar y extraer la descripción y codigo
	 * del movimiento
	 *
	 * @param strCodigo
	 * @param strNaturaleza
	 *
	 * @return resultMov = new ValorEquivalenciaMovimiento(strMovCodTrn,
	 * strMovDescrip);
	 */
	private ValorEquivalenciaMovimiento buscarValorEquivalencia(String strCodigo, String strNaturaleza) {
		ValorEquivalenciaMovimiento resultMov = null;
		String strQuery = "select * from valor_equivalencia where CodigoTransaccion = '" + strCodigo + "'";
		Recordset objRecord = this.objExcel.leerExcel(strQuery);
		try {
			objRecord.moveNext();
			String strMovCodTrn = ConstantManager.VACIO;
			String strMovDescrip = ConstantManager.VACIO;
			if (ConstantManager.LETRA_C.equals(strNaturaleza)) {
				strMovCodTrn = objRecord.getField("MovCodTrnCredito").trim();
				strMovDescrip = objRecord.getField("MovDescripCredito").trim();
			} else if (ConstantManager.LETRA_D.equals(strNaturaleza)) {
				strMovCodTrn = objRecord.getField("MovCodTrnDebito").trim();
				strMovDescrip = objRecord.getField("MovDescripDebito").trim();
			}
			resultMov = new ValorEquivalenciaMovimiento(strMovCodTrn, strMovDescrip);
		} catch (FilloException e) {
			LOGGER.info(e.getMessage());
		}
		return resultMov;

	}

	/**
	 * <pre>
	 * <b>Descripción:</b>
	 * Componente encargado de conusltar los primeros 20 movimientos de una cuenta deposito
	 * se requiere permisos a los siguientes archivos:
	 * VISIONR.DHIS, VISIONR.DTRCDL0, SCILIBRAMD.SCIFFMRCMV
	 *
	 * @param strNumeroCuenta - Número de la cuenta deposito ej: "40613569001"
	 * @param strTipoCuenta - Tipo de la cuenta deposito : "S" o "D"
	 * @return Se retorna una lista con 20 movimientos.
	 * @throws SQLException the sql exception
	 */
	public List<DatosMovimiento> consultaMovimientosSCIFFMRCMV(String strNumeroCuenta, String strTipoCuenta) throws SQLException {
		String strTipoCuentaCMV = ConstantManager.VACIO;
		List<DatosMovimiento> listaMovimientos = new ArrayList<>();
		ResultSet objResult = null;

		if (strTipoCuenta.equals("S")) {
			strTipoCuentaCMV = "7";
		}
		if (strTipoCuenta.equals("D")) {
			strTipoCuentaCMV = "1";
		}

		strNumeroCuenta = strNumeroCuenta.replace(ConstantManager.GUION, ConstantManager.VACIO);
		String sql = QueryManager.ISERIES.getString("SQL.SCIFFMRCMV.consultaMovimientos");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consultas = conexion.prepareStatement(sql)) {
			consultas.setString(1, strNumeroCuenta);
			consultas.setString(2, strTipoCuenta);
			consultas.setString(3, strTipoCuentaCMV);

			objResult = consultas.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {

					String strFechaMovimiento = objResult.getString("FECHA").trim();
					String strDescripcion = objResult.getString("DESCRIP").trim();
					String strValorMovimiento = objResult.getString("VLRTOTAL").trim();

					if (objResult.getString("TIPOTRN").trim().equals("C")) {
						strValorMovimiento = "-" + strValorMovimiento;
					}

					DatosMovimiento datosMovimiento = new DatosMovimiento(strFechaMovimiento, strDescripcion,
							strValorMovimiento);
					listaMovimientos.add(datosMovimiento);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
			return listaMovimientos;
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
	}

	/**
	 * <pre>
	 * <b>Descripción:</b>
	 * Componente que se encarga de validar la lista de movimeintos obtenida vs la lista esperada.
	 *
	 * @param listaObtenida - es la lista que se consulta previamente del lado backend
	 * @param listaEsperada - es la lista esperada del lado front, la cual se compara con lista            obtenida del lado backend
	 * @param strCanal - es el canal por el cual se recibe la lista esperada ya sea APP,            SVP o SVE.
	 * @return Se retorna un valor boolean "true" o "false"
	 */
	public Boolean validarMovimientos(List<DatosMovimiento> listaObtenida,
	                                  List<DatosMovimiento> listaEsperada, String strCanal) {
		boolean resultado = false;

		for (DatosMovimiento movimientosEsperados : listaEsperada) {

			String strFechaMovEsperado = movimientosEsperados.getStrFechaMovimiento();
			String strDescripcionMovEsperado = movimientosEsperados.getStrDescripcion().toUpperCase();
			String strValorMovEsperado = movimientosEsperados.getStrValorMovimiento();

			for (DatosMovimiento movimientosObtenidos : listaObtenida) {
				resultado = false;
				String strFechaMovObtenido = movimientosObtenidos.getStrFechaMovimiento();
				String strDescripcionMovObtenido = movimientosObtenidos.getStrDescripcion().toUpperCase();
				String strValorMovObtenido = movimientosObtenidos.getStrValorMovimiento();
				if (ConstantManager.APP.equals(strCanal) || ConstantManager.SVP.equals(strCanal) || ConstantManager.SVE.equals(strCanal)) {

					strFechaMovEsperado = strFechaMovEsperado.replace("/", ConstantManager.VACIO);
					strValorMovEsperado = strValorMovEsperado.replace(",", ConstantManager.VACIO);

					if (strFechaMovEsperado.equals(strFechaMovObtenido)
							&& strDescripcionMovEsperado.equals(strDescripcionMovObtenido)
							&& strValorMovEsperado.equals(strValorMovObtenido)) {
						resultado = true;
						LOGGER.info("Valor obtenido es igual al esperado \n" + "Valor Esperado : "
								+ strFechaMovEsperado + " | " + strDescripcionMovEsperado + " | " + strValorMovEsperado
								+ " \n" + "Valor Obtenido : " + strFechaMovObtenido + " | " + strDescripcionMovObtenido
								+ " | " + strValorMovObtenido + " \n");
						break;
					}
				}
			}
			if (!resultado) {
				LOGGER.info("El valor esperado no fue encontrado \n" + "Valor Esperado : " + strFechaMovEsperado
						+ " | " + strDescripcionMovEsperado + " | " + strValorMovEsperado + " \n");
				break;
			}
		}
		return resultado;
	}
}