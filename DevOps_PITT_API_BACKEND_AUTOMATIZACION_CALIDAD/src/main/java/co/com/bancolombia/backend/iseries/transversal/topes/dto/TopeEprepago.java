package co.com.bancolombia.backend.iseries.transversal.topes.dto;

/**
 * The type Tope eprepago.
 */
public class TopeEprepago {

	//Valor mínimo y máximo carga
	private String cargaMin;
	private String cargaMax;

	//Valor mínimo y máxim descarga
	private String descargaMin;
	private String descargaMax;

	/**
	 * Instantiates a new Tope eprepago.
	 *
	 * @param cargaMin    the carga min
	 * @param cargaMax    the carga max
	 * @param descargaMin the descarga min
	 * @param descargaMax the descarga max
	 */
	public TopeEprepago(String cargaMin, String cargaMax, String descargaMin, String descargaMax) {
		this.cargaMin = cargaMin;
		this.cargaMax = cargaMax;
		this.descargaMin = descargaMin;
		this.descargaMax = descargaMax;
	}

	/**
	 * Gets carga max.
	 *
	 * @return the cargaMax
	 */
	public String getCargaMax() {
		return cargaMax;
	}

	/**
	 * Gets carga min.
	 *
	 * @return the cargaMin
	 */
	public String getCargaMin() {
		return cargaMin;
	}

	/**
	 * Gets descarga max.
	 *
	 * @return the descargaMax
	 */
	public String getDescargaMax() {
		return descargaMax;
	}

	/**
	 * Gets descarga min.
	 *
	 * @return the descargaMin
	 */
	public String getDescargaMin() {
		return descargaMin;
	}
}
