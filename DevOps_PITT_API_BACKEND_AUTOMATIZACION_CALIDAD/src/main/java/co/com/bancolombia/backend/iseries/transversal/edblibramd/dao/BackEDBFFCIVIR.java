package co.com.bancolombia.backend.iseries.transversal.edblibramd.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.edblibramd.dto.DatosCuenta;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Back edbffcivir.
 */
public class BackEDBFFCIVIR {

	private static final Logger LOGGER = LogManager.getLogger(BackEDBFFCIVIR.class.getName());

	/**
	 * <b>Descripción:</b> Método que permite obtener del archivo EDBLIBRAMD.EDBFFCIVIR, datos de inversión
	 * virtual que tiene un cliente tales como Número de cuenta y Saldo; utilizando como datos de entrada el
	 * documento del cliente y el trace de la transacción. El resultado se almacena en un objeto tipo lista.
	 *
	 * @param strDocumento: ésta variable almacena el documento de identificación del cliente
	 * @param strTrace:     ésta variable almacena el código único de identificación de la transacción
	 *
	 * @return lista de cuentas de inversión virtual del cliente consultado en el backend
	 */


	public List<DatosCuenta> consultaEDBFFPREST(String strDocumento, String strTrace) throws SQLException {
		List<DatosCuenta> listaInversiones = new ArrayList<>();
		ResultSet objResult = null;
		String sql = QueryManager.ISERIES.getString("SQL.EDBFFCIVIR.consultaEDBFFPREST");

		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strDocumento);
			consulta.setString(2, strTrace);
			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strNroCuenta = objResult.getString("CIVIRNUMIN").trim();
					String strSaldoDisp = objResult.getString("CIVIRCAPIN").trim();
					DatosCuenta datosCuenta = new DatosCuenta(strNroCuenta, ConstantManager.VACIO, strSaldoDisp);
					listaInversiones.add(datosCuenta);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return listaInversiones;
	}

	public Boolean validarResultado(List<DatosCuenta> listaObtenida, List<DatosCuenta> listaEsperada, String strCanal) {
		boolean resultado = false;
		for (DatosCuenta cuentaEsperada : listaEsperada) {
			String strNroCuentaEsperado = cuentaEsperada.getStrNroCuenta();
			String strSaldoEsperado = cuentaEsperada.getStrSaldoDisp();
			String strTipoEsperado = cuentaEsperada.getStrTipoCuenta();
			for (DatosCuenta cuentaObtenida : listaObtenida) {
				resultado = false;
				String strNroCuentaObtenido = cuentaObtenida.getStrNroCuenta();
				String strSaldoObtenido = cuentaObtenida.getStrSaldoDisp();
				if (ConstantManager.APP.equals(strCanal) || ConstantManager.SVP.equals(strCanal)) {
					strTipoEsperado = strTipoEsperado.toUpperCase();
					strSaldoEsperado = strSaldoEsperado.replace(",", "");
					strSaldoEsperado = strSaldoEsperado.replace(".", ",");

					if (strNroCuentaEsperado.equals(strNroCuentaObtenido) && strSaldoEsperado.equals(strSaldoObtenido)) {
						resultado = true;
						LOGGER.info(ExceptionCodesManager.SAME_PRODUCT.getMsg());
						break;
					}
				}
				if (ConstantManager.SVE.equals(strCanal)) {
					resultado = true;
				}
			}
			if (!resultado) {
				LOGGER.info(ExceptionCodesManager.VALUE_NOTFOUND.getMsg());
				break;
			}
		}
		return resultado;
	}
}
