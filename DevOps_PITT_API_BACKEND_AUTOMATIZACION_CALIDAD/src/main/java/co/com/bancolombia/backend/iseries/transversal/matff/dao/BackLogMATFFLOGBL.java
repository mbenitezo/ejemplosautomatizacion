package co.com.bancolombia.backend.iseries.transversal.matff.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.matff.dto.LogClavePrincipal;
import co.com.bancolombia.backend.utilidades.DateManager;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * The type Back log matfflogbl.
 */
public class BackLogMATFFLOGBL {
	private static final Logger LOGGER = LogManager.getLogger(BackLogMATFFLOGBL.class.getName());

	/**
	 * Método que consulta el Log que contiene el estado de la clave principal de un cliente
	 * según la autenticación ejecutada (filtro por fecha y hora)
	 *
	 * @param strSistema   :   Recibe el sistema para hacer la consulta.
	 * @param strDocumento : Recibe el documento del cliente para hacer la consulta.
	 * @param strTipoDoc   :   Recibe el tipo docuemnto del cliente para hacer la consulta.
	 * @param strHora      :      Recibe la hora en que se efectuo la transacción.
	 *
	 * @return LogClavePrincipal log clave principal
	 *
	 * @throws SQLException the sql exception
	 */
	public LogClavePrincipal consultarLogClvPpal(String strSistema, String strDocumento, String strTipoDoc, String strHora) throws SQLException {
		LogClavePrincipal objLogClavePpal = null;
		ResultSet objResult = null;
		String strFecha = DateManager.obtenerFechaSistema(ConstantManager.FORMATO_FECHA);
		String sql = QueryManager.ISERIES.getString("SQL.MATFFLOGBL.consultarLogClvPpal");
		try (Connection conexion = IseriesConnectionManager.getConnection();
		     PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, strSistema);
			consulta.setString(2, strDocumento);
			consulta.setString(3, strTipoDoc);
			consulta.setString(4, strFecha);
			consulta.setString(5, strHora);
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					String strCodigoClvTrn = objResult.getString("LGCDGTRN").trim();
					String strDescripcionClvTrn = objResult.getString("LGDSCTRN").trim();
					String strEstadoClv = objResult.getString("LGESTADO").trim();
					String strDescripcionEstClv = objResult.getString("LGDESEST").trim();
					objLogClavePpal = new LogClavePrincipal(strCodigoClvTrn, strDescripcionClvTrn, strEstadoClv, strDescripcionEstClv);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				objLogClavePpal = new LogClavePrincipal();
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
		return objLogClavePpal;
	}

	/**
	 * Método que se encarga de validar código y la descricion de la transacción de la clave principal
	 * además del validar el estado y la descripción de la clave un cliente.
	 * De acuerdo al estado de la clave extraido del data driven se realizan las validaciones
	 * de los campos.
	 *
	 * @param objValorObtenido : valor del dato obtenido (extraido de la base de datos)
	 * @param strEstadoClave   :   los estados validos son (CLAVENV, ACTIVAPIF, CLAVEREINICIO)
	 *
	 * @return resultadoValidacion boolean
	 */
	public boolean validarLogClave(LogClavePrincipal objValorObtenido, String strEstadoClave) {
		boolean resultadoValidacion = true;
		String strCodigoTrn = ConstantManager.VACIO;
		String strDescripcionTrn = ConstantManager.VACIO;
		String strEstado = ConstantManager.VACIO;
		String strDescripcionEstado = ConstantManager.VACIO;

		switch (strEstadoClave) {

			case "CLAVENV":
				//Variables descriptivas de los campos relacionados con los estados
				strCodigoTrn = "12";
				strDescripcionTrn = ConstantManager.ACTUALIZA_INTENTOS;
				strEstado = ConstantManager.LETRA_A;
				strDescripcionEstado = ConstantManager.ACTIVA;
				break;

			case "ACTIVAPIF":
				strCodigoTrn = "12";
				strDescripcionTrn = ConstantManager.ACTUALIZA_INTENTOS;
				strEstado = "F";
				strDescripcionEstado = ConstantManager.BLOQUE_INT_FALLIDOS;
				break;

			case "CLAVEREINICIO":
				strCodigoTrn = "11";
				strDescripcionTrn = ConstantManager.REINICIO_INTENTOS;
				strEstado = ConstantManager.LETRA_A;
				strDescripcionEstado = ConstantManager.ACTIVA;
				break;
		}

		//Validación de código de la transacción
		String strCodigoClvTrnEsperado = strCodigoTrn;
		String strCodigoClvTrnObtenido = objValorObtenido.getStrCodigoClvTrn();
		resultadoValidacion = validarResultado(strCodigoClvTrnEsperado, strCodigoClvTrnObtenido, resultadoValidacion);

		//Validación de la descripción del código de la transacción
		String strDescripcionClvTrnEsperado = strDescripcionTrn;
		String strDescripcionClvTrnObtenido = objValorObtenido.getStrDescripcionClvTrn();
		resultadoValidacion = validarResultado(strDescripcionClvTrnEsperado, strDescripcionClvTrnObtenido, resultadoValidacion);

		//Validación del estado de la transacción
		String strEstadoClvEsperado = strEstado;
		String strEstadoClvObtenido = objValorObtenido.getStrEstadoClv();
		resultadoValidacion = validarResultado(strEstadoClvEsperado, strEstadoClvObtenido, resultadoValidacion);

		//Validación de la descripción del estado de la transacción
		String strDescripcionEstClvEsperado = strDescripcionEstado;
		String strDescripcionEstClvObtenido = objValorObtenido.getStrDescripcionEstClv();
		resultadoValidacion = validarResultado(strDescripcionEstClvEsperado, strDescripcionEstClvObtenido, resultadoValidacion);

		return resultadoValidacion;
	}

	private boolean validarResultado(String strEsperado, String strObtenido, boolean resultadoAnterior) {
		if (resultadoAnterior && !strObtenido.isEmpty()) {
			return strEsperado.equalsIgnoreCase(strObtenido);
		} else {
			return resultadoAnterior;
		}
	}
}
