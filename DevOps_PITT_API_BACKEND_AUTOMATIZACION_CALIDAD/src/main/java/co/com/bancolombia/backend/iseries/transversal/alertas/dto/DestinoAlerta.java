package co.com.bancolombia.backend.iseries.transversal.alertas.dto;

import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Destino alerta.
 */
public class DestinoAlerta {

	private String correoAlerta;
	private String movilAlerta;
	private String indicadorActivo;

	/**
	 * CONSTRUCTOR: se encarga de capturar los diferentes destinos de alertas (movil
	 * o correo) y guarda si la alerta esta activa
	 *
	 * @param correoAlerta    the correo alerta
	 * @param movilAlerta     the movil alerta
	 * @param indicadorActivo the indicador activo
	 */
	public DestinoAlerta(String correoAlerta, String movilAlerta, String indicadorActivo) {
		this.correoAlerta = correoAlerta;
		this.movilAlerta = movilAlerta;
		this.indicadorActivo = indicadorActivo;
	}

	/**
	 * Instantiates a new Destino alerta.
	 */
	public DestinoAlerta() {
		correoAlerta = ConstantManager.ESPACIO_BLANCO;
		movilAlerta = ConstantManager.ESPACIO_BLANCO;
		indicadorActivo = ConstantManager.ESPACIO_BLANCO;
	}

	/**
	 * Gets correo alerta.
	 *
	 * @return the correo alerta
	 */
	public String getCorreoAlerta() {
		return correoAlerta;
	}

	/**
	 * Sets correo alerta.
	 *
	 * @param correoAlerta the correo alerta
	 */
	public void setCorreoAlerta(String correoAlerta) {
		this.correoAlerta = correoAlerta;
	}

	/**
	 * Gets movil alerta.
	 *
	 * @return the movil alerta
	 */
	public String getMovilAlerta() {
		return movilAlerta;
	}

	/**
	 * Sets movil alerta.
	 *
	 * @param movilAlerta the movil alerta
	 */
	public void setMovilAlerta(String movilAlerta) {
		this.movilAlerta = movilAlerta;
	}

	/**
	 * Gets indicador activo.
	 *
	 * @return the indicador activo
	 */
	public String getIndicadorActivo() {
		return indicadorActivo;
	}

	/**
	 * Sets indicador activo.
	 *
	 * @param indicadorActivo the indicador activo
	 */
	public void setIndicadorActivo(String indicadorActivo) {
		this.indicadorActivo = indicadorActivo;
	}

}
