package co.com.bancolombia.backend.iseries.transversal.fondos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.excepciones.BackEndExceptions;
import co.com.bancolombia.backend.iseries.transversal.fondos.dto.DatosSolarc;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;
import co.com.bancolombia.backend.utilidades.QueryManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantFondo;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;

/**
 * The type Back solarc para transacciones de fondos de inversión.
 */

public class BackSolarc {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(BackSolarc.class.getName());

	/**
	 * Módulo que consulta el archivo SOLARC para la librería FIDLIBC1MD en el cual
	 * se encuentra el registro de las transacciones relacionadas con fondos de
	 * inversión (Adiciones, Retiros, Constitución y Cancelación).
	 *
	 * @param strDocumento
	 *            : Recibe el documento del cliente para hacer la consulta.
	 * @param strNroProducto
	 *            the str nro producto
	 * @param strCuentaDepos
	 *            the str cuenta depos
	 * @param strCodTrn
	 *            : Recibe el código de la transacción
	 * @return objSolarc :
	 * @throws SQLException
	 *             the sql exception
	 */

	public DatosSolarc consultaTrxFondo(String strDocumento, String strNroProducto, String strCuentaDepos,
			String strCodTrn, String libreria) throws SQLException {
		DatosSolarc objSolarc = null;
		String codigoTrn = castCodigoTransaccion(strCodTrn);
		String numOficina = strNroProducto.substring(0, 4);
		String numEncargo = strNroProducto.substring(4);
		ResultSet objResult = null;

		String sql = QueryManager.ISERIES.getString("SQL.SOLARC.consultaFondo");
		String sqlFormat = String.format(sql, libreria);
		try (Connection conexion = IseriesConnectionManager.getConnection();
				PreparedStatement consulta = conexion.prepareStatement(sqlFormat)) {
			consulta.setString(1, strDocumento);
			consulta.setString(2, numOficina);
			consulta.setString(3, numEncargo);
			consulta.setString(4, strCuentaDepos);
			consulta.setString(5, codigoTrn);
			objResult = consulta.executeQuery();

			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					double strValorTransferir = objResult.getDouble("SOLEFE");
					String strCodTransaccion = objResult.getString("SOLCO2").trim();
					objSolarc = new DatosSolarc(strValorTransferir, strCodTransaccion);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
				objSolarc = new DatosSolarc();
			}
			return objSolarc;
		} finally {
			if (objResult != null) {
				objResult.close();
			}

		}
	}

	/**
	 * Método que se encarga de validar el valor transado para los casos de apertura
	 * (CON), adición (ADI) y retiro (RET) de un fondo de inversión.
	 *
	 * @param strValorTransferir
	 *            the str valor transferir
	 * @param objValorObtenido
	 *            : valor tranferido (extraido de la base de datos)
	 * @return resultadoValidacion boolean
	 */

	public boolean verifcarVlrEfectivo(String strValorTransferir, DatosSolarc objValorObtenido) {
		boolean resultadoValidacion = false;
		String codTransaccion = objValorObtenido.getStrCodTransaccion();
		double valorTransferirObtenido = objValorObtenido.getStrValorTransferir();
		double valorTransferirEsperado = 0;
		if (!strValorTransferir.equals(ConstantManager.VACIO)) {
			valorTransferirEsperado = Double.parseDouble(strValorTransferir);
		}
		if (codTransaccion.equalsIgnoreCase(ConstantFondo.CODIGO_CANCELACION)) {
			resultadoValidacion = valorTransferirObtenido == 0;
		} else {
			resultadoValidacion = validarResultado(valorTransferirEsperado, valorTransferirObtenido);
		}
		return resultadoValidacion;
	}

	/**
	 * Validar resultado.
	 *
	 * @param esperado
	 *            the esperado
	 * @param obtenido
	 *            the obtenido
	 * @return true, if successful
	 */
	private boolean validarResultado(double esperado, double obtenido) {

		if (esperado != 0) {
			return esperado == obtenido;
		} else {
			return false;
		}
	}

	/**
	 * Cast codigo transaccion.
	 *
	 * @param strCodTrn
	 *            the str cod trn
	 * @return the string
	 */
	private String castCodigoTransaccion(String strCodTrn) {
		String codigoTrn = "";
		switch (strCodTrn) {
		case "0439":
		case "0539":
			codigoTrn = ConstantFondo.CODIGO_ADICION;
			break;
		case "0449":
		case "0549":
			codigoTrn = ConstantFondo.CODIGO_RETIRO;
			break;
		case "0513":
			codigoTrn = ConstantFondo.CODIGO_CONSTITUCION;
		case "0516":
			codigoTrn = ConstantFondo.CODIGO_CANCELACION;
		default:
			throw new BackEndExceptions(ExceptionCodesManager.TRANS_NOT_FOUND.getMsg());
		}
		return codigoTrn;
	}

}