package co.com.bancolombia.backend.iseries.transversal.movimientos.dto;

/**
 * The type Datos movimiento.
 */
public class DatosMovimiento {

	private String strFechaMovimiento;
	private String strDescripcion;
	private String strValorMovimiento;

	/**
	 * Instantiates a new Datos movimiento.
	 *
	 * @param strFechaMovimiento the str fecha movimiento
	 * @param strDescripcion     the str descripcion
	 * @param strValorMovimiento the str valor movimiento
	 */
	public DatosMovimiento(String strFechaMovimiento, String strDescripcion, String strValorMovimiento) {
		this.strFechaMovimiento = strFechaMovimiento;
		this.strDescripcion = strDescripcion;
		this.strValorMovimiento = strValorMovimiento;
	}

	/**
	 * Gets str fecha movimiento.
	 *
	 * @return the str fecha movimiento
	 */
	public String getStrFechaMovimiento() {
		return strFechaMovimiento;
	}

	/**
	 * Sets str fecha movimiento.
	 *
	 * @param strFechaMovimiento the str fecha movimiento
	 */
	public void setStrFechaMovimiento(String strFechaMovimiento) {
		this.strFechaMovimiento = strFechaMovimiento;
	}

	/**
	 * Gets str descripcion.
	 *
	 * @return the str descripcion
	 */
	public String getStrDescripcion() {
		return strDescripcion;
	}

	/**
	 * Sets str descripcion.
	 *
	 * @param strDescripcion the str descripcion
	 */
	public void setStrDescripcion(String strDescripcion) {
		this.strDescripcion = strDescripcion;
	}

	/**
	 * Gets str valor movimiento.
	 *
	 * @return the str valor movimiento
	 */
	public String getStrValorMovimiento() {
		return strValorMovimiento;
	}

	/**
	 * Sets str valor movimiento.
	 *
	 * @param strValorMovimiento the str valor movimiento
	 */
	public void setStrValorMovimiento(String strValorMovimiento) {
		this.strValorMovimiento = strValorMovimiento;
	}
}
