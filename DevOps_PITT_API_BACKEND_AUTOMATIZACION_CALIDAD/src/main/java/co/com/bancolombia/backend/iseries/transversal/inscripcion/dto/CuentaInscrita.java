package co.com.bancolombia.backend.iseries.transversal.inscripcion.dto;

public class CuentaInscrita {
	private String strCanal;
	private String strTipoCuenta;
	private String strNumCuenta;
	private String strTipoDocPropietario;
	private String strNumDocPropietario;
	private String strFechaInscripcion;
	private String strTipoDocIns;
	private String strNumDocIns;
	private String strEstCuenta;
	private String strDescripEstCuenta;
	
	
	public CuentaInscrita() {
	}
	
	public CuentaInscrita(String strCanalBack, String strTipoCuentaBack, String strNumCuentaBack,
			String strTipoDocPropieBack, String strNumDocPropieBack, String strFechaInsBack, String strTipoDocInsBack,
			String strNumDocInsBack, String strEstCuentaBack, String strDescripEstCuentaBack) {
		this.strCanal = strCanalBack;
		this.strTipoCuenta = strTipoCuentaBack;
		this.strNumCuenta = strNumCuentaBack;
		this.strTipoDocPropietario = strTipoDocPropieBack;
		this.strNumDocPropietario = strNumDocPropieBack;
		this.strFechaInscripcion = strFechaInsBack;
		this.strTipoDocIns = strTipoDocInsBack;
		this.strNumDocIns = strNumDocInsBack;
		this.strEstCuenta = strEstCuentaBack;
		this.strDescripEstCuenta = strDescripEstCuentaBack;
	}
	public String getStrCanal() {
		return strCanal;
	}
	public void setStrCanal(String strCanalBack) {
		this.strCanal = strCanalBack;
	}
	public String getStrTipoCuenta() {
		return strTipoCuenta;
	}
	public void setStrTipoCuenta(String strTipoCuentaBack) {
		this.strTipoCuenta = strTipoCuentaBack;
	}
	public String getStrNumCuenta() {
		return strNumCuenta;
	}
	public void setStrNumCuenta(String strNumCuentaBack) {
		this.strNumCuenta = strNumCuentaBack;
	}
	public String getStrTipoDocPropietario() {
		return strTipoDocPropietario;
	}
	public void setStrTipoDocPropietario(String strTipoDocPropieBack) {
		this.strTipoDocPropietario = strTipoDocPropieBack;
	}
	public String getStrNumDocPropietario() {
		return strNumDocPropietario;
	}
	public void setStrNumDocPropietario(String strNumDocPropieBack) {
		this.strNumDocPropietario = strNumDocPropieBack;
	}
	public String getStrFechaInscripcion() {
		return strFechaInscripcion;
	}
	public void setStrFechaInscripcion(String strFechaInsBack) {
		this.strFechaInscripcion = strFechaInsBack;
	}
	public String getStrTipoDocIns() {
		return strTipoDocIns;
	}
	public void setStrTipoDocIns(String strTipoDocInsBack) {
		this.strTipoDocIns = strTipoDocInsBack;
	}
	public String getStrNumDocIns() {
		return strNumDocIns;
	}
	public void setStrNumDocIns(String strNumDocInsBack) {
		this.strNumDocIns = strNumDocInsBack;
	}
	public String getStrEstCuenta() {
		return strEstCuenta;
	}
	public void setStrEstCuenta(String strEstCuentaBack) {
		this.strEstCuenta = strEstCuentaBack;
	}
	public String getStrDescripEstCuenta() {
		return strDescripEstCuenta;
	}
	public void setStrDescripEstCuenta(String strDescripEstCuentaBack) {
		this.strDescripEstCuenta = strDescripEstCuentaBack;
	}
	
	
		

}
