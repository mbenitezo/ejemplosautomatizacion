package co.com.bancolombia.backend.iseries.transversal.log.sti.dao;

import co.com.bancolombia.backend.conexion.iseries.IseriesConnectionManager;
import co.com.bancolombia.backend.iseries.transversal.log.sti.dto.LogSti;
import co.com.bancolombia.backend.utilidades.*;
import co.com.bancolombia.backend.utilidades.constant.ConstantLogCanal;
import co.com.bancolombia.backend.utilidades.constant.ConstantManager;
import co.com.bancolombia.backend.utilidades.constant.ConstantOrientacion;

import com.codoid.products.fillo.Recordset;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que se encarga de manejar todos los componentes del log STI se tocan
 * los archivos STILIBRAMD.STIFFLG%canal%
 *
 * @author david.c.gonzalez
 */
public class BackLogSti {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LogManager.getLogger(BackLogSti.class.getName());

	/** The obj excel. */
	private ExcelManager objExcel;

	/** The obj log sti. */
	private LogSti objLogSti;

	/** The mtx resultado esperado. */
	private List<List<String>> mtxResultadoEsperado;

	/** The mtx resultado obtenido. */
	private List<List<String>> mtxResultadoObtenido;

	/** The lista codigos trasacciones. */
	private List<String> listaCodigosTrasacciones;

	/** The str canal. */
	private String strCanal;

	/** The str ruta archivo. */
	private String strRutaArchivo;

	/**
	 * Instantiates a new back log sti.
	 */
	public BackLogSti(LogSti objLogSti, String strRutaPlantilla, String strNombreEvidencia) {
		this.objLogSti = objLogSti;
		objExcel = new ExcelManager();
		mtxResultadoEsperado = new ArrayList<>();
		mtxResultadoObtenido = new ArrayList<>();
		listaCodigosTrasacciones = new ArrayList<>();
		this.strRutaArchivo = strRutaPlantilla;
		crearCopiaEvidencia(strNombreEvidencia);

		listaCodigoTransacciones();

	}

	/**
	 * Lista codigo transacciones.
	 */
	private void listaCodigoTransacciones() {
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_AHRRO_FONDO.getId());
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_CORR_FONDO.getId());
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_FONDO_AHORR.getId());
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_FONDO_CORR.getId());

		listaCodigosTrasacciones.add(TransactionCodes.TRANS_1.getId());
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_2.getId());
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_3.getId());
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_4.getId());

		listaCodigosTrasacciones.add(TransactionCodes.TRANS_5.getId());
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_6.getId());
		listaCodigosTrasacciones.add(TransactionCodes.TRANS_7.getId());
	}

	/**
	 * Metodo que se encarga de trar todos las filas del excel (resultado esperado),
	 * siempre y cuando la columna del codigo este llena.
	 */
	private void traerResultadoEsperado() {
		String strQuery = String.format(QueryManager.EXCEL.getString("SQL.SELECT"), ConstantManager.PARAMETROS);
		boolean blnEsValido = true;
		try {
			Recordset objRecord = objExcel.leerExcel(strQuery);
			while (blnEsValido) {
				objRecord.moveNext();
				ArrayList<String> arrFilaActual = new ArrayList<>();
				String strEsperadomodulo = objRecord.getField("MODULO ESP").trim();
				String strEsperadoAccion = objRecord.getField("FLUJO ESPERADO").trim();
				String strEsperadocodigo = objRecord.getField(objLogSti.getStrCodigoError()).trim();
				if (StringUtils.isNotEmpty(strEsperadocodigo)) {
					arrFilaActual.add(strEsperadomodulo);
					arrFilaActual.add(strEsperadoAccion);
					arrFilaActual.add(strEsperadocodigo);
					mtxResultadoEsperado.add(arrFilaActual);
				}
			}
		} catch (Exception e) {
			blnEsValido = false;
		}
	}

	/**
	 * Metodo que se encarga de traer de la base de datos el resultado obtenido, si
	 * esta relacionado con fondos de inversion hace la busqueda en el archivo
	 * STILIBRAMD.STIFFLGAUD, de lo contrario depende del canal
	 *
	 * @throws SQLException
	 *             the SQL exception
	 */
	private void traerResultadoObtenido() throws SQLException {
		ResultSet objResult = null;
		String strTransCode = objLogSti.getStrCodigoTransaccion();
		if (listaCodigosTrasacciones.contains(strTransCode)) {
			strCanal = ConstantManager.CANAL_FONDO;
		}
		String sql = String.format(QueryManager.ISERIES.getString("SQL.STIFFLG.traerResultadoObtenido"), strCanal);
		try (Connection conexion = IseriesConnectionManager.getConnection();
				PreparedStatement consulta = conexion.prepareStatement(sql)) {
			consulta.setString(1, objLogSti.getStrTrace());
			objResult = consulta.executeQuery();
			if (objResult.isBeforeFirst()) {
				while (objResult.next()) {
					ArrayList<String> arrCurrentRow = new ArrayList<>();
					String strEsperadomodule = objResult.getString("MODRESPU").trim();
					String strEsperadoAction = objResult.getString("OBSERVA").split(ConstantManager.GUION)[0].trim();
					String strEsperadocode = objResult.getString("CODRESPU");
					arrCurrentRow.add(strEsperadomodule);
					arrCurrentRow.add(strEsperadoAction);
					arrCurrentRow.add(strEsperadocode);
					mtxResultadoObtenido.add(arrCurrentRow);
				}
			} else {
				LOGGER.info(ExceptionCodesManager.EMPTY_REQUEST.getMsg());
			}
		} finally {
			if (objResult != null) {
				objResult.close();
			}
		}
	}

	/**
	 * Merodo que se encarga de validar si el log sti esta correcto de acuerdo a lo
	 * que obtubo de la base de datos y lo que se parametrizo anteriormente en el
	 * archivo de excel.
	 *
	 * @param objLogSti
	 *            the obj log sti
	 * @return boolean boolean
	 * @throws SQLException
	 *             the sql exception
	 */
	public boolean verificarLogSti() throws SQLException {

		strCanal = objLogSti.getStrCanal();
		boolean result = false;
		traerResultadoEsperado();
		traerResultadoObtenido();
		if (!mtxResultadoEsperado.isEmpty() && !mtxResultadoObtenido.isEmpty()) {
			result = true;
			for (int i = 0; i < mtxResultadoEsperado.size(); i++) {
				List<String> rowEsperado = mtxResultadoEsperado.get(i);
				String modE = rowEsperado.get(0);
				String flujE = rowEsperado.get(1);
				String codE = rowEsperado.get(2);
				List<String> rowObtenido;
				String modO = ConstantManager.VACIO;
				String flujO = ConstantManager.VACIO;
				String codO = ConstantManager.VACIO;

				if (i < mtxResultadoObtenido.size()) {
					rowObtenido = mtxResultadoObtenido.get(i);
					modO = rowObtenido.get(0);
					flujO = rowObtenido.get(1);
					codO = rowObtenido.get(2);
				}

				String res = ConstantOrientacion.ERROR;
				if (modE.equals(modO) && flujE.equals(flujO) && codE.equals(codO)) {
					res = ConstantLogCanal.PASO;
				}
				String strQuery = String.format(QueryManager.EXCEL.getString("SQL.INSERT.validateLogSti"), modE, flujE,
						codE, modO, flujO, codO, res);

				objExcel.escribirExcel(strQuery);
				if (result && res.equals(ConstantOrientacion.ERROR)) {
					result = false;
				}
			}
		}
		return result;
	}

	/**
	 * Metodo que se encarga de crear la evidencia de acuerdo al nombre dado.
	 * internamente se concatena el caso de prueba que se este ejecutando.
	 *
	 * @param strNombreArchivo
	 *            the str nombre archivo
	 */
	private void crearCopiaEvidencia(String strNombreArchivo) {
		String strNameFileCopy = String.format(ConstantManager.FORMATO_COPIA, objLogSti.getStrCasoId(),
				strNombreArchivo);
		String strRutaArchivoEvidencia = objExcel.crearCopiaExcel(strRutaArchivo, objLogSti.getStrRutaEvidencia(),
				strNameFileCopy);

		objExcel.strRutaArchivo(strRutaArchivoEvidencia);
	}

}
