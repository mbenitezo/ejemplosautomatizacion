package co.com.bancolombia.backend.utilidades.constant;

import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;

/**
 * The Class ConstantLogCanal.
 */
public class ConstantLogCanal {

	/**
	 * Instantiates a new constant log canal.
	 */
	private ConstantLogCanal() {
		throw new IllegalStateException(ExceptionCodesManager.UTILITY_CLASS.getMsg());
	}

	/** The Constant TRAMA_INPUT. */
	public static final String TRAMA_INPUT = "TramaInput";

	/** The Constant TRAMA_OUTPUT. */
	public static final String TRAMA_OUTPUT = "TramaOutput";

	/**
	 * The Constant NO_APLICA.
	 */
	public static final String NO_APLICA = "NO APLICA";

	/**
	 * The Constant IGNORE.
	 */
	public static final String IGNORE = "IGNORE";

	/**
	 * The constant PASO.
	 */
	public static final String PASO = "PASÓ";

	/** The Constant CANAL_WWW. */
	public static final String CANAL_WWW = "WWW";

}
