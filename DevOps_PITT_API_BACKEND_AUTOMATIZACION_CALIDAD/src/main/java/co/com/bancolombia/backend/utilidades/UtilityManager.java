package co.com.bancolombia.backend.utilidades;

import org.apache.commons.lang3.StringUtils;

/**
 * The type Utility manager.
 */
public class UtilityManager {


	/**
	 * Formato valor string.
	 *
	 * @param valor the valor
	 *
	 * @return the string
	 */
	public static String formatoValor(String valor) {
		return StringUtils.replaceEach(valor, new String[]{",", "."}, new String[]{"", ""});
	}
}
