package co.com.bancolombia.backend.conexion.iseries;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import co.com.bancolombia.backend.excepciones.BackEndExceptions;
import co.com.bancolombia.backend.utilidades.ExceptionCodesManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * La clase IseriesConnectionManager se encarga de administrar las solicitudes de conexi&oacute;n de los Data Acess Object
 * para la ejecuci&oacute;n de sentencias SQL.
 *
 * @author Oscar Armando Vallejo Tovar
 */
public class IseriesConnectionManager {
	private static final Logger LOGGER = LogManager.getLogger(IseriesConnectionManager.class.getName());
	private static DataSource dataSource = null;

	private IseriesConnectionManager() {
		throw new IllegalStateException(ExceptionCodesManager.UTILITY_CLASS.getMsg());
	}

	/**
	 * El m&eacute;todo se encarga de obtener el DataSource del pool de conexiones
	 * de iseries
	 *
	 * @author Oscar Armando Vallejo Tovar
	 */
	private static void initConnection() {
		dataSource = IseriesConnectionPoolManager.getDataSource();

	}

	/**
	 * El m&eacute;todo se encarga de de la administracion de las conexiones
	 * solicitadas por los Data Acces Object
	 *
	 * @return connection conexi&oacute;n para ejecutar las sentencias SQL
	 *
	 * @author Oscar Armando Vallejo Tovar
	 */
	public static Connection getConnection() {
		Connection conexion = null;
		if (null == dataSource) {
			initConnection();
		}

		try {
			conexion = dataSource.getConnection();

		} catch (SQLException e) {
			throw new BackEndExceptions(ExceptionCodesManager.CONNECTION_FAILED.getMsg(), e);
		}
		IseriesConnectionPoolManager.connectionStatus();

		return conexion;
	}
}
