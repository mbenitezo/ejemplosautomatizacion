## A fantastic Todo application.

This application lets you do **great things**, like:

* Add new todo items
* Mark your items as complete
* Share your todo lists with your friends