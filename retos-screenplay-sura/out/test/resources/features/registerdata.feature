#Author: mbenitezo@choucairtesting.com
@version:Release-1
@Regression
Feature: As a user I want to register in the web Suraenlinea

@Register
Scenario: Register in the Web Suraenlinea
Given that User wants to access the Suraenlinea
When he performs the registration on the page
|documenttype          |documentnumber |expeditionyear  |name     	  |surname  |gender     |birthdate  |city               |direction         |cellphone  |email              |eps       |
|CÉDULA DE CIUDADANÍA  |1111111111     |2006-03-29      |PEDRO CORAL  |TABERA   |MASCULINO  |1987-09-25 |MEDELLIN, ANTIOQUIA|Calle 49 A #63-55 |3003003000 |correo1@gmail.com  |EPS SURA  |
Then verify that the screen register successful


  @color:green
  @salads
  @snacks
  @manual
  Scenario: Grow Granny Smith apples
    Given I have a field
    When I plant some Granny Smith apples trees
    Then some apples should grow
    And the apples should be green

  @color:red
  @snacks
  @Manual
  @manual-result:failed
  Scenario: Grow Red Delicious apples
    Given I have a field
    When I plant some Red Delicious apples trees
    Then some apples should grow
    And the apples should be red

  @color:red
  @snacks
  @Manual
  @manual-result:passed
  Scenario: Grow Juicy Red Delicious apples
    Given I have a field
    When I plant some Red Delicious apples trees
    Then some apples should grow
    And the apples should be red