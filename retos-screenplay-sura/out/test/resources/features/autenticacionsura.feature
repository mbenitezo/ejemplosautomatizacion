Feature: Authenticate in Suraenlinea

@Register
Scenario: Authenticate in the Web Suraenlinea
Given that User wants to access the SuraenLineaAutenticate
When he performs the authentication on the page
|documenttype     |documentnumber |pass   |
|CEDULA           |71768154       |2580   |
Then verify that the screen appears "SEGURO SALUD DIGITAL"
