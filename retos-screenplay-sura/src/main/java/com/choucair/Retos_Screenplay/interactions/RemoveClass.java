package com.choucair.Retos_Screenplay.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class RemoveClass implements Interaction {

	private String elementRemove;
	private String tag;

	public RemoveClass(String elementRemove, String tag) {
		this.elementRemove = elementRemove;
		this.tag = tag;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		WebDriver driver = BrowseTheWeb.as(actor).getDriver();
		List<WebElement> inputs = driver.findElements(By.tagName(tag));
		for (WebElement input : inputs) {
			((JavascriptExecutor) driver).executeScript(
					"arguments[0].removeAttribute('"+elementRemove+"')",input);
		}
	}

	public static RemoveClass attribute(String elementRemove, String tag) {
		 return instrumented(RemoveClass.class, elementRemove, tag);
	}
}
