package com.choucair.Retos_Screenplay.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;

public class Input implements Interaction {

    private String value;
    Target input;

    public Input(String value, Target input) {
        this.value = value;
        this.input = input;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        input.resolveFor(actor).click();
        input.resolveFor(actor).type(value);
    }

    public static Input data(String value, Target input) {
        return new Input(value, input);
    }
}
