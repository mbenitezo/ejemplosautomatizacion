package com.choucair.Retos_Screenplay.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

public class SelectOtherList implements Interaction {

    private String documenttype;
    Target list;

    public SelectOtherList(String documenttype, Target list) {
        this.documenttype = documenttype;
        this.list = list;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<WebElement> contenedorTipoDocumento = list.resolveFor(actor).findElements(By.tagName("option"));
        for(int i=0; i<contenedorTipoDocumento.size(); i++) {
            if(contenedorTipoDocumento.get(i).getText().trim().equals(this.documenttype)) {
                contenedorTipoDocumento.get(i).click();
                break;
            }
        }
    }

    public static SelectOtherList data(String documenttype, Target list) {
        return new SelectOtherList(documenttype, list);
    }
}
