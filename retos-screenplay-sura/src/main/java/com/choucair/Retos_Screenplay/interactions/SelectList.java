package com.choucair.Retos_Screenplay.interactions;

import com.choucair.Retos_Screenplay.ui.SuraenlineaPageRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class SelectList implements Interaction {

    private SuraenlineaPageRegister suraenlineaPageRegister;
    private String documenttype;
    Target list;

    public SelectList(String documenttype, Target list) {
        this.documenttype = documenttype;
        this.list = list;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        //WebElement tipoDocumento = driver.findElement(By.xpath("//select-material-design[@label='Tipo de documento']"));
        WebElement shadowTipoDocumento = expandRootElement(list.resolveFor(actor));
        shadowTipoDocumento.findElement(By.tagName("select")).sendKeys(documenttype);
    }

    public WebElement expandRootElement(WebElement element) {
        WebElement ele = (WebElement) ((JavascriptExecutor)suraenlineaPageRegister.getDriver())
                .executeScript("return arguments[0].shadowRoot", element);
        return ele;
    }

    public static SelectList data(String documenttype, Target list) {
        return new SelectList(documenttype, list);
    }
}
