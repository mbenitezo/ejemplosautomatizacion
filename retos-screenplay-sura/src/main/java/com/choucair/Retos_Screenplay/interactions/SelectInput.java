package com.choucair.Retos_Screenplay.interactions;

import com.choucair.Retos_Screenplay.ui.SuraenlineaPageRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class SelectInput implements Interaction {

    private SuraenlineaPageRegister suraenlineaPageRegister;
    private String value;
    Target list;

    public SelectInput(String value, Target list) {
        this.value = value;
        this.list = list;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        WebElement shadowValue = expandRootElement(list.resolveFor(actor));
        shadowValue.findElement(By.tagName("input")).click();
        shadowValue.findElement(By.tagName("input")).sendKeys(value);
    }

    public WebElement expandRootElement(WebElement element) {
        WebElement ele = (WebElement) ((JavascriptExecutor)suraenlineaPageRegister.getDriver())
                .executeScript("return arguments[0].shadowRoot", element);
        return ele;
    }

    public static SelectInput data(String value, Target list) {
        return new SelectInput(value, list);
    }
}
