package com.choucair.Retos_Screenplay.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import static com.choucair.Retos_Screenplay.ui.SuraenlineaAutenticacionPage.BUTTON_SECURE_KEYBOARD;

public class PressNumber implements Interaction {

    private char number;

    public PressNumber(char value) {
        this.number = value;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        BUTTON_SECURE_KEYBOARD.of(String.valueOf(number)).resolveFor(actor).click();
    }

    public static PressNumber secureKeyboard(char number) {
        return new PressNumber(number);
    }
}
