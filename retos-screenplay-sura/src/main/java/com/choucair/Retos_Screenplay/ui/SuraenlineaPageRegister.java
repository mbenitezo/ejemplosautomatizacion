package com.choucair.Retos_Screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://suraenlinea:suraenlinea@lab.suraenlinea.com/salud-digital/sura/datos")
public class SuraenlineaPageRegister extends PageObject{
	
	public static final Target SELECT_DOCUMENT_TYPE = Target.the("Lista desplegable tipo de documento")
			.located(By.xpath("//select-material-design[@label='Tipo de documento']"));

	public static final Target TEXT_DOCUMENT_NUMBER = Target.the("Campo número de documento")
			.located(By.xpath("//input-material-design[@label='Número de documento']"));

	public static final Target TEXT_EXPEDITION = Target.the("Campo fecha de expedición")
			.located(By.xpath("//input[@placeholder='Fecha de expedición']"));

	public static final Target TEXT_NAME = Target.the("Campo nombre cliente")
			.located(By.xpath("//input-material-design[@label='Nombres']"));

	public static final Target TEXT_SURNAME = Target.the("Campo apellido cliente")
			.located(By.xpath("//input-material-design[@label='Apellidos']"));

	public static final Target SELECT_GENDER = Target.the("Lista desplegable género")
			.located(By.xpath("//select-material-design[@label='Género']"));

	public static final Target TEXT_BIRTHDATE = Target.the("Campo fecha de nacimiento")
			.located(By.xpath("//input[@placeholder='Fecha de nacimiento']"));

	public static final Target SELECT_CITY = Target.the("Lista desplegable ciudades")
			.located(By.xpath("//select-material-design[@label='Ciudad de residencia']"));

	public static final Target TEXT_DIRECTION = Target.the("Campo dirección cliente")
			.located(By.xpath("//input-material-design[@label='Dirección']"));

	public static final Target TEXT_CELLPHONE = Target.the("Campo celular cliente")
			.located(By.xpath("//input-material-design[@label='Celular']"));

	public static final Target TEXT_EMAIL = Target.the("Campo correo cliente")
			.located(By.xpath("//input-material-design[@label='Correo']"));

	public static final Target SELECT_EPS = Target.the("Lista desplegable eps")
			.located(By.xpath("//select-material-design[@label='EPS']"));

	public static final Target CHECK_CONDITIONS_TERM = Target.the("Checkbox términos y condiciones")
			.located(By.xpath("//span[@class='checkbox__checkmark']"));

	public static final Target BUTTON_PAY = Target.the("Botón de pagar")
			.located(By.xpath("//button[@type='submit']"));

	public static final Target LBL_DETAILS_SHOP = Target.the("Label detalle compra")
			.located(By.xpath("//h6[contains(text(),'Detalles de la compra')]"));
}


