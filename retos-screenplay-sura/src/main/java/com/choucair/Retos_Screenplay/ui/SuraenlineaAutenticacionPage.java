package com.choucair.Retos_Screenplay.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://apisaluddigital.segurossura.com.co/login")
public class SuraenlineaAutenticacionPage extends PageObject{
	
	public static final Target SELECT_DOCUMENT_TYPE = Target.the("Lista desplegable tipo de documento")
			.located(By.xpath("//select"));

	public static final Target TEXT_DOCUMENT_NUMBER = Target.the("Campo de texto número de documento")
			.located(By.id("suraName"));

	public static final Target TEXT_PASS = Target.the("Campo de contraseña")
			.located(By.id("suraPassword"));

	public static final Target BUTTON_SECURE_KEYBOARD = Target.the("Buttons secure keyboard").
			locatedBy("//div[@name='default']//button[@data-value='{0}']");

	public static final Target BUTTON_LOGIN = Target.the("Button Login")
			.located(By.id("session-internet"));

	public static final Target LABEL_MESSAGE_EXPECTED = Target.the("Label message expected")
			.located(By.xpath("//h4"));

}


