package com.choucair.Retos_Screenplay.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import org.assertj.core.api.SoftAssertions;
import static com.choucair.Retos_Screenplay.ui.SuraenlineaAutenticacionPage.LABEL_MESSAGE_EXPECTED;

public class TheMessage implements Question<Boolean> {

    private SoftAssertions validate = new SoftAssertions();
    private String mensaje;

    public TheMessage(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean flag = false;
        if(LABEL_MESSAGE_EXPECTED.resolveFor(actor).isPresent()){
            flag = true;
            validate.assertThat(Text.of(LABEL_MESSAGE_EXPECTED).viewedBy(actor).asString()).isEqualTo(this.mensaje);
            validate.assertAll();
        }
        return flag;
    }

    public static TheMessage isExpected(String mensaje) {
        return new TheMessage(mensaje);
    }
}
