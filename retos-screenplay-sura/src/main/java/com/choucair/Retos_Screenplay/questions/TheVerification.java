package com.choucair.Retos_Screenplay.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import static com.choucair.Retos_Screenplay.ui.SuraenlineaPageRegister.LBL_DETAILS_SHOP;

public class TheVerification implements Question<String>{

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(LBL_DETAILS_SHOP).viewedBy(actor).asString();
	}

	public static TheVerification Home() {
		return new TheVerification();
	}

}
