package com.choucair.Retos_Screenplay.exceptions;

public class AssertionsRegister extends AssertionError {

    private static final long serialVersionUID = 1L;

    public static final String NOFOUNDELEMENT = "El elemento a verificar no es visible";

    public AssertionsRegister(String message, Throwable cause) {
        super(message, cause);
    }

    public AssertionsRegister(String message) {
        super(message);
    }


}
