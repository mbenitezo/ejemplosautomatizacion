package com.choucair.Retos_Screenplay.tasks;

import com.choucair.Retos_Screenplay.interactions.*;
import com.choucair.Retos_Screenplay.model.AuthenticateSuraenlinea;
import com.choucair.Retos_Screenplay.util.Divide;
import com.choucair.Retos_Screenplay.util.Wait;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import java.util.List;

import static com.choucair.Retos_Screenplay.ui.SuraenlineaAutenticacionPage.*;

public class CompleteDataAuthentication implements Task{

	private  List<AuthenticateSuraenlinea> data;
	public CompleteDataAuthentication(List<AuthenticateSuraenlinea> data) {
		this.data = data;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(SelectOtherList.data(data.get(0).getDocumenttype(), SELECT_DOCUMENT_TYPE));
		actor.attemptsTo(Enter.theValue(data.get(0).getDocumentnumber()).into(TEXT_DOCUMENT_NUMBER));
		actor.attemptsTo(Click.on(TEXT_PASS));

		actor.attemptsTo(RemoveClass.attribute("class", "button"));

		actor.attemptsTo(PressNumber.secureKeyboard(Divide.pass(data.get(0).getPass(),"1")));
		actor.attemptsTo(PressNumber.secureKeyboard(Divide.pass(data.get(0).getPass(),"2")));
		actor.attemptsTo(PressNumber.secureKeyboard(Divide.pass(data.get(0).getPass(),"3")));
		actor.attemptsTo(PressNumber.secureKeyboard(Divide.pass(data.get(0).getPass(),"4")));

		actor.attemptsTo(Click.on(TEXT_DOCUMENT_NUMBER));
		actor.attemptsTo(Click.on(BUTTON_LOGIN));

		actor.attemptsTo(Wait.theNext(10));

	}

	public static CompleteDataAuthentication inTheForm(List<AuthenticateSuraenlinea> data) {
		return new CompleteDataAuthentication(data);
	}

		

}
