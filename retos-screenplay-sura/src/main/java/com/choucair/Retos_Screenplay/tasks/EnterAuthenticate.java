package com.choucair.Retos_Screenplay.tasks;


import com.choucair.Retos_Screenplay.ui.SuraenlineaAutenticacionPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class EnterAuthenticate implements Task{
	
	private SuraenlineaAutenticacionPage suraenlineaAutenticacionPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(suraenlineaAutenticacionPage));
	}
	
	public static EnterAuthenticate toSuraenlinea() {
		return instrumented(EnterAuthenticate.class);
	}

}
