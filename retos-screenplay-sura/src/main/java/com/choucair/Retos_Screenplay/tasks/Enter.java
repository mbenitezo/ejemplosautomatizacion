package com.choucair.Retos_Screenplay.tasks;



import com.choucair.Retos_Screenplay.ui.SuraenlineaPageRegister;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import net.serenitybdd.screenplay.actions.Open;

public class Enter implements Task{
	
	private SuraenlineaPageRegister suraenlineaPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(suraenlineaPage));
	}
	
	public static Enter toSuraenlinea() {
		return instrumented(Enter.class);
	}

}
