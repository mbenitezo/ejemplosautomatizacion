package com.choucair.Retos_Screenplay.tasks;

import java.util.List;

import com.choucair.Retos_Screenplay.interactions.*;

import com.choucair.Retos_Screenplay.model.RegisterSuraenlinea;
import com.choucair.Retos_Screenplay.ui.SuraenlineaPageRegister;
import static com.choucair.Retos_Screenplay.ui.SuraenlineaPageRegister.*;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import org.openqa.selenium.JavascriptExecutor;


public class CompleteData implements Task{
	private SuraenlineaPageRegister suraenlineaPageRegister;
	private  List<RegisterSuraenlinea> data;
	
	public CompleteData(List<RegisterSuraenlinea> data2) {
		this.data = data2;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(SelectList.data(data.get(0).getDocumenttype(), SELECT_DOCUMENT_TYPE));
		actor.attemptsTo(SelectInput.data(data.get(0).getDocumentnumber(), TEXT_DOCUMENT_NUMBER));
		actor.attemptsTo(Input.data(data.get(0).getExpeditionyear(), TEXT_EXPEDITION));
		actor.attemptsTo(SelectInput.data(data.get(0).getName(), TEXT_NAME));
		actor.attemptsTo(SelectInput.data(data.get(0).getSurname(), TEXT_SURNAME));
		actor.attemptsTo(SelectList.data(data.get(0).getGender(), SELECT_GENDER));
		actor.attemptsTo(Input.data(data.get(0).getBirthdate(), TEXT_BIRTHDATE));
		actor.attemptsTo(SelectList.data(data.get(0).getCity(), SELECT_CITY));
		actor.attemptsTo(SelectInput.data(data.get(0).getDirection(), TEXT_DIRECTION));
		actor.attemptsTo(SelectInput.data(data.get(0).getCellphone(), TEXT_CELLPHONE));
		actor.attemptsTo(Move.page("Vertical", 500));
		actor.attemptsTo(SelectInput.data(data.get(0).getEmail(), TEXT_EMAIL));
		actor.attemptsTo(SelectList.data(data.get(0).getEps(), SELECT_EPS));
		actor.attemptsTo(Move.page("Vertical", -500));
		actor.attemptsTo(Click.on(CHECK_CONDITIONS_TERM));
		actor.attemptsTo(Click.on(BUTTON_PAY));
		actor.attemptsTo(Wait.theNext(10));

	}

	public static CompleteData inTheForm(List<RegisterSuraenlinea> data2) {
		return new CompleteData(data2);
	}

		

}
