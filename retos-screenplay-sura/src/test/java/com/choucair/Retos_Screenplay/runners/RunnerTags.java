package com.choucair.Retos_Screenplay.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/registerdata.feature",
				 glue="com.choucair.Retos_Screenplay.stepdefinitions",
		  		 monochrome = true,
				 plugin = {"pretty", "html:target/cucumber"},
				 strict = true,
				 snippets=SnippetType.CAMELCASE)
public class RunnerTags {

}

