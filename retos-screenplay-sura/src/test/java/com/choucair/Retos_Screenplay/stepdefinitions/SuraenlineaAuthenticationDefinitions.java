package com.choucair.Retos_Screenplay.stepdefinitions;

import com.choucair.Retos_Screenplay.exceptions.AssertionAutentication;
import com.choucair.Retos_Screenplay.model.AuthenticateSuraenlinea;
import com.choucair.Retos_Screenplay.questions.TheMessage;
import com.choucair.Retos_Screenplay.tasks.CompleteDataAuthentication;
import com.choucair.Retos_Screenplay.tasks.EnterAuthenticate;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import java.util.List;

import static org.hamcrest.Matchers.is;

public class SuraenlineaAuthenticationDefinitions {

	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor pepe = Actor.named("pepe");

	@Before
	public void InitialConfiguration() {
		pepe.can(BrowseTheWeb.with(hisBrowser));
	}

	@Given("^that User wants to access the SuraenLineaAutenticate$")
	public void thatUserWantsToAccessTheSuraenLineaAutenticate() {
		pepe.wasAbleTo(EnterAuthenticate.toSuraenlinea());
	}

	@When("^he performs the authentication on the page$")
	public void hePerformsTheAuthenticationOnThePage(List<AuthenticateSuraenlinea> data) {
		pepe.attemptsTo(CompleteDataAuthentication.inTheForm(data));
	}

	@Then("^verify that the screen appears \"([^\"]*)\"$")
	public void verifyThatTheScreenAppears(String mensaje) {
		pepe.should(GivenWhenThen.seeThat(TheMessage.isExpected(mensaje), is(true))
				.orComplainWith(AssertionAutentication.class, AssertionAutentication.NOFOUNDELEMENT));
	}

}
