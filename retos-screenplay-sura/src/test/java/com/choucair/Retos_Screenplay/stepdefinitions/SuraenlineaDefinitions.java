package com.choucair.Retos_Screenplay.stepdefinitions;

import java.util.List;

import static org.hamcrest.Matchers.*;

import com.choucair.Retos_Screenplay.exceptions.AssertionsRegister;
import net.serenitybdd.screenplay.GivenWhenThen;
import org.junit.AssumptionViolatedException;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.choucair.Retos_Screenplay.model.RegisterSuraenlinea;
import com.choucair.Retos_Screenplay.questions.TheVerification;
import com.choucair.Retos_Screenplay.tasks.CompleteData;
import com.choucair.Retos_Screenplay.tasks.Enter;


import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import static net.serenitybdd.screenplay.GivenWhenThen.*;

public class SuraenlineaDefinitions {

	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor pepe = Actor.named("pepe");

	@Before
	public void InitialConfiguration() {
		pepe.can(BrowseTheWeb.with(hisBrowser));
	}

	@Given("^that User wants to access the Suraenlinea$")
	public void thatUserWantsToAccessTheSuraenlinea(){
		pepe.wasAbleTo(Enter.toSuraenlinea());
	}

	@When("^he performs the registration on the page$")
	public void hePerformsTheRegistrationOnThePage(List<RegisterSuraenlinea> data){
		pepe.attemptsTo(CompleteData.inTheForm(data));
	}

	@Then("^verify that the screen register successful$")
	public void verifyThatTheScreenRegisterSuccessful(){
		pepe.should(GivenWhenThen.seeThat(TheVerification.Home(), equalTo("Detalles de la compra"))
			 .orComplainWith(AssertionsRegister.class, AssertionsRegister.NOFOUNDELEMENT));
	}


	@Given("^I have a field$")
	public void i_have_a_field() {

	}


	@When("^I plant some Granny Smith apples trees$")
	public void i_plant_some_Granny_Smith_apples_trees() {

	}

	@Then("^some apples should grow$")
	public void some_apples_should_grow() {

	}

	@Then("^the apples should be green$")
	public void the_apples_should_be_green() {

	}

	@When("^I plant some Red Delicious apples trees$")
	public void i_plant_some_Red_Delicious_apples_trees() {

	}

	@Then("^the apples should be red$")
	public void the_apples_should_be_red() {

	}
}
