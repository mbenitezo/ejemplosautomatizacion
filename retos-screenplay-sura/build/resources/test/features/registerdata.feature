#Author: mbenitezo@choucairtesting.com
@Regression
Feature: As a user I want to register in the web Suraenlinea

@Register
Scenario: Register in the Web Suraenlinea
Given that User wants to access the Suraenlinea
When he performs the registration on the page
|documenttype          |documentnumber |expeditionyear  |name     	  |surname  |gender     |birthdate  |city               |direction         |cellphone  |email              |eps       |
|CÉDULA DE CIUDADANÍA  |1111111111     |2006-03-29      |PEDRO CORAL  |TABERA   |MASCULINO  |1987-09-25 |MEDELLIN, ANTIOQUIA|Calle 49 A #63-55 |3003003000 |correo1@gmail.com  |EPS SURA  |

Then verify that the screen register successful