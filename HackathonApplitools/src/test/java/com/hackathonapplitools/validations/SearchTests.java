package com.hackathonapplitools.validations;

import com.hackathonapplitools.base.BaseTests;
import com.hackathonapplitools.pages.SearchPage;
import org.junit.Test;

public class SearchTests extends BaseTests {

    private SearchPage page = new SearchPage(driver);


    @Test
    public void testSearchByFullTitle(){
        //String title = "Test";
        String title = "Agile Testing";
        page.search(title);
        validateWindow();
    }

}
