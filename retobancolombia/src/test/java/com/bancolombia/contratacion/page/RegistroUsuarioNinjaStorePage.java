package com.bancolombia.contratacion.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class RegistroUsuarioNinjaStorePage extends PageObject {
	
	//Objetos para registrar un nuevo usuario en la aplicación
	@FindBy(id = "input-firstname")
	public WebElementFacade txtFirstName;
	
	@FindBy(id = "input-lastname")
	public WebElementFacade txtLastName;
	
	@FindBy(id = "input-email")
	public WebElementFacade txtEmail;
	
	@FindBy(id = "input-telephone")
	public WebElementFacade txtTelephone;
	
	@FindBy(id = "input-password")
	public WebElementFacade txtPassword;
	
	@FindBy(id = "input-confirm")
	public WebElementFacade txtConfirPassword;
	
	@FindBy(xpath = "//*[@id=\'content\']/form/fieldset[3]/div/div/label[1]/input")
	public WebElementFacade rbtSubscribeYes;
	
	@FindBy(xpath = "//*[@id=\'content\']/form/fieldset[3]/div/div/label[2]/input")
	public WebElementFacade rbtSubscribeNo;
	
	@FindBy(name = "agree")
	public WebElementFacade checkPolicy;
	
	@FindBy(xpath = "//*[@id=\'content\']/form/div/div/input[2]")
	public WebElementFacade btnContinue;
	
	@FindBy(xpath = "//*[@id=\'content\']/p[1]")
	public WebElementFacade lblMensajeConfirmacion;
	
	@FindBy(xpath = "//*[@id=\'content\']/div/div/a")
	public WebElementFacade btnContinuar;
	
	//Metodos que interatuan con los objetos para registrar un usuario
	public void FirstName(String Dato) {
		txtFirstName.click();
		txtFirstName.clear();
		txtFirstName.sendKeys(Dato);
	}
	
	public void LastName(String Dato) {
		txtLastName.click();
		txtLastName.clear();
		txtLastName.sendKeys(Dato);
	}
	
	public void Email(String Dato) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(Dato);
	}
	
	public void Telephone(String Dato) {
		txtTelephone.click();
		txtTelephone.clear();
		txtTelephone.sendKeys(Dato);
	}
	
	public void Password(String Dato) {
		txtPassword.click();
		txtPassword.clear();
		txtPassword.sendKeys(Dato);
	}
	
	public void ConfirPassword(String Dato) {
		txtConfirPassword.click();
		txtConfirPassword.clear();
		txtConfirPassword.sendKeys(Dato);
	}
	
	public void Subscribe(String Dato) {
		
		if(Dato.equals("Yes")){
			rbtSubscribeYes.click();
		}
		else {
			
			if(Dato.equals("No")){
				rbtSubscribeNo.click();
		}
			else {
				System.out.println("El valor no es correcto");
			}	
		}
	}
	
	public void AceptoPolicy() {
		checkPolicy.click();
	}

	//Boton para continuar y crear el registro del usuario
	public void BotonContinue() {
		btnContinue.click();
	}
	
	//Verificar creación exitosa de la cuenta
	public void VerificarMensajeCuentaCreada () {
		String lblExitoso = "Congratulations! Your new account has been successfully created!";
		assertThat(lblMensajeConfirmacion.getText(), containsString(lblExitoso));
		
	}
	
	//Boton para continuar e ingresar a la cuenta
	public void BotonContinuar() {
		btnContinuar.click();
	}
	
	
}

