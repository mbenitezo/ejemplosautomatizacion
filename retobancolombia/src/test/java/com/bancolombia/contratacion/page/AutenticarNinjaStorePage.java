package com.bancolombia.contratacion.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AutenticarNinjaStorePage extends PageObject {

	//Objetos para la autenticación en la aplicación
	@FindBy(id = "input-email")
	public WebElementFacade txtEmail;
	
	@FindBy(id = "input-password")
	public WebElementFacade txtPassword;
	
	@FindBy(xpath = "//*[@id=\'content\']/div/div[2]/div/form/input")
	public WebElementFacade btnLogin;
	
	//Metodos para la auteticación en la aplicación
	public void Email(String Dato) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(Dato);
	}
	
	public void Password(String Dato) {
		txtPassword.click();
		txtPassword.clear();
		txtPassword.sendKeys(Dato);
	}
	
	public void BotonLogin() {
		btnLogin.click();
	}
}
