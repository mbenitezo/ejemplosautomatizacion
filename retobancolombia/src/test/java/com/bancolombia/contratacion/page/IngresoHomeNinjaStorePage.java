package com.bancolombia.contratacion.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class IngresoHomeNinjaStorePage extends PageObject {
	
	//Objeto de la opción Address Book
	@FindBy(xpath = "//*[@id=\"content\"]/ul[1]/li[3]/a")
	public WebElementFacade opcAddressBook;
	
	//Metodo para ingresar a la opción Address Book
	public void OpcionAddrssBook() {
		opcAddressBook.click();
	}
	
}
