package com.bancolombia.contratacion.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;

public class AdiccionarNuevaDireccionNinjaStorePage extends PageObject {
	
	//Objetos para crear una nueva dirección en la aplicación
	@FindBy(xpath = "//*[@id=\'content\']/div[2]/div[2]/a")
	public WebElementFacade btnNewAddress ;
	
	@FindBy(id = "input-firstname")
	public WebElementFacade txtFirstName;
	
	@FindBy(id = "input-lastname")
	public WebElementFacade txtLastName;

	@FindBy(id = "input-company")
	public WebElementFacade txtCompany;
	
	@FindBy(id = "input-address-1")
	public WebElementFacade txtAddress1;
	
	@FindBy(id = "input-address-2")
	public WebElementFacade txtAddress2;
	
	@FindBy(id = "input-city")
	public WebElementFacade txtCity;
	
	@FindBy(id = "input-postcode")
	public WebElementFacade txtPostcode;

	@FindBy(id = "input-country")
	public WebElementFacade cmbCountry;
	
	@FindBy(id = "input-zone")
	public WebElementFacade cmbRegion;
	
	@FindBy(xpath = "//*[@id=\'content\']/form/fieldset/div[10]/div/label[1]/input")
	public WebElementFacade rdbDefaultAddressSI;
	
	@FindBy(xpath = "//*[@id=\'content\']/form/fieldset/div[10]/div/label[2]")
	public WebElementFacade rdbDefaultAddressNO;
	
	@FindBy(xpath= "//*[@id=\'content\']/form/div/div[2]/input")
	public WebElementFacade btnContinuar;
	
	@FindBy(xpath= "//*[@id=\'account-address\']/div[1]")
	public WebElementFacade lblMensaje;
	
	@FindBy(className = "text-danger") 
    public WebElementFacade validationMessage; 

	//Metodos para crear una nueva dirección en la aplicación	
	public void BotonNewAddress() {
		btnNewAddress.click();
	}
	
	public void FirstName(String Dato) {
		txtFirstName.click();
		txtFirstName.clear();
		txtFirstName.sendKeys(Dato);
	}
	
	public void LastName(String Dato) {
		txtLastName.click();
		txtLastName.clear();
		txtLastName.sendKeys(Dato);
	}
	
	public void Company(String Dato) {
		txtCompany.click();
		txtCompany.clear();
		txtCompany.sendKeys(Dato);
	}
	
	public void Address1(String Dato) {
		txtAddress1.click();
		txtAddress1.clear();
		txtAddress1.sendKeys(Dato);
	}
	
	public void Address2(String Dato) {
		txtAddress2.click();
		txtAddress2.clear();
		txtAddress2.sendKeys(Dato);
	}
	
	public void City(String Dato) {
		txtCity.click();
		txtCity.clear();
		txtCity.sendKeys(Dato);
	}
	
	public void Postcode(String Dato) {
		txtPostcode.click();
		txtPostcode.clear();
		txtPostcode.sendKeys(Dato);
	}
	
	public void Country(String Dato) {
		cmbCountry.click();
		cmbCountry.selectByVisibleText(Dato);
	}
	
	public void Region(String Dato) {
		cmbRegion.click();
		cmbRegion.selectByVisibleText(Dato);
	}
	
	//Seleccionar la opción de default adress dependiendo del valor enviado en los datos
	public void DefaultAddress(String DatoPrueba) {
		
		if(DatoPrueba.equals("Yes")){
			rdbDefaultAddressSI.click();
		}
		else {
			
			if(DatoPrueba.equals("No")){
			rdbDefaultAddressNO.click();
		}
			else {
				System.out.println("El valor no es correcto");
			}	
		}
		
	}
	
	public void BotonContinuar() {
		btnContinuar.click();
	}
	
	//Verifica que la cuenta fue creada de forma exitosa
	public void VerificarMensajeDireccionCreada () {
		String lblExitoso = "Your address has been successfully added";
		assertThat(lblMensaje.getText(), containsString(lblExitoso));
		
	}
	
	//Verifica que se presenta el mensaje de error al no diligenciar el fomulario de forma correcta
	public void DiligeciaFormularioConErrores() { 
        assertThat(validationMessage.isCurrentlyVisible(),is(true)); 
	}
	
	
}
