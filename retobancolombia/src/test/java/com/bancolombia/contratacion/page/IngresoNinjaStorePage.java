package com.bancolombia.contratacion.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://tutorialsninja.com/demo/index.php")
public class IngresoNinjaStorePage extends PageObject {
	
	//Metodos para ingresar al login y al registro de un usuario
	@FindBy(xpath = "//*[@id=\'top-links\']/ul/li[2]/a")
	public WebElementFacade cmbMyAccount;
	
	@FindBy(xpath = "//*[@id=\'top-links\']/ul/li[2]/ul/li[2]/a")
	public WebElementFacade opcLogin;
	
	@FindBy(xpath = "//*[@id=\'top-links\']/ul/li[2]/ul/li[1]/a")
	public WebElementFacade opcRegistrar;
	
	//Ingreso a la opcion MyAccount 
	public void MyAccount() {
		cmbMyAccount.click();
	}
	
	//Ingreso a la auteticación
	public void Login() {
		opcLogin.click();
	}
	
	//Ingreso al registro de un nuevo usuario
	public void Registrar() {
		opcRegistrar.click();
	}
}

