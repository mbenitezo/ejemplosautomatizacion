package com.bancolombia.contratacion.definition;

import java.util.List;

import com.bancolombia.contratacion.steps.AdiccionarNuevaDireccionNinjaStoreSteps;
import com.bancolombia.contratacion.steps.AutenticarNinjaStoreSteps;
import com.bancolombia.contratacion.steps.IngresoPagPrincipalNinjaStoreSteps;
import com.bancolombia.contratacion.steps.IngresoNinjaStoreSteps;
import com.bancolombia.contratacion.steps.RegistroUsuarioNinjaStoreSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class NinjaStoreDefinition {

	@Steps
	IngresoNinjaStoreSteps ingresoNinjaStoreSteps;
	@Steps
	AutenticarNinjaStoreSteps autenticarNinjaStoreSteps;
	@Steps
	IngresoPagPrincipalNinjaStoreSteps ingresoHomeNinjaStoreSteps;
	@Steps
	AdiccionarNuevaDireccionNinjaStoreSteps adiccionarNuevaDireccionNinjaStoreSteps;
	@Steps
	RegistroUsuarioNinjaStoreSteps registroUsuarioNinjaStoreSteps;
	
		
	@Given("^Que me encuentro en la aplicacion The Ninja Store$")
	public void queMeEncuentroEnLaAplicacionTheNinjaStore() {

		ingresoNinjaStoreSteps.AbrirAplicacion();
		ingresoNinjaStoreSteps.IngresoMyAccount();
		ingresoNinjaStoreSteps.IngresoLogin();
		
	}

	@When("^Ingreso a la autenticacion y diligencio los datos solicitados$")
	public void ingresoALaAutenticacionYDiligencioLosDatosSolicitados(DataTable Datos) {

				
		List<List<String>> Data=Datos.raw();
		
		for(int i=0; i<Data.size();i++) {
			autenticarNinjaStoreSteps.Autenticacion(Data, i);
		}
	}

	@When("^Ingreso a la opcion Address Book y adicciono una nueva direccion$")
	public void ingresoALaOpcionAddressBookYAdiccionoUnaNuevaDireccion() {
		ingresoHomeNinjaStoreSteps.IngresoAddressBook();
		adiccionarNuevaDireccionNinjaStoreSteps.AdiccionarNuevaDireccion();
	}

	@When("^Diligenciar el formulario con los datos de la nueva direccion$")
	public void diligenciarElFormularioConLosDatosDeLaNuevaDireccion(DataTable Datos) {

		List<List<String>> Data=Datos.raw();
		
		for(int i=0; i<Data.size();i++) {
			adiccionarNuevaDireccionNinjaStoreSteps.DiligendiarFormulario(Data, i);
		}
	}

	@Then("^Verifico la nueva direccion ingresada$")
	public void verificoLaNuevaDireccionIngresada() {
 
		adiccionarNuevaDireccionNinjaStoreSteps.VerificarMensajeDireccionCreada();
		
	}
	
	@Then("^Verifico mensaje de error$")
	public void verificoMensajeDeError() {
	 
		adiccionarNuevaDireccionNinjaStoreSteps.DiligeciaFormularioConErrores();
	}
	
	
	@Given("^Que necesito registrarme en la aplicacion de The Ninja Store$")
	public void queNecesitoRegistrarmeEnLaAplicacionDeTheNinjaStore() {
		ingresoNinjaStoreSteps.AbrirAplicacion();
		ingresoNinjaStoreSteps.IngresoMyAccount();
		ingresoNinjaStoreSteps.IngresoRegistrar();
		
	}
	
	
	@When("^Se crea un nuevo usuario$")
	public void seCreaUnNuevoUsuario(DataTable Datos) {
		
		List<List<String>> Data=Datos.raw();
		
		for(int i=0; i<Data.size();i++) {
			registroUsuarioNinjaStoreSteps.DiligenciarFormularioRegistro(Data, i);
		}
	}


	@Then("^Verifico el registro del usaurio$")
	public void verificoElRegistroDelUsaurio() {

		registroUsuarioNinjaStoreSteps.VerificarMensajeCuentaCreada();
	}
}
