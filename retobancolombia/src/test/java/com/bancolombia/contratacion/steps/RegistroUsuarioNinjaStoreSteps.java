package com.bancolombia.contratacion.steps;

import java.util.List;
import com.bancolombia.contratacion.page.RegistroUsuarioNinjaStorePage;
import net.thucydides.core.annotations.Step;

public class RegistroUsuarioNinjaStoreSteps {
	
	RegistroUsuarioNinjaStorePage registroUsuarioNinjaStorePage;
		
	@Step
	public void DiligenciarFormularioRegistro(List<List<String>> Dato, int id) {
		
	registroUsuarioNinjaStorePage.FirstName(Dato.get(id).get(0).trim());
	registroUsuarioNinjaStorePage.LastName(Dato.get(id).get(1).trim());
	registroUsuarioNinjaStorePage.Email(Dato.get(id).get(2).trim());
	registroUsuarioNinjaStorePage.Telephone(Dato.get(id).get(3).trim());
	registroUsuarioNinjaStorePage.Password(Dato.get(id).get(4).trim());
	registroUsuarioNinjaStorePage.ConfirPassword(Dato.get(id).get(5).trim());
	registroUsuarioNinjaStorePage.Subscribe(Dato.get(id).get(6).trim());
	registroUsuarioNinjaStorePage.AceptoPolicy();
	registroUsuarioNinjaStorePage.BotonContinue();
	
	
	}
	
	@Step
	public void VerificarMensajeCuentaCreada() {
		registroUsuarioNinjaStorePage.VerificarMensajeCuentaCreada();
	}
	
	
	
}

