package com.bancolombia.contratacion.steps;

import java.util.List;
import com.bancolombia.contratacion.page.AutenticarNinjaStorePage;
import net.thucydides.core.annotations.Step;

public class AutenticarNinjaStoreSteps {
	
	AutenticarNinjaStorePage autenticarNinjaStorePage;
	
	@Step
	public void Autenticacion(List<List<String>> data,int i) {
		autenticarNinjaStorePage.Email(data.get(i).get(0).trim() );
		autenticarNinjaStorePage.Password(data.get(i).get(1).trim());
		autenticarNinjaStorePage.BotonLogin();
	}

}
