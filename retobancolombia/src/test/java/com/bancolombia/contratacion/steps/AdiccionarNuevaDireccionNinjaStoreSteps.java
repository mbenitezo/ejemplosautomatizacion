package com.bancolombia.contratacion.steps;

import java.util.List;
import com.bancolombia.contratacion.page.AdiccionarNuevaDireccionNinjaStorePage;
import net.thucydides.core.annotations.Step;

public class AdiccionarNuevaDireccionNinjaStoreSteps {
	
	AdiccionarNuevaDireccionNinjaStorePage adiccionarNuevaDireccionNinjaStorePage;
	
	@Step
	public void AdiccionarNuevaDireccion() {
		adiccionarNuevaDireccionNinjaStorePage.BotonNewAddress();
	}
	
	@Step
	public void DiligendiarFormulario(List<List<String>> Dato, int id) {
		adiccionarNuevaDireccionNinjaStorePage.FirstName(Dato.get(id).get(0).trim());
		adiccionarNuevaDireccionNinjaStorePage.LastName(Dato.get(id).get(1).trim());
		adiccionarNuevaDireccionNinjaStorePage.Company(Dato.get(id).get(2).trim());
		adiccionarNuevaDireccionNinjaStorePage.Address1(Dato.get(id).get(3).trim());
		adiccionarNuevaDireccionNinjaStorePage.Address2(Dato.get(id).get(4).trim());
		adiccionarNuevaDireccionNinjaStorePage.City(Dato.get(id).get(5).trim());
		adiccionarNuevaDireccionNinjaStorePage.Postcode(Dato.get(id).get(6).trim());
		adiccionarNuevaDireccionNinjaStorePage.Country(Dato.get(id).get(7).trim());
		adiccionarNuevaDireccionNinjaStorePage.Region(Dato.get(id).get(8).trim());
		adiccionarNuevaDireccionNinjaStorePage.DefaultAddress(Dato.get(id).get(9).trim());
		adiccionarNuevaDireccionNinjaStorePage.BotonContinuar();
	}
	
	@Step
	public void VerificarMensajeDireccionCreada() {
		adiccionarNuevaDireccionNinjaStorePage.VerificarMensajeDireccionCreada();
	}
	
	@Step
	public void DiligeciaFormularioConErrores() {
		adiccionarNuevaDireccionNinjaStorePage.DiligeciaFormularioConErrores();
		
	}
	
}
