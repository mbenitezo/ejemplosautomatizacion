#Author: csgirald@bancolombia.com.co

@Regresion
Feature: Ingresar una nueva dirección en mi cuenta de The Ninja Store 
				Yo como usuario de The Ninja Store registrado en la aplicación,
				Quiero ingresar una nueva dirección en mi cuenta
				Para que los pedidos lleguen a la dirección registrada


 @CasoExitosoDireccion
  Scenario Outline: Ingreso exitoso de una nueva dirección en mi cuenta de The Ninja Store 
    Given Que me encuentro en la aplicacion The Ninja Store
    When Ingreso a la autenticacion y diligencio los datos solicitados 
    |<Email>|<Password>|
    And Ingreso a la opcion Address Book y adicciono una nueva direccion
    And Diligenciar el formulario con los datos de la nueva direccion
    |<FirstName>|<LastName>|<Company>|<Address1>|<Address2>|<City>|<PostCode>|<Country>|<Region/State>|<DefaultAddress>|
    Then Verifico la nueva direccion ingresada
    
        Examples: 
      |Id|Email|Password|FirstName|LastName|Company|Address1|Address2|City|PostCode|Country|Region/State|DefaultAddress|
      ##@externaldata@./src/test/resources/Datadriven/dtDatos.xlsx@Datos@1
   |1   |usertest@test.com   |usertest   |lina   |Varela   |Choucair Testing   |Calle 100 # 22 - 22   |Calle 101 # 25-25   |Medellin   |50005   |Colombia   |Antioquia   |No|


 @CasoAlternoDireccion
  Scenario Outline: Ingreso NO exitoso de una nueva dirección en mi cuenta de The Ninja Store 
    Given Que me encuentro en la aplicacion The Ninja Store
    When Ingreso a la autenticacion y diligencio los datos solicitados 
    |<Email>|<Password>|
    And Ingreso a la opcion Address Book y adicciono una nueva direccion
    And Diligenciar el formulario con los datos de la nueva direccion
    |<FirstName>|<LastName>|<Company>|<Address1>|<Address2>|<City>|<PostCode>|<Country>|<Region/State>|<DefaultAddress>|
    Then Verifico mensaje de error
    
           Examples: 
      |Id|Email|Password|FirstName|LastName|Company|Address1|Address2|City|PostCode|Country|Region/State|DefaultAddress|
      ##@externaldata@./src/test/resources/Datadriven/dtDatos.xlsx@Datos@2
   |2   |usertest@test.com   |usertest   |   |Varela   |Choucair Testing   |Calle 100 # 22 - 22   |Calle 101 # 25-25   |Medellin   |50005   |Colombia   |Antioquia   |No|
    

@CasoExitosoRegistro
  Scenario Outline: Registro de un nuevo usuario en la aplicacion de The Ninja Store 
    Given Que necesito registrarme en la aplicacion de The Ninja Store 
    When Se crea un nuevo usuario
    |<FirstName>|<LastName>|<E-Mail>|<Telephone>|<Password>|<PasswordConfirm>|<Subscribe>|
    Then Verifico el registro del usaurio

    Examples: 
    |FirstName|LastName|E-Mail|Telephone|Password|PasswordConfirm|Subscribe|
    ##@externaldata@./src/test/resources/Datadriven/dtDatos.xlsx@REGISTRO@1
   |Andrea   |Gil   |csgiraldo10@choucairtesting.com   |3001231212   |prueba   |prueba   |No|


