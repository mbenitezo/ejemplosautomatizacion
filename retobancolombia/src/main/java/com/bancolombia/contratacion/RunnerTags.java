package com.bancolombia.contratacion;

import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.runner.RunWith;
import com.bancolombia.contratacion.utilities.BeforeSuite;
import com.bancolombia.contratacion.utilities.DataToFeature;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

@CucumberOptions (features = "src/test/resources/features/ninja_Store.feature",tags= {"@Regresion"}, snippets= SnippetType.CAMELCASE)
@RunWith(RunnerPersonalizado.class)
public class RunnerTags {
	@BeforeSuite
	public static void test() throws InvalidFormatException, IOException {
			DataToFeature.overrideFeatureFiles("./src/test/resources/features/ninja_Store.feature");
	}
}