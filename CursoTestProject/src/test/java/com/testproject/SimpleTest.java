package com.testproject;

import io.testproject.sdk.drivers.web.SafariDriver;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.safari.SafariOptions;

public class SimpleTest {

    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        driver = new SafariDriver(new SafariOptions());
        driver.get("https://example.cypress.io/commands/actions");
    }

    @Test
    public void shouldOpen(){
        Assert.assertEquals("Cypress.io: Kitchen Sink", driver.getTitle());
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }
}
