package com.bancolombia.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	private static XSSFRow Row;

	public static void setExcelFile(String Path,String SheetName) throws Exception {
					FileInputStream ExcelFile = new FileInputStream(Path);
					ExcelWBook = new XSSFWorkbook(ExcelFile);
					ExcelWSheet = ExcelWBook.getSheet(SheetName);
	}
	
	public static String getCellData(int RowNum, int ColNum) throws Exception{
			String CellData = "";
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			CellData = Cell.getStringCellValue(); 
			CellData = Cell.getRawValue(); 
			return CellData;
	}
	
	public static int ContarFilas() throws Exception 
	{
		int NumFilas = ExcelWSheet.getLastRowNum();
		return NumFilas;
	}
	
	public static void setCellData(int RowNum, int ColNum, Object TextObtenido) {
	            Row = ExcelWSheet.createRow(RowNum);
	                XSSFCell cell = Row.createCell(ColNum);
	                if (TextObtenido instanceof String) {
	                    cell.setCellValue((String) TextObtenido);
	                } else if (TextObtenido instanceof Integer) {
	                    cell.setCellValue((Integer) TextObtenido);
	                }
	}	
	
	public static void SaveData(String Path) throws IOException, Exception {
			FileOutputStream ExcelFileOut = new FileOutputStream(Path);
			ExcelWBook.write(ExcelFileOut);
			CerrarBook();
	}
	
	public static void CerrarBook() throws IOException {
		ExcelWBook.close();
	}
}