package com.bancolombia.util;

import java.util.List;
import org.openqa.selenium.WebElement;

public class UtilSeleccionarDeLista 
{
	public static void Seleccionar(String datoPrueba, List<WebElement> Elemento){
		String textoComparar = datoPrueba;
		WebElement auxiliar = null;
		for( WebElement product : Elemento ){
			if(product.getText().contains( textoComparar )) {
				auxiliar = product;
				break;
			}
		}
		auxiliar.click();
	}	
}