package com.bancolombia;

import java.io.IOException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.runner.RunWith;
import com.bancolombia.util.BeforeSuite;
import com.bancolombia.util.DataToFeature;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(features="src/test/resources/features/cine_colombia_datos.feature" , 
				plugin = {"json:target/cucumber_json/cucumber.json"}, tags = "@casoExitoso", snippets= SnippetType.CAMELCASE )
@RunWith(CucumberWithSerenity.class)
public class RunnerAppium {
	@BeforeSuite
	public static void test() throws InvalidFormatException, IOException {
			DataToFeature.overrideFeatureFiles("./src/test/resources/features/cine_colombia_datos.feature");
	}

}
