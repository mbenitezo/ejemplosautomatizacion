package com.bancolombia.steps;

import java.io.IOException;
import java.util.List;

import com.bancolombia.pageobjects.CineColombiaGuardarPage;

import net.thucydides.core.annotations.Step;

public class CineColombiaGuardarSteps {

	CineColombiaGuardarPage cineColombiaGuardarPage;
	
	@Step
	public void IniciarApp() throws InterruptedException {
		//cineColombiaGuardarPage.PresionarListaCiudades();
		cineColombiaGuardarPage.PresionarSiguiente();		
	}
	
	@Step
	public void Siguiente(List<List<String>> data, int id) throws InterruptedException {
		//cineColombiaGuardarPage.PresionarNext(data.get(id).get(0).trim());
		cineColombiaGuardarPage.PresionarMenu();
	}
	
	@Step
	public void Menu() throws InterruptedException {

		cineColombiaGuardarPage.PresionarOpcionOpera();
		cineColombiaGuardarPage.PresionarMenuForma2();
		cineColombiaGuardarPage.PresionarOpcionBallet();
		cineColombiaGuardarPage.PresionarMenuForma2();
		cineColombiaGuardarPage.PresionarOpcionTeatro();
		cineColombiaGuardarPage.PresionarMenuForma2();
	}
	
	@Step
	public void ProcesoExcel(String archivo, String hoja) throws Exception {
		cineColombiaGuardarPage.AbrirExcel(archivo, hoja);
	}
	
	@Step
	public void IngresarAOpera() throws InterruptedException {
		cineColombiaGuardarPage.PresionarOpcionOpera();
	}
	
	
	@Step
	public void InformacionOpera() throws InterruptedException {
		cineColombiaGuardarPage.GuardarObrasOpera();
	}
	
	@Step
	public void MenuForma2() throws InterruptedException {
		cineColombiaGuardarPage.PresionarMenuForma2();
	}

	
	@Step
	public void IngresarABallet() throws InterruptedException {
		cineColombiaGuardarPage.PresionarOpcionBallet();
	}
	
	@Step
	public void InformacionBallet() throws InterruptedException {
		cineColombiaGuardarPage.GuardarObrasBallet();
	}

	@Step
	public void IngresarATeatro() throws InterruptedException {
		cineColombiaGuardarPage.PresionarOpcionTeatro();
	}
	
	@Step
	public void InformacionTeatro() throws InterruptedException {
		cineColombiaGuardarPage.GuardarObrasTeatro();
	}
	
	@Step
	public void CerrarProcesoExcel(String archivo) throws IOException, Exception {
		cineColombiaGuardarPage.CerrarExcel(archivo);
	}
	
}
