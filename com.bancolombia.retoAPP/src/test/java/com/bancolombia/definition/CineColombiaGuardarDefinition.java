package com.bancolombia.definition;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;

import com.bancolombia.steps.CineColombiaGuardarSteps;

public class CineColombiaGuardarDefinition {

 	private String archivo = "D://PracticasAutomatizacion//com.bancolombia.retoAPP//src//test//resources//DD//ObrasEnCarteleras.xlsx"; 
 	private String hoja = "Hoja1";
	
	@Steps
	CineColombiaGuardarSteps cineColombiaGuardarSteps;
	
	@Given("^Que yo ejecuto la app de cinecolombia$")
	public void que_yo_ejecuto_la_app_de_cinecolombia() throws InterruptedException{
		cineColombiaGuardarSteps.IniciarApp();
	}

	@When("^selecciono la ciudad e ingreso al listado de obras$")
	public void selecciono_la_ciudad_e_ingreso_al_listado_de_obras(DataTable dtDatosForm) throws InterruptedException{
		List<List<String>> data = dtDatosForm.raw();
		for(int i=1; i<data.size(); i++) {
			cineColombiaGuardarSteps.Siguiente(data, i);
		}
		cineColombiaGuardarSteps.Menu();
	}

	@Then("^guardo el listado en un archivo de Excel$")
	public void guardo_el_listado_en_un_archivo_de_Excel() throws Exception{
		cineColombiaGuardarSteps.ProcesoExcel(archivo, hoja);
		cineColombiaGuardarSteps.IngresarAOpera();
		cineColombiaGuardarSteps.InformacionOpera();
		cineColombiaGuardarSteps.MenuForma2();
		cineColombiaGuardarSteps.IngresarABallet();
		cineColombiaGuardarSteps.InformacionBallet();
		cineColombiaGuardarSteps.MenuForma2();
		cineColombiaGuardarSteps.IngresarATeatro();
		cineColombiaGuardarSteps.InformacionTeatro();
		cineColombiaGuardarSteps.MenuForma2();
		cineColombiaGuardarSteps.CerrarProcesoExcel(archivo);
	}
	
}
