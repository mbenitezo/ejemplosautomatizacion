package com.bancolombia.pageobjects;

import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.bancolombia.util.MobilePageObject;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import net.serenitybdd.core.annotations.findby.By;
import com.bancolombia.util.UtilSeleccionarDeLista;
import com.bancolombia.util.ExcelReader;;

public class CineColombiaGuardarPage extends MobilePageObject {

	public CineColombiaGuardarPage(WebDriver driver) {
		super(driver);
	}

		//campo Spinner
	@AndroidFindBy(id = "com.cinepapaya.cinecolombia:id/spiCities")
	private WebElement ListCiudades;
	
		//campo Contenedor de ciudades
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView")
	private List<WebElement> ContenedorElementosCiudades;
	
		//campo Botón continuar
	@AndroidFindBy(id = "com.cinepapaya.cinecolombia:id/btnNext")
	private WebElement btnNext;
	
		//campo continuarSegundaVentana
	@AndroidFindBy(id = "com.cinepapaya.cinecolombia:id/btnContinue")
	private WebElement btnContinuar;
	
		//campo Menú		
	@AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navegar hacia arriba\"]")
	private WebElement menuPrincipal;

		//campo <nombre>
	@AndroidFindBy(id = "com.cinepapaya.cinecolombia:id/tviOpera")
	private WebElement OpcionOpera;
	
		//campo <nombre>
	@AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Cine Colombia S.A.\"]")
	private WebElement menuPrincipalMetodo2;
	
		//campo <nombre>
	@AndroidFindBy(id = "com.cinepapaya.cinecolombia:id/tviBallet")
	private WebElement OpcionBallet;
	
		//campo <nombre>
	@AndroidFindBy(id = "com.cinepapaya.cinecolombia:id/tviTeatro")
	private WebElement OpcionTeatro;
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout")
	private List<WebElement> ContenedorElementosObras;
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")
	private List<WebElement> ValoresContendor;

	@AndroidFindBy(id = "com.cinepapaya.cinecolombia:id/tviCinecoEmpty")
	private WebElement InformacionBallet;
	
	

	public void PresionarListaCiudades(){
		ListCiudades.click();

	}

		/* Campo btnContinuar */
	public void PresionarNext(String strCiudad) throws InterruptedException {
		Seleccionar(strCiudad, ContenedorElementosCiudades);
		btnNext.click();
		esperar_segundos(2);
	}
	
	public void PresionarSiguiente() throws InterruptedException {
			btnContinuar.click();
			esperar_segundos(2);
	}
	
	public void PresionarMenu() throws InterruptedException {
			menuPrincipal.click();
			esperar_segundos(2);
	}
	
	
	
	
	
	public void PresionarOpcionOpera() throws InterruptedException {
		OpcionOpera.click();
		esperar_segundos(5);
	}
	
	public void PresionarMenuForma2() throws InterruptedException {
		menuPrincipalMetodo2.click();
		esperar_segundos(2);
	}

	public void PresionarOpcionBallet() throws InterruptedException {
		OpcionBallet.click();
		esperar_segundos(5);
	}
	
	public void PresionarOpcionTeatro() throws InterruptedException {
		OpcionTeatro.click();
		esperar_segundos(5);
	}
	
	public void AbrirExcel(String archivo, String hoja) throws Exception {
		ExcelReader.setExcelFile(archivo, hoja);
	}
	
	public void CerrarExcel(String archivo) throws IOException, Exception {
		ExcelReader.SaveData( archivo );		
	}

	
	public void GuardarObrasOpera() throws InterruptedException{
		int i=0;
		int j=0;
		int contar = ContenedorElementosObras.size();
		int contar2 = ValoresContendor.size();
		System.out.println(contar);
		System.out.println(contar2);
			for(i=1; i<=contar; i++) {
				for(j=1; j<=contar2; j++) {
				  System.out.println("Interno " + getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout["+i+"]")).getText());
					ExcelReader.setCellData(j, i, getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout["+i+"]/android.widget.TextView["+j+"]")).getText());					
				}
				System.out.println("Interno " + getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.FrameLayout["+i+"]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")).getText());
				ExcelReader.setCellData(j+1, i, getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.FrameLayout["+i+"]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")).getText());
				if(i==3) {
					break;
					//presionaMoverArriba();
				}
			}
		esperar_segundos(5);
	}
	
	public void presionaMoverArriba() {
		TouchAction actions = new TouchAction((MobileDriver<?>) getDriver());
		actions.press(590, 438).moveTo(100, 200).release().perform();
	}

	public void GuardarObrasBallet() throws InterruptedException{
		System.out.println(InformacionBallet.getText());
		ExcelReader.setCellData(6, 0, InformacionBallet.getText());
		esperar_segundos(5);
	}
	
	public void GuardarObrasTeatro() throws InterruptedException{
		int i=0;
		int j=0;
		int contar = ContenedorElementosObras.size();
		int contar2 = ValoresContendor.size();
		System.out.println(contar);
		System.out.println(contar2);
			for(i=1; i<=contar; i++) {
				for(j=1; j<=contar2; j++) {
					//ExcelReader.setCellData(i+6, j, getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.View/android.support.v7.widget.RecyclerView/android.widget.FrameLayout["+i+"]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView["+j+"]")).getText());
					  System.out.println("Interno " + getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout["+i+"]")).getText());
						ExcelReader.setCellData(j, i, getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout["+i+"]/android.widget.TextView["+j+"]")).getText());					
				}
				System.out.println("Interno " + getDriver().findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.FrameLayout["+i+"]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")).getText());
				if(i==3) {
					break;
					//presionaMoverArriba();
				}
			}
		esperar_segundos(5);
//		TraerDatosContenedor(ContenedorElementosObras, ValoresContendor);
	}

	public void Seleccionar(String datoPrueba, List<WebElement> Elemento){
		String textoComparar = datoPrueba;

		WebElement auxiliar = null;
		for( WebElement product : Elemento ){
//			System.out.println("Texto " + product.getText());
			if(product.getText().contains( textoComparar )) {
				auxiliar = product;
				break;
			}
		}
		auxiliar.click();
	}

	public void esperar_segundos(int intEspera) throws InterruptedException {
		Thread.sleep(intEspera * 1000);
	}
	
}
