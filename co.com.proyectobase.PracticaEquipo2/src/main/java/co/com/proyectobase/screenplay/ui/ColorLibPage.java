package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class ColorLibPage extends PageObject{

	public static final Target USUARIO = Target.the("Campo para ingresar usuario")
			.located(By.xpath("//*[@id=\"login\"]/form/input[1]"));
	
	public static final Target CLAVE = Target.the("Campo para la clave de usuario")
			.located(By.xpath("//*[@id=\"login\"]/form/input[2]"));
	
	public static final Target BOTON_LOGIN = Target.the("Bot�n para loguear a la pagina")
			.located(By.xpath("//*[@id=\"login\"]/form/button"));
	
}
