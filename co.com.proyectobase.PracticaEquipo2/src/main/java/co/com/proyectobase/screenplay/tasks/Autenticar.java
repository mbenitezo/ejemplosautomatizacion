package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.ColorLibPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Autenticar implements Task {

	private ColorLibPage colorLibPage;
	private String usuario;
	private String clave;
	
	public Autenticar(String usuario, String clave) {
		this.usuario = usuario;
		this.clave = clave;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
	
		actor.attemptsTo(Enter.theValue(usuario).into(ColorLibPage.USUARIO));
		actor.attemptsTo(Enter.theValue(clave).into(ColorLibPage.CLAVE));
		actor.attemptsTo(Click.on(ColorLibPage.BOTON_LOGIN));
		
	}

	public static Autenticar ConLosDatosCorrectos(String usuario, String clave) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(Autenticar.class, usuario, clave);
	}

}
