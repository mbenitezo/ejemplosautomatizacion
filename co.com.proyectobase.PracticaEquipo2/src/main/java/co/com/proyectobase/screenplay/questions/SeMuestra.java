package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.ui.ColorlibHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class SeMuestra implements Question<String> {

	private ColorlibHomePage colorlibHomePage;
	
	@Override
	public String answeredBy(Actor actor) {

		return Text.of(colorlibHomePage.MENSAJE_PRINCIPAL).viewedBy(actor).asString().trim();
	}

	public static SeMuestra elMensaje() {
		
		return new SeMuestra();
	}

}
