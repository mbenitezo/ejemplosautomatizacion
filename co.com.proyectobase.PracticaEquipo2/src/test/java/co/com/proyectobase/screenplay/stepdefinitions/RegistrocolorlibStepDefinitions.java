package co.com.proyectobase.screenplay.stepdefinitions;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.SeMuestra;
import co.com.proyectobase.screenplay.tasks.Autenticar;
import co.com.proyectobase.screenplay.tasks.Ingresar;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class RegistrocolorlibStepDefinitions {

	@Managed(driver = "chrome")
	private WebDriver driver;
	Actor anacleto = Actor.named("Anacleto");
	
	@Before
	public void setup() {
		
		anacleto.can(BrowseTheWeb.with(driver));
		
	}
	
	@Given("^Anacleto quiere autenticarse en la pagina de color lib$")
	public void anacletoQuiereAutenticarseEnLaPaginaDeColorLib() {

		anacleto.wasAbleTo(Ingresar.AlaPagina());
		
	}


	@When("^el ingresa (.*) y (.*)$")
	public void elIngresaUsuarioYClave(String usuario, String clave) {

		anacleto.attemptsTo(Autenticar.ConLosDatosCorrectos(usuario, clave));
				
	}

	@Then("^el debe ver el mensaje (.*)$")
	public void elDebeVerElMensajeMetis(String mensaje) {

		anacleto.should(GivenWhenThen.seeThat(SeMuestra.elMensaje(), Matchers.equalTo(mensaje)));
		
	}
	
}
