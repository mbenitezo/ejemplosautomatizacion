package com.bancolombia.tasas.steps;

import java.util.List;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.AccionesprocesoMontospage;

public class AccionesprocesoMontossteps {

	AccionesprocesoMontospage accionesprocesoMontospage;
	
	/*OPCION DE MONTOS*/	
	@Step
	public void OpcionMontos(List<List<String>> OpcionMontos, int id) {
		for(int i=0; i<OpcionMontos.size(); i++) {
			accionesprocesoMontospage.PresionarOpcionMontos(OpcionMontos.get(id).get(i).trim());
		}
	}
	
	@Step
	public void BotonAgregar() {
		accionesprocesoMontospage.PresionarBotonAgregar();
	}

	
	@Step
	public void LlenarFormularioMontos(List<List<String>> data, int id) throws InterruptedException {
		accionesprocesoMontospage.IngresarNombre(data.get(id).get(0).trim());
		accionesprocesoMontospage.IngresarMontoDesde(data.get(id).get(1).trim());
		accionesprocesoMontospage.IngresarMontoHasta(data.get(id).get(2).trim());
		accionesprocesoMontospage.PresionarBotonGuardar();
	}
}
