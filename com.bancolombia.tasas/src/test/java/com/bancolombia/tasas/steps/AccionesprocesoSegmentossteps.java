package com.bancolombia.tasas.steps;

import java.util.List;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.AccionesprocesoSegmentospage;

public class AccionesprocesoSegmentossteps {

	AccionesprocesoSegmentospage accionesprocesoSegmentospage;
	
	/*OPCION DE SEGMENTOS*/	
	@Step
	public void OpcionSegmentos(List<List<String>> OpcionSegmento, int id) {
		for(int i=0; i<OpcionSegmento.size(); i++) {
			accionesprocesoSegmentospage.PresionarOpcionSegmentos(OpcionSegmento.get(id).get(i).trim());
		}
	}
	
	@Step
	public void BotonAgregar() {
		accionesprocesoSegmentospage.PresionarBotonAgregar();
	}

	
	@Step
	public void LlenarFormularioSegmentos(List<List<String>> data, int id) throws InterruptedException {
		accionesprocesoSegmentospage.IngresarNombre(data.get(id).get(0).trim());
		accionesprocesoSegmentospage.IngresarTipo(data.get(id).get(1).trim());
		accionesprocesoSegmentospage.IngresarDescripcion(data.get(id).get(2).trim());
		accionesprocesoSegmentospage.PresionarBotonGuardar();
	}
}
