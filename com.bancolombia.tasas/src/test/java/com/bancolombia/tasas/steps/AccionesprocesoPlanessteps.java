package com.bancolombia.tasas.steps;

import java.util.List;
import com.bancolombia.tasas.pageobjects.AccionesprocesoPlanespage;
import net.thucydides.core.annotations.Step;

public class AccionesprocesoPlanessteps {

	AccionesprocesoPlanespage accionesprocesoPlanespage;
	
	/*OPCION DE PLANES */	
	@Step
	public void OpcionPlanes(List<List<String>> OpcionPlanes, int id) {
		for(int i=0; i<OpcionPlanes.size(); i++) {
			accionesprocesoPlanespage.PresionarOpcionPlanes(OpcionPlanes.get(id).get(i).trim());
		}
	}
	
	@Step
	public void BotonAgregar() {
		accionesprocesoPlanespage.PresionarBotonAgregar();
	}

	
	@Step
	public void LlenarFormularioPlanes(List<List<String>> data, int id) throws InterruptedException {
		accionesprocesoPlanespage.IngresarNombre(data.get(id).get(0).trim());
		accionesprocesoPlanespage.IngresarPuntos(data.get(id).get(1).trim());
		accionesprocesoPlanespage.PresionarBotonGuardar();
	}
	
}
