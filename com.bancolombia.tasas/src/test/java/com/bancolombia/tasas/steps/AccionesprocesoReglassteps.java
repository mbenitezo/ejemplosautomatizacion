package com.bancolombia.tasas.steps;

import java.util.List;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.AccionesprocesoMontospage;
import com.bancolombia.tasas.pageobjects.AccionesprocesoReglaspage;

public class AccionesprocesoReglassteps {

	AccionesprocesoReglaspage accionesprocesoReglaspage;
	
	/*OPCION DE REGLAS*/	
	@Step
	public void OpcionReglas(List<List<String>> OpcionReglas, int id) {
		for(int i=0; i<OpcionReglas.size(); i++) {
			accionesprocesoReglaspage.PresionarOpcionReglas(OpcionReglas.get(id).get(i).trim());
		}
	}
	
	@Step
	public void BotonAgregar() {
		accionesprocesoReglaspage.PresionarBotonAgregar();
	}

	
	@Step
	public void LlenarFormularioReglas(List<List<String>> data, int id) throws InterruptedException {
		accionesprocesoReglaspage.IngresarNombre(data.get(id).get(0).trim());
		accionesprocesoReglaspage.SeleccionarGrupo(data.get(id).get(1).trim());
		accionesprocesoReglaspage.SeleccionarSegmento(data.get(id).get(2).trim());
		accionesprocesoReglaspage.SeleccionarPlazo(data.get(id).get(3).trim());
		accionesprocesoReglaspage.SeleccionarMonto(data.get(id).get(4).trim());
		accionesprocesoReglaspage.IngresarPuntos(data.get(id).get(5).trim());
		accionesprocesoReglaspage.PresionarBotonGuardar();
	}
}
