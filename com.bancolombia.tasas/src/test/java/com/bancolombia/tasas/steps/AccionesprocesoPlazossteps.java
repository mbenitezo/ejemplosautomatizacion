package com.bancolombia.tasas.steps;

import java.util.List;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.AccionesprocesoPlazospage;

public class AccionesprocesoPlazossteps {

	AccionesprocesoPlazospage accionesprocesoPlazospage;
	
	/*OPCION DE PLAZOS*/	
	@Step
	public void OpcionPlazos(List<List<String>> OpcionPlazos, int id) {
		for(int i=0; i<OpcionPlazos.size(); i++) {
			accionesprocesoPlazospage.PresionarOpcionPlazos(OpcionPlazos.get(id).get(i).trim());
		}
	}
	
	@Step
	public void BotonAgregar() {
		accionesprocesoPlazospage.PresionarBotonAgregar();
	}

	
	@Step
	public void LlenarFormularioPlazos(List<List<String>> data, int id) throws InterruptedException {
		accionesprocesoPlazospage.IngresarNombre(data.get(id).get(0).trim());
		accionesprocesoPlazospage.IngresarPlazoDesde(data.get(id).get(1).trim());
		accionesprocesoPlazospage.IngresarPlazoHasta(data.get(id).get(2).trim());
		accionesprocesoPlazospage.PresionarBotonGuardar();
	}
}
