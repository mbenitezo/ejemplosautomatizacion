package com.bancolombia.tasas.steps;

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.AccionesprocesoGrupopage;

public class AccionesprocesoGruposteps {
	
	AccionesprocesoGrupopage accionesprocesoGrupopage;
	
	@Step
	public void MenuParametrosTasas(List<List<String>> Menu, int id) {
		for(int i=0; i<Menu.size(); i++) {
			accionesprocesoGrupopage.PresionarMenuParamentrosTasas(Menu.get(id).get(i).trim());
		}
	}

/*OPCION DE GRUPOS */	
	
	@Step
	public void OpcionGrupos(List<List<String>> OpcionGrupo, int id) {
		for(int i=0; i<OpcionGrupo.size(); i++) {
			accionesprocesoGrupopage.PresionarOpcionGrupos(OpcionGrupo.get(id).get(i).trim());
		}
	}
	@Step
	public void BotonAgregar() {
		accionesprocesoGrupopage.PresionarBotonAgregar();
	}
	@Step
	public void LlenarFormularioGrupo(List<List<String>> data, int id) throws InterruptedException {
		accionesprocesoGrupopage.IngresarNombre(data.get(id).get(0).trim());
		accionesprocesoGrupopage.IngresarTasaBase(data.get(id).get(1).trim());
		accionesprocesoGrupopage.PresionarBotonGuardar();
	}	
}