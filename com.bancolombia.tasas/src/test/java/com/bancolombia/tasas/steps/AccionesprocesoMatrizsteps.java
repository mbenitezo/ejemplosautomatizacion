package com.bancolombia.tasas.steps;

import java.util.List;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.AccionesprocesoMatrizpage;

public class AccionesprocesoMatrizsteps {

	AccionesprocesoMatrizpage accionesprocesoMatrizpage;
	
	@Step
	public void LlenarFormularioMatriz(List<List<String>> data, int i) throws InterruptedException {
		
		accionesprocesoMatrizpage.SeleccionarGrupo(data.get(i).get(0).trim());
		
	}

}
