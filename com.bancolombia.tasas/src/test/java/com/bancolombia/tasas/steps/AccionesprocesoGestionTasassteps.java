package com.bancolombia.tasas.steps;

import java.util.List;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.AccionesprocesoGestionTasaspage;

public class AccionesprocesoGestionTasassteps {

	AccionesprocesoGestionTasaspage accionesprocesoGestionTasaspage;
	
	@Step
	public void MenuGestionTasas(List<List<String>> Menu, int id) {
		for(int i=0; i<Menu.size(); i++) {
			accionesprocesoGestionTasaspage.PresionarMenuGestionTasas(Menu.get(id).get(i).trim());
		}
	}
	
/*OPCION DE MATRIZ CONSOLIDADA */	
	
	@Step
	public void OpcionMatriz(List<List<String>> OpcionGrupo, int id) {
		for(int i=0; i<OpcionGrupo.size(); i++) {
			accionesprocesoGestionTasaspage.PresionarOpcionGestionTasas(OpcionGrupo.get(id).get(i).trim());
		}
	}
	
}
