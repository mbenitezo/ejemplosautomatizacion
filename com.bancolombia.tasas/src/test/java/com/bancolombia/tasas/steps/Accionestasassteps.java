package com.bancolombia.tasas.steps;


import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.Accionestasaspage;

public class Accionestasassteps {
	
	Accionestasaspage accionestasaspage;
	
	@Step
	public void iniciarWeb() {
		accionestasaspage.open();
	}
	
	@Step
	public void ingresarDatosAcceso(List<List<String>> data, int id) {
		accionestasaspage.IngresarUsuario(data.get(id).get(0).trim());
		accionestasaspage.IngresarClave(data.get(id).get(1).trim());
		
	}
	
	@Step
	public void botonIngresar() throws InterruptedException {
		accionestasaspage.PresionarBoton();
	}
	
	@Step
	public void validar(List<List<String>> data, int id) {
		accionestasaspage.ValidarMensaje(data.get(id).get(0).trim());
	}
	
}