package com.bancolombia.tasas.steps;

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import com.bancolombia.tasas.pageobjects.AccionesAdministradortasaspage;

public class Accionesadministradortasassteps {
	
	AccionesAdministradortasaspage accionesAdministradortasaspage;
	
	@Step
	public void iniciarWeb() {
		accionesAdministradortasaspage.open();
	}
	
	@Step
	public void ingresarDatosAcceso(List<List<String>> data, int id) {
		accionesAdministradortasaspage.IngresarUsuario(data.get(id).get(0).trim());
		accionesAdministradortasaspage.IngresarClave(data.get(id).get(1).trim());
		
	}
	
	@Step
	public void botonIngresar() throws InterruptedException {
		accionesAdministradortasaspage.PresionarBoton();
	}
	
	@Step
	public void validar(List<List<String>> data, int id) {
		for(int i=0; i<data.size()+2; i++) {
		accionesAdministradortasaspage.ValidarOpcionesMenu(data.get(id).get(i).trim(), i);
		}
	}
	
}