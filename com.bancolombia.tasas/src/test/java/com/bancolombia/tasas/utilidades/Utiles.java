package com.bancolombia.tasas.utilidades;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;

public class Utiles {

	public static void AccionClicElemento(String TextoABuscar, List<WebElement> Contenedor){
		String textoComparar = TextoABuscar;
		WebElement auxiliar = null;
		int i = 0;
		for( WebElement webElement : Contenedor ){
			i++;
			if(webElement.getText().trim().contains( textoComparar )) {
				auxiliar = webElement;
				break;
			}
		}
		auxiliar.click();
	}	
	public static void AccionClicElementoE(String TextoABuscar, List<WebElement> Contenedor){
		String textoComparar = TextoABuscar;
		System.out.println("Buscado " + textoComparar);
		System.out.println("Cantidad " + Contenedor.size());
		WebElement auxiliar = null;
		int i = 0;
		for( WebElement webElement : Contenedor ){
			System.out.println(" Elemento " + i + " " +webElement.getText());
			i++;
			if(webElement.getText().trim().contains( textoComparar )) {
				auxiliar = webElement;
				break;
			}
		}
		auxiliar.click();
	}	
	
	public static void esperar_segundos(int intEspera) throws InterruptedException {
		Thread.sleep(intEspera * 1000);
	}
}
