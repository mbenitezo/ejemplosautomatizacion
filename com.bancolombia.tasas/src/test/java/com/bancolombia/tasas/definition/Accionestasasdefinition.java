package com.bancolombia.tasas.definition;

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;
import com.bancolombia.tasas.steps.Accionestasassteps;;

public class Accionestasasdefinition {
	
	@Steps
	Accionestasassteps accionestasassteps;
	
	
//	@Steps

	
//	@Steps
	
	@Given("^Yo quiero loguearme en el sistema$")
	public void yoQuieroLoguearmeEnElSistema(){
		accionestasassteps.iniciarWeb();
	}

	@When("^Ingreso los datos de acceso al sistema$")
	public void ingresoLosDatosDeAccesoAlSistema(DataTable Datos) throws InterruptedException{
		List<List<String>> data = Datos.raw();
		for(int i=0; i<data.size(); i++) {
			accionestasassteps.ingresarDatosAcceso(data, i);
		}
		accionestasassteps.botonIngresar();
	}

	@Then("^valido que las opciones si sean permitidas para el usuario$")
	public void validoQueLasOpcionesSiSeanPermitidasParaElUsuario(DataTable OpcionesMenu){
		List<List<String>> datoOptMenu = OpcionesMenu.raw();
		for(int j=0; j<datoOptMenu.size(); j++) {
			accionestasassteps.validar(datoOptMenu, j);
		}
	}
	
}