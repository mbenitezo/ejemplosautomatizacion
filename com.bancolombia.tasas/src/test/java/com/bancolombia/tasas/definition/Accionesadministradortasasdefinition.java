package com.bancolombia.tasas.definition;

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;
import com.bancolombia.tasas.steps.Accionesadministradortasassteps;
import com.bancolombia.tasas.steps.AccionesprocesoGruposteps;
import com.bancolombia.tasas.steps.AccionesprocesoMatrizsteps;
import com.bancolombia.tasas.steps.AccionesprocesoMontossteps;
import com.bancolombia.tasas.steps.AccionesprocesoPlanessteps;
import com.bancolombia.tasas.steps.AccionesprocesoPlazossteps;
import com.bancolombia.tasas.steps.AccionesprocesoReglassteps;
import com.bancolombia.tasas.steps.AccionesprocesoSegmentossteps;
import com.bancolombia.tasas.steps.AccionesprocesoGestionTasassteps;

public class Accionesadministradortasasdefinition {
	
	@Steps
	Accionesadministradortasassteps accionesadministradortasassteps;
	
	@Steps
	AccionesprocesoGruposteps accionesprocesoGruposteps;

	@Steps
	AccionesprocesoSegmentossteps accionesprocesoSegmentossteps;

	@Steps
	AccionesprocesoPlanessteps accionesprocesoPlanessteps;

	@Steps
	AccionesprocesoMontossteps accionesprocesoMontossteps;

	@Steps
	AccionesprocesoPlazossteps accionesprocesoPlazossteps;

	@Steps
	AccionesprocesoReglassteps accionesprocesoReglassteps;
	
	@Steps
	AccionesprocesoGestionTasassteps accionesprocesoGestionTasassteps;
	
	@Steps
	AccionesprocesoMatrizsteps accionesprocesoMatrizsteps;
	

	@Given("^Yo quiero loguearme en el sistema como administrador$")
	public void yoQuieroLoguearmeEnElSistemaComoAdministrador(){
		accionesadministradortasassteps.iniciarWeb();
	}

	@When("^Ingreso los datos de acceso al sistema correspondientemente$")
	public void ingresoLosDatosDeAccesoAlSistemaCorrespondientemente(DataTable Datos) throws InterruptedException{
		List<List<String>> data = Datos.raw();
		for(int i=0; i<data.size(); i++) {
			accionesadministradortasassteps.ingresarDatosAcceso(data, i);
		}
		accionesadministradortasassteps.botonIngresar();
	}

	@Then("^valido que las opciones si sean permitidas para el administrador$")
	public void validoQueLasOpcionesSiSeanPermitidasParaElAdministrador(DataTable OpcionesMenu){
		
		List<List<String>> datoOptMenu = OpcionesMenu.raw();
		for(int j=0; j<datoOptMenu.size(); j++) {
			accionesadministradortasassteps.validar(datoOptMenu, j);
		}
		
	}
	
	
	@When("^Ingresa al menu requerido$")
	public void ingresaAlMenuRequerido(DataTable DatoMenu){
		List<List<String>> dMenu = DatoMenu.raw();
		for(int i=0; i<dMenu.size(); i++) {
			accionesprocesoGruposteps.MenuParametrosTasas(dMenu, i);
		}
	}

	@When("^Selecciona la opcion del menu de Grupos$")
	public void seleccionaLaOpcionDelMenuDeGrupos(DataTable DatoOpcionMenu){
//		List<List<String>> dOpcMenu = DatoOpcionMenu.raw();
//		for(int i=0; i<dOpcMenu.size(); i++) {
//			accionesprocesoGruposteps.OpcionGrupos(dOpcMenu, i);
//		}	
	}

	@When("^Ingresa la informacion del formulario de Grupos y la guarda$")
	public void ingresaLaInformacionDelFormularioDeGruposYLaGuarda(DataTable DatosFormulario) throws InterruptedException{
//		accionesprocesoGruposteps.BotonAgregar();
//		List<List<String>> dFormulario = DatosFormulario.raw();
//		for(int i=0; i<dFormulario.size();i++) {
//			accionesprocesoGruposteps.LlenarFormularioGrupo(dFormulario, i);
//		}
	}

	@When("^Selecciona la opcion del menu de Segmentos$")
	public void seleccionaLaOpcionDelMenuDeSegmentos(DataTable DatoOpcionMenu){
		List<List<String>> dOpcMenu = DatoOpcionMenu.raw();
		for(int i=0; i<dOpcMenu.size(); i++) {
			accionesprocesoSegmentossteps.OpcionSegmentos(dOpcMenu, i);
		}
	}

	@When("^Ingresa la informacion del formulario de Segmentos y la guarda$")
	public void ingresaLaInformacionDelFormularioDeSegmentosYLaGuarda(DataTable DatosFormulario) throws InterruptedException{
		accionesprocesoSegmentossteps.BotonAgregar();
		List<List<String>> dFormulario = DatosFormulario.raw();
		for(int i=0; i<dFormulario.size();i++) {
			accionesprocesoSegmentossteps.LlenarFormularioSegmentos(dFormulario, i);
		}
	}

	@When("^Selecciona la opcion del menu de Planes$")
	public void seleccionaLaOpcionDelMenuDePlanes(DataTable DatoOpcionMenu){
		List<List<String>> dOpcMenu = DatoOpcionMenu.raw();
		for(int i=0; i<dOpcMenu.size(); i++) {
			accionesprocesoPlanessteps.OpcionPlanes(dOpcMenu, i);
		}
	}

	@When("^Ingresa la informacion del formulario de Planes y la guarda$")
	public void ingresaLaInformacionDelFormularioDePlanesYLaGuarda(DataTable DatosFormulario) throws InterruptedException{
		accionesprocesoPlanessteps.BotonAgregar();
		List<List<String>> dFormulario = DatosFormulario.raw();
		for(int i=0; i<dFormulario.size();i++) {
			accionesprocesoPlanessteps.LlenarFormularioPlanes(dFormulario, i);
		}		
	}

	@When("^Selecciona la opcion del menu de Montos$")
	public void seleccionaLaOpcionDelMenuDeMontos(DataTable DatoOpcionMenu){
		List<List<String>> dOpcMenu = DatoOpcionMenu.raw();
		for(int i=0; i<dOpcMenu.size(); i++) {
			accionesprocesoMontossteps.OpcionMontos(dOpcMenu, i);
		}		
	}

	@When("^Ingresa la informacion del formulario de Montos y la guarda$")
	public void ingresaLaInformacionDelFormularioDeMontosYLaGuarda(DataTable DatosFormulario) throws InterruptedException{
		accionesprocesoMontossteps.BotonAgregar();
		List<List<String>> dFormulario = DatosFormulario.raw();
		for(int i=0; i<dFormulario.size();i++) {
			accionesprocesoMontossteps.LlenarFormularioMontos(dFormulario, i);
		}		
	}

	@When("^Selecciona la opcion del menu de Plazos$")
	public void seleccionaLaOpcionDelMenuDePlazos(DataTable DatoOpcionMenu){
		List<List<String>> dOpcMenu = DatoOpcionMenu.raw();
		for(int i=0; i<dOpcMenu.size(); i++) {
			accionesprocesoPlazossteps.OpcionPlazos(dOpcMenu, i);
		}
	}

	@When("^Ingresa la informacion del formulario de Plazos y la guarda$")
	public void ingresaLaInformacionDelFormularioDePlazosYLaGuarda(DataTable DatosFormulario) throws InterruptedException{
		accionesprocesoPlazossteps.BotonAgregar();
		List<List<String>> dFormulario = DatosFormulario.raw();
		for(int i=0; i<dFormulario.size();i++) {
			accionesprocesoPlazossteps.LlenarFormularioPlazos(dFormulario, i);
		}		
	}

	@When("^Selecciona la opcion del menu de Reglas de calculo$")
	public void seleccionaLaOpcionDelMenuDeReglasDeCalculo(DataTable DatoOpcionMenu){
		List<List<String>> dOpcMenu = DatoOpcionMenu.raw();
		for(int i=0; i<dOpcMenu.size(); i++) {
			accionesprocesoReglassteps.OpcionReglas(dOpcMenu, i);
		}
	}

	@When("^Ingresa la informacion del formulario de Reglas de calculo y la guarda$")
	public void ingresaLaInformacionDelFormularioDeReglasDeCalculoYLaGuarda(DataTable DatosFormulario) throws InterruptedException{
		accionesprocesoReglassteps.BotonAgregar();
		List<List<String>> dFormulario = DatosFormulario.raw();
		for(int i=0; i<dFormulario.size();i++) {
			accionesprocesoReglassteps.LlenarFormularioReglas(dFormulario, i);
		}		
	}

	@When("^Ingresa al menu Gestion de tasas$")
	public void ingresaAlMenuGestionDeTasas(DataTable DatoMenu){

		List<List<String>> dMenu = DatoMenu.raw();
		for(int i=0; i<dMenu.size(); i++) {
			accionesprocesoGestionTasassteps.MenuGestionTasas(dMenu, i);
		}
		
	}

	@When("^Selecciona la opcion del menu de Matriz consolidada$")
	public void seleccionaLaOpcionDelMenuDeMatrizConsolidada(DataTable DatoOpcionMenu){

		List<List<String>> dOpcMenu = DatoOpcionMenu.raw();
		for(int i=0; i<dOpcMenu.size(); i++) {
			accionesprocesoGestionTasassteps.OpcionMatriz(dOpcMenu, i);
		}
		
	}

	@Then("^valido que los calculos se esten realizando de manera correcta$")
	public void validoQueLosCalculosSeEstenRealizandoDeManeraCorrecta(DataTable DatosFormulario) throws InterruptedException{
		List<List<String>> dFormulario = DatosFormulario.raw();
		for(int i=0; i<dFormulario.size();i++) {
			accionesprocesoMatrizsteps.LlenarFormularioMatriz(dFormulario, i);
		}		

	}
	
}