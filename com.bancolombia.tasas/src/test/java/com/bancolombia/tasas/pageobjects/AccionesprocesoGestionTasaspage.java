package com.bancolombia.tasas.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import com.bancolombia.tasas.utilidades.Utiles;

public class AccionesprocesoGestionTasaspage extends PageObject{

	@FindBy(css = ".z-tab-accordion-text")
	public List<WebElement> lblOpcionMenu;
	
	@FindBy(xpath = "//*[contains(@id,'e2-cave')]")
	public List<WebElement> lblOpcion;


	public void PresionarMenuGestionTasas(String Menu) {
		Utiles.AccionClicElemento(Menu, lblOpcionMenu);
	}
	
	public void PresionarOpcionGestionTasas(String OpcionMenu) {
		Utiles.AccionClicElemento(OpcionMenu, lblOpcion);
	}
	

	
}
