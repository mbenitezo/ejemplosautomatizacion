package com.bancolombia.tasas.pageobjects;

import java.util.List;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://serviciosqapseries.bancolombia.corp/ADM-Tasas-Web/pages/login.zul")
public class Accionestasaspage extends PageObject {
	
	@FindBy(xpath = "//INPUT[contains(@placeholder, \"Ingresa tu usuario\")]")
	public WebElementFacade txtLogin;
	
	@FindBy(xpath = "//INPUT[contains(@placeholder, \"Ingresa tu clave\")]")
	public WebElementFacade txtPassword;
	
	@FindBy(css = "button.btn-ingresar.z-button-os")
	public WebElementFacade btnIngresar;

	@FindBy(css = ".z-tab-accordion-text")
	public WebElementFacade lblOpcion;
	
	public void IngresarUsuario(String dato) {
		txtLogin.click();
		txtLogin.clear();
		txtLogin.sendKeys(dato);
	}
	
	public void IngresarClave(String dato) {
		txtPassword.click();
		txtPassword.clear();
		txtPassword.sendKeys(dato);
	}
	
	public void PresionarBoton() throws InterruptedException {
		btnIngresar.click();
		esperar_segundos(5);
	}
	
	public void ValidarMensaje(String mensaje) {
		String lblmensaje = lblOpcion.getText();
		assertThat(mensaje, containsString(lblmensaje));
	}
	
	public void esperar_segundos(int intEspera) throws InterruptedException {
		Thread.sleep(intEspera * 1000);
	}
	
}