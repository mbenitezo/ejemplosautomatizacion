package com.bancolombia.tasas.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.bancolombia.tasas.utilidades.Utiles;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AccionesprocesoReglaspage extends PageObject {

	@FindBy(css = ".z-label")
	public List<WebElement> lblOpcion;
	
	@FindBy(xpath = "//img[contains(@src, 'new.png')]")
	public WebElementFacade btnBoton;
	
	@FindBy(xpath = "//*[contains(@id,\"u-real\")]")
	public WebElementFacade txtNombre;
	
	@FindBy(xpath = "//*[contains(@id,\"_0-btn\")]")
	public WebElementFacade btnBuscarGrupo;

	@FindBy(xpath = "//*[contains(@id,\"10-rows\")]/tr")
	public List<WebElement> ContenedorGrupo;
	
	@FindBy(xpath = "//*[contains(@id,\"70-btn\")]")
	public WebElementFacade btnBuscarSegmento;
	
	@FindBy(xpath = "//*[contains(@id,'a-rows')]/tr")
	public List<WebElement> ContenedorSegmento;
	
	@FindBy(xpath = "//*[contains(@id,\"a0-btn\")]")
	public WebElementFacade btnBuscarPlazo;

	@FindBy(xpath = "//*[contains(@id,'c0-rows')]/tr")
	public List<WebElement> ContenedorPlazo;
	
	@FindBy(xpath = "//*[contains(@id,\"h0-btn\")]")
	public WebElementFacade btnBuscarMonto;

	@FindBy(xpath = "//*[contains(@id,'j0-rows')]/tr")
	public List<WebElement> ContenedorMonto;
	
	@FindBy(xpath = "//*[contains(@id,\"o0-real\")]")
	public WebElementFacade txtPuntos;
	
	@FindBy(xpath = "//img[contains(@src, 'save.png')]")
	public WebElementFacade btnGuardar;
	
	@FindBy(xpath = "//img[contains(@src, 'back.png')]")
	public WebElementFacade btnRegresar;
	
	public void PresionarOpcionReglas(String OpcionMenu) {
		Utiles.AccionClicElemento(OpcionMenu, lblOpcion);
	}
	
	public void PresionarBotonAgregar() {
		btnBoton.click();
	}
	
	public void IngresarNombre(String Dato) {
		txtNombre.click();
		txtNombre.clear();
		txtNombre.sendKeys(Dato);
	}
	
	public void SeleccionarGrupo(String Dato) throws InterruptedException {
		btnBuscarGrupo.click();	
		Utiles.esperar_segundos(2);
		Utiles.AccionClicElemento(Dato, ContenedorGrupo);
		Utiles.esperar_segundos(2);
	}

	public void SeleccionarSegmento(String Dato) throws InterruptedException {
		btnBuscarSegmento.click();
		Utiles.esperar_segundos(2);
		Utiles.AccionClicElemento(Dato, ContenedorSegmento);
		Utiles.esperar_segundos(2);
	}

	public void SeleccionarPlazo(String Dato) throws InterruptedException {
		btnBuscarPlazo.click();	
		Utiles.esperar_segundos(2);
		Utiles.AccionClicElemento(Dato, ContenedorPlazo);
		Utiles.esperar_segundos(2);
	}

	public void SeleccionarMonto(String Dato) throws InterruptedException {
		btnBuscarMonto.click();	
		Utiles.esperar_segundos(2);
		Utiles.AccionClicElemento(Dato, ContenedorMonto);
		Utiles.esperar_segundos(2);
	}
	
	public void IngresarPuntos(String Dato) {
		txtPuntos.click();
		txtPuntos.clear();
		txtPuntos.sendKeys(Dato);
	}
	
	public void PresionarBotonGuardar() throws InterruptedException {
		btnGuardar.click();
		Utiles.esperar_segundos(3);
		btnRegresar.click();
		Utiles.esperar_segundos(5);
	}
	
	
}
