package com.bancolombia.tasas.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import com.bancolombia.tasas.utilidades.Utiles;

public class AccionesprocesoGrupopage extends PageObject{

	@FindBy(css = ".z-tab-accordion-text")
	public List<WebElement> lblOpcionMenu;
	
	@FindBy(css = ".z-label")
	public List<WebElement> lblOpcion;


	@FindBy(xpath = "//img[contains(@src, 'new.png')]")
	public WebElementFacade btnBoton;
	
	@FindBy(css = ".z-textbox-rounded-inp")
	public WebElementFacade txtnombre;

	@FindBy(css = ".z-decimalbox-rounded-inp")
	public WebElementFacade txtTasa;

	@FindBy(xpath = "//img[contains(@src, 'save.png')]")
	public WebElementFacade btnGuardar;

	@FindBy(xpath = "//img[contains(@src, 'back.png')]")
	public WebElementFacade btnRegresar;
	
	public void PresionarMenuParamentrosTasas(String Menu) {
		Utiles.AccionClicElemento(Menu, lblOpcionMenu);
	}
	
	public void PresionarOpcionGrupos(String OpcionMenu) {
		Utiles.AccionClicElemento(OpcionMenu, lblOpcion);
	}
	
	public void PresionarBotonAgregar() {
		btnBoton.click();
	}
		  
	public void IngresarNombre(String Dato) {
		txtnombre.click();
		txtnombre.clear();
		txtnombre.sendKeys(Dato);
	}

	public void IngresarTasaBase(String Dato) {
		txtTasa.click();
		txtTasa.clear();
		txtTasa.sendKeys(Dato);
	}
	
	public void PresionarBotonGuardar() throws InterruptedException {
		btnGuardar.click();
		Utiles.esperar_segundos(3);
		btnRegresar.click();
		Utiles.esperar_segundos(5);
	}
	
}
