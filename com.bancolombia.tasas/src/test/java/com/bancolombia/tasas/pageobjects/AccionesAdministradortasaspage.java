package com.bancolombia.tasas.pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.bancolombia.tasas.utilidades.Utiles;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://serviciosqapseries.bancolombia.corp/ADM-Tasas-Web/pages/login.zul")
public class AccionesAdministradortasaspage extends PageObject {
	
	@FindBy(xpath = "//INPUT[contains(@placeholder, \"Ingresa tu usuario\")]")
	public WebElementFacade txtLogin;
	
	@FindBy(xpath = "//INPUT[contains(@placeholder, \"Ingresa tu clave\")]")
	public WebElementFacade txtPassword;
	
	@FindBy(css = "button.btn-ingresar.z-button-os")
	public WebElementFacade btnIngresar;

	@FindBy(css = ".z-tab-accordion-text")
	public List<WebElement> lblOpcion;
	
	public void IngresarUsuario(String dato) {
		txtLogin.click();
		txtLogin.clear();
		txtLogin.sendKeys(dato);
	}
	
	public void IngresarClave(String dato) {
		txtPassword.click();
		txtPassword.clear();
		txtPassword.sendKeys(dato);
	}
	
	public void PresionarBoton() throws InterruptedException {
		btnIngresar.click();
		Utiles.esperar_segundos(5);
	}

	public void ValidarOpcionesMenu(String mensaje, int indice) {
		for (int i=indice; i<lblOpcion.size(); i++) {
			if(lblOpcion.get(i).getText().contains( mensaje )) {
				assertThat(lblOpcion.get(i).getText(), containsString(mensaje));
				break;
			}
		}
	}
	
	}