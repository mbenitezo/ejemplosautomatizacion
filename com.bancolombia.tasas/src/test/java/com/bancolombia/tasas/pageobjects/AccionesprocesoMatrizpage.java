package com.bancolombia.tasas.pageobjects;

import java.util.List;

import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.bancolombia.tasas.utilidades.Utiles;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.sourceforge.htmlunit.corejs.javascript.regexp.SubString;

public class AccionesprocesoMatrizpage extends PageObject{
	
	@FindBy(xpath = "//*[contains(@id, 'p-btn')]")
	public WebElementFacade btnGrupo;
	
	@FindBy(xpath = "//*[contains(@id, 'r-rows')]/tr")
	public List<WebElement> contenedorGrupos;

	@FindBy(xpath = "//*[contains(@id, 'u-btn')]")
	public WebElementFacade btnSegmento;
	
	@FindBy(xpath = "//*[contains(@id, 'w-rows')]/tr")
	public List<WebElement> contenedorSegmentos;	

	@FindBy(xpath = "//*[contains(@id, '_0-btn')]")
	public WebElementFacade btnPlan;
	
	@FindBy(xpath = "//*[contains(@id, '10-rows')]/tr")
	public List<WebElement> contenedorPlan;	
	
	@FindBy(xpath = "//*[contains(@id, '50-btn')]")
	public WebElementFacade btnMonto;
	
	@FindBy(xpath = "//*[contains(@id, '70-rows')]/tr")
	public List<WebElement> contenedorMonto;

	@FindBy(xpath = "//*[contains(@id, 'b-btn')]")
	public WebElementFacade btnPlazo;
	
	@FindBy(xpath = "//*[contains(@id, 'd-rows')]/tr")
	public List<WebElement> contenedorPlazo;
	
	@FindBy(xpath = "//*[contains(@id, 'g0-real')]/tr")
	public List<WebElement> txtpreview;
	
	@FindBy(xpath = "//img[contains(@src, 'save.png')]")
	public WebElementFacade btnGuardar;
	

	public void SeleccionarGrupo(String dato) throws InterruptedException {
		btnGrupo.click();
		Utiles.AccionClicElemento(dato, contenedorGrupos);
		WebElement auxiliar = null;
		int i = 0;
		for( WebElement elemento : contenedorGrupos ){
			i++;
			if(elemento.getText().trim().contains( dato )) {
				auxiliar = elemento;
				break;
			}
		}
		System.out.println("Comprobación: " + auxiliar.getText());
		Utiles.esperar_segundos(5);
		
	}

	
		
	public void PresionarBotonGuardar() throws InterruptedException {
//		Utiles.esperar_segundos(5);
	}
	
}
