package com.bancolombia.tasas.pageobjects;

import java.util.List;

import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.bancolombia.tasas.utilidades.Utiles;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.sourceforge.htmlunit.corejs.javascript.regexp.SubString;

public class AccionesprocesoSegmentospage extends PageObject{
	
	@FindBy(css = ".z-label")
	public List<WebElement> lblOpcion;
	
	@FindBy(xpath = "//img[contains(@src, 'new.png')]")
	public WebElementFacade btnBoton;
	
	@FindBy(xpath = "//img[contains(@src, 'save.png')]")
	public WebElementFacade btnGuardar;
	
	@FindBy(xpath = "//img[contains(@src, 'back.png')]")
	public WebElementFacade btnRegresar;
	
	@FindBy(xpath = "//*[contains(@id,\"11-real\")]")
	public WebElementFacade txtNombre;

	@FindBy(xpath = "//*[contains(@id,\"41-real\")]")
	public WebElementFacade txtTipo;

	@FindBy(xpath = "//*[contains(@id,\"a1-real\")]")
	public WebElementFacade txtDescripcion;
	
	public void PresionarOpcionSegmentos(String OpcionMenu) {
		Utiles.AccionClicElemento(OpcionMenu, lblOpcion);
	}
	
	public void PresionarBotonAgregar() {
		btnBoton.click();
	}

	public void IngresarNombre(String Dato) {
		txtNombre.click();
		txtNombre.clear();
		txtNombre.sendKeys(Dato);
	}

	public void IngresarTipo(String Dato) {
		txtTipo.click();
		txtTipo.clear();
		txtTipo.sendKeys(Dato);
	}

	public void IngresarDescripcion(String Dato) {
		txtDescripcion.click();
		txtDescripcion.clear();
		txtDescripcion.sendKeys(Dato);
	}
	
	public void PresionarBotonGuardar() throws InterruptedException {
		btnGuardar.click();
		Utiles.esperar_segundos(3);
		btnRegresar.click();
		Utiles.esperar_segundos(5);
	}
	
}
