package com.bancolombia.tasas.pageobjects;

import java.util.List;
import org.openqa.selenium.WebElement;
import com.bancolombia.tasas.utilidades.Utiles;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AccionesprocesoPlanespage extends PageObject{

	@FindBy(css = ".z-label")
	public List<WebElement> lblOpcion;
	
	@FindBy(xpath = "//img[contains(@src, 'new.png')]")
	public WebElementFacade btnBoton;
	
	@FindBy(xpath = "//*[contains(@id,\"u-real\")]")
	public WebElementFacade txtNombre;
	
	@FindBy(xpath = "//*[contains(@id,\"_0-real\")]")
	public WebElementFacade txtPuntos;
	
	@FindBy(xpath = "//img[contains(@src, 'save.png')]")
	public WebElementFacade btnGuardar;
	
	@FindBy(xpath = "//img[contains(@src, 'back.png')]")
	public WebElementFacade btnRegresar;

	public void PresionarOpcionPlanes(String OpcionMenu) {
		Utiles.AccionClicElemento(OpcionMenu, lblOpcion);
	}
	
	public void PresionarBotonAgregar() {
		btnBoton.click();
	}
	
	public void IngresarNombre(String Dato) {
		txtNombre.click();
		txtNombre.clear();
		txtNombre.sendKeys(Dato);
	}

	public void IngresarPuntos(String Dato) {
		txtPuntos.click();
		txtPuntos.clear();
		txtPuntos.sendKeys(Dato);
	}

	public void PresionarBotonGuardar() throws InterruptedException {
		btnGuardar.click();
		Utiles.esperar_segundos(3);
		btnRegresar.click();
		Utiles.esperar_segundos(5);
	}
	
}
