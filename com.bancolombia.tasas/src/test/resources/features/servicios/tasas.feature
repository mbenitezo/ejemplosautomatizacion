#Author: your.email@your.domain.com

@Regresion
Feature: Validación de perfiles de usuario y acciones dentro de tasas
  
  @LoginExitosoUsuario
  Scenario Outline: Login exitoso de usuario y verificacion de opciones de menu
    Given Yo quiero loguearme en el sistema
    When Ingreso los datos de acceso al sistema
    	| <usuario>	|	<clave>	|
    Then valido que las opciones si sean permitidas para el usuario
      |	<opciones> |

    Examples: 
   | usuario 	 | clave 				 	| opciones  |
##@externaldata@./src/test/resources/Datadriven/Datos.xlsx@Usuario@1
   |imoscoss   |Amb13nt32017*   |Reportes|
 

 @LoginExitosoAdministrador
  Scenario Outline: Login del administrador y verificacion de opciones de menu
    Given Yo quiero loguearme en el sistema como administrador
    When Ingreso los datos de acceso al sistema correspondientemente
    	| <usuario>	|	<clave>	|
    Then valido que las opciones si sean permitidas para el administrador
      |	<opcion1> |	<opcion2> |	<opcion3>	|

    Examples: 
   | usuario 	 | clave 			 | opcion1  				 |	opcion2					 |opcion3	|
##@externaldata@./src/test/resources/Datadriven/Datos.xlsx@Administrador@1
   |aoria   |D5i/AoHVw   |Parámetros tasas   |Gestión de tasas   |Reportes|


 @ProcesoTasas
 Scenario Outline: Login del administrador y verificacion de opciones de menu
    Given Yo quiero loguearme en el sistema como administrador
    When Ingreso los datos de acceso al sistema correspondientemente
    	| <usuario>				|	<clave>				|
    And Ingresa al menu requerido
    	|	<Opcion_menu_1>	|
    And Selecciona la opcion del menu de Grupos
    	|	<Item_menu_1>		|
    And Ingresa la informacion del formulario de Grupos y la guarda
    	|	<Nombre_grupo>	|	<Tasa_grupo>	|
    And Selecciona la opcion del menu de Segmentos
    	|	<Item_menu_2>		|
    And Ingresa la informacion del formulario de Segmentos y la guarda
    	|<Nombre_segmento>|<Tipo_segmento>|<Descripcion_segmento>|
    And Selecciona la opcion del menu de Planes
    	|	<Item_menu_3>		|	
    And Ingresa la informacion del formulario de Planes y la guarda
    	|	<Nombre_plan>		|	<Puntos_plan>	|
    And Selecciona la opcion del menu de Montos 
    	|	<Item_menu_4>		|
    And Ingresa la informacion del formulario de Montos y la guarda
    	|	<Monto_nombre>	|<Monto_inicial>|<Monto_final>|
    And Selecciona la opcion del menu de Plazos
    	|	<Item_menu_5>		|
    And Ingresa la informacion del formulario de Plazos y la guarda
    	|	<Nombre_plazo>	|	<Plazo_desde>	|<Plazo_hasta>|
    And Selecciona la opcion del menu de Reglas de calculo
    	|	<Item_menu_6>		|	
    And Ingresa la informacion del formulario de Reglas de calculo y la guarda
    	|	<Nombre_regla>	|<Nombre_grupo_1>|	<Nombre_segmento1> |	<Monto_inicial1>	|	<Plazo_desde1>	|	<Puntos_regla>	|
    And Ingresa al menu Gestion de tasas
    	|	<Opcion_menu_2>	|
		And Selecciona la opcion del menu de Matriz consolidada
			|	<Item_menu_7>		|
    Then valido que los calculos se esten realizando de manera correcta
     	|	<Nombre_grupo_2>|	<Nombre_segmento_1>|<Nombre_plan_1>|<Monto_inicial_1>|<Plazo_desde_1>|

   Examples: 
   | usuario 	 | clave 			 | Opcion_menu_1    |Item_menu_1| Nombre_grupo	|	Tasa_grupo	|Item_menu_2 |Nombre_segmento|Tipo_segmento|Descripcion_segmento|Item_menu_3|Nombre_plan|Puntos_plan|Item_menu_4 |Monto_nombre	| Monto_inicial	| Monto_final|Item_menu_5|Nombre_plazo	|	Plazo_desde	|Plazo_hasta| Item_menu_6				 | Nombre_regla	|	Nombre_grupo_1	|	Nombre_segmento1	|	Monto_inicial1	|	Plazo_desde1	|	Puntos_regla	|	Opcion_menu_2			| Item_menu_7					| Nombre_grupo_2 |	Nombre_segmento_1 | Nombre_plan_1 | Monto_inicial_1 | Plazo_desde_1 |	 
##@externaldata@./src/test/resources/Datadriven/Datos.xlsx@Proceso@1
   |aoria   |D5i/AoHVw   |Parámetros tasas   |Grupos   |testG7   |2   |Segmentos   |testS3   |tipotest   |descripcion test   |Planes   |testP99   |3   |Montos   |testRM9   |600000001   |699999999   |Plazos   |testRP4   |62   |62   |Reglas de cálculo   |testRC5   |TESTG7   |TESTS3   |62   |600,000,001.00   |10   |Gestión de tasas   |Matriz consolidada   |TESTG7   |TESTS3   |TESTP99   |600,000,001.00   |62|


