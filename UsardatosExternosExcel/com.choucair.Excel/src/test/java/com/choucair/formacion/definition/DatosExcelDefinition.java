package com.choucair.formacion.definition;

import net.serenitybdd.core.Serenity;

/**
 * Estructura básica de clase para Definition DatosExcelDefinition
 *
 * @author  Mario A. Benítez
 * @version  24/05/2018 - 10:07:09 a. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;
import com.choucair.formacion.steps.DatosExcelSteps;
import com.choucair.formacion.utilities.ExcelReader;

public class DatosExcelDefinition {

	@Steps
	DatosExcelSteps datosExcelSteps;
	

	
	
//	@Steps
	
	
//	@Steps
	
	@Given("^Quiero acceder a la plataforma Zoho$")
	public void quiero_acceder_a_la_plataforma_Zoho() {

		datosExcelSteps.AccesoInicioSesion();
	}

	@When("^Realizo la autenticacion con datos de Excel$")
	public void realizo_la_autenticacion_con_datos_de_Excel() throws Throwable {

//		String rutaArchivo = "//com.choucair.Excel//src//test//resources//Datadriven//Datos.xlsx";
		String rutaArchivo = "D:\\PracticasAutomatizacion\\UsardatosExternosExcel\\com.choucair.Excel\\src\\test\\resources\\Datadriven\\Datos.xlsx";
		String hojaDeCalculo = "Hoja1";
		ExcelReader.setExcelFile(rutaArchivo, hojaDeCalculo);
		
		int numFilas = ExcelReader.ContarFilas();
		//System.out.println(" TOTAL   TOTAL  -- -" + numFilas);
		for(int i = 1; i <= numFilas; i++) {
			datosExcelSteps.LogueaseAlSistema(i);
		}
		ExcelReader.CerrarBook();
	}

	@Then("^Verifico acceso exitoso$")
	public void verifico_acceso_exitoso() {

		datosExcelSteps.IniciarSesion();
		
	}	
	
	
	
	
}