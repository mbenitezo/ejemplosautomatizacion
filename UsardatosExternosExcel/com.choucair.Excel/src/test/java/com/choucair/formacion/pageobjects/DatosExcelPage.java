package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object DatosExcelPage
 *
 * @author  Mario A. Benítez
 * @version  24/05/2018 - 10:20:09 a. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;

import com.choucair.formacion.utilities.ExcelReader;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.zoho.com/es-xl/crm/")
public class DatosExcelPage extends PageObject {
	
	@FindBy(xpath = "//*[@id=\'block-block-3\']/div/div[1]/a[2]")
	public WebElementFacade btnIniciarSesion;

	@FindBy(xpath = "//*[@id=\'lid\']")
	public WebElementFacade txtUsuario;

@FindBy(xpath = "//*[@id=\'pwd\']")
	public WebElementFacade txtClave;

@FindBy(xpath = "//*[@id=\'signin_submit\']")
	public WebElementFacade btnIniciar;

	public void AbrirFormularioInicioSesion() {
		btnIniciarSesion.click();
	}
	
	public void Loguearse(int i) throws Exception {
		txtUsuario.click();
		txtUsuario.clear();
		txtUsuario.sendKeys( ExcelReader.getCellData(i, 0).trim() );
		txtClave.click();
		txtClave.clear();
		txtClave.sendKeys( ExcelReader.getCellData(i, 1).trim() );
	}
	
	public void BotonAutenticar() {
		btnIniciar.click();
	}

}