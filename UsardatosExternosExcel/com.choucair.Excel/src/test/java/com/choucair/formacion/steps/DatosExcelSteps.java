package com.choucair.formacion.steps;

/**
 * Estructura básica de clase para Steps DatosExcelSteps
 *
 * @author  Mario A. Benítez
 * @version  24/05/2018 - 1:10:38 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.DatosExcelPage;

public class DatosExcelSteps {
	
	DatosExcelPage datosExcelPage;

	@Step
	public void AccesoInicioSesion() {
		datosExcelPage.open();
		datosExcelPage.AbrirFormularioInicioSesion();
	}

	
	@Step
	public void LogueaseAlSistema(int i) throws Exception {
		datosExcelPage.Loguearse(i);
	}
		
	@Step
	public void IniciarSesion() {
		datosExcelPage.BotonAutenticar();
	}
	
	
}