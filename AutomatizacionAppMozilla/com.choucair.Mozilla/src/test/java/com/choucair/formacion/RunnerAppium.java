package com.choucair.formacion;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/Mozilla/BuscarDato.feature" , 
				plugin = {"json:target/cucumber_json/cucumber.json"}, tags = "@CasoExitoso" )
public class RunnerAppium {

}
