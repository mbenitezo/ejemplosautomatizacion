package com.choucair.formacion.definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.choucair.formacion.steps.BuscarDatoSteps;

public class BuscarDatoDefinition {

	@Steps
	BuscarDatoSteps buscarDatoSteps;
	
	@Given("^Se requiere acceder a la pagina del tiempo$")
	public void se_requiere_acceder_a_la_pagina_del_tiempo() throws InterruptedException{

		buscarDatoSteps.IniciarApp();
		
	}

	@Given("^Me autentico en la plataforma con mi usuario \"([^\"]*)\" y contrasena \"([^\"]*)\"$")
	public void me_autentico_en_la_plataforma_con_mi_usuario_y_contrasena(String arg1, String arg2){

		
	}

	@When("^Realizo la busqueda de informacion con el criterio \"([^\"]*)\"$")
	public void realizo_la_busqueda_de_informacion_con_el_criterio(String arg1){

		
	}

	@Then("^Valido que se realice la busqueda$")
	public void valido_que_se_realice_la_busqueda(){

		
	}

	@Then("^Me deslogueo de la plataforma$")
	public void me_deslogueo_de_la_plataforma(){

		
	}
	
}
