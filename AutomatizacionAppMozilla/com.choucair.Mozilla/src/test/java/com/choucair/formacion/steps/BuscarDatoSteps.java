package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.BuscarDatoPage;
import net.thucydides.core.annotations.Step;

public class BuscarDatoSteps {

	BuscarDatoPage buscarDatoPage;

	@Step
	public void IniciarApp() throws InterruptedException {
		buscarDatoPage.TocarBarraEnlace();
	}
}
