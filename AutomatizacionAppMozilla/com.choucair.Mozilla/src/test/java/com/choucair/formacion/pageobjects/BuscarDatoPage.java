package com.choucair.formacion.pageobjects;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.choucair.formacion.util.MobilePageObject;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class BuscarDatoPage extends MobilePageObject {

	public BuscarDatoPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	//campo titulo
	@AndroidFindBy(id = "org.mozilla.firefox:id/url_bar_title")
	private WebElement txtBarraTitulo;
	
	/* Campo txtBarraTitulo  */
	public void TocarBarraEnlace() throws InterruptedException {
		
		
		
		String pagina = "www.eltiempo.com";
		txtBarraTitulo.click();
		txtBarraTitulo.clear();
		txtBarraTitulo.clear();
		esperar_segundos(2);
		txtBarraTitulo.sendKeys( pagina );
		txtBarraTitulo.sendKeys( pagina );
		txtBarraTitulo.sendKeys( pagina );
		MobileElement element = (MobileElement) driver.findElementByAccessibilityId("SomeAccessibilityID");
		element.sendKeys("Hello world!");
//		txtBarraTituloXpath.sendKeys( pagina );
//		txtBarraTitulo.sendKeys(Keys.ENTER);
//		txtBarraTituloXpath.sendKeys( pagina );
		esperar_segundos(2);
	}
	
	public void esperar_segundos(int intEspera) throws InterruptedException {
		Thread.sleep(intEspera * 1000);
	}
	
}
