#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Busqueda de informacion en la pagina de El Tiempo

  @CasoExitoso
  Scenario: Buscar informacion en la plataforma de El Tiempo
    Given Se requiere acceder a la pagina del tiempo
    And Me autentico en la plataforma con mi usuario "maalben@gmail.com" y contrasena "123456"
    When Realizo la busqueda de informacion con el criterio "Rusia 2018"
    Then Valido que se realice la busqueda
    And Me deslogueo de la plataforma

#  @tag2
#  Scenario Outline: Title of your scenario outline
#    Given I want to write a step with <name>
#    When I check for the <value> in step
#    Then I verify the <status> in step

#    Examples: 
#      | name  | value | status  |
#      | name1 |     5 | success |
#      | name2 |     7 | Fail    |
