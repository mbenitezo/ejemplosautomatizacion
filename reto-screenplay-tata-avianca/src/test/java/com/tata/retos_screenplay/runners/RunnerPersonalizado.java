package com.tata.retos_screenplay.runners;

import com.tata.retos_screenplay.util.exceldata.BeforeSuite;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import static com.tata.retos_screenplay.exceptions.ExceptionWeb.MESSAGE_EXCEPTION_INTERACTION_FIRST_SCREEN;

public class RunnerPersonalizado extends Runner {
     
        private Class<CucumberWithSerenity> classValue;
       private CucumberWithSerenity cucumberWithSerenity;
       
       private static final Logger LOGGER = LogManager.getLogger(com.tata.retos_screenplay.runners.RunnerPersonalizado.class.getName());
       
        
        public RunnerPersonalizado(Class<CucumberWithSerenity> classValue) throws Exception {
               this.classValue = classValue;
               cucumberWithSerenity = new CucumberWithSerenity(classValue);
           }
       
         @Override 
           public Description getDescription() { 
               return cucumberWithSerenity.getDescription();
           }
         
         private void runAnnotatedMethods(Class<?> annotation) throws Exception {
               if (!annotation.isAnnotation()) {
                   return;
               }
               Method[] methods = this.classValue.getMethods();
               for (Method method : methods) {
                   Annotation[] annotations = method.getAnnotations();
                   for (Annotation item : annotations) {
                       if (item.annotationType().equals(annotation)) {
                           method.invoke(null);
                           break;
                       }
                   }
               }
         }
         
         @Override
           public void run(RunNotifier notifier) {
               try {
                   runAnnotatedMethods(BeforeSuite.class);
                   cucumberWithSerenity = new CucumberWithSerenity(classValue);
               } catch (Exception e) {
                  LOGGER.error(MESSAGE_EXCEPTION_INTERACTION_FIRST_SCREEN + e.getMessage(),e);
               }
               cucumberWithSerenity.run(notifier);
           }
}

