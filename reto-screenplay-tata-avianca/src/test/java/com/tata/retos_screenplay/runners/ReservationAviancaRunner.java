package com.tata.retos_screenplay.runners;

import com.tata.retos_screenplay.util.exceldata.BeforeSuite;
import com.tata.retos_screenplay.util.exceldata.DataToFeature;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.runner.RunWith;
import java.io.IOException;
import static com.tata.retos_screenplay.util.constant.ConstantTypeClass.UTILITY_CLASS;

@RunWith(RunnerPersonalizado.class)
@CucumberOptions(features="src/test/resources/features/aviancaReserve.feature",
				 glue="com.tata.retos_screenplay.stepdefinitions",
		  		 monochrome = true,
				 plugin = {"pretty", "html:target/cucumber"},
				 strict = true,
				 snippets=SnippetType.CAMELCASE)
public class ReservationAviancaRunner {
	private ReservationAviancaRunner(){
		throw new IllegalStateException(UTILITY_CLASS);
	}
	@BeforeSuite
	public static void test() throws InvalidFormatException, IOException {
		DataToFeature.overrideFeatureFiles("./src/test/resources/features/aviancaReserve.feature");
	}
}

