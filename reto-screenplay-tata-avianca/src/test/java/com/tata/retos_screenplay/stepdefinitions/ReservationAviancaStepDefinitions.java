package com.tata.retos_screenplay.stepdefinitions;

import com.tata.retos_screenplay.exceptions.AssertionsReservation;
import com.tata.retos_screenplay.model.DataReservationAvianca;
import com.tata.retos_screenplay.questions.VerifyThatRate;
import com.tata.retos_screenplay.tasks.CompleteDataReservationTask;
import com.tata.retos_screenplay.tasks.EnterAvianca;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import java.util.List;
import static org.hamcrest.core.IsEqual.equalTo;

public class ReservationAviancaStepDefinitions {

	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor mario = Actor.named("Mario");

	@Before
	public void InitialConfiguration() {
		mario.can(BrowseTheWeb.with(hisBrowser));
	}

	@Given("^I want to access the Avianca page$")
	public void iWantToAccessTheAviancaPage() {
		mario.wasAbleTo(EnterAvianca.page());
	}

	@When("^I fill out the form to make a reservation$")
	public void iFillOutTheFormToMakeAReservation(List<DataReservationAvianca> data) {
		mario.attemptsTo(CompleteDataReservationTask.inTheForm(data));
	}

	@Then("^I validate the reservation fee$")
	public void iValidateTheReservationFee() {
		mario.should(GivenWhenThen.seeThat(VerifyThatRate.isDisplayed(), equalTo(true))
		.orComplainWith(AssertionsReservation.class, AssertionsReservation.NOFOUNDELEMENT));
	}
}
