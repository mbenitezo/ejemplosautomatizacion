#Author: Mario Alejandro Benítez Orozco
@version:Release-1
@Regression
Feature: Make reservation on Avianca platform

@SuccessfulBooking
Scenario Outline: Make successful reservation
  Given I want to access the Avianca page
  When I fill out the form to make a reservation
    |from       |to       |  adults   |   children   |  babies  |
    |<from>       |<to>       |  <adults>   |   <children>   |  <babies>  |
  Then I validate the reservation fee

  Examples:
    |from       |to       |  adults   |   children   |  babies  |
    ##@externaldata@./src/test/resources/datadriven/datos.xlsx@Datos@1
   |Medellín   |Bogotá   |2   |1   |1|

