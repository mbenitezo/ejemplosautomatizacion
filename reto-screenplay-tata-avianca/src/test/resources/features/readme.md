## Avianca Challenge with Screenplay.

This challenge allows:

* Evaluate a test scenario
* Validate implementation of the Screenplay pattern
* Let the challenge work when it runs