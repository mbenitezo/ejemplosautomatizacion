package com.tata.retos_screenplay.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.waits.WaitUntil;
import static com.tata.retos_screenplay.userinterface.AviancaReservationPage.BUTTON_RATE_SELECTED;
import static com.tata.retos_screenplay.util.constant.Constants.WAIT_TIME;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isClickable;

public class VerifyThatRate implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean elementVisible = false;
        actor.attemptsTo(WaitUntil.the(BUTTON_RATE_SELECTED, isClickable()).forNoMoreThan(WAIT_TIME).seconds());
        if(BUTTON_RATE_SELECTED.resolveFor(actor).isPresent()){
            elementVisible = true;
        }
        return elementVisible;
    }

    public static VerifyThatRate isDisplayed() {
        return new VerifyThatRate();
    }
}
