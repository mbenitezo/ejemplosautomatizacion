package com.tata.retos_screenplay.tasks;

import com.tata.retos_screenplay.interactions.PushButtonAdults;
import com.tata.retos_screenplay.interactions.PushButtonNoAdults;
import com.tata.retos_screenplay.model.DataReservationAvianca;
import com.tata.retos_screenplay.util.Wait;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;
import static com.tata.retos_screenplay.util.constant.Constants.WAIT_TIME;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isClickable;
import java.util.List;
import static com.tata.retos_screenplay.userinterface.AviancaReservationPage.*;

public class CompleteDataReservationTask implements Task{

	private  List<DataReservationAvianca> data;
	public CompleteDataReservationTask(List<DataReservationAvianca> data) {
		this.data = data;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {

		actor.attemptsTo(WaitUntil.the(BUTTON_COOKIES_CONFIRM, isClickable()).forNoMoreThan(WAIT_TIME).seconds());
		actor.attemptsTo(Click.on(BUTTON_COOKIES_CONFIRM));

		actor.attemptsTo(Click.on(LINK_FLY_RESERVATION));
		actor.attemptsTo(Click.on(LINK_ONE_WAY));

		actor.attemptsTo(Wait.theNext(1));
		actor.attemptsTo(WaitUntil.the(TEXT_TRAVEL_FROM, isClickable()).forNoMoreThan(WAIT_TIME).seconds());
		actor.attemptsTo(Click.on(TEXT_TRAVEL_FROM));
		actor.attemptsTo(Enter.theValue(data.get(0).getFrom()).into(TEXT_TRAVEL_FROM));
		actor.attemptsTo(Click.on(TEXT_TRAVEL_FROM));
		actor.attemptsTo(Wait.theNext(1));
		actor.attemptsTo(Enter.keyValues(Keys.DOWN).into(TEXT_TRAVEL_FROM));
		actor.attemptsTo(Enter.keyValues(Keys.ENTER).into(TEXT_TRAVEL_FROM));
		actor.attemptsTo(Enter.keyValues(Keys.TAB).into(TEXT_TRAVEL_FROM));

		actor.attemptsTo(Wait.theNext(1));
		actor.attemptsTo(WaitUntil.the(TEXT_TRAVEL_TO, isClickable()).forNoMoreThan(WAIT_TIME).seconds());
		actor.attemptsTo(Click.on(TEXT_TRAVEL_TO));
		actor.attemptsTo(Enter.theValue(data.get(0).getTo()).into(TEXT_TRAVEL_TO));
		actor.attemptsTo(Click.on(TEXT_TRAVEL_TO));
		actor.attemptsTo(Wait.theNext(1));
		actor.attemptsTo(Enter.keyValues(Keys.DOWN).into(TEXT_TRAVEL_TO));
		actor.attemptsTo(Enter.keyValues(Keys.ENTER).into(TEXT_TRAVEL_TO));

		actor.attemptsTo(Wait.theNext(3));

		actor.attemptsTo(Click.on(TEXT_DATE));
		actor.attemptsTo(Click.on(TABLE_DATE_ONE_WAY));

		actor.attemptsTo(Click.on(TEXT_PASSENGERS));

		actor.attemptsTo(PushButtonAdults.increaseWith(BUTTON_ADD_PASSENGER_ADULTS, data.get(0).getAdults()));
		actor.attemptsTo(PushButtonNoAdults.increaseWith(BUTTON_ADD_PASSENGER_CHILDREN, data.get(0).getChildren()));
		actor.attemptsTo(PushButtonNoAdults.increaseWith(BUTTON_ADD_PASSENGER_BABIES, data.get(0).getBabies()));

		actor.attemptsTo(Click.on(BUTTON_CONTINUE));
		actor.attemptsTo(Click.on(BUTTON_SEARCH_FLIES));
	}

	public static CompleteDataReservationTask inTheForm(List<DataReservationAvianca> data) {
		return new CompleteDataReservationTask(data);
	}

		

}
