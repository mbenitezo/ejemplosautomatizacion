package com.tata.retos_screenplay.tasks;

import com.tata.retos_screenplay.userinterface.AviancaReservationPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class EnterAvianca implements Task{
	
	private AviancaReservationPage aviancaReservationPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(aviancaReservationPage));
	}
	
	public static EnterAvianca page() {
		return instrumented(EnterAvianca.class);
	}

}
