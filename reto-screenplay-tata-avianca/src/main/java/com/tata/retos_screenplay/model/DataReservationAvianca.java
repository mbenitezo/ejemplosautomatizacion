package com.tata.retos_screenplay.model;

public class DataReservationAvianca {

    private String from;
    private String to;
    private String adults;
    private String children;
    private String babies;

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getAdults() {
        return adults;
    }

    public String getChildren() {
        return children;
    }

    public String getBabies() {
        return babies;
    }
}
