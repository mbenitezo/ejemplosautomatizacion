package com.tata.retos_screenplay.userinterface;

import com.tata.retos_screenplay.util.Date;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://www.avianca.com/co/es/")
public class AviancaReservationPage extends PageObject{

	public static final Target BUTTON_COOKIES_CONFIRM = Target.the("Button cookies confirm")
			.located(By.name("cookies-confirm"));

	public static final Target LINK_FLY_RESERVATION = Target.the("Link fly reservation")
			.located(By.id("reservatuvuelo"));

	public static final Target LINK_ONE_WAY = Target.the("Link one way")
			.located(By.xpath("//a[contains(text(), 'Solo ida')]"));

    public static final Target TEXT_TRAVEL_FROM = Target.the("Text travel from")
            .located(By.xpath("//input[contains(@id,'pbOrigen_2')]"));

	public static final Target TEXT_TRAVEL_TO = Target.the("Text travel to")
			.located(By.xpath("//input[contains(@id,'pbDestino_2')]"));

	public static final Target TEXT_DATE = Target.the("Text date")
			.located(By.xpath("//div[@role='tabpanel' and contains(@id,'ida_1')]//form[@name='flight']//div[@class='panel-tab']//div[@class='col-xs-12 campo-fechas col-sm-5 col-md-3 new-margin-r-booking']//input[@name='pbFechaIda']"));

	public static final Target TABLE_DATE_ONE_WAY = Target.the("Table date one way")
			.located(By.xpath("//div[@class='calendar-container hidden-xs right new-calendar-pos calendar-home']//td[@data-date='"+Date.now()+"']"));

	public static final Target TEXT_PASSENGERS = Target.the("Text passengers")
			.located(By.xpath("//input[contains(@id,'pbPasajeros_2')]"));

	public static final Target BUTTON_ADD_PASSENGER_ADULTS = Target.the("Button add passenger adults")
			.located(By.xpath("//div[@class='passenger float']//div[@class='controls adults' and @aria-label='Adultos Igual o mayores de 12 años']//div[@class='plus control']"));

	public static final Target BUTTON_ADD_PASSENGER_CHILDREN = Target.the("Button add passenger children")
			.located(By.xpath("//div[@class='passenger float']//div[@class='controls noadults' and @aria-label='Niños  2 - 11 años']//div[@class='plus control']"));

	public static final Target BUTTON_ADD_PASSENGER_BABIES = Target.the("Button add passenger babies")
			.located(By.xpath("//div[@class='passenger float']//div[@class='controls noadults' and @aria-label='Bebés Menores de 2 años']//div[@class='plus control']"));

	public static final Target BUTTON_CONTINUE = Target.the("Button continue")
			.located(By.xpath("//div[contains(@data-name,'pbPasajeros_2')]//button"));

	public static final Target BUTTON_SEARCH_FLIES = Target.the("Button search flies")
			.located(By.xpath("//div[@class='col-xs-12 campo-pasajerosboton col-sm-12 col-md-5 new-margin-l-booking new-margin-r-booking comoViajas-container']//button[@title='Buscar vuelos']"));

	public static final Target BUTTON_RATE_SELECTED = Target.the("Button rate selected")
			.located(By.xpath("//button[@class='day selected ng-star-inserted']"));

}