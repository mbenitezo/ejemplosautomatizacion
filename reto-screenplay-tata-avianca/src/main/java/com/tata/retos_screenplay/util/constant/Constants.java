package com.tata.retos_screenplay.util.constant;

public class Constants {
    public static final int WAIT_TIME = 60;

    private Constants() {
    }
}
