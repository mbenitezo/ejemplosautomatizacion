package com.tata.retos_screenplay.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Date {

    public static String now(){
        Calendar c = new GregorianCalendar();
        String dia;
        String mes;
        String annio;
        dia = Integer.toString(c.get(Calendar.DATE));

        if(c.get(Calendar.MONTH)+1<=9){
            mes = "0"+Integer.toString(c.get(Calendar.MONTH)+1);
        }else{
            mes = Integer.toString(c.get(Calendar.MONTH)+1);
        }

        annio = Integer.toString(c.get(Calendar.YEAR));

        return annio+"-"+mes+"-"+dia;
    }

    private Date(){

    }
}
