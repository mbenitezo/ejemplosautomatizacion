package com.tata.retos_screenplay.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;

public class PushButtonAdults implements Interaction {

    Target buttonIncrementPassengers;
    String quantityPress;

    public PushButtonAdults(Target buttonIncrementPassengers, String quantityPress) {
        this.buttonIncrementPassengers = buttonIncrementPassengers;
        this.quantityPress = quantityPress;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        int quantity = Integer.parseInt(quantityPress);
        for(int i=0; i<quantity-1; i++ ){
            actor.attemptsTo(Click.on(buttonIncrementPassengers));
        }
    }

    public static PushButtonAdults increaseWith(Target buttonIncrementPassengers, String quantityPress) {
        return new PushButtonAdults(buttonIncrementPassengers, quantityPress);
    }
}
