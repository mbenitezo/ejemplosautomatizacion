package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.DireccionSucursalesEscogerSucursalPage;

import net.thucydides.core.annotations.Step;

public class DireccionSucursalesDetallesDireccionSteps {

	DireccionSucursalesEscogerSucursalPage direccionSucursalesEscogerSucursalPage;
	@Step
	public void SeleccionarSede() {
		
		direccionSucursalesEscogerSucursalPage.VerDetalle();
		
		direccionSucursalesEscogerSucursalPage.EnviarConsola();
	}
	
}
