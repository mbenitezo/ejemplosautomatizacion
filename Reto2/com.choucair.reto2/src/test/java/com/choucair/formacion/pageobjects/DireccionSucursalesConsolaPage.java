package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class DireccionSucursalesConsolaPage extends PageObject {

	@FindBy(xpath="//*[@id=\"mapa\"]/div/div/div[1]/div[4]/div[4]/div[1]/div")
	public WebElementFacade rectangulo;
	
	@FindBy(xpath="//*[@id=\"tab1\"]/div[1]/div[6]/div[1]/div/div[2]/h3[1]")
	public WebElementFacade lblNombreBanco;

	
	public void EnviarConsola() {
System.out.println(".");

	
	System.out.println("Aqui el titulo: ");
	System.out.println( lblNombreBanco.getText() );
	System.out.println( rectangulo.getText() );

	}
	
}
