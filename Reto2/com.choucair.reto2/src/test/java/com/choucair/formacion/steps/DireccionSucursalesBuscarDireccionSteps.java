package com.choucair.formacion.steps;

import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.DireccionSucursalesDarDireccionPage;

public class DireccionSucursalesBuscarDireccionSteps {

	DireccionSucursalesDarDireccionPage direccionSucursalesDarDireccionPage;
	
@Step
public void EscribirDireccion() {
	direccionSucursalesDarDireccionPage.Direccion();
}

@Step
public void BuscarDireccion() {
	direccionSucursalesDarDireccionPage.Buscar();
}

}
