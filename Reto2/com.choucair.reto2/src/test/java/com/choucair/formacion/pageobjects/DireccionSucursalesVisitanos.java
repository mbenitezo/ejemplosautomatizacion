package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class DireccionSucursalesVisitanos extends PageObject {
	
	@FindBy(xpath="//*[@id=\'footer-content\']/div[1]/div/div/div[4]")
	public WebElementFacade btnVisitanos;
	
	public void Visitanos() {
		btnVisitanos.click();
	}
	
}
