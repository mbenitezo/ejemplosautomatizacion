package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.DireccionSucrusalesSeleccionarDireccionPage;

import net.thucydides.core.annotations.Step;

public class DireccionSucursalesSeleccionarDireccionSteps {

	DireccionSucrusalesSeleccionarDireccionPage direccionSucrusalesSeleccionarDireccionPage;
	
	@Step
	public void SeleccionarPrimeraDireccion() {
		
		direccionSucrusalesSeleccionarDireccionPage.EscogeDireccion();
		
	}
	
}
