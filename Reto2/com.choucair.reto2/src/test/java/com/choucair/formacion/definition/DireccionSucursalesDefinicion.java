package com.choucair.formacion.definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import com.choucair.formacion.steps.DireccionSucursalesEntrarVisitanosSteps;
import com.choucair.formacion.steps.DireccionSucursalesBuscarDireccionSteps;
import com.choucair.formacion.steps.DireccionSucursalesSeleccionarDireccionSteps;
import com.choucair.formacion.steps.DireccionSucursalesDetallesDireccionSteps;
import com.choucair.formacion.steps.DireccionSucursalesEnviarAConsolaSteps;

public class DireccionSucursalesDefinicion {

	@Steps
	DireccionSucursalesEntrarVisitanosSteps direccionSucursalesEntrarVisitanosSteps;
	
	@Steps
	DireccionSucursalesBuscarDireccionSteps direccionSucursalesBuscarDireccionSteps;
	
	@Steps
	DireccionSucursalesSeleccionarDireccionSteps direccionSucursalesSeleccionarDireccionSteps;
	
	@Steps
	DireccionSucursalesDetallesDireccionSteps direccionSucursalesDetallesDireccionSteps;
	
	@Steps
	DireccionSucursalesEnviarAConsolaSteps direccionSucursalesEnviarAConsolaSteps;
	
	@Given("^Ingreso a la opcion de Visitanos$")
	public void ingreso_a_la_opcion_de_Visitanos(){
		
		
		direccionSucursalesEntrarVisitanosSteps.EntrarAVisitanos();
		
	}

	@Given("^Escribir direccion$")
	public void escribir_direccion(){
		direccionSucursalesBuscarDireccionSteps.EscribirDireccion();
	}

	@Given("^Buscar direccion en el mapa$")
	public void buscar_direccion_en_el_mapa(){
		direccionSucursalesBuscarDireccionSteps.BuscarDireccion();
	}

	@Given("^Capturar la primera direccion$")
	public void capturar_la_primera_direccion(){
		direccionSucursalesSeleccionarDireccionSteps.SeleccionarPrimeraDireccion();
		
	}

	@When("^Buscar oficina en el mapa$")
	public void buscar_oficina_en_el_mapa(){

		direccionSucursalesDetallesDireccionSteps.SeleccionarSede();
		

	}

	@When("^Mostrar oficina en consola$")
	public void mostrar_oficina_en_consola(){
		direccionSucursalesEnviarAConsolaSteps.LlevarDatosSucursalAConsola();
		

	}
	
}
