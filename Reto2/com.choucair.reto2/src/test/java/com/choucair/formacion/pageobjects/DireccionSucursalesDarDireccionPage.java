package com.choucair.formacion.pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class DireccionSucursalesDarDireccionPage extends PageObject {
	
	@FindBy(xpath="//*[@id=\'srch-term\']")
	public WebElementFacade txtdireccion;
	
	@FindBy(xpath="//*[@id=\'tab1\']/div[1]/div[1]/div/button")
	public WebElementFacade btnBuscar;

	@FindBy(xpath="//*[@id=\'tab1\']/div[1]/div[6]/div[1]/div/div[1]/button")
	public WebElementFacade btnVer;
	
	public void Direccion() {
	String direccion = "El Poblado ";
	txtdireccion.click();
	txtdireccion.clear();
	txtdireccion.sendKeys(direccion);

	this.getDriver().findElement(By.id("srch-term")).sendKeys(Keys.DOWN);
	this.getDriver().findElement(By.id("srch-term")).sendKeys(Keys.ENTER);
	
	try {
		Thread.sleep(3000);
		btnVer.waitUntilVisible();
		btnVer.click();
		

} catch (InterruptedException e) {
	// TODO Auto-generated catch block
//	e.printStackTrace();
}
	
		}
	
	public void Buscar(){
		
		
	

		
	}
	
}
