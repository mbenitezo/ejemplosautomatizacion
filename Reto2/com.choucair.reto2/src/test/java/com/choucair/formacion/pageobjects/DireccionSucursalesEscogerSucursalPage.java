package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class DireccionSucursalesEscogerSucursalPage extends PageObject {

	
	
	@FindBy(xpath="//*[@id=\"mapa\"]/div/div/div[1]/div[4]/div[4]/div[1]/div", timeoutInSeconds="5")
	public WebElementFacade rectangulo;
	
	
	public void VerDetalle() {
		
	
		rectangulo.waitUntilVisible();
	
	}
	
	public void EnviarConsola() {

			System.out.println( "Información de la dirección del banco:" );	

	
	
		
	}
	
}
