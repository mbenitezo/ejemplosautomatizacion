package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.DireccionSucursalesConsolaPage;

import net.thucydides.core.annotations.Step;

public class DireccionSucursalesEnviarAConsolaSteps {

	DireccionSucursalesConsolaPage direccionSucursalesConsolaPage;
	
	@Step
	public void LlevarDatosSucursalAConsola() {
		
		direccionSucursalesConsolaPage.EnviarConsola();
		
	}
	
}
