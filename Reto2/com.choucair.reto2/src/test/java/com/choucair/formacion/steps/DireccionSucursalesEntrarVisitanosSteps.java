package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.DireccionSucursalesVisitanos;

import net.thucydides.core.annotations.Step;

public class DireccionSucursalesEntrarVisitanosSteps {

	DireccionSucursalesVisitanos direccionSucursalesVisitanos;
	
@Step
public void EntrarAVisitanos() {
	direccionSucursalesVisitanos.open();
	direccionSucursalesVisitanos.Visitanos();
	
}
	
}
