#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Buscar información de dirección de sucursales
  El usuario debe poder ingresar a la opción Visitanos,
  luego escribir una direccion para consultar las mas cercanas,
  una vez desplegadas las oficinas cercanas, capturar de la página
  web la primera dirección listada y con dicha información buscar
  la oficina y verla en el mapa de la página.

  @CasoExitoso
  Scenario: Mostrar oficina en el mapa
    Given Ingreso a la opcion de Visitanos
    And Escribir direccion
    And Buscar direccion en el mapa
    And Capturar la primera direccion
    And Buscar oficina en el mapa
    When Mostrar oficina en consola
    

  @tag2
  Scenario Outline: Title of your scenario outline
    Given I want to write a step with <name>
    When I check for the <value> in step
    Then I verify the <status> in step

    Examples: 
      | name  | value | status  |
      | name1 |     5 | success |
      | name2 |     7 | Fail    |
