#Author: mbenitezo@choucairtesting.com

@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @CasoFeliz
  Scenario Outline: Consultar table de clientes CNAME y verificar resultados
    Given Consultar CNAME
			|	<Documento>			|	<Tipo Dcto>	|	<Nombre>				|	<Control Terceros>	|

    Examples: 
			|	Documento				|	Tipo Dcto		|	Nombre					|		Control Terceros	|
      | 000008000003931 |     3 			| PERFORMANCE 		|					8						|
     	| 000008000004576 |     3				| ELIANA MORALES	|					8						|			
