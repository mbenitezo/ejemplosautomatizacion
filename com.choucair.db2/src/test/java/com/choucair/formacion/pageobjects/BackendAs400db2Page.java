package com.choucair.formacion.pageobjects;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.choucair.formacion.utilities.Sql_Execute;

import net.serenitybdd.core.pages.PageObject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class BackendAs400db2Page extends PageObject{

	public String Armar_Query_Consulta_CNAME(String strDocumento) {
		System.out.println("Docu esperado   "  +  strDocumento);
		String strQuery = "SELECT * FROM GPLILIBRA.TMPCNAME WHERE CNNOSS = '<documento>'";
		strQuery = strQuery.replace("<documento>", strDocumento);

		return strQuery;
	}
	
	public ResultSet Ejecutar_Query(String Query) throws SQLException {
		Sql_Execute DAO = new Sql_Execute();
		ResultSet rs = DAO.sql_Execute(Query);
		return rs;
	}
	
	public void Verificar_Consulta_CNAME(ResultSet rs, List<List<String>> data) throws SQLException {
		while(rs.next()) {
			String Documento_recibido = rs.getString(1);
			String Documento_esperado = data.get(0).get(0);
			
			
			assertThat(Documento_recibido, equalTo(Documento_esperado));

			String TipoDoc_recibido = rs.getString(2);
			String TipoDoc_esperado = data.get(0).get(1);
			assertThat(TipoDoc_recibido, equalTo(TipoDoc_esperado));
			
			String Nombre_recibido = rs.getString(3);
			String Nombre_esperado = data.get(0).get(2);
			assertThat(Nombre_recibido.trim(), equalTo(Nombre_esperado.trim()));
			
			String CtrlTercero_recibido = rs.getString(4);
			String CtrlTercero_esperado = data.get(0).get(3);
			assertThat(CtrlTercero_recibido.trim(), equalTo(CtrlTercero_esperado.trim()));
			
			
		}
	}
	
}
