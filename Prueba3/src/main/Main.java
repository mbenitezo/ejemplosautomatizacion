package main;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		double notas[] = new double[5];
		int porcentaje[] = {20, 10, 30, 20, 20};
		
		notas[0] = 4.5;
		notas[1] = 3.2;
		notas[2] = 5;
		notas[3] = 1.5;
		notas[4] = 4.3;
		
		double promedio = 0;
		
		for(int i=0; i <= notas.length; i++) {
			try {
			promedio = (notas[i] * (porcentaje[i]) / 100) + promedio;
			}catch(ArrayIndexOutOfBoundsException ex) {
				System.out.println("Desbordamiento");
			}
		}
		
		System.out.println("El promedio del curso es: " + Math.round(promedio));
		
		System.out.println("**********************************************");
		
		int numeros[][] = new int[5][4];
		
		for(int filas = 0; filas < 5; filas++) {
			for(int columnas = 0; columnas < 4; columnas++) {
				numeros[filas][columnas] = (int) Math.ceil(Math.random()*100);
			}
		}
		
		for(int filas = 0; filas < 5; filas++) {
			for(int columnas = 0; columnas < 4; columnas++) {
				System.out.print(numeros[filas][columnas] + " ");
			}
			System.out.println();
		}
		
		System.out.println("**********************************************");
		
		List<String> lista = new ArrayList<String>();
		
		lista.add("Colombia");
		lista.add("Titiribi");
		lista.add("Medellín");
		
		System.out.println("La posición en memoria de Colombia es " + lista.indexOf("Colombia"));
		System.out.println("Ejemplo de get " + lista.get(2));
		
	}
}