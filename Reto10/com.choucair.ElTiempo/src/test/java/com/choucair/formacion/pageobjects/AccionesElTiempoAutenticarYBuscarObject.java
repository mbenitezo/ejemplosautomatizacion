package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object AccionesElTiempoAutenticarYBuscarObject
 *
 * @author  Mario A. Benítez
 * @version  7/06/2018 - 11:50:48 a. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("http://www.eltiempo.com")
public class AccionesElTiempoAutenticarYBuscarObject extends PageObject {
	
	@FindBy(className = "login-bar")
	public WebElementFacade lnkLogin;
	
	@FindBy(id = "username")
	public WebElementFacade txtUsuario;
	
	@FindBy(id = "password")
	public WebElementFacade txtPassword;

	@FindBy(id = "submitbutton")
	public WebElementFacade btniniciar;
	
	@FindBy(css = "span[class='buscador-icon']")
	public WebElementFacade btniconoBuscar1;
	
	@FindBy(name = "q")
	public WebElementFacade txtbuscar;
	
	@FindBy(className = "search-results-title")
		public WebElementFacade lblresultado;
	
	@FindBy(id = "sas_defaultCloseButton_7947978")
		public WebElementFacade btnCerrarPublicidad;
	
	
	/* Campo Login  */
	public void IngresarLogin() {
		lnkLogin.click();
	}
	
	/* Campo txtUsuario  */
	public void IngresarUsuario(String Dato) {
		this.getDriver().switchTo().frame("iframe_login");
		txtUsuario.click();
		txtUsuario.clear();
		txtUsuario.sendKeys(Dato);
	}

	/* Campo txtPassword  */
	public void IngresarPassword(String Dato) {
		txtPassword.click();
		txtPassword.clear();
		txtPassword.sendKeys(Dato);
		JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
		this.getDriver().switchTo().defaultContent();
		js.executeScript("window.scrollBy(0,500)");
		this.getDriver().switchTo().frame("iframe_login");
	}

	/* Campo btniniciar  */
	public void PresionarBoton() {
		btniniciar.click();
	}
	
	public void BuscarCriterio(String Dato){
		btniconoBuscar1.click();
		txtbuscar.click();
		txtbuscar.clear();
		txtbuscar.sendKeys( Dato );
		txtbuscar.sendKeys(Keys.ENTER);
	}
	
	public void AccionVerificacionResultados(String Dato) {
		JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
		js.executeScript("window.scrollBy(0,100)");
		String strMensaje = Dato;
		String textCompleto = lblresultado.getText().trim();
		if(textCompleto.contains( strMensaje )) {
			System.out.println(" Criterio comprobado : " + textCompleto);
		}
	}
	
	public void VerificarResultados(String Dato){
		
			if(btnCerrarPublicidad.isDisplayed()) {
				btnCerrarPublicidad.click();
				AccionVerificacionResultados(Dato);
			}else {
				AccionVerificacionResultados(Dato);
			}
		

	}

	}