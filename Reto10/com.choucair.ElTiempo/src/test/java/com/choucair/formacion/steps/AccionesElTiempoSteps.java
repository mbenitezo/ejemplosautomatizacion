package com.choucair.formacion.steps;

/**
 * Estructura básica de clase para Steps AccionesElTiempoSteps
 *
 * @author  Mario A. Benítez
 * @version  6/06/2018 - 5:24:38 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.AccionesElTiempoObject;
import com.choucair.formacion.utilities.ExcelReader;

public class AccionesElTiempoSteps {
	
	AccionesElTiempoObject accionesElTiempoObject;
	
	@Step
	public void abrirSitio() {
		accionesElTiempoObject.open();
	}
	
	@Step
	public void RegistrarseAlSistema(int i) throws Exception{
		accionesElTiempoObject.Nombre(ExcelReader.getCellData(i, 0).trim());
		accionesElTiempoObject.Apellido(ExcelReader.getCellData(i, 1).trim());
		accionesElTiempoObject.Email(ExcelReader.getCellData(i, 2).trim());
		accionesElTiempoObject.Password(ExcelReader.getCellData(i, 3).trim());
	}
	
	@Step
	public void AceptarTerminos() {
		accionesElTiempoObject.Terminos();
	}
	
	@Step
	public void CrearCuenta(){
		accionesElTiempoObject.Boton();
		accionesElTiempoObject.ConfirmarCorreo();
	}
	
	@Step
	public void VerificoCreacionCuenta() throws InterruptedException {
		accionesElTiempoObject.VerificarMensaje();
	}
	
	
}