package com.choucair.formacion.definition;

/**
 * Estructura básica de clase para Definition AccionesElTiempoDefinition
 *
 * @author  Mario A. Benítez
 * @version  6/06/2018 - 5:16:50 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.util.List;

import com.choucair.formacion.steps.AccionesElTiempoSteps;
import com.choucair.formacion.steps.AccionesElTiempoAutenticarYBuscarSteps;
import com.choucair.formacion.utilities.ExcelReader;

public class AccionesElTiempoDefinition {
	private String rutaArchivo = "D:\\PracticasAutomatizacion\\Reto10\\com.choucair.ElTiempo\\src\\test\\resources\\Datadriven\\Datos.xlsx";
	private String hojaDeCalculo = "Hoja1";
	private int cantidadFilas;
	
		public AccionesElTiempoDefinition() throws Exception {
			ExcelReader.setExcelFile(this.rutaArchivo, this.hojaDeCalculo);
			this.cantidadFilas = ExcelReader.ContarFilas();
		}
	
	@Steps
	AccionesElTiempoSteps accionesElTiempoSteps;
	
	@Steps
	AccionesElTiempoAutenticarYBuscarSteps accionesElTiempoAutenticarYBuscarSteps;
	
	@Given("^Ingreso al sitio de El Tiempo para registrarme$")
	public void ingreso_al_sitio_de_El_Tiempo_para_registrarme() {
		accionesElTiempoSteps.abrirSitio();
	}

	@Given("^Ingreso la informacion del formulario para el registro$")
	public void ingreso_la_informacion_del_formulario_para_el_registro() throws Exception {
		int posicionFila = 1;
		accionesElTiempoSteps.RegistrarseAlSistema(posicionFila);
		ExcelReader.CerrarBook();
		accionesElTiempoSteps.AceptarTerminos();
		accionesElTiempoSteps.CrearCuenta();
	}

	@Then("^Verifico el registro realizado$")
	public void verifico_el_registro_realizado() throws InterruptedException {
		accionesElTiempoSteps.VerificoCreacionCuenta();
	}	
	
	
	@Given("^Ingreso al sitio de El Tiempo$")
	public void ingreso_al_sitio_de_El_Tiempo(){
		accionesElTiempoAutenticarYBuscarSteps.AccesoLogin();
	}

	@Given("^Me logueo en el sitio con un usuario registrado$")
	public void me_logue_en_el_sitio_con_un_usuario_registrado() throws Exception{
		int posicionFila = 4;
		accionesElTiempoAutenticarYBuscarSteps.IngresarDatosLogin(posicionFila);
		ExcelReader.CerrarBook();
		accionesElTiempoAutenticarYBuscarSteps.PresionarBotonLogin();
	}

	@When("^Busco el criterio desde Excel$")
	public void busco_el_criterio_Rusia_desde_Excel() throws Exception{
		int posicionFila = 7;
		accionesElTiempoAutenticarYBuscarSteps.BuscarEnLaPagina(posicionFila);
		ExcelReader.CerrarBook();
	}

	@Then("^Verifico los resultados encontrados$")
	public void verifico_los_resultados_encontrados() throws Exception {
		int posicionFila = 7;
		accionesElTiempoAutenticarYBuscarSteps.ComprobarResultados(posicionFila);
		ExcelReader.CerrarBook();
	}

}