package com.choucair.formacion.pageobjects;

/**
 * Estructura básica de clase para Object AccionesElTiempoObject
 *
 * @author  Mario A. Benítez
 * @version  6/06/2018 - 5:18:14 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

//@DefaultUrl("http://www.eltiempo.com/")
@DefaultUrl("http://www.eltiempo.com/zona-usuario/crear")
public class AccionesElTiempoObject extends PageObject {
	
	private String Correo;
	
	@FindBy(id = "first_name")
	public WebElementFacade txtNombre;
	
	@FindBy(id = "last_name")
	public WebElementFacade txtapellido;
	
	@FindBy(id = "email")
	public WebElementFacade txtemail;

	@FindBy(id = "password")
	public WebElementFacade txtpassword;
	
	@FindBy(xpath = "//LABEL[@for='terms']")
		public WebElementFacade chkterms;
	
	@FindBy(id = "submitbutton")
		public WebElementFacade btnCrear;

	@FindBy(id = "b2")
		public WebElementFacade btnConfirmar;
	
	@FindBy(css = "span[class='buscador-icon']")
		public WebElementFacade btniconoBuscar1;
	
	@FindBy(name = "q")
	public WebElementFacade txtbuscar;
	
	@FindBy(className = "useremail")
		public WebElementFacade lblMensaje;
	
	/* Campo txtNombre  */
	public void Nombre(String Dato) {
		this.getDriver().switchTo().frame("iframe_registro");
		txtNombre.click();
		txtNombre.clear();
		txtNombre.sendKeys(Dato);
	}
	
	/* Campo txtapellido  */
	public void Apellido(String Dato) {
		txtapellido.click();
		txtapellido.clear();
		txtapellido.sendKeys(Dato);
	}
	
	/* Campo txtemail  */
	public void Email(String Dato) {
		this.Correo = Dato;
		txtemail.click();
		txtemail.clear();
		txtemail.sendKeys(Dato);
	}
	
	/* Campo txtpassword  */
	public void Password(String Dato) {
		txtpassword.click();
		txtpassword.clear();
		txtpassword.sendKeys(Dato);
	}
	
	/* Campo chkterms  */
	public void Terminos() {
		chkterms.click();
		JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
		this.getDriver().switchTo().defaultContent();
		js.executeScript("window.scrollBy(0,300)");
		this.getDriver().switchTo().frame("iframe_registro");
	}
	
	/* Campo btnCrear  */
	public void Boton() {

		btnCrear.click();
		this.getDriver().switchTo().defaultContent();
	}
	

	public void ConfirmarCorreo(){
		JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
		js.executeScript("window.scrollBy(0,5)");
		this.getDriver().switchTo().frame("iframe_registro");
		btnConfirmar.click();
	}
	
	public void VerificarMensaje() throws InterruptedException {
//		Thread.sleep(20000);
		btniconoBuscar1.click();
		txtbuscar.sendKeys(Keys.ENTER);
		String strMensaje = this.Correo;
		String textCompleto = lblMensaje.getText().trim();
		if(textCompleto.contains( strMensaje )) {
			System.out.println("Registro exitoso.");
		}
	}
	
	
	
}