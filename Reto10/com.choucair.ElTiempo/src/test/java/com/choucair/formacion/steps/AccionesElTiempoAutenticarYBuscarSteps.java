package com.choucair.formacion.steps;

/**
 * Estructura básica de clase para Steps AccionesElTiempoAutenticarYBuscarSteps
 *
 * @author  Mario A. Benítez
 * @version  7/06/2018 - 1:32:48 p. m.
 * @since 1.0
 * @see http://www.choucairtesting.com
 *
 */

import java.util.List;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import com.choucair.formacion.pageobjects.AccionesElTiempoAutenticarYBuscarObject;
import com.choucair.formacion.utilities.ExcelReader;

public class AccionesElTiempoAutenticarYBuscarSteps {
	
	AccionesElTiempoAutenticarYBuscarObject accionesElTiempoAutenticarYBuscarObject;
	
	@Step
	public void AccesoLogin() {
		accionesElTiempoAutenticarYBuscarObject.open();
		accionesElTiempoAutenticarYBuscarObject.IngresarLogin();
	}
	
	@Step
	public void IngresarDatosLogin(int i) throws Exception {
		accionesElTiempoAutenticarYBuscarObject.IngresarUsuario(ExcelReader.getCellData(i, 0).trim());
		accionesElTiempoAutenticarYBuscarObject.IngresarPassword(ExcelReader.getCellData(i, 1).trim());
	}
	
	@Step
	public void PresionarBotonLogin() {
		accionesElTiempoAutenticarYBuscarObject.PresionarBoton();
	}

	@Step
	public void BuscarEnLaPagina(int i) throws Exception{
		accionesElTiempoAutenticarYBuscarObject.BuscarCriterio(ExcelReader.getCellData(i, 0).trim());
	}

	public void ComprobarResultados(int i) throws Exception{
		accionesElTiempoAutenticarYBuscarObject.VerificarResultados(ExcelReader.getCellData(i, 0).trim());
	}
	
	
	
}