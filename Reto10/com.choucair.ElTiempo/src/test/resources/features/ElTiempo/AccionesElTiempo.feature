#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Recepcion de informacion de la plataforma de El tiempo

  @CasoExitoso
  Scenario: Registro en la pagina del tiempo
    Given Ingreso al sitio de El Tiempo para registrarme
    And Ingreso la informacion del formulario para el registro
|	<Nombre>	|	<Apellidos>	|	<Email>	|	<Password>	|
    Then Verifico el registro realizado

  @CasoBuscarCriterio
  Scenario: Busqueda de publicacion en el tiempo
    Given Ingreso al sitio de El Tiempo
    And Me logueo en el sitio con un usuario registrado
    When Busco el criterio desde Excel
    Then Verifico los resultados encontrados

|	Nombre	|	Apellidos	|	Email	|	Password	|
##@externaldata@./src/test/resources/Datadriven/Datos.xlsx@Hoja1@1

    