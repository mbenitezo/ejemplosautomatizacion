package com.RetoPractico.steps;

import com.RetoPractico.pageobjects.RetoPageObject;
import net.thucydides.core.annotations.Step;

public class RetoSteps {

	RetoPageObject retoPageObject;

	@Step
	public void abrirNavegador() {
		retoPageObject.open();
	}

	@Step
	public void autenticarseEnColorLib(String strUsuario, int strPass) {
		retoPageObject.ingresaDatos(strUsuario, strPass);
	}

	@Step
	public void validarAcceso(){
		retoPageObject.verificarHome();
	}
}
