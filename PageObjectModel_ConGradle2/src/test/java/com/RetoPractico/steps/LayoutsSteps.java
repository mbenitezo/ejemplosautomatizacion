package com.RetoPractico.steps;

import com.RetoPractico.pageobjects.LayoutsPageObject;
import net.thucydides.core.annotations.Step;

public class LayoutsSteps {

	LayoutsPageObject layoutsPageObject;

	@Step
	public void ingresarOpcionMenuLayouts() {
		layoutsPageObject.ingresaOpcionLayouts();
	}

	@Step
	public void ingresarOpcionSubMenuLayouts() {
		layoutsPageObject.ingresaOpcionNoHeader();
	}

	@Step
	public void validarTituloNoHeader(String titulo){
		layoutsPageObject.verificarNoHeader(titulo);
	}
}
