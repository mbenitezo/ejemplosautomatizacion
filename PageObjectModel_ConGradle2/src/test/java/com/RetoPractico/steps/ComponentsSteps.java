package com.RetoPractico.steps;

import com.RetoPractico.pageobjects.ComponentsPageObject;
import net.thucydides.core.annotations.Step;

public class ComponentsSteps {

	ComponentsPageObject componentsPageObject;

	@Step
	public void ingresarOpcionMenuComponents() {
		componentsPageObject.ingresaOpcionComponents();
	}

	@Step
	public void ingresarOpcionSubMenuBgColor() {
		componentsPageObject.ingresaOpcionBgColor();
	}

	@Step
	public void validarTituloBackgroundColor(){
		componentsPageObject.verificarBackgroundColor();
	}

}
