package com.RetoPractico.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class ComponentsPageObject extends PageObject {

	@FindBy(xpath = "//ul[@id='menu']/li/a/span[1][text()=\"Components\"]")
	public WebElementFacade optionMenuComponents;

	@FindBy(xpath = "//ul[@id='menu']/li[5]/ul/li[1]/a")
	public WebElementFacade optionBgColor;

	@FindBy(xpath = "//div[@id='content']/div/div/h3")
	public WebElementFacade lblBackgroundColor;

	public void ingresaOpcionComponents() {
		optionMenuComponents.click();
	}

	public void ingresaOpcionBgColor() {
			optionBgColor.click();
	}

	public void verificarBackgroundColor() {
		String labelEsperado = "Background Color";
		String mensaje = lblBackgroundColor.getText().trim();
		assertThat(labelEsperado, equalTo(mensaje));
	}

 }
