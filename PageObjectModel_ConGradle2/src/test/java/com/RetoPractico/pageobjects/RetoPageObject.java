package com.RetoPractico.pageobjects;

import net.thucydides.core.annotations.DefaultUrl;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class RetoPageObject extends PageObject {

	@FindBy(xpath = "//input[@placeholder='Username']")
	public WebElementFacade txtUsername;

	@FindBy(xpath = "//input[@placeholder='Password']")
	public WebElementFacade txtPassword;

	@FindBy(xpath = "//button[@type='submit' and @class='btn btn-lg btn-primary btn-block']")
	public WebElementFacade btnsignin;

	@FindBy(id = "bootstrap-admin-template")
	public WebElementFacade lblHomePpal;

	public void ingresaDatos(String usuario, int pass) {
		txtUsername.sendKeys(usuario);
		txtPassword.sendKeys(String.valueOf(pass));
		btnsignin.click();
	}

	public void verificarHome() {
		String labelEsperado = "Bootstrap-Admin-Template";
		String mensaje = lblHomePpal.getText();
		assertThat(mensaje, containsString(labelEsperado));
	}

 }
