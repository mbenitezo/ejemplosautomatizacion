package main;

import java.util.Scanner;

import clases.Cuadrado;
import clases.Triangulo;

public class Main {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Seleccione una opción: ");
		System.out.println("1. Cálculo Cuadrado.");
		System.out.println("2. Cálculo Triángulo.");
		System.out.println("\nEsperando respuesta: ");
		int opcion = teclado.nextInt();
		
		String colorUsuario;
		
		switch(opcion) {
		
		case 1:

			System.out.println("Ingresa el nombre de un color");
			colorUsuario = teclado.next();
			
			System.out.println("Ingresa el lado del cuadrado: ");
			double ladoUsuario = teclado.nextDouble();
			
			Cuadrado cuadrado = new Cuadrado(colorUsuario, ladoUsuario);
			
			System.out.println("El color del cuadrado es " + cuadrado.getColor());
			System.out.println("El área del cuadrado es " + cuadrado.calcularArea());
			
			break;
			
		case 2:
			
			System.out.println("Ingresa el nombre de un color");
			colorUsuario = teclado.next();
			
			System.out.println("Ingrese la base del triángulo");
			double baseUsuario = teclado.nextDouble();
			
			System.out.println("Ingrese la altura del triángulo");
			double alturaUsuario = teclado.nextDouble();
			
			Triangulo triangulo = new Triangulo(colorUsuario, baseUsuario, alturaUsuario);
			
			System.out.println("El color del triángulo es " + triangulo.getColor());
			System.out.println("El área del triángulo es " + triangulo.calcularArea());
			
			break;
			
			default:
				
				System.out.println("Opción incorrecta.");
				break;
		
		}
		
		

		
	}

}
