package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefinitions {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor anacleto = Actor.named("Anacleto");
	
	@Before
	public void configuracionInicial() {
		anacleto.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^Que Anacleto quiere usar el traductor de Google$")
	public void queAnacletoQuiereUsarElTraductorDeGoogle() throws Exception {
		
		anacleto.wasAbleTo(Abrir.LaPaginaDeGoogle());

	}


	@When("^el traduce la palabra (.*) del ingles al espanol$")
	public void elTraduceLaPalabraTableDelInglSAlEspaOl(String palabra) throws Exception {
		
		anacleto.wasAbleTo(Traducir.DeInglesAEspanolLa(palabra));

	}

	@Then("^el deberia ver la palabra (.*) en la pantalla$")
	public void elDeberiaVerLaPalabraMesaEnLaPantalla(String palabraEsperada) throws Exception {

		anacleto.should(seeThat(LaRespuesta.es(), equalTo(palabraEsperada)));

	}

	
}
