# language: es
# autor: Mario Alejandro Benítez Orozco

Característica: Validar autenticación
  Para validar la funcionalidad de la autenticación
  Como usuario del sitio colorlib
  Quiero intentar loguearme de manera exitosa

@FuncionalidadAutenticacionExitoso
  Esquema del escenario:
    Dado que ingreso al sitio web
    Cuando me intento autenticar con los dato de acceso
      |  <idCase> | <Usuario>  | <Password> |
    Entonces debería ver el mensaje

  Ejemplos:
    |  idCase  | Usuario  | Password |
       ##@externaldata@./src/test/resources/datadriven/autenticacion/Datos.xlsx@Datos@1

