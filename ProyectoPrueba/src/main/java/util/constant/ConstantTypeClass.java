package util.constant;

public class ConstantTypeClass {

	public static final String UTILITY_CLASS = "Utility Class";
	public static final String UI_CLASS = "User interface Class";
	public static final String CONSTANT_CLASS = "Constant Class";
	public static final String QUESTION_CLASS = "Question Class";
	public static final String ENTITY_CLASS = "Entity Class";
	public static final String BUILDER_CLASS = "Builder Class";
	public static final String INTEGRATION_CLASS  = "Integration class for conect to Back";

	private ConstantTypeClass() {
		throw new IllegalStateException(CONSTANT_CLASS);
	}
}
