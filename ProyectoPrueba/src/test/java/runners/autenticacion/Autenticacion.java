package runners.autenticacion;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import org.junit.runner.RunWith;
import runners.RunnerPersonalizado;

//import
//import util.exceldata.BeforeSuite;
//import util.exceldata.DataToFeature;
//import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

@RunWith(RunnerPersonalizado.class)
@CucumberOptions(
        features = {"src/test/resources/features/autenticacion/autenticacion.feature"},
        glue = {"stepdefinitions"},
        snippets = SnippetType.CAMELCASE
)
public class Autenticacion {
 /*   @BeforeSuite
    public static void test() throws InvalidFormatException, IOException {
        DataToFeature.overrideFeatureFiles("./src/test/resources/features/autenticacion/autenticacion.feature");
    } */
    
}
