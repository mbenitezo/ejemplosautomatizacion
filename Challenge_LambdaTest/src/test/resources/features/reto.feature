@Regresion
Feature: Formulario Popup Validation
  Description: El usuario debe poder ingresar al formulario los datos requeridos.
  Cada campo del formulario realizar validaciones de obligatoriedad,
  longitud y formato, el sistema debe presentar las validaciones respectivas
  para cada campo a través de un globo informativo.

  Background: Diligenciamiento exitoso del formulario Popup Validation, no se presenta ningún mensaje de validación.
    Given Ingreso a la plataforma  colorlib
    When Me autentico con usuario "demo" y clave 123
    Then Verifico ingreso exitoso

  @UserStory:HU10000
  Scenario: Ver el titulo de No header
    Given Yo hago clic en Layouts del menu lateral
    When Yo hago clic en el opcion No Header
    Then Yo veo el titulo "No Header"

  @UserStory:HU10001
  Scenario: Ver el titulo de Background Color
    Given Yo hago clic en Components del menu lateral
    When Yo hago clic en el opcion Bg Color
    Then Yo veo el titulo Background Color

  @color:green
  @UserStory:HU10002
  @manual
  @manual-result:failed
  Scenario: Grow Granny Smith apples
    Given I have a field
    When I plant some Granny Smith apples trees
    Then some apples should grow
    And the apples should be green

  @color:red
  @UserStory:HU10003
  @manual
  @manual-result:passed
  Scenario: Grow Red Delicious apples
    Given I have a field
    When I plant some Red Delicious apples trees
    Then some apples should grow
    And the apples should be red

  @color:red
  @UserStory:HU10004
  @manual
  @manual-result:passed
  Scenario: Grow Juicy Red Delicious apples
    Given I have two fields
    When I plant some Red Delicious apples trees
    Then some apples should grow
    And the apples should be red

  #@manual-result:failed
  #@manual-result:pending
  #@manual-result:passed
  #@UserStory:10002