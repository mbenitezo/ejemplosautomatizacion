package com.RetoPractico.definitions;

import com.RetoPractico.steps.ComponentsSteps;
import com.RetoPractico.steps.LayoutsSteps;
import com.RetoPractico.steps.RetoSteps;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.IgnoredStepException;
import net.serenitybdd.core.SkipStepException;
import net.thucydides.core.annotations.Steps;

public class RetoDefinitions {

    @Steps
    public RetoSteps retoSteps;

    @Steps
    public LayoutsSteps layoutsSteps;

    @Steps
    public ComponentsSteps componentsSteps;

    @Given("^Ingreso a la plataforma  colorlib$")
    public void ingreso_a_la_plataforma_colorlib() {
        retoSteps.abrirNavegador();
    }

    @When("^Me autentico con usuario \"([^\"]*)\" y clave (\\d+)$")
    public void me_autentico_con_usuario_y_clave(String usuario, int pass) {
        retoSteps.autenticarseEnColorLib(usuario, pass);
    }

    @Then("^Verifico ingreso exitoso$")
    public void verifico_ingreso_exitoso() {
        retoSteps.validarAcceso();
    }


    @Given("^Yo hago clic en Layouts del menu lateral$")
    public void yoHagoClicEnLayoutsDelMenuLateral() {
        layoutsSteps.ingresarOpcionMenuLayouts();
    }


    @When("^Yo hago clic en el opcion No Header$")
    public void yoHagoClicEnElOpcionNoHeader() {
        layoutsSteps.ingresarOpcionSubMenuLayouts();
    }

    @Then("^Yo veo el titulo \"([^\"]*)\"$")
    public void yoVeoElTituloNoHeader(String titulo) {
        layoutsSteps.validarTituloNoHeader(titulo);
    }

    @Given("^Yo hago clic en Components del menu lateral$")
    public void yoHagoClicEnComponentsDelMenuLateral() {
        componentsSteps.ingresarOpcionMenuComponents();
    }

    @When("^Yo hago clic en el opcion Bg Color$")
    public void yoHagoClicEnElOpcionBgColor() {
        componentsSteps.ingresarOpcionSubMenuBgColor();
    }

    @Then("^Yo veo el titulo Background Color$")
    public void yoVeoElTituloBackgroundColor() {
        componentsSteps.validarTituloBackgroundColor();
    }

    @Given("^I have a field$")
    public void i_have_a_field(){
        try{

        }catch (IgnoredStepException | SkipStepException e){
            throw new SkipStepException("Escenario manual");
        }
    }

    @Given("^I have two fields$")
    public void iHaveTwoFields() {
        try{

        }catch (IgnoredStepException | SkipStepException e){
            throw new SkipStepException("Escenario manual");
        }
    }

    @When("^I plant some Granny Smith apples trees$")
    public void i_plant_some_Granny_Smith_apples_trees() {

    }

    @Then("^some apples should grow$")
    public void some_apples_should_grow() {

    }

    @Then("^the apples should be green$")
    public void the_apples_should_be_green() {

    }

    @When("^I plant some Red Delicious apples trees$")
    public void i_plant_some_Red_Delicious_apples_trees() {

    }

    @Then("^the apples should be red$")
    public void the_apples_should_be_red() {

    }
}