package com.RetoPractico.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class LayoutsPageObject extends PageObject {

	@FindBy(xpath = "//ul[@id='menu']/li/a/span[1][text()=\"Layouts\"]")
	public WebElementFacade optionMenuLayouts;

	@FindBy(xpath = "//ul[@id='menu']/li/ul/li[12]/a")
	public WebElementFacade optionNoHeader;

	@FindBy(xpath = "//h2")
	public WebElementFacade lblNoHeader;

	public void ingresaOpcionLayouts() {
		optionMenuLayouts.click();
	}

	public void ingresaOpcionNoHeader() {
			optionNoHeader.click();
	}

	public void verificarNoHeader(String titulo) {
		String labelEsperado = titulo;
		String mensaje = lblNoHeader.getText();
		assertThat(mensaje, containsString(labelEsperado));
	}
 }
