package com.RetoPractico.runner;

import cucumber.api.SnippetType;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "src/test/resources/features/reto.feature",
		glue = "com.RetoPractico.definitions",
		plugin = {"html:target/cucumber", "pretty:target/pretty", "junit:target/cucumber.xml"},
		monochrome = true,
		strict = true,
		snippets= SnippetType.CAMELCASE
)
public class RetoRunner {}
